﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DISTQCPAPI.Domain.MySQLModel;

namespace DISTQCPAPI.Application.Interface
{
     interface IUserApplaction
    {
        Task<bool> CheckUser(string username, string password,out user user);
        string Encrypt(string originPassword);
        string Decrypt(string encryptionPassword);
    }
}

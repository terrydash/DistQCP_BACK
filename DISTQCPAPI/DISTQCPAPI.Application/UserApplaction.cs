﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DISTQCPAPI.Application.Interface;
using DISTQCPAPI.Common;
using DISTQCPAPI.Domain.MySQLModel;

namespace DISTQCPAPI.Application
{
    public class UserApplaction:IUserApplaction
    {
        public  Task<bool> CheckUser(string username, string password, out user user)
        {
            throw new NotImplementedException();
        }

        public string Encrypt(string originPassword)
        {
            RSAHelper rsaHelper=new RSAHelper();
            return rsaHelper.Encrypt(originPassword);
        }

        public string Decrypt(string encryptionPassword)
        {
            RSAHelper rsaHelper = new RSAHelper();
            return rsaHelper.Decrypt(encryptionPassword);
        }
    }
}

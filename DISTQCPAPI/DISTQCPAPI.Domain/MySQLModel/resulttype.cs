﻿using System;
using System.Linq;
using System.Text;

namespace DISTQCPAPI.Domain.MySQLModel
{
    ///<summary>
    ///
    ///</summary>
    public partial class resulttype
    {
           public resulttype(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}

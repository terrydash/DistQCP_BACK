﻿using System;
using System.Linq;
using System.Text;

namespace DISTQCPAPI.Domain.MySQLModel
{
    ///<summary>
    ///
    ///</summary>
    public partial class special
    {
           public special(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? DeptID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

    }
}

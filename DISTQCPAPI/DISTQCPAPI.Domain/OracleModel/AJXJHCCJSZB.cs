﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class AJXJHCCJSZB
    {
           public AJXJHCCJSZB(){


           }
           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099是否可查
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKC {get;set;}

           /// <summary>
           /// Desc:099成绩总表打印的等级考试名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYDJKSMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ALZSLDMB
    {
           public ALZSLDMB(){


           }
           /// <summary>
           /// Desc:099类代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LDM {get;set;}

           /// <summary>
           /// Desc:099类名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LMC {get;set;}

    }
}

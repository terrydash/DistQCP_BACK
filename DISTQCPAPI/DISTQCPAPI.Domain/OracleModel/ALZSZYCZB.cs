﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ALZSZYCZB
    {
           public ALZSZYCZB(){


           }
           /// <summary>
           /// Desc:099类名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LMC {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

    }
}

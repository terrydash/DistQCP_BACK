﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BBKZB
    {
           public BBKZB(){


           }
           /// <summary>
           /// Desc:099版本日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BBRQ {get;set;}

           /// <summary>
           /// Desc:099代理
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PROXY1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PROXY_PORT1 {get;set;}

           /// <summary>
           /// Desc:099主机地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HOST1 {get;set;}

           /// <summary>
           /// Desc:099端口号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HOST_PORT1 {get;set;}

           /// <summary>
           /// Desc:099用户名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string USER1 {get;set;}

           /// <summary>
           /// Desc:099密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PASSWORD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099教材版本日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBRQ_JC {get;set;}

           /// <summary>
           /// Desc:099实验版本日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBRQ_SY {get;set;}

           /// <summary>
           /// Desc:099收费版本日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBRQ_SF {get;set;}

           /// <summary>
           /// Desc:评价版本日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBRQ_PJ {get;set;}

           /// <summary>
           /// Desc:实践版本日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBRQ_SJ {get;set;}

    }
}

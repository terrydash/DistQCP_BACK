﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BDLBDMB
    {
           public BDLBDMB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short BDLBDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BDLBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQY {get;set;}

           /// <summary>
           /// Desc:099是否记入调停补课程的总学时量计算
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJLZHXS {get;set;}

    }
}

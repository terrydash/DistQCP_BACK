﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BDZCZDB
    {
           public BDZCZDB(){


           }
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ID {get;set;}

           /// <summary>
           /// Desc:报到注册字段名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BDZCZDMC {get;set;}

           /// <summary>
           /// Desc:报到注册字段类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BDZCZDLB {get;set;}

    }
}

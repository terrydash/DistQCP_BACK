﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BHGZB
    {
           public BHGZB(){


           }
           /// <summary>
           /// Desc:编号名称 
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BHMC {get;set;}

           /// <summary>
           /// Desc:字段名称 
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZDMC {get;set;}

           /// <summary>
           /// Desc:字段中文名称 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDZWMC {get;set;}

           /// <summary>
           /// Desc:起始位 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QSW {get;set;}

           /// <summary>
           /// Desc:位数 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? WS {get;set;}

           /// <summary>
           /// Desc:起始编号 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSBH {get;set;}

           /// <summary>
           /// Desc:是否补零 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBL {get;set;}

           /// <summary>
           /// Desc:是否固定值 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGDZ {get;set;}

           /// <summary>
           /// Desc:排序 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? PX {get;set;}

    }
}

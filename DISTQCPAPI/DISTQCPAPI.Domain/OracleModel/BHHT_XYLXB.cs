﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BHHT_XYLXB
    {
           public BHHT_XYLXB(){


           }
           /// <summary>
           /// Desc:001角色
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JS {get;set;}

           /// <summary>
           /// Desc:002类型（三种类型分别为开课学院、申请学院和学生学院）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LX {get;set;}

           /// <summary>
           /// Desc:003状态（1表示放开，其它为关闭）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZT {get;set;}

    }
}

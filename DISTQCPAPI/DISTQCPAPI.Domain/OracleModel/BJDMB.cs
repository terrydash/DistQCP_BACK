﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BJDMB
    {
           public BJDMB(){


           }
           /// <summary>
           /// Desc:001班级代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJDM {get;set;}

           /// <summary>
           /// Desc:002班级名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:003所属专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSZYDM {get;set;}

           /// <summary>
           /// Desc:004年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:099款项
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KX {get;set;}

           /// <summary>
           /// Desc:099托管学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXYDM {get;set;}

           /// <summary>
           /// Desc:099所属校区代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXQDM {get;set;}

           /// <summary>
           /// Desc:099毕业年度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? BYND {get;set;}

           /// <summary>
           /// Desc:099辅导员姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDYXM {get;set;}

           /// <summary>
           /// Desc:099辅导员联系方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDYLXFS {get;set;}

           /// <summary>
           /// Desc:099班级简称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJJC {get;set;}

           /// <summary>
           /// Desc:099学制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XZ {get;set;}

           /// <summary>
           /// Desc:099层次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CC {get;set;}

           /// <summary>
           /// Desc:099注册人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZCRS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSCBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KBBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JCRS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BDS {get;set;}

           /// <summary>
           /// Desc:099班主任姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZRXM {get;set;}

           /// <summary>
           /// Desc:099班主任职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZRZGH {get;set;}

           /// <summary>
           /// Desc:099导师姓名1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSXM1 {get;set;}

           /// <summary>
           /// Desc:099导师姓名2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSXM2 {get;set;}

           /// <summary>
           /// Desc:099班主任姓名2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZRXM2 {get;set;}

           /// <summary>
           /// Desc:099导师1联系方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DS1LXFS {get;set;}

           /// <summary>
           /// Desc:099导师2联系方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DS2LXFS {get;set;}

           /// <summary>
           /// Desc:099班主任1联系方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZR1LXFS {get;set;}

           /// <summary>
           /// Desc:099班主任2联系方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZR2LXFS {get;set;}

           /// <summary>
           /// Desc:099所属学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXSXYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KBBZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSQDDM {get;set;}

           /// <summary>
           /// Desc:099是否有效
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYX {get;set;}

           /// <summary>
           /// Desc:000班级英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJYWMC {get;set;}

           /// <summary>
           /// Desc:099排课校区代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKXQDM {get;set;}

           /// <summary>
           /// Desc:099办学形式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BXXS {get;set;}

           /// <summary>
           /// Desc:每周最大学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MZZDXS {get;set;}

           /// <summary>
           /// Desc:序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSH {get;set;}

           /// <summary>
           /// Desc:班长学号(xsjbxxb.xh)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZXH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BJDMB_JC
    {
           public BJDMB_JC(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KX {get;set;}

           /// <summary>
           /// Desc:099托管学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXQDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? BYND {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDYXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDYLXFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJJC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZCRS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSCBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JCRS {get;set;}

           /// <summary>
           /// Desc:099所属学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXSXYDM {get;set;}

           /// <summary>
           /// Desc:099导师姓名1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSXM1 {get;set;}

           /// <summary>
           /// Desc:099导师姓名2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSXM2 {get;set;}

           /// <summary>
           /// Desc:099班主任姓名2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZRXM2 {get;set;}

           /// <summary>
           /// Desc:099导师1联系方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DS1LXFS {get;set;}

           /// <summary>
           /// Desc:099导师2联系方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DS2LXFS {get;set;}

           /// <summary>
           /// Desc:099班主任1联系方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZR1LXFS {get;set;}

           /// <summary>
           /// Desc:099班主任2联系方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZR2LXFS {get;set;}

           /// <summary>
           /// Desc:099班主任姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZRXM {get;set;}

           /// <summary>
           /// Desc:099班主任职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZRZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KBBZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSQDDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJYWMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKXQDM {get;set;}

           /// <summary>
           /// Desc:099办学形式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BXXS {get;set;}

           /// <summary>
           /// Desc:每周最大学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MZZDXS {get;set;}

           /// <summary>
           /// Desc:序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSH {get;set;}

    }
}

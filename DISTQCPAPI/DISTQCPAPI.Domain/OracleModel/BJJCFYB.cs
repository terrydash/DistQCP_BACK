﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BJJCFYB
    {
           public BJJCFYB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099教材费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JCFY {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:099行政班
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:099当前年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DQNJ {get;set;}

    }
}

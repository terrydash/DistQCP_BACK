﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BJJTKCB
    {
           public BJJTKCB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099推荐课表代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJKBMC {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XH {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ11 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ12 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ13 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ14 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ15 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ21 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ22 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ23 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ24 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ25 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ31 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ32 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ33 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ34 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ35 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ41 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ42 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ43 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ44 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ45 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ51 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ52 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ53 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ54 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ55 {get;set;}

    }
}

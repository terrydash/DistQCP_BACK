﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BJRSB
    {
           public BJRSB(){


           }
           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RS {get;set;}

    }
}

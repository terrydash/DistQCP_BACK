﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BKDMB
    {
           public BKDMB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short NJ {get;set;}

           /// <summary>
           /// Desc:099板块课程名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKKCMC {get;set;}

           /// <summary>
           /// Desc:099板块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short BKDM {get;set;}

           /// <summary>
           /// Desc:099板块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTY {get;set;}

           /// <summary>
           /// Desc:板块课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKKCDM {get;set;}

    }
}

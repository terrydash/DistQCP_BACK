﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BKKCDMB
    {
           public BKKCDMB(){


           }
           /// <summary>
           /// Desc:099代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DM {get;set;}

           /// <summary>
           /// Desc:099板块课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKKCDM {get;set;}

           /// <summary>
           /// Desc:099板块课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKKCMC {get;set;}

           /// <summary>
           /// Desc:099课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFFJJX {get;set;}

    }
}

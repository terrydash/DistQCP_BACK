﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BKMDB
    {
           public BKMDB(){


           }
           /// <summary>
           /// Desc:001补考学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKXN {get;set;}

           /// <summary>
           /// Desc:002补考学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKXQ {get;set;}

           /// <summary>
           /// Desc:003选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:004学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:005姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:006课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:007学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:008成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:009折算成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? ZSCJ {get;set;}

           /// <summary>
           /// Desc:010绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JD {get;set;}

           /// <summary>
           /// Desc:018备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGSJ {get;set;}

           /// <summary>
           /// Desc:099修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGS {get;set;}

           /// <summary>
           /// Desc:099重修标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? CXBJ {get;set;}

           /// <summary>
           /// Desc:011平时成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSCJ {get;set;}

           /// <summary>
           /// Desc:012期末成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QMCJ {get;set;}

           /// <summary>
           /// Desc:013实验成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYCJ {get;set;}

           /// <summary>
           /// Desc:010补考成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKCJ {get;set;}

           /// <summary>
           /// Desc:014重修成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXCJ {get;set;}

           /// <summary>
           /// Desc:017课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:015期中成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZCJ {get;set;}

           /// <summary>
           /// Desc:016补考确认
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKQR {get;set;}

           /// <summary>
           /// Desc:018录入成绩教师 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099考试时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:099是否毕业补考
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SFBYBK {get;set;}

           /// <summary>
           /// Desc:099考试地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMC {get;set;}

           /// <summary>
           /// Desc:099备注2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? BZ2 {get;set;}

           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099座位号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZWH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MM {get;set;}

           /// <summary>
           /// Desc:016录入设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QRSJ {get;set;}

           /// <summary>
           /// Desc:099是否手工
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ECKS {get;set;}

           /// <summary>
           /// Desc:099补考段代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJD {get;set;}

           /// <summary>
           /// Desc:099补考段代码名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJDMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFCXBK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYCJ {get;set;}

           /// <summary>
           /// Desc:010开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSQMCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSBKBZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKCJ_BZ {get;set;}

           /// <summary>
           /// Desc:099补考是否打印
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BK_SFDY {get;set;}

           /// <summary>
           /// Desc:099收费标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBJ {get;set;}

           /// <summary>
           /// Desc:099是否可确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKQR {get;set;}

           /// <summary>
           /// Desc:019第几次补考
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJCBK {get;set;}

           /// <summary>
           /// Desc:099学生网上报名轮次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LC {get;set;}

           /// <summary>
           /// Desc:099手工增加记录时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGZJSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKFLMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSSYCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSPSCJ {get;set;}

           /// <summary>
           /// Desc:099卷一补考确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYBKQR {get;set;}

           /// <summary>
           /// Desc:099卷二补考确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JEBKQR {get;set;}

           /// <summary>
           /// Desc:099辅修标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXBJ {get;set;}

           /// <summary>
           /// Desc:099提交确认人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJQRR {get;set;}

           /// <summary>
           /// Desc:099提交确认时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJQRSJ {get;set;}

           /// <summary>
           /// Desc:099提交时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJSJ {get;set;}

           /// <summary>
           /// Desc:099上学期是否已开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYK {get;set;}

           /// <summary>
           /// Desc:099系提交确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTJQR {get;set;}

           /// <summary>
           /// Desc:099系提交确认人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTJQRR {get;set;}

           /// <summary>
           /// Desc:099系提交确认时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTJQRSJ {get;set;}

           /// <summary>
           /// Desc:099院系提交确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXTJQR {get;set;}

           /// <summary>
           /// Desc:099院系提交确认人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXTJQRR {get;set;}

           /// <summary>
           /// Desc:099院系提交确认时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXTJQRSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSFQR {get;set;}

    }
}

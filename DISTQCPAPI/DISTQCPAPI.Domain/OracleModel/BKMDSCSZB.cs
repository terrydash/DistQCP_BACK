﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BKMDSCSZB
    {
           public BKMDSCSZB(){


           }
           /// <summary>
           /// Desc:099控件类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KJLX {get;set;}

           /// <summary>
           /// Desc:099控件名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KJMC {get;set;}

           /// <summary>
           /// Desc:099控件默认值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KJMRZ {get;set;}

           /// <summary>
           /// Desc:099用户名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHM {get;set;}

           /// <summary>
           /// Desc:099菜单名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MENUNAME {get;set;}

    }
}

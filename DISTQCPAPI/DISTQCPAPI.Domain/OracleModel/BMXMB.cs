﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BMXMB
    {
           public BMXMB(){


           }
           /// <summary>
           /// Desc:099名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MC {get;set;}

           /// <summary>
           /// Desc:099类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LB {get;set;}

           /// <summary>
           /// Desc:099是否要选
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYX {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKSMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXDX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDJC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDDG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKSMC2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJGX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKSMC3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKSMC4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJGX2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDX1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDX2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDX3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDX4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RS {get;set;}

           /// <summary>
           /// Desc:099报名费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMFY {get;set;}

           /// <summary>
           /// Desc:099报名批次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BMPC {get;set;}

           /// <summary>
           /// Desc:099组号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZH {get;set;}

           /// <summary>
           /// Desc:099成绩保留条件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBLTJ {get;set;}

           /// <summary>
           /// Desc:099语言级别代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXJBDM {get;set;}

           /// <summary>
           /// Desc:099学校代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXDM {get;set;}

           /// <summary>
           /// Desc:099网上报名是否可以退选
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKYTX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBLCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TQBKKCYQ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TQBKKCYQ2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TQBKKCGX {get;set;}

           /// <summary>
           /// Desc:099是否为普通话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFPTH {get;set;}

           /// <summary>
           /// Desc:报名次数限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BMCSXZ {get;set;}

           /// <summary>
           /// Desc:099证书费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSF {get;set;}

           /// <summary>
           /// Desc:099资料费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZLF {get;set;}

           /// <summary>
           /// Desc:099教材费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCF {get;set;}

           /// <summary>
           /// Desc:099其他费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QTF {get;set;}

           /// <summary>
           /// Desc:英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWMC {get;set;}

           /// <summary>
           /// Desc:099是否需要照片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LBSATE {get;set;}

    }
}

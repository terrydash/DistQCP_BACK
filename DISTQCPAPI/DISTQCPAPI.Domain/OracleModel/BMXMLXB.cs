﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BMXMLXB
    {
           public BMXMLXB(){


           }
           /// <summary>
           /// Desc:099报名名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BMMC {get;set;}

           /// <summary>
           /// Desc:099报名类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BMLB {get;set;}

    }
}

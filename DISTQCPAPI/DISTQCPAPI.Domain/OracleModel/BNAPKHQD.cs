﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BNAPKHQD
    {
           public BNAPKHQD(){


           }
           /// <summary>
           /// Desc:099推荐课表代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJKBDM {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099课时数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KSS {get;set;}

           /// <summary>
           /// Desc:099任课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKJS {get;set;}

           /// <summary>
           /// Desc:099不能安排原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BNAPYY {get;set;}

           /// <summary>
           /// Desc:099上课内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SKNR {get;set;}

           /// <summary>
           /// Desc:099第几次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BSLWBTB
    {
           public BSLWBTB(){


           }
           /// <summary>
           /// Desc:099标题代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BTDM {get;set;}

           /// <summary>
           /// Desc:099标题名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BTMC {get;set;}

           /// <summary>
           /// Desc:099分数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BTFS {get;set;}

           /// <summary>
           /// Desc:099评价对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJDX {get;set;}

    }
}

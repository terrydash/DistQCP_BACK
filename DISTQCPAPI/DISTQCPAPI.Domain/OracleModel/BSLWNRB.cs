﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BSLWNRB
    {
           public BSLWNRB(){


           }
           /// <summary>
           /// Desc:099内容代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NRDM {get;set;}

           /// <summary>
           /// Desc:099专业类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYLBDM {get;set;}

           /// <summary>
           /// Desc:099内容名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NRMC {get;set;}

           /// <summary>
           /// Desc:099分数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NRFS {get;set;}

           /// <summary>
           /// Desc:099标题代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BTDM {get;set;}

    }
}

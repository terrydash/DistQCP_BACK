﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BSLWXMB
    {
           public BSLWXMB(){


           }
           /// <summary>
           /// Desc:099项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMDM {get;set;}

           /// <summary>
           /// Desc:099内容代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NRDM {get;set;}

           /// <summary>
           /// Desc:099项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMMC {get;set;}

           /// <summary>
           /// Desc:099分数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMFS {get;set;}

    }
}

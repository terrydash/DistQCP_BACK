﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BSLXB
    {
           public BSLXB(){


           }
           /// <summary>
           /// Desc:比赛类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BSLX {get;set;}

           /// <summary>
           /// Desc:加分值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JFZ {get;set;}

           /// <summary>
           /// Desc:比赛类型代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BSLXDM {get;set;}

           /// <summary>
           /// Desc:是否停用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTY {get;set;}

    }
}

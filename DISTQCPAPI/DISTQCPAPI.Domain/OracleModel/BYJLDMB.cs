﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYJLDMB
    {
           public BYJLDMB(){


           }
           /// <summary>
           /// Desc:099毕业奖励代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BYJLDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYJLMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYCXZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYLWTMLXDMB
    {
           public BYLWTMLXDMB(){


           }
           /// <summary>
           /// Desc:001题目类型代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TMLXDM {get;set;}

           /// <summary>
           /// Desc:002题目类型名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TMLXMC {get;set;}

    }
}

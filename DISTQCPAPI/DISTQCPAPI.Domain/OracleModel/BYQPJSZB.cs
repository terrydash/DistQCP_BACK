﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYQPJSZB
    {
           public BYQPJSZB(){


           }
           /// <summary>
           /// Desc:099评价开关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? PJKG {get;set;}

           /// <summary>
           /// Desc:099查询开关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? CXKG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXQ {get;set;}

           /// <summary>
           /// Desc:099评价年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJNJ {get;set;}

    }
}

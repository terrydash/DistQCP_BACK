﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSCJDYSZB
    {
           public BYSCJDYSZB(){


           }
           /// <summary>
           /// Desc:099学号 
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:申请学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQXN {get;set;}

           /// <summary>
           /// Desc:申请学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQXQ {get;set;}

           /// <summary>
           /// Desc:状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZT {get;set;}

           /// <summary>
           /// Desc:099申请时间 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:099学院审核结果 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHJG {get;set;}

           /// <summary>
           /// Desc:099教务处审核结果 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHJG {get;set;}

    }
}

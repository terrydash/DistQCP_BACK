﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSFZXXB
    {
           public BYSFZXXB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099毕业论文成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJCJ {get;set;}

           /// <summary>
           /// Desc:099毕业设计指导教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJZDJS {get;set;}

           /// <summary>
           /// Desc:099毕业审查意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSCYJ {get;set;}

           /// <summary>
           /// Desc:099工作单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZDW {get;set;}

           /// <summary>
           /// Desc:099工作地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZDZ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXCJ {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSXLH {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWXLH {get;set;}

           /// <summary>
           /// Desc:099毕业结论
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYJR {get;set;}

           /// <summary>
           /// Desc:099毕业证书号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYZSH {get;set;}

           /// <summary>
           /// Desc:099英文姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWXW {get;set;}

           /// <summary>
           /// Desc:099学位类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWLX {get;set;}

           /// <summary>
           /// Desc:099学位证书号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWZSH {get;set;}

           /// <summary>
           /// Desc:099有无换证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWHZ {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTM {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYNF {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XZ {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXTM {get;set;}

           /// <summary>
           /// Desc:099毕业实习成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXCJ {get;set;}

           /// <summary>
           /// Desc:099??????????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LWLX {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LWXZ {get;set;}

           /// <summary>
           /// Desc:099???????????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJJSZGH {get;set;}

           /// <summary>
           /// Desc:099毕业论文学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJXF {get;set;}

           /// <summary>
           /// Desc:099????????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJKCXZ {get;set;}

           /// <summary>
           /// Desc:099???????????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSXJSZGH {get;set;}

           /// <summary>
           /// Desc:099????????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSXJSXM {get;set;}

           /// <summary>
           /// Desc:099毕业实习学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSXXF {get;set;}

           /// <summary>
           /// Desc:099????????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSXKCXZ {get;set;}

           /// <summary>
           /// Desc:099辅修证编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXZBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXLWTM {get;set;}

           /// <summary>
           /// Desc:099论文盒编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LWHBH {get;set;}

           /// <summary>
           /// Desc:099件号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JH {get;set;}

           /// <summary>
           /// Desc:099责任者
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZRZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LWWJBH {get;set;}

           /// <summary>
           /// Desc:011体育达标
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYDB {get;set;}

           /// <summary>
           /// Desc:099毕业日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYRQ {get;set;}

           /// <summary>
           /// Desc:099辅修专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXZY {get;set;}

           /// <summary>
           /// Desc:099第二学位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DRXW {get;set;}

           /// <summary>
           /// Desc:099结业证书号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYZSH {get;set;}

           /// <summary>
           /// Desc:099辅修专业证书号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXZYZSH {get;set;}

           /// <summary>
           /// Desc:099第二学位证书号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DRXWZSH {get;set;}

           /// <summary>
           /// Desc:099毕业综合成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYZHCJ {get;set;}

           /// <summary>
           /// Desc:099毕业理论考核成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYLLKHCJ {get;set;}

           /// <summary>
           /// Desc:099技能考核成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JNKHCJ {get;set;}

           /// <summary>
           /// Desc:099综合鉴定成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHJDCJ {get;set;}

           /// <summary>
           /// Desc:辅修专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYZMBH {get;set;}

           /// <summary>
           /// Desc:099辅修学位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXXW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WPDW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WPDWDQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WBYYY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSYXWYY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWSYDWM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYDWM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWLBM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDEXW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFFXXW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HXWRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWPY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJYWMC {get;set;}

           /// <summary>
           /// Desc:099学生信息是否要转入历史库
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFRLSK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWYWLX {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTMLY {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTMLX {get;set;}

           /// <summary>
           /// Desc:009换发证书时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HFZSSJ {get;set;}

           /// <summary>
           /// Desc:009补授学位时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BSXWSJ {get;set;}

           /// <summary>
           /// Desc:099毕业发证日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYFZRQ {get;set;}

           /// <summary>
           /// Desc:099学位发证日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWFZRQ {get;set;}

           /// <summary>
           /// Desc:099辅修发证日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXFZRQ {get;set;}

           /// <summary>
           /// Desc:099第二学位发证日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DEFZRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJYSM {get;set;}

           /// <summary>
           /// Desc:099学位授予情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWSYQK {get;set;}

           /// <summary>
           /// Desc:099英语学位学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YYXWXF {get;set;}

           /// <summary>
           /// Desc:099毕业应修学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYYXXF {get;set;}

           /// <summary>
           /// Desc:099英语毕业学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YYBYXF {get;set;}

           /// <summary>
           /// Desc:099综合素质学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHSZXF {get;set;}

           /// <summary>
           /// Desc:099证书打印时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYSJ {get;set;}

           /// <summary>
           /// Desc:099证书上显示打印日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSDYRQ {get;set;}

           /// <summary>
           /// Desc:099二专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EZYDM {get;set;}

           /// <summary>
           /// Desc:099二专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EZYMC {get;set;}

           /// <summary>
           /// Desc:099二专业证书号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EZYZSH {get;set;}

           /// <summary>
           /// Desc:099实习单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXDW {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSHDXFB
    {
           public BYSHDXFB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:099获得学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HDXF {get;set;}

           /// <summary>
           /// Desc:099专业方向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFX {get;set;}

           /// <summary>
           /// Desc:099毕业结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYJG {get;set;}

           /// <summary>
           /// Desc:099学位结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWJG {get;set;}

           /// <summary>
           /// Desc:099学分绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XFJD {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

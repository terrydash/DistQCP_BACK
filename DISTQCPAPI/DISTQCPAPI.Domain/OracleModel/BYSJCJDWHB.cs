﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJCJDWHB
    {
           public BYSJCJDWHB(){


           }
           /// <summary>
           /// Desc:099成绩段代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CJDDM {get;set;}

           /// <summary>
           /// Desc:099成绩段名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDMC {get;set;}

           /// <summary>
           /// Desc:099成绩段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJD {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJCSDMB
    {
           public BYSJCSDMB(){


           }
           /// <summary>
           /// Desc:099毕业设计场所代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BSCSDM {get;set;}

           /// <summary>
           /// Desc:099毕业设计场所名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BSCSMC {get;set;}

    }
}

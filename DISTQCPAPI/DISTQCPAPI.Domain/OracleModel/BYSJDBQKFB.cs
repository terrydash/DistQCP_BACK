﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJDBQKFB
    {
           public BYSJDBQKFB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:毕业设计题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTM {get;set;}

           /// <summary>
           /// Desc:毕业设计题目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTMDM {get;set;}

           /// <summary>
           /// Desc:教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:答辩情况分1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBQKF1 {get;set;}

           /// <summary>
           /// Desc:答辩情况分2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBQKF2 {get;set;}

           /// <summary>
           /// Desc:答辩情况分3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBQKF3 {get;set;}

           /// <summary>
           /// Desc:答辩情况分4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBQKF4 {get;set;}

           /// <summary>
           /// Desc:答辩情况分5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBQKF5 {get;set;}

           /// <summary>
           /// Desc:总答辩情况分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDBQKF {get;set;}

           /// <summary>
           /// Desc:答辩评语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBPY {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJECDBMDB
    {
           public BYSJECDBMDB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTMDM {get;set;}

           /// <summary>
           /// Desc:099指导教师成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJSCJ {get;set;}

           /// <summary>
           /// Desc:099评阅成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYCJ {get;set;}

           /// <summary>
           /// Desc:099答辩成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBCJ {get;set;}

           /// <summary>
           /// Desc:099综合成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZPCJ {get;set;}

    }
}

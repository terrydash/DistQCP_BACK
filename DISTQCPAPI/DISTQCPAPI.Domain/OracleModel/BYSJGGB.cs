﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJGGB
    {
           public BYSJGGB(){


           }
           /// <summary>
           /// Desc:099公告或材料的ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ID {get;set;}

           /// <summary>
           /// Desc:099公告标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGBT {get;set;}

           /// <summary>
           /// Desc:099公告正文（url）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGZW {get;set;}

           /// <summary>
           /// Desc:099公告说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGSM {get;set;}

           /// <summary>
           /// Desc:099发表单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FBDW {get;set;}

           /// <summary>
           /// Desc:099发表时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FBSJ {get;set;}

           /// <summary>
           /// Desc:099有效期限
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXQX {get;set;}

           /// <summary>
           /// Desc:099面向大对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDDX {get;set;}

           /// <summary>
           /// Desc:099面向小对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXXDX {get;set;}

           /// <summary>
           /// Desc:099类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXF {get;set;}

    }
}

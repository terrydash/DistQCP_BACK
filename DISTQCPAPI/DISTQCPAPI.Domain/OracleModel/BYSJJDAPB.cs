﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJJDAPB
    {
           public BYSJJDAPB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal JDXH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:设计（论文）各阶段名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDMC {get;set;}

           /// <summary>
           /// Desc:起止日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? MAX_JDXH {get;set;}

    }
}

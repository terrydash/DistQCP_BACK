﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJJDJHB
    {
           public BYSJJDJHB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTMDM {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTM {get;set;}

           /// <summary>
           /// Desc:099任务与要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWYYQ {get;set;}

           /// <summary>
           /// Desc:099起始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSSJ {get;set;}

           /// <summary>
           /// Desc:099结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099指导教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJS {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH1 {get;set;}

           /// <summary>
           /// Desc:099日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ {get;set;}

           /// <summary>
           /// Desc:099工作内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZNR {get;set;}

           /// <summary>
           /// Desc:099执行情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXJH {get;set;}

           /// <summary>
           /// Desc:099指导教师审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSH {get;set;}

           /// <summary>
           /// Desc:099教师对进度计划实施情况总评
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZP {get;set;}

           /// <summary>
           /// Desc:099总评审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZPSH {get;set;}

    }
}

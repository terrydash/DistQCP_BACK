﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJJSSQB
    {
           public BYSJJSSQB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTMDM {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTM {get;set;}

           /// <summary>
           /// Desc:099题目来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMLY {get;set;}

           /// <summary>
           /// Desc:099题目类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMLX {get;set;}

           /// <summary>
           /// Desc:099题目性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMXZ {get;set;}

           /// <summary>
           /// Desc:099起始结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSJSZ {get;set;}

           /// <summary>
           /// Desc:099要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099实习单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXDW {get;set;}

           /// <summary>
           /// Desc:099起始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSSJ {get;set;}

           /// <summary>
           /// Desc:099结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:099是否选课
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXK {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJS {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? RS {get;set;}

           /// <summary>
           /// Desc:099上课地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKDD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXSHJG {get;set;}

           /// <summary>
           /// Desc:N099任务书要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWSYQ {get;set;}

           /// <summary>
           /// Desc:009主要参考资料
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYCKZL {get;set;}

           /// <summary>
           /// Desc:009学院意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYYJ {get;set;}

           /// <summary>
           /// Desc:009教务处意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCYJ {get;set;}

           /// <summary>
           /// Desc:009辅助指导教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LZDJS {get;set;}

           /// <summary>
           /// Desc:099校外指导老师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWJSXM {get;set;}

           /// <summary>
           /// Desc:099校外指导老师单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWJSDW {get;set;}

           /// <summary>
           /// Desc:099校外指导老师职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWJSZC {get;set;}

           /// <summary>
           /// Desc:099校外指导老师年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWJSNL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZZDJS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KYXMMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TSYQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZGXY {get;set;}

           /// <summary>
           /// Desc:099题目难度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMND {get;set;}

           /// <summary>
           /// Desc:099职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:099符合培养目标
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FHPYMB {get;set;}

           /// <summary>
           /// Desc:是否曾用题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFCYT {get;set;}

           /// <summary>
           /// Desc:主管系所
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BMMC {get;set;}

           /// <summary>
           /// Desc:系所审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSHJG {get;set;}

           /// <summary>
           /// Desc:系所意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSYJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJS2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJS4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJS3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZZDJSZC {get;set;}

           /// <summary>
           /// Desc:科研项目编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KYXMBH {get;set;}

           /// <summary>
           /// Desc:科研项目来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KYXMLY {get;set;}

           /// <summary>
           /// Desc:099实施地所在工作室 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSDSZGZS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FJXGBJ {get;set;}

           /// <summary>
           /// Desc:校外所在单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWSZDW {get;set;}

           /// <summary>
           /// Desc:N99 英文题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJYWTM {get;set;}

           /// <summary>
           /// Desc:099任务内容附件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWNRFJ {get;set;}

           /// <summary>
           /// Desc:起始年月日
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSNYR {get;set;}

           /// <summary>
           /// Desc:结束年月日
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSNYR {get;set;}

           /// <summary>
           /// Desc:099论文是否在实践中完成（1：是；其他：否）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LWSFZSJZWC {get;set;}

           /// <summary>
           /// Desc:参与科研项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMMC {get;set;}

           /// <summary>
           /// Desc:项目主持人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMZCR {get;set;}

           /// <summary>
           /// Desc:项目来源或类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMLYLB {get;set;}

           /// <summary>
           /// Desc:项目经费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMJF {get;set;}

           /// <summary>
           /// Desc:立项时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXSJ {get;set;}

           /// <summary>
           /// Desc:结项时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXSJ {get;set;}

           /// <summary>
           /// Desc:学生承担主要任务
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSCDZYRW {get;set;}

           /// <summary>
           /// Desc:参与社会实践活动内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJNR {get;set;}

           /// <summary>
           /// Desc:社会实践对象（单位名称）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJDX {get;set;}

    }
}

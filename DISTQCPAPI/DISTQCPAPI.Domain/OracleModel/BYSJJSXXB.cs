﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJJSXXB
    {
           public BYSJJSXXB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099email地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EMAIL {get;set;}

           /// <summary>
           /// Desc:099手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJH {get;set;}

           /// <summary>
           /// Desc:099固定电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GDDH {get;set;}

           /// <summary>
           /// Desc:099自我介绍
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZWJS {get;set;}

           /// <summary>
           /// Desc:099QQ号码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BGDZ {get;set;}

           /// <summary>
           /// Desc:职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:教师来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLY {get;set;}

    }
}

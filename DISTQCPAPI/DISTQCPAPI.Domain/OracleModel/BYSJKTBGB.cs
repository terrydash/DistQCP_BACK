﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJKTBGB
    {
           public BYSJKTBGB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:课题的意义和工作内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZNR {get;set;}

           /// <summary>
           /// Desc:进度安排
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDAP {get;set;}

           /// <summary>
           /// Desc:预期要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQXG {get;set;}

           /// <summary>
           /// Desc:指导教师意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJSYJ {get;set;}

           /// <summary>
           /// Desc:学生录入时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSKTLRSJ {get;set;}

           /// <summary>
           /// Desc:教师填写意见时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSKTLRSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:中期检查进度计划
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQJCJDJH {get;set;}

           /// <summary>
           /// Desc:中期检查已经完成内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQJCWCNR {get;set;}

           /// <summary>
           /// Desc:中期检查预期效果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQJCYQXG {get;set;}

           /// <summary>
           /// Desc:中期检查尚未完成的内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQJCSWWC {get;set;}

           /// <summary>
           /// Desc:中期检查存在的问题和拟采取的办法
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQJCWTHFF {get;set;}

           /// <summary>
           /// Desc:中期检查教师意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQJCJSYJ {get;set;}

           /// <summary>
           /// Desc:中期检查学生录入时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSZQLRSJ {get;set;}

           /// <summary>
           /// Desc:中期检查教师录入时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZQLRSJ {get;set;}

           /// <summary>
           /// Desc:开题报告是否提交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSFTJ {get;set;}

           /// <summary>
           /// Desc:中期检查是否提交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQJCSFTJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQJCJSSFTJ {get;set;}

           /// <summary>
           /// Desc:学生论文摘要
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSLWZY {get;set;}

           /// <summary>
           /// Desc:成绩表指导教师评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CJBJSPF {get;set;}

           /// <summary>
           /// Desc:成绩表指导教师评语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBJSPY {get;set;}

           /// <summary>
           /// Desc:成绩表学生是否提交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBSFTJ {get;set;}

           /// <summary>
           /// Desc:成绩表教师是否提交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBJSSFTJ {get;set;}

           /// <summary>
           /// Desc:099答辩成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBCJ {get;set;}

           /// <summary>
           /// Desc:099评阅成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYCJ {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTMDM {get;set;}

           /// <summary>
           /// Desc:099综合成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZPCJ {get;set;}

           /// <summary>
           /// Desc:099二次答辩成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZPCJ1 {get;set;}

           /// <summary>
           /// Desc:099答辩过程问答情况记录
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBJL {get;set;}

           /// <summary>
           /// Desc:099评阅人评语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYRPY {get;set;}

           /// <summary>
           /// Desc:099答辩评审评语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBPY {get;set;}

           /// <summary>
           /// Desc:099答辩成绩是否提交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBCJSFTJ {get;set;}

           /// <summary>
           /// Desc:099二次答辩成绩是否提交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZPCJ2SFTJ {get;set;}

           /// <summary>
           /// Desc:指导教师开题报告审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJSKTSH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJKZB
    {
           public BYSJKZB(){


           }
           /// <summary>
           /// Desc:099序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XH {get;set;}

           /// <summary>
           /// Desc:099起始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSSJ {get;set;}

           /// <summary>
           /// Desc:099结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:099是否改退选
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGTX {get;set;}

           /// <summary>
           /// Desc:099当前轮次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DQLC {get;set;}

           /// <summary>
           /// Desc:099指导教师带学生人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSRS {get;set;}

           /// <summary>
           /// Desc:099学生允许选题个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMGS {get;set;}

           /// <summary>
           /// Desc:099提交周志时间间隔
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJJG {get;set;}

           /// <summary>
           /// Desc:099提交周志时间提醒
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJTX {get;set;}

           /// <summary>
           /// Desc:099说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SM {get;set;}

           /// <summary>
           /// Desc:099选题学年学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTXNXQ {get;set;}

           /// <summary>
           /// Desc:099接受学生人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXSS {get;set;}

           /// <summary>
           /// Desc:099申请学分数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQXFS {get;set;}

           /// <summary>
           /// Desc:099最高申请金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZGSQJE {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSZS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XY {get;set;}

    }
}

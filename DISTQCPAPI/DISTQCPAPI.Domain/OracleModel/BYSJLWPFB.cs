﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJLWPFB
    {
           public BYSJLWPFB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:评价对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJDX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:评价人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJR {get;set;}

           /// <summary>
           /// Desc:被评价人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BPJR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:一级指标代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YJZBDM {get;set;}

           /// <summary>
           /// Desc:一级指标名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJZBMC {get;set;}

           /// <summary>
           /// Desc:二级指标代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string EJZBDM {get;set;}

           /// <summary>
           /// Desc:二级指标代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EJZBMC {get;set;}

           /// <summary>
           /// Desc:评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJH {get;set;}

           /// <summary>
           /// Desc:评价内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNR {get;set;}

           /// <summary>
           /// Desc:评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PF {get;set;}

           /// <summary>
           /// Desc:轮次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SFECDB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJSJ {get;set;}

    }
}

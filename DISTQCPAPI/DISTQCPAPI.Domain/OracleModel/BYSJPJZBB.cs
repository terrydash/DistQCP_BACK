﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJPJZBB
    {
           public BYSJPJZBB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJH {get;set;}

           /// <summary>
           /// Desc:评价内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNR {get;set;}

           /// <summary>
           /// Desc:具体要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTYQ {get;set;}

           /// <summary>
           /// Desc:总分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZF {get;set;}

           /// <summary>
           /// Desc:指标类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZBLB {get;set;}

           /// <summary>
           /// Desc:专业类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYLB {get;set;}

    }
}

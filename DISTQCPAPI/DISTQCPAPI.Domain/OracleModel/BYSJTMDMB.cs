﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJTMDMB
    {
           public BYSJTMDMB(){


           }
           /// <summary>
           /// Desc:099毕业设计题目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BYSJTMDM {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTM {get;set;}

           /// <summary>
           /// Desc:099题目来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMLY {get;set;}

           /// <summary>
           /// Desc:099题目类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMLX {get;set;}

           /// <summary>
           /// Desc:099题目性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMXZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BSCSMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJDW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWSNRYQ {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:N99 英文题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJYWTM {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJTMXZB
    {
           public BYSJTMXZB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TMXZDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMXZMC {get;set;}

           /// <summary>
           /// Desc:毕业设计课题类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMXZLB {get;set;}

    }
}

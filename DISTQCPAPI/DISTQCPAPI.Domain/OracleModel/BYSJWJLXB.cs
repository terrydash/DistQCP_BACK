﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJWJLXB
    {
           public BYSJWJLXB(){


           }
           /// <summary>
           /// Desc:099文件类型号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string WJLXH {get;set;}

           /// <summary>
           /// Desc:099文件类型名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WJLXMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJWJSCSJSZDMB
    {
           public BYSJWJSCSJSZDMB(){


           }
           /// <summary>
           /// Desc:099文件类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string WJLX {get;set;}

           /// <summary>
           /// Desc:099起始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSSJ {get;set;}

           /// <summary>
           /// Desc:099结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XYMC {get;set;}

    }
}

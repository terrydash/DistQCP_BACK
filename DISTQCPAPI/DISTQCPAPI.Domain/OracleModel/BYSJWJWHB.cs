﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJWJWHB
    {
           public BYSJWJWHB(){


           }
           /// <summary>
           /// Desc:选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:文件类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LX {get;set;}

           /// <summary>
           /// Desc:文件存放地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DZ {get;set;}

           /// <summary>
           /// Desc:学生是否可以上传（是/否）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSCKZ {get;set;}

           /// <summary>
           /// Desc:教师是否可以上传（是/否）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSCKZ {get;set;}

           /// <summary>
           /// Desc:教师审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSH {get;set;}

           /// <summary>
           /// Desc:学院审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSH {get;set;}

           /// <summary>
           /// Desc:小组审核（填写意见）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZSH {get;set;}

    }
}

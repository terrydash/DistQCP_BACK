﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJXSAPB
    {
           public BYSJXSAPB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTMDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMLY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMLX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMXZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJSZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:第二辅导教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJSZGH1 {get;set;}

           /// <summary>
           /// Desc:第二辅导教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJSXM1 {get;set;}

           /// <summary>
           /// Desc:取得阶段性成果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QDJDXCG {get;set;}

           /// <summary>
           /// Desc:受到生产部门重视或准备采纳
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSCZSCN {get;set;}

           /// <summary>
           /// Desc:有较大经济或实用价值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JJSYJZ {get;set;}

           /// <summary>
           /// Desc:有较高学术水平
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JGXSSP {get;set;}

           /// <summary>
           /// Desc:毕业设计地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BSDD {get;set;}

           /// <summary>
           /// Desc:毕业设计生经费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BSSJF {get;set;}

    }
}

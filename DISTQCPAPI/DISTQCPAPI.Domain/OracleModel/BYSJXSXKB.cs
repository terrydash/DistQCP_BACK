﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJXSXKB
    {
           public BYSJXSXKB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099选课时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKSJ {get;set;}

           /// <summary>
           /// Desc:099选上否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSF {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:099答辩组号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZPCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSPY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSBCZT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSBCZT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSPCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSPY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DF {get;set;}

           /// <summary>
           /// Desc:099答辩时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBSJ {get;set;}

           /// <summary>
           /// Desc:099答辩地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBDD {get;set;}

           /// <summary>
           /// Desc:099检查阶段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCJD {get;set;}

           /// <summary>
           /// Desc:099是否选中
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXZ {get;set;}

           /// <summary>
           /// Desc:099抽查结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCJG {get;set;}

           /// <summary>
           /// Desc:099抽查结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCJG2 {get;set;}

           /// <summary>
           /// Desc:099答辩审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBSH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GCCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJSPDCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYJSPDCJ {get;set;}

           /// <summary>
           /// Desc:099流水号（选课伦次）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSH {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099改后毕业设计题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTM {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTMDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GHBYSJTM {get;set;}

           /// <summary>
           /// Desc:099审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJSYJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYJSYJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYDJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJSZGH1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJSXM1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJSZGH2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJSXM2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJSZGH3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJSXM3 {get;set;}

           /// <summary>
           /// Desc:099题目出处
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMCC {get;set;}

           /// <summary>
           /// Desc:099选题方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTFS {get;set;}

           /// <summary>
           /// Desc:099选题性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTXZ {get;set;}

           /// <summary>
           /// Desc:099选题难度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTND {get;set;}

           /// <summary>
           /// Desc:099选题意义
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTYY {get;set;}

           /// <summary>
           /// Desc:099拟题时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NTSJ {get;set;}

           /// <summary>
           /// Desc:099训练内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XLNR {get;set;}

           /// <summary>
           /// Desc:099评阅人一
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYR1 {get;set;}

           /// <summary>
           /// Desc:099评阅人一职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC1 {get;set;}

           /// <summary>
           /// Desc:099评阅人一学位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XW1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ1 {get;set;}

           /// <summary>
           /// Desc:099评阅人二
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYR2 {get;set;}

           /// <summary>
           /// Desc:099评阅人二职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC2 {get;set;}

           /// <summary>
           /// Desc:099评阅人二学位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XW2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ2 {get;set;}

           /// <summary>
           /// Desc:099二次答辩成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ECDBCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDQRBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDXTBJ {get;set;}

           /// <summary>
           /// Desc:099毕业设计选题题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJXTTM {get;set;}

           /// <summary>
           /// Desc:099毕业设计选题英文题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJXTYWTM {get;set;}

           /// <summary>
           /// Desc:099毕业设计英文题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJYWTM {get;set;}

           /// <summary>
           /// Desc:评阅教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYJSZGH {get;set;}

           /// <summary>
           /// Desc:评阅教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYJSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJSZJ {get;set;}

           /// <summary>
           /// Desc:答辩小组意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBXZYJ {get;set;}

           /// <summary>
           /// Desc:099转入成绩表审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZRCJSH {get;set;}

           /// <summary>
           /// Desc:N99 改后英文题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GHBYSJYWTM {get;set;}

           /// <summary>
           /// Desc:N99 备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSBH {get;set;}

           /// <summary>
           /// Desc:099答辩组号时间地点序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBZH_XH {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099中期成绩，苏州大学
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQCJ {get;set;}

           /// <summary>
           /// Desc:099中期评语，苏州大学
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQPY {get;set;}

           /// <summary>
           /// Desc:099中期成绩保存状态，苏州大学
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQBCZT {get;set;}

           /// <summary>
           /// Desc:099检查组号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCZH {get;set;}

           /// <summary>
           /// Desc:099抽查结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCJG3 {get;set;}

           /// <summary>
           /// Desc:099志愿（选课志愿）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:099教师确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJSCJBL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYJSCJBL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBCJBL {get;set;}

           /// <summary>
           /// Desc:外审成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSCJ {get;set;}

    }
}

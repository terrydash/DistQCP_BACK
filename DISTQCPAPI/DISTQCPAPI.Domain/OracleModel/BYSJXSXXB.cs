﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJXSXXB
    {
           public BYSJXSXXB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099住所
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZS {get;set;}

           /// <summary>
           /// Desc:099email地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EMAIL {get;set;}

           /// <summary>
           /// Desc:099手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJH {get;set;}

           /// <summary>
           /// Desc:099固定电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GDDH {get;set;}

           /// <summary>
           /// Desc:099爱好兴趣
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AHXQ {get;set;}

           /// <summary>
           /// Desc:099自我介绍
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZWJS {get;set;}

           /// <summary>
           /// Desc:099QQ号码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QQ {get;set;}

    }
}

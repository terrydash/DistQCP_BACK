﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJZLLXDMB
    {
           public BYSJZLLXDMB(){


           }
           /// <summary>
           /// Desc:099类型代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LXDM {get;set;}

           /// <summary>
           /// Desc:099类型名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXMC {get;set;}

           /// <summary>
           /// Desc:099类型组号代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHDM {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJZYCJBLSZB
    {
           public BYSJZYCJBLSZB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:中期成绩比例
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ZQCJBL {get;set;}

           /// <summary>
           /// Desc:答辩成绩比例
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal DBCJBL {get;set;}

           /// <summary>
           /// Desc:软件成绩比例
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RJCJBL {get;set;}

           /// <summary>
           /// Desc:是否可修改，0，可以；1，不可。默认不可
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SFKXG {get;set;}

    }
}

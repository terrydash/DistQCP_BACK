﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSJZZB
    {
           public BYSJZZB(){


           }
           /// <summary>
           /// Desc:099ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ID {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099教师回复内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSHF {get;set;}

           /// <summary>
           /// Desc:099回复时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HFSJ {get;set;}

           /// <summary>
           /// Desc:099前一阶段总结
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYJDZJ {get;set;}

           /// <summary>
           /// Desc:099后一阶段计划
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HYJDJH {get;set;}

           /// <summary>
           /// Desc:099问题与建议
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WTYJY {get;set;}

           /// <summary>
           /// Desc:099提交时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJSJ {get;set;}

           /// <summary>
           /// Desc:第几周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJZ {get;set;}

    }
}

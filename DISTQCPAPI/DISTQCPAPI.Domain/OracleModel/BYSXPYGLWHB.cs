﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYSXPYGLWHB
    {
           public BYSXPYGLWHB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099学号/职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099获奖级别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJJB {get;set;}

           /// <summary>
           /// Desc:099对象类别（区分学生，教师）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXLB {get;set;}

    }
}

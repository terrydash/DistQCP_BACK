﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BYTJKZB
    {
           public BYTJKZB(){


           }
           /// <summary>
           /// Desc:099条件名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJMC {get;set;}

           /// <summary>
           /// Desc:099是否可见,1可见，0不可见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SFKJ {get;set;}

           /// <summary>
           /// Desc:099类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LB {get;set;}

    }
}

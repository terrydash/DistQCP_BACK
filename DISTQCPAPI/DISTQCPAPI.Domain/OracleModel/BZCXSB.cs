﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BZCXSB
    {
           public BZCXSB(){


           }
           /// <summary>
           /// Desc:001学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:003性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:004出生日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSRQ {get;set;}

           /// <summary>
           /// Desc:005学生类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSLB {get;set;}

           /// <summary>
           /// Desc:006学历
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XL {get;set;}

           /// <summary>
           /// Desc:007职称职务
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZCZW {get;set;}

           /// <summary>
           /// Desc:008电话邮编
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DHYB {get;set;}

           /// <summary>
           /// Desc:009来源单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYDW {get;set;}

           /// <summary>
           /// Desc:010学习期限
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XXQX {get;set;}

           /// <summary>
           /// Desc:011入学日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RXRQ {get;set;}

           /// <summary>
           /// Desc:012毕业日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYRQ {get;set;}

           /// <summary>
           /// Desc:013进修学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXXY {get;set;}

           /// <summary>
           /// Desc:014辅修专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXZY {get;set;}

           /// <summary>
           /// Desc:015备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MM {get;set;}

           /// <summary>
           /// Desc:099进修班级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXBJ {get;set;}

           /// <summary>
           /// Desc:016年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BZRPFB
    {
           public BZRPFB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099班级代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJDM {get;set;}

           /// <summary>
           /// Desc:099评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJH {get;set;}

           /// <summary>
           /// Desc:099评价内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNR {get;set;}

           /// <summary>
           /// Desc:099评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PF {get;set;}

           /// <summary>
           /// Desc:099是否提交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SFTJ {get;set;}

           /// <summary>
           /// Desc:099评价时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJSJ {get;set;}

           /// <summary>
           /// Desc:099等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJ {get;set;}

           /// <summary>
           /// Desc:099评价信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJXX {get;set;}

           /// <summary>
           /// Desc:099对象代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXDM {get;set;}

    }
}

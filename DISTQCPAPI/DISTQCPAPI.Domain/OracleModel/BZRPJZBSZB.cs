﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class BZRPJZBSZB
    {
           public BZRPJZBSZB(){


           }
           /// <summary>
           /// Desc:099评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJH {get;set;}

           /// <summary>
           /// Desc:099评价内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNR {get;set;}

           /// <summary>
           /// Desc:099权重
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? QZ {get;set;}

    }
}

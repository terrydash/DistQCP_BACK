﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CBSDMB
    {
           public CBSDMB(){


           }
           /// <summary>
           /// Desc:099出版社代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CBSDM {get;set;}

           /// <summary>
           /// Desc:099出版社名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBSMC {get;set;}

    }
}

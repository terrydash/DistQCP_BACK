﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJBLSZB
    {
           public CJBLSZB(){


           }
           /// <summary>
           /// Desc:099平时成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? PSCJ {get;set;}

           /// <summary>
           /// Desc:099实验成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SYCJ {get;set;}

           /// <summary>
           /// Desc:099期末成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QMCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QZCJ {get;set;}

    }
}

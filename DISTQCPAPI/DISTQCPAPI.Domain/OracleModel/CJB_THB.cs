﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJB_THB
    {
           public CJB_THB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099替换课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string THKCDM {get;set;}

           /// <summary>
           /// Desc:099替换课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THKCMC {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:099重修标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? CXBJ {get;set;}

           /// <summary>
           /// Desc:099修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGR {get;set;}

           /// <summary>
           /// Desc:099修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGSJ {get;set;}

    }
}

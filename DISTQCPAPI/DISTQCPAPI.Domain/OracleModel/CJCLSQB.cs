﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJCLSQB
    {
           public CJCLSQB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:教学班名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXBMC {get;set;}

           /// <summary>
           /// Desc:上课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKJS {get;set;}

           /// <summary>
           /// Desc:申请人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQR {get;set;}

           /// <summary>
           /// Desc:申请原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQYY {get;set;}

           /// <summary>
           /// Desc:申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:审核类型 0：教务处;1：学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHLX {get;set;}

           /// <summary>
           /// Desc:审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

           /// <summary>
           /// Desc:审核标记 0：未审核；1：审核通过；2：审核不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHBJ {get;set;}

           /// <summary>
           /// Desc:审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:是否补考
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBK {get;set;}

           /// <summary>
           /// Desc:申请次数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SQCS {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJDSQDYJLB
    {
           public CJDSQDYJLB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal LSH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:预约登记的时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? YYDJSJ {get;set;}

           /// <summary>
           /// Desc:预约打印的份数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YYDYFS {get;set;}

           /// <summary>
           /// Desc:成绩单的类别代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDLBDM {get;set;}

           /// <summary>
           /// Desc:领取时间代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LQSJDM {get;set;}

           /// <summary>
           /// Desc:是否违约
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFWY {get;set;}

           /// <summary>
           /// Desc:是否免费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFMF {get;set;}

           /// <summary>
           /// Desc:是否已经处理
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYCL {get;set;}

           /// <summary>
           /// Desc:取消标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QXBJ {get;set;}

    }
}

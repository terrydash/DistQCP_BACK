﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJDZB
    {
           public CJDZB(){


           }
           /// <summary>
           /// Desc:099成绩
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:099对应成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DYCJ {get;set;}

           /// <summary>
           /// Desc:099级别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JB {get;set;}

           /// <summary>
           /// Desc:099快捷键
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KJJ {get;set;}

           /// <summary>
           /// Desc:百分制转非百分制时使用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CXCS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZJCX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFCJPM {get;set;}

           /// <summary>
           /// Desc:N012用来在成绩对照表中确定显示该种成绩的情况下学生是否参加了考试
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSF {get;set;}

           /// <summary>
           /// Desc:099对应英文
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYYWMC {get;set;}

           /// <summary>
           /// Desc:N099 是否允许网上录入
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEBSFYXLR {get;set;}

    }
}

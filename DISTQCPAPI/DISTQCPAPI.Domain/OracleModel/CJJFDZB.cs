﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJJFDZB
    {
           public CJJFDZB(){


           }
           /// <summary>
           /// Desc:加分类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JFLB {get;set;}

           /// <summary>
           /// Desc:加分大类
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JFDL {get;set;}

           /// <summary>
           /// Desc:加分分数段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JFFSD {get;set;}

           /// <summary>
           /// Desc:对应分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFZ {get;set;}

    }
}

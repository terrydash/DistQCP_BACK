﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJPMSZB
    {
           public CJPMSZB(){


           }
           /// <summary>
           /// Desc:序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:成绩比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBL {get;set;}

           /// <summary>
           /// Desc:评价比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJBL {get;set;}

    }
}

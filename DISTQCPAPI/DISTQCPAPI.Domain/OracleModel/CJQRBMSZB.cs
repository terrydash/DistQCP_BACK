﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJQRBMSZB
    {
           public CJQRBMSZB(){


           }
           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:099结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:099是否限制选课
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXZXK {get;set;}

           /// <summary>
           /// Desc:099是否限制查成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXZCCJ {get;set;}

           /// <summary>
           /// Desc:099有效否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXF {get;set;}

           /// <summary>
           /// Desc:099每学期最大门数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? MXQZDMS {get;set;}

           /// <summary>
           /// Desc:099查分成绩段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXCFFSD {get;set;}

           /// <summary>
           /// Desc:099允许查分课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXCFKCXZ {get;set;}

    }
}

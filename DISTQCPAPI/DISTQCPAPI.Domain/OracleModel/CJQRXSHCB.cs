﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJQRXSHCB
    {
           public CJQRXSHCB(){


           }
           /// <summary>
           /// Desc:099申请编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ID {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099平时成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSCJ {get;set;}

           /// <summary>
           /// Desc:099期中成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZCJ {get;set;}

           /// <summary>
           /// Desc:099实验成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYCJ {get;set;}

           /// <summary>
           /// Desc:099期末成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QMCJ {get;set;}

           /// <summary>
           /// Desc:099原成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQCJ {get;set;}

           /// <summary>
           /// Desc:099新成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XCJ {get;set;}

           /// <summary>
           /// Desc:099申请理由
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQLY {get;set;}

           /// <summary>
           /// Desc:099核查内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCNR {get;set;}

           /// <summary>
           /// Desc:099教师核查
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSHC {get;set;}

           /// <summary>
           /// Desc:099教师建议
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSYJ {get;set;}

           /// <summary>
           /// Desc:099学院核查
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYHC {get;set;}

           /// <summary>
           /// Desc:099学院建议
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYYJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCHC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCYJ {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099学院核查人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYHCR {get;set;}

           /// <summary>
           /// Desc:099教务处核查人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCHCR {get;set;}

           /// <summary>
           /// Desc:099保存状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BCZT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TGZR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDY {get;set;}

           /// <summary>
           /// Desc:N99联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXDH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJSHB
    {
           public CJSHB(){


           }
           /// <summary>
           /// Desc:学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:审核状态 0：待审核 1：审核通过 2：审核不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHZT {get;set;}

           /// <summary>
           /// Desc:审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

           /// <summary>
           /// Desc:审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

           /// <summary>
           /// Desc:0:少数民族不及格成绩申请 1:补考缓考申请
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LX {get;set;}

           /// <summary>
           /// Desc:成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099处理状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLZT {get;set;}

           /// <summary>
           /// Desc:099学院审核状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHZT {get;set;}

           /// <summary>
           /// Desc:099学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHSJ {get;set;}

           /// <summary>
           /// Desc:099学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:加分类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFLX {get;set;}

    }
}

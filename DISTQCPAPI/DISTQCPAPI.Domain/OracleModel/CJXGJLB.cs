﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJXGJLB
    {
           public CJXGJLB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099次数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CS {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:099任课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKJS {get;set;}

           /// <summary>
           /// Desc:099任课教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKJSZGH {get;set;}

           /// <summary>
           /// Desc:099修改前成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGQCJ {get;set;}

           /// <summary>
           /// Desc:099修改后成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGHCJ {get;set;}

           /// <summary>
           /// Desc:099修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGR {get;set;}

           /// <summary>
           /// Desc:099修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGSJ {get;set;}

    }
}

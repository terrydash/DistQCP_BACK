﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJXGSQB
    {
           public CJXGSQB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099修改前课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YKCDM {get;set;}

           /// <summary>
           /// Desc:099修改前课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YKCMC {get;set;}

           /// <summary>
           /// Desc:099修改前学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXF {get;set;}

           /// <summary>
           /// Desc:099修改前课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YKCXZ {get;set;}

           /// <summary>
           /// Desc:099修改后课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKCDM {get;set;}

           /// <summary>
           /// Desc:099修改后课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKCMC {get;set;}

           /// <summary>
           /// Desc:099修改后学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXF {get;set;}

           /// <summary>
           /// Desc:099修改后课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKCXZ {get;set;}

           /// <summary>
           /// Desc:099申请人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQR {get;set;}

           /// <summary>
           /// Desc:099申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:099审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

           /// <summary>
           /// Desc:099审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

           /// <summary>
           /// Desc:099审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJG {get;set;}

           /// <summary>
           /// Desc:099是否转入
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZR {get;set;}

           /// <summary>
           /// Desc:099转入人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZRR {get;set;}

           /// <summary>
           /// Desc:099转入时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZRSJ {get;set;}

    }
}

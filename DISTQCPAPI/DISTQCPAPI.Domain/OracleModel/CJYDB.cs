﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CJYDB
    {
           public CJYDB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099操作人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZR {get;set;}

           /// <summary>
           /// Desc:099操作时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CZSJ {get;set;}

           /// <summary>
           /// Desc:099操作标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZBJ {get;set;}

           /// <summary>
           /// Desc:099操作原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZYY {get;set;}

    }
}

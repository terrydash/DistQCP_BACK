﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CLB
    {
           public CLB(){


           }
           /// <summary>
           /// Desc:N99材料编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CLBH {get;set;}

           /// <summary>
           /// Desc:N99材料名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLMC {get;set;}

           /// <summary>
           /// Desc:N99型号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:N99规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GG {get;set;}

           /// <summary>
           /// Desc:N99数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SL {get;set;}

           /// <summary>
           /// Desc:N99计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLDW {get;set;}

           /// <summary>
           /// Desc:N99单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? DJ {get;set;}

           /// <summary>
           /// Desc:N99购买日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GMRQ {get;set;}

           /// <summary>
           /// Desc:N99材料性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLXZ {get;set;}

           /// <summary>
           /// Desc:N99厂家或产地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJHCD {get;set;}

           /// <summary>
           /// Desc:N99隶属实验室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSSYS {get;set;}

           /// <summary>
           /// Desc:N99隶属部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSBM {get;set;}

           /// <summary>
           /// Desc:N99状况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZK {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099品牌
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PP {get;set;}

           /// <summary>
           /// Desc:099总价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJ {get;set;}

           /// <summary>
           /// Desc:099购置人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZRXM {get;set;}

           /// <summary>
           /// Desc:099购置人职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZRZGH {get;set;}

           /// <summary>
           /// Desc:099入库日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKSJ {get;set;}

           /// <summary>
           /// Desc:099入库标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKBJ {get;set;}

           /// <summary>
           /// Desc:099材料表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLDM {get;set;}

           /// <summary>
           /// Desc:099进货商名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHSMC {get;set;}

           /// <summary>
           /// Desc:099邮政编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YZBM {get;set;}

           /// <summary>
           /// Desc:099电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DH {get;set;}

           /// <summary>
           /// Desc:099传真
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLMCPY {get;set;}

    }
}

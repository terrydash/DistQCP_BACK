﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CLDWDMB
    {
           public CLDWDMB(){


           }
           /// <summary>
           /// Desc:001材料单位代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CLDWDM {get;set;}

           /// <summary>
           /// Desc:002材料单位名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLDWMC {get;set;}

    }
}

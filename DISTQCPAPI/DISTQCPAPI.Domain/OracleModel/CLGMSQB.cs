﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CLGMSQB
    {
           public CLGMSQB(){


           }
           /// <summary>
           /// Desc:N99申请号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SQH {get;set;}

           /// <summary>
           /// Desc:N99材料编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLBH {get;set;}

           /// <summary>
           /// Desc:N99材料名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLMC {get;set;}

           /// <summary>
           /// Desc:N99型号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:N99规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GG {get;set;}

           /// <summary>
           /// Desc:N99数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SL {get;set;}

           /// <summary>
           /// Desc:N99计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLDW {get;set;}

           /// <summary>
           /// Desc:N99单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? DJ {get;set;}

           /// <summary>
           /// Desc:N99购买日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GMRQ {get;set;}

           /// <summary>
           /// Desc:N99材料性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLXZ {get;set;}

           /// <summary>
           /// Desc:N99厂家或产地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJHCD {get;set;}

           /// <summary>
           /// Desc:N99隶属实验室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSSYS {get;set;}

           /// <summary>
           /// Desc:N99隶属部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSBM {get;set;}

           /// <summary>
           /// Desc:N99状况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZK {get;set;}

           /// <summary>
           /// Desc:N99备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:N99申请人编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQRBH {get;set;}

           /// <summary>
           /// Desc:N99申请人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQRXM {get;set;}

           /// <summary>
           /// Desc:N99购买原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GMYY {get;set;}

           /// <summary>
           /// Desc:N99购买用途
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GMYT {get;set;}

           /// <summary>
           /// Desc:N99审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJG {get;set;}

    }
}

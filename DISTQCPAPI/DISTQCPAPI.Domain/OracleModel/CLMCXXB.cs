﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CLMCXXB
    {
           public CLMCXXB(){


           }
           /// <summary>
           /// Desc:099材料代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CLDM {get;set;}

           /// <summary>
           /// Desc:099材料名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLMC {get;set;}

           /// <summary>
           /// Desc:099品牌
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PP {get;set;}

           /// <summary>
           /// Desc:099规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GG {get;set;}

           /// <summary>
           /// Desc:099型号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSDLDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSLBDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJSL {get;set;}

           /// <summary>
           /// Desc:099单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLDWMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLMCPY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CLXNXQ_XJYDJKB2
    {
           public CLXNXQ_XJYDJKB2(){


           }
           /// <summary>
           /// Desc:011学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:012学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:013学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:014警告结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JGJG {get;set;}

           /// <summary>
           /// Desc:014是否手工修改
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGXG {get;set;}

           /// <summary>
           /// Desc:015备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZXX {get;set;}

           /// <summary>
           /// Desc:099累计警告标志。为0表示连续警告；为1表示累计警告。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJJGBZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JGBH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CLYTDMB
    {
           public CLYTDMB(){


           }
           /// <summary>
           /// Desc:001材料用途代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CLYTDM {get;set;}

           /// <summary>
           /// Desc:002材料用途名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLYTMC {get;set;}

    }
}

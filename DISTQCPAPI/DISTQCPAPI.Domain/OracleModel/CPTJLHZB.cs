﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CPTJLHZB
    {
           public CPTJLHZB(){


           }
           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BM {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJH {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJF {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXDM {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZGF {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDF {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QJ {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZC {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CYXS {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZ {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MYD {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CPYS {get;set;}

    }
}

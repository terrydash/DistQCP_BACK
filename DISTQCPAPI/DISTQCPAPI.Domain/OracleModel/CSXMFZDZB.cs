﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CSXMFZDZB
    {
           public CSXMFZDZB(){


           }
           /// <summary>
           /// Desc:099性别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:099项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DLDM {get;set;}

           /// <summary>
           /// Desc:099序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XH {get;set;}

           /// <summary>
           /// Desc:099成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:099分值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NJ {get;set;}

    }
}

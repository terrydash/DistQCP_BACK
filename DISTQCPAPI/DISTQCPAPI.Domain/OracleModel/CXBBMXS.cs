﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CXBBMXS
    {
           public CXBBMXS(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:099专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXQ {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJF {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XBX {get;set;}

           /// <summary>
           /// Desc:N10可报名的轮次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJC {get;set;}

           /// <summary>
           /// Desc:099考试时间选择，0期末考试，1下学期期初考试
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EXAMTIME {get;set;}

           /// <summary>
           /// Desc:099第几次重修报名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJCCX {get;set;}

           /// <summary>
           /// Desc:099是否生成
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSC {get;set;}

           /// <summary>
           /// Desc:099标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJ {get;set;}

    }
}

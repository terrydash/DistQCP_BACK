﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CXKCKSB
    {
           public CXKCKSB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:003课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:004课程名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:005人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? RS {get;set;}

           /// <summary>
           /// Desc:006任务是否开设
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWKS {get;set;}

           /// <summary>
           /// Desc:007学院确定能否开设
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NFKS {get;set;}

    }
}

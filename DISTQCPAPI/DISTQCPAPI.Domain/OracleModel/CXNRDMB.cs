﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CXNRDMB
    {
           public CXNRDMB(){


           }
           /// <summary>
           /// Desc:099创新内容代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXNRDM {get;set;}

           /// <summary>
           /// Desc:099创新内容名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CXNRMC {get;set;}

           /// <summary>
           /// Desc:099公选课类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXKLB {get;set;}

           /// <summary>
           /// Desc:099课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:099类型代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXDM {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

    }
}

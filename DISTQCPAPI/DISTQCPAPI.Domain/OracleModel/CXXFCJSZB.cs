﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CXXFCJSZB
    {
           public CXXFCJSZB(){


           }
           /// <summary>
           /// Desc:奖项代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXDM {get;set;}

           /// <summary>
           /// Desc:奖项名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXMC {get;set;}

           /// <summary>
           /// Desc:对应成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYCJ {get;set;}

    }
}

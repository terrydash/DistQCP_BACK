﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class CZRZKGB
    {
           public CZRZKGB(){


           }
           /// <summary>
           /// Desc:099登入开关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DRKG {get;set;}

           /// <summary>
           /// Desc:099操作开关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZKG {get;set;}

           /// <summary>
           /// Desc:099开关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEBKG {get;set;}

    }
}

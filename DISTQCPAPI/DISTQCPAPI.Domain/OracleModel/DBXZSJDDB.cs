﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DBXZSJDDB
    {
           public DBXZSJDDB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099答辩组号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DBZH {get;set;}

           /// <summary>
           /// Desc:099答辩日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DBRQ {get;set;}

           /// <summary>
           /// Desc:099答辩周次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBZC {get;set;}

           /// <summary>
           /// Desc:099上下午
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXW {get;set;}

           /// <summary>
           /// Desc:099答辩地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBDD {get;set;}

           /// <summary>
           /// Desc:099答辩组号时间地点序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBZH_XH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DBZYZCDMB
    {
           public DBZYZCDMB(){


           }
           /// <summary>
           /// Desc:099组员级别名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYJBMC {get;set;}

           /// <summary>
           /// Desc:099限定职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XDZC {get;set;}

    }
}

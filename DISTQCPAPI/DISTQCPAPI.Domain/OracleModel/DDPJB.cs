﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DDPJB
    {
           public DDPJB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:被评教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BPJSZGH {get;set;}

           /// <summary>
           /// Desc:督导教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:督导评语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DDPY {get;set;}

           /// <summary>
           /// Desc:被评教师反馈
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BPJSFK {get;set;}

           /// <summary>
           /// Desc:评价时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJSJ {get;set;}

           /// <summary>
           /// Desc:督导评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DDPF {get;set;}

    }
}

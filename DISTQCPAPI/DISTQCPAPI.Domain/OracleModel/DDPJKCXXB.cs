﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DDPJKCXXB
    {
           public DDPJKCXXB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099参评教师
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099被评教师
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099上课地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKDD {get;set;}

           /// <summary>
           /// Desc:099周次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJZ {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQJ {get;set;}

           /// <summary>
           /// Desc:099上课节次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKJC {get;set;}

           /// <summary>
           /// Desc:099评价时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJSJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DDXXFKB
    {
           public DDXXFKB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:督导职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:被评教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BPJSZGH {get;set;}

           /// <summary>
           /// Desc:督导信息反馈
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DDXXFK {get;set;}

           /// <summary>
           /// Desc:被评教师反馈
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BPJSFK {get;set;}

           /// <summary>
           /// Desc:督导信息反馈时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DDXXFKSJ {get;set;}

           /// <summary>
           /// Desc:被评教师反馈时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BPJSFKSJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DGJSSKXXB
    {
           public DGJSSKXXB(){


           }
           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XH {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BM {get;set;}

           /// <summary>
           /// Desc:099上课内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SKNR {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZXS {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYQZZ {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CDBS {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKDD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYFX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? RL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JXBSSX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YXRS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JXBSXX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH_BKSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFPDG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXBMC2 {get;set;}

           /// <summary>
           /// Desc:099实验排课单双周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKDSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYJJLP {get;set;}

           /// <summary>
           /// Desc:099上课具体时间二
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ_JTSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XBZXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSRWQR {get;set;}

           /// <summary>
           /// Desc:099实验排课教室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYPKJSBH {get;set;}

           /// <summary>
           /// Desc:099实验排课楼号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYPKLH {get;set;}

           /// <summary>
           /// Desc:099教学班名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYJXBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PCBZLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PCXZMC {get;set;}

           /// <summary>
           /// Desc:N099 网上选课该教学班是否控制容量 ‘1‘ 控制容量，’0‘ 不控制容量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RLXZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXRS {get;set;}

           /// <summary>
           /// Desc:容量调整标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RLTZBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YSRL {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ_BJEW {get;set;}

           /// <summary>
           /// Desc:实验录教学日历资格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYLJXRLZG {get;set;}

           /// <summary>
           /// Desc:总学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZSX_DGJS {get;set;}

           /// <summary>
           /// Desc:理论学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? LLXS_DGJS {get;set;}

           /// <summary>
           /// Desc:实验学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SYXS_DGJS {get;set;}

           /// <summary>
           /// Desc:讲习学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JXXS_DGJS {get;set;}

           /// <summary>
           /// Desc:是否可评
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKP {get;set;}

    }
}

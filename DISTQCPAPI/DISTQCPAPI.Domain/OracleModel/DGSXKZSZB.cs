﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DGSXKZSZB
    {
           public DGSXKZSZB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099选课学年学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKXNXQ {get;set;}

           /// <summary>
           /// Desc:099学生可选择企业数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXQYS {get;set;}

           /// <summary>
           /// Desc:099起始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSSJ {get;set;}

           /// <summary>
           /// Desc:099结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:099说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SM {get;set;}

    }
}

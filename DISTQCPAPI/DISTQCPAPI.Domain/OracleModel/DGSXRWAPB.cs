﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DGSXRWAPB
    {
           public DGSXRWAPB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099系名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMC {get;set;}

           /// <summary>
           /// Desc:099企业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYMC {get;set;}

           /// <summary>
           /// Desc:099企业开课课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYKKKC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYZSS {get;set;}

           /// <summary>
           /// Desc:099替换课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THKCDM {get;set;}

           /// <summary>
           /// Desc:099替换课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THKCMC {get;set;}

           /// <summary>
           /// Desc:099起始结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSJSZ {get;set;}

           /// <summary>
           /// Desc:099指导教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJSZGH {get;set;}

           /// <summary>
           /// Desc:099指导教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

    }
}

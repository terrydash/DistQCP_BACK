﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DGSXXSSXJL
    {
           public DGSXXSSXJL(){


           }
           /// <summary>
           /// Desc:师傅登陆号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SFDLH {get;set;}

           /// <summary>
           /// Desc:师傅姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXM {get;set;}

           /// <summary>
           /// Desc:电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTELEPHONE {get;set;}

           /// <summary>
           /// Desc:邮箱
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFMAIL {get;set;}

           /// <summary>
           /// Desc:所在单位代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWDM {get;set;}

           /// <summary>
           /// Desc:所属部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSBM {get;set;}

           /// <summary>
           /// Desc:职位职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZWZC {get;set;}

           /// <summary>
           /// Desc:单位名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DWMC {get;set;}

           /// <summary>
           /// Desc:单位地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWDZ {get;set;}

           /// <summary>
           /// Desc:单位联系人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWLXR {get;set;}

           /// <summary>
           /// Desc:单位联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWLXDH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:实习开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXKSSJ {get;set;}

           /// <summary>
           /// Desc:实习结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJSSJ {get;set;}

           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:学生实习岗位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSXGW {get;set;}

           /// <summary>
           /// Desc:学生实习部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSXBM {get;set;}

           /// <summary>
           /// Desc:学生实习岗位和任务
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GWRW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KL {get;set;}

           /// <summary>
           /// Desc:为1则为学生最后评分对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XSQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXBG {get;set;}

           /// <summary>
           /// Desc:单位性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWXZ {get;set;}

           /// <summary>
           /// Desc:单位证明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWZM {get;set;}

           /// <summary>
           /// Desc:专业对口情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDKQK {get;set;}

           /// <summary>
           /// Desc:工资情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZQK {get;set;}

           /// <summary>
           /// Desc:工作地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZD {get;set;}

           /// <summary>
           /// Desc:学生实习岗位性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSXGWXZ {get;set;}

           /// <summary>
           /// Desc:省份代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDM {get;set;}

           /// <summary>
           /// Desc:工作地县市 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZDXS {get;set;}

           /// <summary>
           /// Desc:教师录入信息时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLRSJ {get;set;}

           /// <summary>
           /// Desc:教师修改信息时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXGSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYDQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWSFDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWSDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWXDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXZT {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DGSXXSSXJL_JY
    {
           public DGSXXSSXJL_JY(){


           }
           /// <summary>
           /// Desc:099所在单位代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWDM {get;set;}

           /// <summary>
           /// Desc:099所属部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSBM {get;set;}

           /// <summary>
           /// Desc:099职位职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZWZC {get;set;}

           /// <summary>
           /// Desc:099单位名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DWMC {get;set;}

           /// <summary>
           /// Desc:099单位地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWDZ {get;set;}

           /// <summary>
           /// Desc:099单位联系人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWLXR {get;set;}

           /// <summary>
           /// Desc:099单位联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWLXDH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099签约时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYSJ {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XQ {get;set;}

           /// <summary>
           /// Desc:099学生就业岗位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSXGW {get;set;}

           /// <summary>
           /// Desc:099学生就业部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSXBM {get;set;}

           /// <summary>
           /// Desc:099学生就业岗位和任务
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GWRW {get;set;}

           /// <summary>
           /// Desc:099单位性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWXZ {get;set;}

           /// <summary>
           /// Desc:099单位证明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWZM {get;set;}

           /// <summary>
           /// Desc:099专业对口情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDKQK {get;set;}

           /// <summary>
           /// Desc:099工资情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZQK {get;set;}

           /// <summary>
           /// Desc:099工作地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZD {get;set;}

           /// <summary>
           /// Desc:099学生就业岗位性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSXGWXZ {get;set;}

           /// <summary>
           /// Desc:099省份代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZDXSDM {get;set;}

           /// <summary>
           /// Desc:099工作地县市 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZDXS {get;set;}

           /// <summary>
           /// Desc:099教师录入信息时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLRSJ {get;set;}

           /// <summary>
           /// Desc:099教师修改信息时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXGSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYDQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYQK {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DGSX_SFPFB
    {
           public DGSX_SFPFB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:师傅登陆号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SFDLH {get;set;}

           /// <summary>
           /// Desc:师傅姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXM {get;set;}

           /// <summary>
           /// Desc:评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJH {get;set;}

           /// <summary>
           /// Desc:对应成绩
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public Single DYCJ {get;set;}

           /// <summary>
           /// Desc:师傅评语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PY {get;set;}

           /// <summary>
           /// Desc:099评价时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNR {get;set;}

    }
}

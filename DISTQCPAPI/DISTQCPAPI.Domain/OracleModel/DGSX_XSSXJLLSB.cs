﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DGSX_XSSXJLLSB
    {
           public DGSX_XSSXJLLSB(){


           }
           /// <summary>
           /// Desc:序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NO {get;set;}

           /// <summary>
           /// Desc:指导教师编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZDJSBH {get;set;}

           /// <summary>
           /// Desc:单位代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DWDM {get;set;}

           /// <summary>
           /// Desc:单位联系人编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXRBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:实习开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXKSSJ {get;set;}

           /// <summary>
           /// Desc:实习结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJSSJ {get;set;}

           /// <summary>
           /// Desc:学生实习岗位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSXGW {get;set;}

           /// <summary>
           /// Desc:学生实习部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSXBM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KL {get;set;}

           /// <summary>
           /// Desc:为1则为学生最后评分对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XSQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXBG {get;set;}

           /// <summary>
           /// Desc:单位证明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWZM {get;set;}

           /// <summary>
           /// Desc:专业对口情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDKQK {get;set;}

           /// <summary>
           /// Desc:工资情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZQK {get;set;}

           /// <summary>
           /// Desc:工作地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZD {get;set;}

           /// <summary>
           /// Desc:省份代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZS {get;set;}

           /// <summary>
           /// Desc:工作地县市 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZDXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLRSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXGSJ {get;set;}

           /// <summary>
           /// Desc:实习状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXZT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZSJ {get;set;}

           /// <summary>
           /// Desc:教师确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSQR {get;set;}

           /// <summary>
           /// Desc:更换单位理由
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string REASON {get;set;}

    }
}

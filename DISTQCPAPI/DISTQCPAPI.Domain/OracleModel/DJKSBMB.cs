﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DJKSBMB
    {
           public DJKSBMB(){


           }
           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? NO {get;set;}

           /// <summary>
           /// Desc:099语言级别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YYJB {get;set;}

           /// <summary>
           /// Desc:099编排学校
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BPXX {get;set;}

           /// <summary>
           /// Desc:099编排校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BPXQ {get;set;}

           /// <summary>
           /// Desc:099考试时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:099考场
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KC {get;set;}

           /// <summary>
           /// Desc:099座位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZW {get;set;}

           /// <summary>
           /// Desc:099准考证
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZKZH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:099证件类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJLX {get;set;}

           /// <summary>
           /// Desc:099证件号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZH {get;set;}

           /// <summary>
           /// Desc:099所属学校代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXX {get;set;}

           /// <summary>
           /// Desc:099所属学校名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXMC {get;set;}

           /// <summary>
           /// Desc:099所属校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXQ {get;set;}

           /// <summary>
           /// Desc:099院系代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYDM {get;set;}

           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099班级代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJDM {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099报名号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BMH {get;set;}

           /// <summary>
           /// Desc:099考场位置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCWZ {get;set;}

           /// <summary>
           /// Desc:099试点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSD {get;set;}

    }
}

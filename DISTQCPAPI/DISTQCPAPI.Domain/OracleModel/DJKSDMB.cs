﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DJKSDMB
    {
           public DJKSDMB(){


           }
           /// <summary>
           /// Desc:099等级考试代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJKSDM {get;set;}

           /// <summary>
           /// Desc:099等级考试名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKSMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWMC {get;set;}

    }
}

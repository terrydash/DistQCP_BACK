﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DJKSHGFSXSZB
    {
           public DJKSHGFSXSZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099等级考试名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJKSMC {get;set;}

           /// <summary>
           /// Desc:099分数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FS {get;set;}

           /// <summary>
           /// Desc:099截止年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JZN {get;set;}

           /// <summary>
           /// Desc:099截止月
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JZY {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DLXSB
    {
           public DLXSB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099大类代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DLDM {get;set;}

           /// <summary>
           /// Desc:099大类名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLMC {get;set;}

           /// <summary>
           /// Desc:099重复系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CFXS {get;set;}

           /// <summary>
           /// Desc:099常量系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLXS {get;set;}

           /// <summary>
           /// Desc:099常量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CL {get;set;}

           /// <summary>
           /// Desc:099所属课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSKCXZ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

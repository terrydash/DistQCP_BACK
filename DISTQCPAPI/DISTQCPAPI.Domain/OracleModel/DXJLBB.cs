﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DXJLBB
    {
           public DXJLBB(){


           }
           /// <summary>
           /// Desc:099贷学金类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXJLBDM {get;set;}

           /// <summary>
           /// Desc:099贷学金类别名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXJLBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JE {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DXQDMB
    {
           public DXQDMB(){


           }
           /// <summary>
           /// Desc:099短学期代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXQDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXQMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class DYBBBZB
    {
           public DYBBBZB(){


           }
           /// <summary>
           /// Desc:099打印类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DYLB {get;set;}

           /// <summary>
           /// Desc:099打印备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYBZ {get;set;}

    }
}

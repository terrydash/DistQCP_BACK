﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ELXFXMDMB
    {
           public ELXFXMDMB(){


           }
           /// <summary>
           /// Desc:创新内容代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMDM {get;set;}

           /// <summary>
           /// Desc:创新内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMMC {get;set;}

           /// <summary>
           /// Desc:创新类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSLX {get;set;}

           /// <summary>
           /// Desc:004创新类型代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSLXDM {get;set;}

           /// <summary>
           /// Desc:005认定机构
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RDJG {get;set;}

           /// <summary>
           /// Desc:006项目学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXF {get;set;}

    }
}

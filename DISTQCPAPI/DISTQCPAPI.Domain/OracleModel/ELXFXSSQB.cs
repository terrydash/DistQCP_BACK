﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ELXFXSSQB
    {
           public ELXFXSSQB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099学分项目
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XFXM {get;set;}

           /// <summary>
           /// Desc:099项目内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMNR {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099教务处审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSH {get;set;}

           /// <summary>
           /// Desc:099学院审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSH {get;set;}

           /// <summary>
           /// Desc:099审核学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHXF {get;set;}

           /// <summary>
           /// Desc:099项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMDM {get;set;}

           /// <summary>
           /// Desc:论文专利发表时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LWZLFBSJ {get;set;}

           /// <summary>
           /// Desc:099教务处审核不通过原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHBZ {get;set;}

           /// <summary>
           /// Desc:099学院审核不通过原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHBZ {get;set;}

           /// <summary>
           /// Desc:学分模块ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSLXDM {get;set;}

    }
}

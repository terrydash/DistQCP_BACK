﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class EXWSFB
    {
           public EXWSFB(){


           }
           /// <summary>
           /// Desc:001学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:003交费金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? JFJE {get;set;}

           /// <summary>
           /// Desc:004交费时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFSJ {get;set;}

           /// <summary>
           /// Desc:005交费编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JFBH {get;set;}

           /// <summary>
           /// Desc:006学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:007学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:008交费方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFFS {get;set;}

           /// <summary>
           /// Desc:009交费操作人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFCZR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFCZSJ {get;set;}

    }
}

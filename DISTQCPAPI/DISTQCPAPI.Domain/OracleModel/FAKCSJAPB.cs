﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class FAKCSJAPB
    {
           public FAKCSJAPB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099组号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZHDM {get;set;}

           /// <summary>
           /// Desc:099组号名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHMC {get;set;}

           /// <summary>
           /// Desc:099方案代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FADM {get;set;}

           /// <summary>
           /// Desc:099方案名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FAMC {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099开始周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? QSZ {get;set;}

           /// <summary>
           /// Desc:099结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JSZ {get;set;}

           /// <summary>
           /// Desc:099结束周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQJ {get;set;}

           /// <summary>
           /// Desc:099开始周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short SJDXH {get;set;}

           /// <summary>
           /// Desc:099结束周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short QSSJD {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DSZ {get;set;}

    }
}

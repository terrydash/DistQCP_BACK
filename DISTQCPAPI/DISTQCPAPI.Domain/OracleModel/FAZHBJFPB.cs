﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class FAZHBJFPB
    {
           public FAZHBJFPB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099组号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZHDM {get;set;}

           /// <summary>
           /// Desc:099组号名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHMC {get;set;}

           /// <summary>
           /// Desc:099方案代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FADM {get;set;}

           /// <summary>
           /// Desc:099方案名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FAMC {get;set;}

           /// <summary>
           /// Desc:099班级代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJDM {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class FBFZJDSZB
    {
           public FBFZJDSZB(){


           }
           /// <summary>
           /// Desc:非百分制成绩名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:非百分制成绩对应绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JD {get;set;}

    }
}

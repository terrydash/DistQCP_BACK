﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class FXSFXSXXB
    {
           public FXSFXSXXB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:003学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:004是否收费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSF {get;set;}

           /// <summary>
           /// Desc:011报名类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BMLB {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class FXZYLBB
    {
           public FXZYLBB(){


           }
           /// <summary>
           /// Desc:099类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LBDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSSFXMDM {get;set;}

    }
}

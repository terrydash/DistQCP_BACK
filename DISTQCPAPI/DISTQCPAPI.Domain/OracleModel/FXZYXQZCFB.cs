﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class FXZYXQZCFB
    {
           public FXZYXQZCFB(){


           }
           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short NJ {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099学期注册费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? XQZCF {get;set;}

    }
}

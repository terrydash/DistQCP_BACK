﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class FXZYZCFSZB
    {
           public FXZYZCFSZB(){


           }
           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short NJ {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099辅修专业注册费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FXZYZCF {get;set;}

           /// <summary>
           /// Desc:099第二专业注册费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DEZYZCF {get;set;}

           /// <summary>
           /// Desc:099学分学费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? XFXF {get;set;}

           /// <summary>
           /// Desc:099辅修专业学费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FXZYXF {get;set;}

           /// <summary>
           /// Desc:099第二专业学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DEZYXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EZYZCF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EXWZCF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXMDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXMMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDR {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class FYSYQKB
    {
           public FYSYQKB(){


           }
           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099楼号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LH {get;set;}

           /// <summary>
           /// Desc:099房号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FH {get;set;}

           /// <summary>
           /// Desc:099朝向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CX {get;set;}

           /// <summary>
           /// Desc:099面积
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? MJ {get;set;}

           /// <summary>
           /// Desc:099总床位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZCWS {get;set;}

           /// <summary>
           /// Desc:099使用床位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SYCWS {get;set;}

           /// <summary>
           /// Desc:099已用床位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YYCWS {get;set;}

           /// <summary>
           /// Desc:099级别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JB {get;set;}

           /// <summary>
           /// Desc:099宿舍电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSDH {get;set;}

           /// <summary>
           /// Desc:099房屋类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FWLX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

    }
}

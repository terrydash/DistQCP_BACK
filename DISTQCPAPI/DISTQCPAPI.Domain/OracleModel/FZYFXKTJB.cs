﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class FZYFXKTJB
    {
           public FZYFXKTJB(){


           }
           /// <summary>
           /// Desc:099学科名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKMC {get;set;}

           /// <summary>
           /// Desc:099编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BH {get;set;}

           /// <summary>
           /// Desc:099是否要机房
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? BYSHJ {get;set;}

           /// <summary>
           /// Desc:099招生数合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZSSHJ {get;set;}

           /// <summary>
           /// Desc:099在校生数合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZXSSHJ {get;set;}

           /// <summary>
           /// Desc:099应届毕业生数合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? YJBYSSHJ {get;set;}

    }
}

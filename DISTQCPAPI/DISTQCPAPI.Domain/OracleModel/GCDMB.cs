﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GCDMB
    {
           public GCDMB(){


           }
           /// <summary>
           /// Desc:099过程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GCDM {get;set;}

           /// <summary>
           /// Desc:099过程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GCMC {get;set;}

    }
}

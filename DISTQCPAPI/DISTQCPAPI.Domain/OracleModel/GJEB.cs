﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GJEB
    {
           public GJEB(){


           }
           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XZYQMC {get;set;}

           /// <summary>
           /// Desc:099专业编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYBH {get;set;}

           /// <summary>
           /// Desc:099年制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? NZ {get;set;}

           /// <summary>
           /// Desc:099毕业生数合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BYSSHJ {get;set;}

           /// <summary>
           /// Desc:099毕业生数授予学位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BYSSSXW {get;set;}

           /// <summary>
           /// Desc:099招生数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZSS {get;set;}

           /// <summary>
           /// Desc:099在校学生数合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZXXSSHJ {get;set;}

           /// <summary>
           /// Desc:099在校学生数一年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZXXSSYNJ {get;set;}

           /// <summary>
           /// Desc:099在校学生数二年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZXXSSENJ {get;set;}

           /// <summary>
           /// Desc:099在校学生数三年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZXXSSSNJ {get;set;}

           /// <summary>
           /// Desc:099在校学生数四年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZXXSSSINJ {get;set;}

           /// <summary>
           /// Desc:099在校学生数五年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZXXSSWNJ {get;set;}

           /// <summary>
           /// Desc:099在校学生数六年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZXXSSLNJ {get;set;}

           /// <summary>
           /// Desc:099毕业班学生数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BYBXSS {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GJYB
    {
           public GJYB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int XH {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099年制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NZ {get;set;}

           /// <summary>
           /// Desc:099毕业生数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? BYSS {get;set;}

           /// <summary>
           /// Desc:099授予学位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SYXWS {get;set;}

           /// <summary>
           /// Desc:099招生时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZSSJ {get;set;}

           /// <summary>
           /// Desc:099任务下发标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZSSYJS {get;set;}

           /// <summary>
           /// Desc:099招生数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZSSCJZS {get;set;}

           /// <summary>
           /// Desc:099在校生数计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZXSSJ {get;set;}

           /// <summary>
           /// Desc:099学生数1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZXSS1 {get;set;}

           /// <summary>
           /// Desc:099学生数2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZXSS2 {get;set;}

           /// <summary>
           /// Desc:099学生数3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZXSS3 {get;set;}

           /// <summary>
           /// Desc:099学生数4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZXSS4 {get;set;}

           /// <summary>
           /// Desc:099学生数5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZXSS5 {get;set;}

           /// <summary>
           /// Desc:099应届毕业生数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? YJBYSS {get;set;}

    }
}

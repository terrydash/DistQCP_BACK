﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GNMKDMB
    {
           public GNMKDMB(){


           }
           /// <summary>
           /// Desc:099功能模块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GNMKDM {get;set;}

           /// <summary>
           /// Desc:099功能模块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GNMKMC {get;set;}

           /// <summary>
           /// Desc:099对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DX {get;set;}

           /// <summary>
           /// Desc:099读写
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? WR {get;set;}

           /// <summary>
           /// Desc:099授权否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJSQF {get;set;}

           /// <summary>
           /// Desc:功能模块英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWGNMKMC {get;set;}

           /// <summary>
           /// Desc:图片名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TPMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GNMKWJM
    {
           public GNMKWJM(){


           }
           /// <summary>
           /// Desc:099功能模块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GNMKDM {get;set;}

           /// <summary>
           /// Desc:099功能模块文件名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string gnmkwjm {get;set;}

           /// <summary>
           /// Desc:099是否显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXS {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

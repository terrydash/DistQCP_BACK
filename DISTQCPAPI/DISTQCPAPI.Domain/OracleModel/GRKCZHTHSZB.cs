﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GRKCZHTHSZB
    {
           public GRKCZHTHSZB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THKCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THKCMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSH {get;set;}

           /// <summary>
           /// Desc:099学生学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXYSHR {get;set;}

           /// <summary>
           /// Desc:099学生学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXYSHSJ {get;set;}

           /// <summary>
           /// Desc:099开课学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYSHR {get;set;}

           /// <summary>
           /// Desc:099开课学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYSHSJ {get;set;}

    }
}

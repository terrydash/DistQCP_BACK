﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GXJHB
    {
           public GXJHB(){


           }
           /// <summary>
           /// Desc:011教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:011课程归属
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCGS {get;set;}

           /// <summary>
           /// Desc:011学分要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ {get;set;}

           /// <summary>
           /// Desc:011开课学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JYXDXQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSH {get;set;}

    }
}

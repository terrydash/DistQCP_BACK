﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GXKCZHB
    {
           public GXKCZHB(){


           }
           /// <summary>
           /// Desc:转换编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZHBH {get;set;}

           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:申请类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQLB {get;set;}

           /// <summary>
           /// Desc:申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:国外课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GWKCDM {get;set;}

           /// <summary>
           /// Desc:国外课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GWKCMC {get;set;}

           /// <summary>
           /// Desc:国外课程中英文简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GWKCJJ {get;set;}

           /// <summary>
           /// Desc:国外课程成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GWCJ {get;set;}

           /// <summary>
           /// Desc:国外课程学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GWXF {get;set;}

           /// <summary>
           /// Desc:转换后课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THKCDM {get;set;}

           /// <summary>
           /// Desc:转换后课程成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THCJ {get;set;}

           /// <summary>
           /// Desc:转换后课程学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THXF {get;set;}

           /// <summary>
           /// Desc:转换后课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THKCXZ {get;set;}

           /// <summary>
           /// Desc:教务处审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHR {get;set;}

           /// <summary>
           /// Desc:教务处审核标记0：待审1：通过2：不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHBJ {get;set;}

           /// <summary>
           /// Desc:教务处审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHSJ {get;set;}

           /// <summary>
           /// Desc:教务处审核意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCYJ {get;set;}

           /// <summary>
           /// Desc:学校来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXLY {get;set;}

           /// <summary>
           /// Desc:转换后课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THKCMC {get;set;}

    }
}

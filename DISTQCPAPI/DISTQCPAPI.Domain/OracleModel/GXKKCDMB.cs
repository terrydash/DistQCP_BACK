﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GXKKCDMB
    {
           public GXKKCDMB(){


           }
           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:课程中文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCZWMC {get;set;}

           /// <summary>
           /// Desc:课程英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCYWMC {get;set;}

           /// <summary>
           /// Desc:课程简称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCJC {get;set;}

           /// <summary>
           /// Desc:学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:总学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXS {get;set;}

           /// <summary>
           /// Desc:讲课学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKXS {get;set;}

           /// <summary>
           /// Desc:实验学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXS {get;set;}

           /// <summary>
           /// Desc:上机学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXS {get;set;}

           /// <summary>
           /// Desc:课程类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLB {get;set;}

           /// <summary>
           /// Desc:课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:开课部门代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKBMDM {get;set;}

           /// <summary>
           /// Desc:考核方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHFS {get;set;}

           /// <summary>
           /// Desc:停开标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKBJ {get;set;}

           /// <summary>
           /// Desc:课程简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCJJ {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GXKXZTJ
    {
           public GXKXZTJ(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099已选门次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXMC {get;set;}

           /// <summary>
           /// Desc:099已选学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXXF {get;set;}

    }
}

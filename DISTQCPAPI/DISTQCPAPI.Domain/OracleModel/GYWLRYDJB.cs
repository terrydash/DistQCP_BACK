﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GYWLRYDJB
    {
           public GYWLRYDJB(){


           }
           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:099身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZH {get;set;}

           /// <summary>
           /// Desc:099工作单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZDW {get;set;}

           /// <summary>
           /// Desc:099电话号码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DHHM {get;set;}

           /// <summary>
           /// Desc:099毕业生数授予学位数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJSJ {get;set;}

           /// <summary>
           /// Desc:099校内人员姓名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XNRYXM {get;set;}

           /// <summary>
           /// Desc:099专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:099行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099楼号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LH {get;set;}

           /// <summary>
           /// Desc:099房号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FH {get;set;}

           /// <summary>
           /// Desc:099床位号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? CWH {get;set;}

           /// <summary>
           /// Desc:099来客时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LKSJ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GYWSJCB
    {
           public GYWSJCB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099教材编码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCBM {get;set;}

           /// <summary>
           /// Desc:099校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQU {get;set;}

           /// <summary>
           /// Desc:099楼号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LH {get;set;}

           /// <summary>
           /// Desc:099房号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FH {get;set;}

           /// <summary>
           /// Desc:099检查时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCSJ {get;set;}

           /// <summary>
           /// Desc:099方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FS {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

    }
}

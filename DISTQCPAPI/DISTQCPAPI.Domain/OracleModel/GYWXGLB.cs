﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GYWXGLB
    {
           public GYWXGLB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQU {get;set;}

           /// <summary>
           /// Desc:099楼号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LH {get;set;}

           /// <summary>
           /// Desc:099房号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FH {get;set;}

           /// <summary>
           /// Desc:099学生数2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BXSJ {get;set;}

           /// <summary>
           /// Desc:099维修内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string WXNR {get;set;}

           /// <summary>
           /// Desc:099维修部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXBM {get;set;}

           /// <summary>
           /// Desc:099维修人员
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string WXRY {get;set;}

           /// <summary>
           /// Desc:099维修时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string WXSJ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GYXSGLB
    {
           public GYXSGLB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099房号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FH {get;set;}

           /// <summary>
           /// Desc:099床位号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? CWH {get;set;}

           /// <summary>
           /// Desc:099项目类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMLB {get;set;}

           /// <summary>
           /// Desc:099功能模块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJSJ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LH {get;set;}

    }
}

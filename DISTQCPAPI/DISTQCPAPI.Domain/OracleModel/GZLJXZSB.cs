﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GZLJXZSB
    {
           public GZLJXZSB(){


           }
           /// <summary>
           /// Desc:选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:起始结束周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string QSJSZ {get;set;}

           /// <summary>
           /// Desc:实习周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXZS {get;set;}

           /// <summary>
           /// Desc:值周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZS {get;set;}

           /// <summary>
           /// Desc:放假周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FJZS {get;set;}

           /// <summary>
           /// Desc:099理论课时增减量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LLKSZJL {get;set;}

           /// <summary>
           /// Desc:099上机课时增减量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJKSZJL {get;set;}

    }
}

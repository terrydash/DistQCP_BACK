﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GZLZCXSSZB
    {
           public GZLZCXSSZB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS {get;set;}

           /// <summary>
           /// Desc:099人数标准
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RSBZ {get;set;}

           /// <summary>
           /// Desc:099标准金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZJE {get;set;}

           /// <summary>
           /// Desc:099人数增幅标准
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RSZFBZ {get;set;}

           /// <summary>
           /// Desc:099增幅标准金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZFBZJE {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class GZYRSTJB
    {
           public GZYRSTJB(){


           }
           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short NJ {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099总人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZRS {get;set;}

           /// <summary>
           /// Desc:099注册人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZCRS {get;set;}

           /// <summary>
           /// Desc:099提交确认
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJRQ {get;set;}

    }
}

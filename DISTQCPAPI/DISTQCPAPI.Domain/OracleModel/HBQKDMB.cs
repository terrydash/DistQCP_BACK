﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class HBQKDMB
    {
           public HBQKDMB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099班级代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJDM {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099合班名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string HBMC {get;set;}

           /// <summary>
           /// Desc:099使用班级代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJKBDM {get;set;}

    }
}

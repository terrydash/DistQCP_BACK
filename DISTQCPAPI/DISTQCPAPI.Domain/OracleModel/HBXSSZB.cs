﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class HBXSSZB
    {
           public HBXSSZB(){


           }
           /// <summary>
           /// Desc:099合班方式
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string HBFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XS {get;set;}

    }
}

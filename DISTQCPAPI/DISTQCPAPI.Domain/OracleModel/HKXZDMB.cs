﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class HKXZDMB
    {
           public HKXZDMB(){


           }
           /// <summary>
           /// Desc:099户口性质代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string HKXZDM {get;set;}

           /// <summary>
           /// Desc:099户口性质名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HKXZMC {get;set;}

    }
}

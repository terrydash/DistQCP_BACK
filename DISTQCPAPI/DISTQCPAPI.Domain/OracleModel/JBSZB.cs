﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JBSZB
    {
           public JBSZB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NO {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJDC_WJMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKXN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKXQ {get;set;}

           /// <summary>
           /// Desc:补考段状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKDZT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? TSBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? TSXH {get;set;}

           /// <summary>
           /// Desc:当为多次补考时第几次补考
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKDJC {get;set;}

           /// <summary>
           /// Desc:099多重修段重修学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXXN {get;set;}

           /// <summary>
           /// Desc:099多重修段重修学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXXQ {get;set;}

           /// <summary>
           /// Desc:099多重修段第几次重修
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJCCX {get;set;}

    }
}

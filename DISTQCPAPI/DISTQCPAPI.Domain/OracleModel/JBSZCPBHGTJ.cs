﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JBSZCPBHGTJ
    {
           public JBSZCPBHGTJ(){


           }
           /// <summary>
           /// Desc:099基本素质测评不合格条件
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BHGTJDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BHGTJ {get;set;}

    }
}

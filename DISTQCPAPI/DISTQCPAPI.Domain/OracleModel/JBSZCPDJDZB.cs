﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JBSZCPDJDZB
    {
           public JBSZCPDJDZB(){


           }
           /// <summary>
           /// Desc:099等级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJ {get;set;}

           /// <summary>
           /// Desc:099成绩
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JBSZCPHZB
    {
           public JBSZCPHZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:099民主分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MZF {get;set;}

           /// <summary>
           /// Desc:099班级分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJF {get;set;}

           /// <summary>
           /// Desc:099班主任分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZRF {get;set;}

           /// <summary>
           /// Desc:099总成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZCJ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JBSZCPQZXSB
    {
           public JBSZCPQZXSB(){


           }
           /// <summary>
           /// Desc:099评分代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PFDM {get;set;}

           /// <summary>
           /// Desc:099评分名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PFMC {get;set;}

           /// <summary>
           /// Desc:099权重
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? QZXS {get;set;}

    }
}

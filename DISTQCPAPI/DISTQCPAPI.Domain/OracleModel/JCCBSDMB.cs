﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCCBSDMB
    {
           public JCCBSDMB(){


           }
           /// <summary>
           /// Desc:001出版社代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CBSDM {get;set;}

           /// <summary>
           /// Desc:002出版社名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBSMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCCKB
    {
           public JCCKB(){


           }
           /// <summary>
           /// Desc:001出库凭证
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CKPZ {get;set;}

           /// <summary>
           /// Desc:002序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XH {get;set;}

           /// <summary>
           /// Desc:003教材名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:004单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:005作者
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCZZ {get;set;}

           /// <summary>
           /// Desc:006版别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:007出版社
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:008册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CS {get;set;}

           /// <summary>
           /// Desc:009合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TOTAL {get;set;}

           /// <summary>
           /// Desc:010备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:011人均册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PZ {get;set;}

           /// <summary>
           /// Desc:012出库日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKRQ {get;set;}

           /// <summary>
           /// Desc:013书架号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZSH {get;set;}

           /// <summary>
           /// Desc:099教材对应课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

    }
}

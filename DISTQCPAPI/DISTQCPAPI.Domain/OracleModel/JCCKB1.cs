﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCCKB1
    {
           public JCCKB1(){


           }
           /// <summary>
           /// Desc:001出库凭证
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CKPZ {get;set;}

           /// <summary>
           /// Desc:002出库性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKXZ {get;set;}

           /// <summary>
           /// Desc:003学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:004学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:005领书人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSRXM {get;set;}

           /// <summary>
           /// Desc:006领书人单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSRDW {get;set;}

           /// <summary>
           /// Desc:007出库时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKSJ {get;set;}

           /// <summary>
           /// Desc:008发书人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSRXM {get;set;}

           /// <summary>
           /// Desc:009填单人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TDRXM {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSSFJZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSRMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XIAOQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099流水号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSH {get;set;}

           /// <summary>
           /// Desc:099折扣
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZK {get;set;}

    }
}

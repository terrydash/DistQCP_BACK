﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCCKVIEW
    {
           public JCCKVIEW(){


           }
           /// <summary>
           /// Desc:001出库凭证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CKPZ {get;set;}

           /// <summary>
           /// Desc:002出库性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKXZ {get;set;}

           /// <summary>
           /// Desc:003学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:004学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:005领书人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSRXM {get;set;}

           /// <summary>
           /// Desc:006领书人单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSRDW {get;set;}

           /// <summary>
           /// Desc:007出库时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKSJ {get;set;}

           /// <summary>
           /// Desc:008发书人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSRXM {get;set;}

           /// <summary>
           /// Desc:009填单人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TDRXM {get;set;}

           /// <summary>
           /// Desc:010序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XH {get;set;}

           /// <summary>
           /// Desc:011教材名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:012单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:013作者
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCZZ {get;set;}

           /// <summary>
           /// Desc:014版别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:015出版社
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:016册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CS {get;set;}

           /// <summary>
           /// Desc:017合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TOTAL {get;set;}

           /// <summary>
           /// Desc:018出库日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKRQ {get;set;}

           /// <summary>
           /// Desc:019书架号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJH {get;set;}

    }
}

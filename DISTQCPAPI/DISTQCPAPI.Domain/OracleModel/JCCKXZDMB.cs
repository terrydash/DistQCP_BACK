﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCCKXZDMB
    {
           public JCCKXZDMB(){


           }
           /// <summary>
           /// Desc:001出库性质代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CKXZDM {get;set;}

           /// <summary>
           /// Desc:002出库性质名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKXZMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCDXDMB
    {
           public JCDXDMB(){


           }
           /// <summary>
           /// Desc:001适用对象代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXDM {get;set;}

           /// <summary>
           /// Desc:002适用对象名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXMC {get;set;}

    }
}

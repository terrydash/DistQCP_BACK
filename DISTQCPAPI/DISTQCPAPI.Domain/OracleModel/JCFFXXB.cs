﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCFFXXB
    {
           public JCFFXXB(){


           }
           /// <summary>
           /// Desc:099出库凭证
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CKPZ {get;set;}

           /// <summary>
           /// Desc:099出库序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CKXH {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

    }
}

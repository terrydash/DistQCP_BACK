﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCGNDMB
    {
           public JCGNDMB(){


           }
           /// <summary>
           /// Desc:N099 序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XUH {get;set;}

           /// <summary>
           /// Desc:N099 模块代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MKDM {get;set;}

           /// <summary>
           /// Desc:N099 序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RNUM {get;set;}

           /// <summary>
           /// Desc:N099 相关功能
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGGN {get;set;}

           /// <summary>
           /// Desc:N099 检测说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCSM {get;set;}

           /// <summary>
           /// Desc:N099 学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:N099 学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

    }
}

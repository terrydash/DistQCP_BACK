﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCGXCKB
    {
           public JCGXCKB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099书架号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJH {get;set;}

           /// <summary>
           /// Desc:099单价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:099出库日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKRQ {get;set;}

           /// <summary>
           /// Desc:099合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TOTAL {get;set;}

           /// <summary>
           /// Desc:099是否出库
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFCK {get;set;}

           /// <summary>
           /// Desc:099凭证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? PZ {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099班级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJ {get;set;}

           /// <summary>
           /// Desc:099教材名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:099出版社
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:099版本号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:099教材作者
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCZZ {get;set;}

    }
}

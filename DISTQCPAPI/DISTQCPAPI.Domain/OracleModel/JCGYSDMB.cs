﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCGYSDMB
    {
           public JCGYSDMB(){


           }
           /// <summary>
           /// Desc:001供应商代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GYSDM {get;set;}

           /// <summary>
           /// Desc:002供应商名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GYSMC {get;set;}

    }
}

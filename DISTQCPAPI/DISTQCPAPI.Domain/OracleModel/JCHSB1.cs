﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCHSB1
    {
           public JCHSB1(){


           }
           /// <summary>
           /// Desc:001回收凭证
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal HSPZ {get;set;}

           /// <summary>
           /// Desc:002回收性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HSXZ {get;set;}

           /// <summary>
           /// Desc:003学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:004学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:005经手人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSRXM {get;set;}

           /// <summary>
           /// Desc:006单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DW {get;set;}

           /// <summary>
           /// Desc:007回收时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HSSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XIAOQ {get;set;}

           /// <summary>
           /// Desc:099回库流水号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HKLSH {get;set;}

           /// <summary>
           /// Desc:099填单人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TDRXM {get;set;}

    }
}

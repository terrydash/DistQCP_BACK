﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCHSXXB
    {
           public JCHSXXB(){


           }
           /// <summary>
           /// Desc:099回收凭证
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal HSPZ {get;set;}

           /// <summary>
           /// Desc:099回收序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal HSXH {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

    }
}

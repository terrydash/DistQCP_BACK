﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCJBXXB
    {
           public JCJBXXB(){


           }
           /// <summary>
           /// Desc:001教材名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:002单价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:003教材作者
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCZZ {get;set;}

           /// <summary>
           /// Desc:004版别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:005出版社
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:006库存数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KCSL {get;set;}

           /// <summary>
           /// Desc:007是否优秀教材
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXJC {get;set;}

           /// <summary>
           /// Desc:008教材编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCBH {get;set;}

           /// <summary>
           /// Desc:099教材ISBN
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZSH {get;set;}

           /// <summary>
           /// Desc:010出版日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBDATE {get;set;}

           /// <summary>
           /// Desc:011适用专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZY {get;set;}

           /// <summary>
           /// Desc:012适用对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYDX {get;set;}

           /// <summary>
           /// Desc:013教材类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCLX {get;set;}

           /// <summary>
           /// Desc:014书架号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GYS {get;set;}

           /// <summary>
           /// Desc:015原库存数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YKCSL {get;set;}

           /// <summary>
           /// Desc:099规划级别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCGHJBMC {get;set;}

           /// <summary>
           /// Desc:099获奖情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCHJQKMC {get;set;}

           /// <summary>
           /// Desc:009是否新版本
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXBH {get;set;}

           /// <summary>
           /// Desc:099教材成本价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBJ {get;set;}

           /// <summary>
           /// Desc:017教材条形码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCTXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKBM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCZS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCYS {get;set;}

           /// <summary>
           /// Desc:099教材对应课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBJHJ {get;set;}

           /// <summary>
           /// Desc:099印次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YC {get;set;}

           /// <summary>
           /// Desc:099印刷时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSSJ {get;set;}

           /// <summary>
           /// Desc:099是否外文教材
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFWWJC {get;set;}

           /// <summary>
           /// Desc:099是否本校老师正式出版
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZSCB {get;set;}

           /// <summary>
           /// Desc:099教材停用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCTY {get;set;}

    }
}

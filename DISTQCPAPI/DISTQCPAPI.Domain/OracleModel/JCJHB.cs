﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCJHB
    {
           public JCJHB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:003选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:004开设课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GSKC {get;set;}

           /// <summary>
           /// Desc:005上课教师
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SKJS {get;set;}

           /// <summary>
           /// Desc:006教材名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:007作者
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCZZ {get;set;}

           /// <summary>
           /// Desc:008版别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:009出版社
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:010教材数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCSL {get;set;}

           /// <summary>
           /// Desc:011选必修
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XBX {get;set;}

           /// <summary>
           /// Desc:012备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:013院系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YX {get;set;}

           /// <summary>
           /// Desc:014征订标志
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDBZ {get;set;}

           /// <summary>
           /// Desc:015校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XIAQ {get;set;}

           /// <summary>
           /// Desc:016教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JS {get;set;}

           /// <summary>
           /// Desc:017机动
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZRQ {get;set;}

           /// <summary>
           /// Desc:016专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKRS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKBZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XFZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDDH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZSH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCDJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJQK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCZS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCYS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RLRS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC1 {get;set;}

           /// <summary>
           /// Desc:年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCLX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教务处审核教材
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCJHB1
    {
           public JCJHB1(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099开设课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GSKC {get;set;}

           /// <summary>
           /// Desc:099上课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKJS {get;set;}

           /// <summary>
           /// Desc:099教材名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:099教材作者
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCZZ {get;set;}

           /// <summary>
           /// Desc:099版本号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:099出版社
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:099教材数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCSL {get;set;}

           /// <summary>
           /// Desc:099选必修
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XBX {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099院系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YX {get;set;}

           /// <summary>
           /// Desc:099征订标志
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDBZ {get;set;}

           /// <summary>
           /// Desc:099校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XIAQ {get;set;}

    }
}

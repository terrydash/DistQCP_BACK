﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCKCDMB
    {
           public JCKCDMB(){


           }
           /// <summary>
           /// Desc:001课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:002版别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BB {get;set;}

           /// <summary>
           /// Desc:003课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:004教学单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXDW {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCLXDMB
    {
           public JCLXDMB(){


           }
           /// <summary>
           /// Desc:001教材类型代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCLXDM {get;set;}

           /// <summary>
           /// Desc:002教材类型名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCLXMC {get;set;}

    }
}

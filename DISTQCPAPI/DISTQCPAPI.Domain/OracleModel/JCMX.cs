﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCMX
    {
           public JCMX(){


           }
           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJ {get;set;}

           /// <summary>
           /// Desc:099单位
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DW {get;set;}

           /// <summary>
           /// Desc:099性质
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XZ {get;set;}

           /// <summary>
           /// Desc:099凭证
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal PZH {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XH {get;set;}

           /// <summary>
           /// Desc:099册数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CS {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCRKB
    {
           public JCRKB(){


           }
           /// <summary>
           /// Desc:001入库凭证
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RKPZ {get;set;}

           /// <summary>
           /// Desc:002序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XH {get;set;}

           /// <summary>
           /// Desc:003教材名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:004单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:005作者
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCZZ {get;set;}

           /// <summary>
           /// Desc:006版别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:007出版社
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:008册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CS {get;set;}

           /// <summary>
           /// Desc:009合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TOTAL {get;set;}

           /// <summary>
           /// Desc:010付款
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FK {get;set;}

           /// <summary>
           /// Desc:011折扣
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZK {get;set;}

           /// <summary>
           /// Desc:012书架号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJH {get;set;}

           /// <summary>
           /// Desc:013教材编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDXH {get;set;}

           /// <summary>
           /// Desc:013教材ISBN
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZSH {get;set;}

           /// <summary>
           /// Desc:017教材条形码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCTXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKSJ {get;set;}

           /// <summary>
           /// Desc:099教材对应课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBJHJ {get;set;}

           /// <summary>
           /// Desc:099教材出版日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBSJ {get;set;}

           /// <summary>
           /// Desc:099教材等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCDJ {get;set;}

           /// <summary>
           /// Desc:099教材获奖情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJQK {get;set;}

           /// <summary>
           /// Desc:099入库作废
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKZF {get;set;}

           /// <summary>
           /// Desc:099入库作废操作者
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKZFR {get;set;}

    }
}

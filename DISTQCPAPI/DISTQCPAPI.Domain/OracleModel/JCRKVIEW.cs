﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCRKVIEW
    {
           public JCRKVIEW(){


           }
           /// <summary>
           /// Desc:001入库凭证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? RKPZ {get;set;}

           /// <summary>
           /// Desc:002发票号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FPH {get;set;}

           /// <summary>
           /// Desc:003供应商
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GYS {get;set;}

           /// <summary>
           /// Desc:004入库时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKSJ {get;set;}

           /// <summary>
           /// Desc:005总书金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSJE {get;set;}

           /// <summary>
           /// Desc:006实际金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJJE {get;set;}

           /// <summary>
           /// Desc:007折扣金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZKJE {get;set;}

           /// <summary>
           /// Desc:008学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:009学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:010验收保管人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSBGR {get;set;}

           /// <summary>
           /// Desc:011经办人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JBR {get;set;}

           /// <summary>
           /// Desc:012序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XH {get;set;}

           /// <summary>
           /// Desc:013教材名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:014单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:015作者
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCZZ {get;set;}

           /// <summary>
           /// Desc:016版别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:017出版社
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:018册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CS {get;set;}

           /// <summary>
           /// Desc:019合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TOTAL {get;set;}

           /// <summary>
           /// Desc:020付款
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FK {get;set;}

           /// <summary>
           /// Desc:021折扣
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZK {get;set;}

           /// <summary>
           /// Desc:022书架号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJH {get;set;}

    }
}

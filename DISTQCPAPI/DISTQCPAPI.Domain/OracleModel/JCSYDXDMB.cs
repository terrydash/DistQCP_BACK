﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCSYDXDMB
    {
           public JCSYDXDMB(){


           }
           /// <summary>
           /// Desc:099使用对象代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYDXDM {get;set;}

           /// <summary>
           /// Desc:099使用对象名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYDXMC {get;set;}

    }
}

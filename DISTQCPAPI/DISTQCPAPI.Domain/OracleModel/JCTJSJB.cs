﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCTJSJB
    {
           public JCTJSJB(){


           }
           /// <summary>
           /// Desc:099名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MC {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RS {get;set;}

           /// <summary>
           /// Desc:099册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CS {get;set;}

           /// <summary>
           /// Desc:099金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JE {get;set;}

           /// <summary>
           /// Desc:099人均金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RZJE {get;set;}

           /// <summary>
           /// Desc:099收金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJE {get;set;}

    }
}

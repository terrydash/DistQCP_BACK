﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCTSB1
    {
           public JCTSB1(){


           }
           /// <summary>
           /// Desc:001退库凭证
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal TKPZ {get;set;}

           /// <summary>
           /// Desc:002发票号码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FPH {get;set;}

           /// <summary>
           /// Desc:003供应商
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GYS {get;set;}

           /// <summary>
           /// Desc:004退库时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKSJ {get;set;}

           /// <summary>
           /// Desc:005总金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSJE {get;set;}

           /// <summary>
           /// Desc:006实金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJJE {get;set;}

           /// <summary>
           /// Desc:007退金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZKJE {get;set;}

           /// <summary>
           /// Desc:008学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:009学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:010验收保管人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSBGR {get;set;}

           /// <summary>
           /// Desc:011经办人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JBR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XIAOQ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCXMDMB
    {
           public JCXMDMB(){


           }
           /// <summary>
           /// Desc:001工作人员编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BH {get;set;}

           /// <summary>
           /// Desc:002工作人员姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

    }
}

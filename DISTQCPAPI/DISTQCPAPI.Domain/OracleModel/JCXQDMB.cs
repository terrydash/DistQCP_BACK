﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCXQDMB
    {
           public JCXQDMB(){


           }
           /// <summary>
           /// Desc:001校区代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQDM {get;set;}

           /// <summary>
           /// Desc:002校区名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQMC {get;set;}

           /// <summary>
           /// Desc:003校区简拼
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQJP {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCXSBDB
    {
           public JCXSBDB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:003学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:004姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:005变动前班级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJ1 {get;set;}

           /// <summary>
           /// Desc:006变动后班级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJ2 {get;set;}

           /// <summary>
           /// Desc:007变动日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BDRQ {get;set;}

    }
}

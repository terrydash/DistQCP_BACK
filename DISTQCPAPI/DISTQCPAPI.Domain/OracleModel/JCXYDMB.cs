﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCXYDMB
    {
           public JCXYDMB(){


           }
           /// <summary>
           /// Desc:001学院代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XYDM {get;set;}

           /// <summary>
           /// Desc:002学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

    }
}

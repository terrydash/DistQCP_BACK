﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCZDB
    {
           public JCZDB(){


           }
           /// <summary>
           /// Desc:001序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XH {get;set;}

           /// <summary>
           /// Desc:002教材名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:003单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:004作者
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCZZ {get;set;}

           /// <summary>
           /// Desc:005版别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:006出版社
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:007供应商
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GYS {get;set;}

           /// <summary>
           /// Desc:008必修人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BXRS {get;set;}

           /// <summary>
           /// Desc:009选修人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXRS {get;set;}

           /// <summary>
           /// Desc:012库存数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCSL {get;set;}

           /// <summary>
           /// Desc:013证订册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDCS {get;set;}

           /// <summary>
           /// Desc:014实际到书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJDS {get;set;}

           /// <summary>
           /// Desc:015提交时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TDATE {get;set;}

           /// <summary>
           /// Desc:016使用单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYDW {get;set;}

           /// <summary>
           /// Desc:017是否转出
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JC {get;set;}

           /// <summary>
           /// Desc:018学年学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XNXQ {get;set;}

           /// <summary>
           /// Desc:019证订代号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDDH {get;set;}

           /// <summary>
           /// Desc:099紫金册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJCS {get;set;}

           /// <summary>
           /// Desc:020本部册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQCS {get;set;}

           /// <summary>
           /// Desc:021北固册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXCS {get;set;}

           /// <summary>
           /// Desc:022梦溪册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJCS {get;set;}

           /// <summary>
           /// Desc:023中山册数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HBCS {get;set;}

           /// <summary>
           /// Desc:099紫金库存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJKC {get;set;}

           /// <summary>
           /// Desc:024本部库存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQKC {get;set;}

           /// <summary>
           /// Desc:025北固库存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXKC {get;set;}

           /// <summary>
           /// Desc:026梦溪库存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJKC {get;set;}

           /// <summary>
           /// Desc:027中山库存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HBKC {get;set;}

           /// <summary>
           /// Desc:010教师数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JS {get;set;}

           /// <summary>
           /// Desc:011机动数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZRQ {get;set;}

           /// <summary>
           /// Desc:099教材编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099教材ISBN
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZSH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJQK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZDXH {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCDJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ06CS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ07CS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ08CS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ09CS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ10CS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ06KC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ07KC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ08KC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ09KC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ10KC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

    }
}

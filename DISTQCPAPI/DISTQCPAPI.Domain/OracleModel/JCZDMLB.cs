﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCZDMLB
    {
           public JCZDMLB(){


           }
           /// <summary>
           /// Desc:099征订代号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDDH {get;set;}

           /// <summary>
           /// Desc:099教材编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCBH {get;set;}

           /// <summary>
           /// Desc:099教材名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:099教材作者
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCZZ {get;set;}

           /// <summary>
           /// Desc:099出版社
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:099版本号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:099单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:099适用专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZY {get;set;}

           /// <summary>
           /// Desc:099获奖情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJQKMC {get;set;}

           /// <summary>
           /// Desc:009教材ISBN
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BZSH {get;set;}

           /// <summary>
           /// Desc:009出版日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBDATE {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXBH {get;set;}

           /// <summary>
           /// Desc:017教材条形码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCTXM {get;set;}

           /// <summary>
           /// Desc:016教材规划名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCGHJBMC {get;set;}

           /// <summary>
           /// Desc:012适用对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYDX {get;set;}

           /// <summary>
           /// Desc:099教材停用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCTY {get;set;}

    }
}

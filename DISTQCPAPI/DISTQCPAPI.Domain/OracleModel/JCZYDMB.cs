﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JCZYDMB
    {
           public JCZYDMB(){


           }
           /// <summary>
           /// Desc:001专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:002专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:003学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

    }
}

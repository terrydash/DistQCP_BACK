﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JDDMB
    {
           public JDDMB(){


           }
           /// <summary>
           /// Desc:001基地代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JDDM {get;set;}

           /// <summary>
           /// Desc:002基地名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JDMC {get;set;}

           /// <summary>
           /// Desc:003基地邮编
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDYB {get;set;}

           /// <summary>
           /// Desc:004基地地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDDZ {get;set;}

           /// <summary>
           /// Desc:005联系人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXR {get;set;}

           /// <summary>
           /// Desc:006联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXDH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JDFSDMB
    {
           public JDFSDMB(){


           }
           /// <summary>
           /// Desc:099就读方式代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JDFSDM {get;set;}

           /// <summary>
           /// Desc:099就读方式名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDFSMC {get;set;}

    }
}

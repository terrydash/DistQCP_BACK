﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JFRZB
    {
           public JFRZB(){


           }
           /// <summary>
           /// Desc:099日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RQ {get;set;}

           /// <summary>
           /// Desc:099内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NR {get;set;}

           /// <summary>
           /// Desc:099IP地址
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string IP {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZLX {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JGDMB
    {
           public JGDMB(){


           }
           /// <summary>
           /// Desc:001籍贯代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JGDM {get;set;}

           /// <summary>
           /// Desc:002籍贯名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JGMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWJGMC {get;set;}

    }
}

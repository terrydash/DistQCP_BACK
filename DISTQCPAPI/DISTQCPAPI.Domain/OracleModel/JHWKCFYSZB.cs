﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JHWKCFYSZB
    {
           public JHWKCFYSZB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099重修标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? CXBJ {get;set;}

           /// <summary>
           /// Desc:099辅修标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXBJ {get;set;}

           /// <summary>
           /// Desc:099选课时年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099选课时专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099选课时专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099每学分费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? MXFFY {get;set;}

           /// <summary>
           /// Desc:099操作人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZR {get;set;}

           /// <summary>
           /// Desc:099操作时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZSJ {get;set;}

    }
}

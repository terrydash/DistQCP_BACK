﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JKTJ
    {
           public JKTJ(){


           }
           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:099百分比
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BFB {get;set;}

           /// <summary>
           /// Desc:099总教师个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZJSGS {get;set;}

           /// <summary>
           /// Desc:099学院教师个数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XYJSGS {get;set;}

           /// <summary>
           /// Desc:099学院人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XYRS {get;set;}

           /// <summary>
           /// Desc:099总人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZRS {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JKZXHZB
    {
           public JKZXHZB(){


           }
           /// <summary>
           /// Desc:099监考执行汇总代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JKZXDM {get;set;}

           /// <summary>
           /// Desc:099监考执行汇总名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKZXMC {get;set;}

    }
}

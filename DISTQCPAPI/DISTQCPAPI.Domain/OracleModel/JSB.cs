﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSB
    {
           public JSB(){


           }
           /// <summary>
           /// Desc:099角色序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099角色名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QXJB {get;set;}

           /// <summary>
           /// Desc:099财务系统用户
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFCWYH {get;set;}

    }
}

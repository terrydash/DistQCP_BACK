﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSCGJSBCLB
    {
           public JSCGJSBCLB(){


           }
           /// <summary>
           /// Desc:N99申请号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SQH {get;set;}

           /// <summary>
           /// Desc:N99奖项代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXDM {get;set;}

           /// <summary>
           /// Desc:N99奖项名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXMC {get;set;}

           /// <summary>
           /// Desc:N99项目成果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMCG {get;set;}

           /// <summary>
           /// Desc:N99项目研究时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMYJSJ {get;set;}

           /// <summary>
           /// Desc:N99申请人编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQRBH {get;set;}

           /// <summary>
           /// Desc:N99申请人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQRXM {get;set;}

           /// <summary>
           /// Desc:N99申请人职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQRZC {get;set;}

           /// <summary>
           /// Desc:N99申请原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQLY {get;set;}

           /// <summary>
           /// Desc:N99审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJG {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSCXB
    {
           public JSCXB(){


           }
           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DJ {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQJ {get;set;}

           /// <summary>
           /// Desc:099起始时间段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QSSJD {get;set;}

           /// <summary>
           /// Desc:099上课长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SKCD {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:099板块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMC {get;set;}

           /// <summary>
           /// Desc:099上课地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKDD {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099任务周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWZXS {get;set;}

    }
}

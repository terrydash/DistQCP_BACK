﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSFZXXB
    {
           public JSFZXXB(){


           }
           /// <summary>
           /// Desc:001职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:003原工作单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YGZDW {get;set;}

           /// <summary>
           /// Desc:004原工作单位起始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YGZDWQSSJ {get;set;}

           /// <summary>
           /// Desc:005原工作单位结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YGZDWJSSJ {get;set;}

           /// <summary>
           /// Desc:006户口所在地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HKSZD {get;set;}

           /// <summary>
           /// Desc:007档案所在地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DASZD {get;set;}

           /// <summary>
           /// Desc:008配偶基本情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string POBJQK {get;set;}

           /// <summary>
           /// Desc:099家庭住址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTZZ {get;set;}

           /// <summary>
           /// Desc:099离职时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LZSJ {get;set;}

    }
}

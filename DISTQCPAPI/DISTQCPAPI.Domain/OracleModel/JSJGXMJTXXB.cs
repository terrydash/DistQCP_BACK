﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSJGXMJTXXB
    {
           public JSJGXMJTXXB(){


           }
           /// <summary>
           /// Desc:项目申报编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SBID {get;set;}

           /// <summary>
           /// Desc:项目结题报告
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMJTBG {get;set;}

           /// <summary>
           /// Desc:项目结题报告附件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMJTBGFJ {get;set;}

           /// <summary>
           /// Desc:项目结题时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMJTSJ {get;set;}

           /// <summary>
           /// Desc:项目验收负责人职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMYSFZRZGH {get;set;}

           /// <summary>
           /// Desc:项目验收负责人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMYSFZRXM {get;set;}

           /// <summary>
           /// Desc:项目验收组其它成员
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMYSZQTCY {get;set;}

           /// <summary>
           /// Desc:项目验收报告-附件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMYSBG {get;set;}

    }
}

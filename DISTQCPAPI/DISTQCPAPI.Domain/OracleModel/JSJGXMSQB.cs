﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSJGXMSQB
    {
           public JSJGXMSQB(){


           }
           /// <summary>
           /// Desc:项目申报编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SBID {get;set;}

           /// <summary>
           /// Desc:申报单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SBBM {get;set;}

           /// <summary>
           /// Desc:项目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMDM {get;set;}

           /// <summary>
           /// Desc:项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMMC {get;set;}

           /// <summary>
           /// Desc:项目经费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? XMJF {get;set;}

           /// <summary>
           /// Desc:项目负责人职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMFZRZGH {get;set;}

           /// <summary>
           /// Desc:项目负责人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMFZRXM {get;set;}

           /// <summary>
           /// Desc:项目开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMKSSJ {get;set;}

           /// <summary>
           /// Desc:项目结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMJSSJ {get;set;}

           /// <summary>
           /// Desc:项目来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMLY {get;set;}

           /// <summary>
           /// Desc:审核状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHZT {get;set;}

           /// <summary>
           /// Desc:申报人职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SBRZGH {get;set;}

           /// <summary>
           /// Desc:附件文档
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FJWD {get;set;}

           /// <summary>
           /// Desc:099项目题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMTM {get;set;}

           /// <summary>
           /// Desc:099初审意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSYJ {get;set;}

           /// <summary>
           /// Desc:099初审结论
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSJL {get;set;}

    }
}

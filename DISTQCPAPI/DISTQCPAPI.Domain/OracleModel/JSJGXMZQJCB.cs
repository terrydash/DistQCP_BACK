﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSJGXMZQJCB
    {
           public JSJGXMZQJCB(){


           }
           /// <summary>
           /// Desc:项目申报编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SBID {get;set;}

           /// <summary>
           /// Desc:中检单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJDW {get;set;}

           /// <summary>
           /// Desc:中检开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJKSSJ {get;set;}

           /// <summary>
           /// Desc:中检结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJJSSJ {get;set;}

           /// <summary>
           /// Desc:自检报告
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJBK {get;set;}

           /// <summary>
           /// Desc:自检报告附件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJBKFJ {get;set;}

           /// <summary>
           /// Desc:中检结论
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJJL {get;set;}

           /// <summary>
           /// Desc:中检负责人职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJFZRZGH {get;set;}

           /// <summary>
           /// Desc:中检负责人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJFZRXM {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSJXGZLB
    {
           public JSJXGZLB(){


           }
           /// <summary>
           /// Desc:003课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:007课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:008学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:009周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:010开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:011选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:012教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:013教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:014校区代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQYQ {get;set;}

           /// <summary>
           /// Desc:015校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQMC {get;set;}

           /// <summary>
           /// Desc:016教师职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:017学生所在学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSZXY {get;set;}

           /// <summary>
           /// Desc:018教学班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXBMC {get;set;}

           /// <summary>
           /// Desc:019课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:020起止周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSJSZ {get;set;}

           /// <summary>
           /// Desc:021总周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZZS {get;set;}

           /// <summary>
           /// Desc:022总学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXS {get;set;}

           /// <summary>
           /// Desc:023人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? RS {get;set;}

           /// <summary>
           /// Desc:024合班系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HBXS {get;set;}

           /// <summary>
           /// Desc:025总课时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZKS {get;set;}

           /// <summary>
           /// Desc:026补充说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

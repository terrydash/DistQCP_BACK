﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSJXJLB
    {
           public JSJXJLB(){


           }
           /// <summary>
           /// Desc:099备注信息
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099进修时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXDD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PXLX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXZY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZXX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099进修单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXDW {get;set;}

           /// <summary>
           /// Desc:099进修课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXKC {get;set;}

           /// <summary>
           /// Desc:099进修形式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXXS {get;set;}

           /// <summary>
           /// Desc:099进修目的
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXMD {get;set;}

           /// <summary>
           /// Desc:099进修类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXF {get;set;}

           /// <summary>
           /// Desc:099车旅费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLF {get;set;}

           /// <summary>
           /// Desc:099住宿费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSF {get;set;}

           /// <summary>
           /// Desc:099合计进修费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJJXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQR {get;set;}

    }
}

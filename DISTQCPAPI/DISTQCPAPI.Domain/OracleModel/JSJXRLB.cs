﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSJXRLB
    {
           public JSJXRLB(){


           }
           /// <summary>
           /// Desc:001职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:003学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:004学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:005课程名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:006周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:007日期1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ1 {get;set;}

           /// <summary>
           /// Desc:008教学内容1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR1 {get;set;}

           /// <summary>
           /// Desc:009教学要求1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ1 {get;set;}

           /// <summary>
           /// Desc:010日期2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ2 {get;set;}

           /// <summary>
           /// Desc:011教学内容2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR2 {get;set;}

           /// <summary>
           /// Desc:012教学要求2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ2 {get;set;}

           /// <summary>
           /// Desc:013日期3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ3 {get;set;}

           /// <summary>
           /// Desc:014教学内容3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR3 {get;set;}

           /// <summary>
           /// Desc:015教学要求3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ3 {get;set;}

           /// <summary>
           /// Desc:016日期4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ4 {get;set;}

           /// <summary>
           /// Desc:017教学内容4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR4 {get;set;}

           /// <summary>
           /// Desc:018教学要求4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ4 {get;set;}

           /// <summary>
           /// Desc:019日期5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ5 {get;set;}

           /// <summary>
           /// Desc:020教学内容5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR5 {get;set;}

           /// <summary>
           /// Desc:021教学要求5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ5 {get;set;}

           /// <summary>
           /// Desc:022日期6
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ6 {get;set;}

           /// <summary>
           /// Desc:023教学内容6
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR6 {get;set;}

           /// <summary>
           /// Desc:024教学要求6
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ6 {get;set;}

           /// <summary>
           /// Desc:025日期7
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ7 {get;set;}

           /// <summary>
           /// Desc:026教学内容7
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR7 {get;set;}

           /// <summary>
           /// Desc:027教学要求7
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ7 {get;set;}

           /// <summary>
           /// Desc:028日期8
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ8 {get;set;}

           /// <summary>
           /// Desc:029教学内容8
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR8 {get;set;}

           /// <summary>
           /// Desc:030教学要求8
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ8 {get;set;}

           /// <summary>
           /// Desc:031日期9
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ9 {get;set;}

           /// <summary>
           /// Desc:032教学内容9
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR9 {get;set;}

           /// <summary>
           /// Desc:033教学要求9
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ9 {get;set;}

           /// <summary>
           /// Desc:034日期10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ10 {get;set;}

           /// <summary>
           /// Desc:035教学内容10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR10 {get;set;}

           /// <summary>
           /// Desc:036教学要求10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ10 {get;set;}

           /// <summary>
           /// Desc:037日期11
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ11 {get;set;}

           /// <summary>
           /// Desc:038教学内容11
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR11 {get;set;}

           /// <summary>
           /// Desc:039教学要求11
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ11 {get;set;}

           /// <summary>
           /// Desc:040日期12
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ12 {get;set;}

           /// <summary>
           /// Desc:041教学内容12
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR12 {get;set;}

           /// <summary>
           /// Desc:042教学要求12
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ12 {get;set;}

           /// <summary>
           /// Desc:043日期13
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ13 {get;set;}

           /// <summary>
           /// Desc:044教学内容13
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR13 {get;set;}

           /// <summary>
           /// Desc:045教学要求13
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ13 {get;set;}

           /// <summary>
           /// Desc:046日期14
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ14 {get;set;}

           /// <summary>
           /// Desc:047教学内容14
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR14 {get;set;}

           /// <summary>
           /// Desc:048教学要求14
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ14 {get;set;}

           /// <summary>
           /// Desc:049日期15
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ15 {get;set;}

           /// <summary>
           /// Desc:050教学内容15
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR15 {get;set;}

           /// <summary>
           /// Desc:051教学要求15
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ15 {get;set;}

           /// <summary>
           /// Desc:052日期16
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ16 {get;set;}

           /// <summary>
           /// Desc:053教学内容16
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR16 {get;set;}

           /// <summary>
           /// Desc:054教学要求16
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ16 {get;set;}

           /// <summary>
           /// Desc:099日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ17 {get;set;}

           /// <summary>
           /// Desc:099教学内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR17 {get;set;}

           /// <summary>
           /// Desc:099教学要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ17 {get;set;}

           /// <summary>
           /// Desc:099日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ18 {get;set;}

           /// <summary>
           /// Desc:099教学内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR18 {get;set;}

           /// <summary>
           /// Desc:099教学要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ18 {get;set;}

           /// <summary>
           /// Desc:055备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS12 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS12 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX12 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS12 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS12 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS16 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS16 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX16 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS16 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS16 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS17 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS17 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX17 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS17 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS17 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS18 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS18 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX18 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS18 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS18 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKNSS7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKKWSS7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLKXX8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTKXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYYZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGKC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCSM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKSM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ12 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ16 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ17 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ18 {get;set;}

           /// <summary>
           /// Desc:主要章节
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ12 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ16 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ17 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ18 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ19 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZJ20 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ19 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RQ20 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR19 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXNR20 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ19 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ20 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS19 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSS20 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS19 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSS20 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ19 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYQ20 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXS {get;set;}

    }
}

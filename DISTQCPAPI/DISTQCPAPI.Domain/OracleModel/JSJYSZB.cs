﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSJYSZB
    {
           public JSJYSZB(){


           }
           /// <summary>
           /// Desc:星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQJ {get;set;}

           /// <summary>
           /// Desc:起始时间 
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string QSSJ {get;set;}

           /// <summary>
           /// Desc:申请结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:借用起始时间，可借用几天后的教室
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JYQSTS {get;set;}

    }
}

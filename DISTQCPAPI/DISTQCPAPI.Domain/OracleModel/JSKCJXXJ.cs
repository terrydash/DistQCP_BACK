﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSKCJXXJ
    {
           public JSKCJXXJ(){


           }
           /// <summary>
           /// Desc:序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS {get;set;}

           /// <summary>
           /// Desc:教师姓名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:教师部门
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSBM {get;set;}

           /// <summary>
           /// Desc:人数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RS {get;set;}

           /// <summary>
           /// Desc:教材名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:教材性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCXZ {get;set;}

           /// <summary>
           /// Desc:建议
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JY {get;set;}

           /// <summary>
           /// Desc:学生学习情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXYQK {get;set;}

           /// <summary>
           /// Desc:是否提交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTJ {get;set;}

    }
}

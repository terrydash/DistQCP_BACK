﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSKHYJB
    {
           public JSKHYJB(){


           }
           /// <summary>
           /// Desc:003学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:005教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:006考核等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHDJ {get;set;}

           /// <summary>
           /// Desc:007审核状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHZT {get;set;}

           /// <summary>
           /// Desc:008考核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHR {get;set;}

           /// <summary>
           /// Desc:010考核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHSJ {get;set;}

           /// <summary>
           /// Desc:009审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

           /// <summary>
           /// Desc:010审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSKTJXZLZHPJDFB
    {
           public JSKTJXZLZHPJDFB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099学生评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSPF {get;set;}

           /// <summary>
           /// Desc:099同行评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THPF {get;set;}

           /// <summary>
           /// Desc:099学院评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYPF {get;set;}

           /// <summary>
           /// Desc:099专家评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJPF {get;set;}

           /// <summary>
           /// Desc:099教师自评
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZP {get;set;}

           /// <summary>
           /// Desc:099最终得分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZDF {get;set;}

    }
}

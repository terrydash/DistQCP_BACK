﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSLBDMB
    {
           public JSLBDMB(){


           }
           /// <summary>
           /// Desc:001教室类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSLBDM {get;set;}

           /// <summary>
           /// Desc:002教室类别名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLBMC {get;set;}

           /// <summary>
           /// Desc:099是否是公共资源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGGZY {get;set;}

           /// <summary>
           /// Desc:099在教学场地调度里是否使用设备
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSYSB {get;set;}

           /// <summary>
           /// Desc:099是否可以借用教室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKYJY {get;set;}

           /// <summary>
           /// Desc:099任务安排是否默认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWAPSFMR {get;set;}

    }
}

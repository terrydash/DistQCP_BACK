﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSLCB
    {
           public JSLCB(){


           }
           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSBH6 {get;set;}

           /// <summary>
           /// Desc:099个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? GS {get;set;}

    }
}

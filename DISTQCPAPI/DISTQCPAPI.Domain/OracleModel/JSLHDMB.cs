﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSLHDMB
    {
           public JSLHDMB(){


           }
           /// <summary>
           /// Desc:099教室楼号代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSLHDM {get;set;}

           /// <summary>
           /// Desc:099教室楼号名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLHMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSQXB
    {
           public JSQXB(){


           }
           /// <summary>
           /// Desc:099角色名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSM {get;set;}

           /// <summary>
           /// Desc:099功能模块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GNMKDM {get;set;}

           /// <summary>
           /// Desc:099功能模块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GNMKMC {get;set;}

           /// <summary>
           /// Desc:099对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WR {get;set;}

           /// <summary>
           /// Desc:099不可以操作的学年学期的设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKCZXNXQ {get;set;}

    }
}

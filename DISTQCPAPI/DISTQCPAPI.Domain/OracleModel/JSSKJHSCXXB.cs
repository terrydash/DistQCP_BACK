﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSSKJHSCXXB
    {
           public JSSKJHSCXXB(){


           }
           /// <summary>
           /// Desc:099 教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099 选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099 文件路径
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PATH {get;set;}

           /// <summary>
           /// Desc:099 文件名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WJMC {get;set;}

           /// <summary>
           /// Desc:099 上传时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SCSJ {get;set;}

           /// <summary>
           /// Desc:099 上传次数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SCCS {get;set;}

           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:001学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

    }
}

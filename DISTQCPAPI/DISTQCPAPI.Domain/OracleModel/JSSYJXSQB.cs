﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSSYJXSQB
    {
           public JSSYJXSQB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XQ {get;set;}

           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:是否单开课
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDKK {get;set;}

           /// <summary>
           /// Desc:专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:班级名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XH {get;set;}

           /// <summary>
           /// Desc:试验项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMMC {get;set;}

           /// <summary>
           /// Desc:实验地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYDD {get;set;}

           /// <summary>
           /// Desc:教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:任课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKJS {get;set;}

           /// <summary>
           /// Desc:第几周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJZ {get;set;}

           /// <summary>
           /// Desc:星期几
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQJ {get;set;}

           /// <summary>
           /// Desc:节次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JC {get;set;}

           /// <summary>
           /// Desc:实验类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYLB {get;set;}

           /// <summary>
           /// Desc:实验类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYLX {get;set;}

           /// <summary>
           /// Desc:实验学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SYXS {get;set;}

           /// <summary>
           /// Desc:实验者人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SYZRS {get;set;}

           /// <summary>
           /// Desc:每组人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? MZRS {get;set;}

           /// <summary>
           /// Desc:循环次数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XHCS {get;set;}

           /// <summary>
           /// Desc:变动状况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BDZK {get;set;}

           /// <summary>
           /// Desc:教务处审核意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHYJ {get;set;}

           /// <summary>
           /// Desc:教务处审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHR {get;set;}

           /// <summary>
           /// Desc:教务处审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHSJ {get;set;}

           /// <summary>
           /// Desc:教务处审核标记：0  待审 1 通过 2 不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHBJ {get;set;}

           /// <summary>
           /// Desc:学院审核意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHYJ {get;set;}

           /// <summary>
           /// Desc:学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHR {get;set;}

           /// <summary>
           /// Desc:学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHSJ {get;set;}

           /// <summary>
           /// Desc:学院审核标记：0 待审 1 通过 2 不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHBJ {get;set;}

           /// <summary>
           /// Desc:实验代码（vb程序产生流水码）System.DateTime.Now.Ticks
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYDM {get;set;}

           /// <summary>
           /// Desc:开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYDM {get;set;}

    }
}

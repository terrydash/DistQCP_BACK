﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSWSCJGZSQB
    {
           public JSWSCJGZSQB(){


           }
           /// <summary>
           /// Desc:申请编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SQID {get;set;}

           /// <summary>
           /// Desc:教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:任课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKJS {get;set;}

           /// <summary>
           /// Desc:更正理由
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZLY {get;set;}

           /// <summary>
           /// Desc:教务处审核结果0：待审 1：通过 2：不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHJG {get;set;}

           /// <summary>
           /// Desc:教务处审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHR {get;set;}

           /// <summary>
           /// Desc:教务处审核意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHYJ {get;set;}

           /// <summary>
           /// Desc:申请学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQXN {get;set;}

           /// <summary>
           /// Desc:申请学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQXQ {get;set;}

           /// <summary>
           /// Desc:更正成绩类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZCJLB {get;set;}

           /// <summary>
           /// Desc:教务处审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHSJ {get;set;}

           /// <summary>
           /// Desc:申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? SQSJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSXWXXB
    {
           public JSXWXXB(){


           }
           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099学位类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XWLX {get;set;}

           /// <summary>
           /// Desc:099学位类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XWMC {get;set;}

           /// <summary>
           /// Desc:099入学时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RXSJ {get;set;}

           /// <summary>
           /// Desc:099毕业时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJ {get;set;}

           /// <summary>
           /// Desc:099毕业学校
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYXX {get;set;}

           /// <summary>
           /// Desc:099毕业专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYZY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQR {get;set;}

    }
}

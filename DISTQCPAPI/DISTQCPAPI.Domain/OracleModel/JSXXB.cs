﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSXXB
    {
           public JSXXB(){


           }
           /// <summary>
           /// Desc:001职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:003性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:004出生日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSRQ {get;set;}

           /// <summary>
           /// Desc:005联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXDH {get;set;}

           /// <summary>
           /// Desc:006电子邮箱地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EMLDZ {get;set;}

           /// <summary>
           /// Desc:007教职工类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JZGLB {get;set;}

           /// <summary>
           /// Desc:008所属部门（学院）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BM {get;set;}

           /// <summary>
           /// Desc:009所属科室（系）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KS {get;set;}

           /// <summary>
           /// Desc:011职务
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZW {get;set;}

           /// <summary>
           /// Desc:012职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:014教学质量评价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXZLPJ {get;set;}

           /// <summary>
           /// Desc:015教师简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJJ {get;set;}

           /// <summary>
           /// Desc:099板块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMC {get;set;}

           /// <summary>
           /// Desc:099等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DJ {get;set;}

           /// <summary>
           /// Desc:010学历
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XL {get;set;}

           /// <summary>
           /// Desc:013教学研究方向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYJFX {get;set;}

           /// <summary>
           /// Desc:099优先级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXJ {get;set;}

           /// <summary>
           /// Desc:099课时分布
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSFB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYYX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSRM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKYQ {get;set;}

           /// <summary>
           /// Desc:099有无教师资格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWJSZG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RSZGH {get;set;}

           /// <summary>
           /// Desc:099在职类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TELNUMBER {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TELLX {get;set;}

           /// <summary>
           /// Desc:N99是否实验室人员
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSYSRY {get;set;}

           /// <summary>
           /// Desc:099是否外聘教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFWP {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRXY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSKYF {get;set;}

           /// <summary>
           /// Desc:099教师系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JSXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRBKXY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MTZDKPKS {get;set;}

           /// <summary>
           /// Desc:099上一级职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZC {get;set;}

           /// <summary>
           /// Desc:099上一级职称聘任时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZCSJ {get;set;}

           /// <summary>
           /// Desc:099现任职称聘任时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZCSJ {get;set;}

           /// <summary>
           /// Desc:099通过课件审核课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KJSH {get;set;}

           /// <summary>
           /// Desc:099是否有多媒体教室操作证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DMTCZZ {get;set;}

           /// <summary>
           /// Desc:099取得教师资格时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QDJSZGSJ {get;set;}

           /// <summary>
           /// Desc:099是否在岗
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZG {get;set;}

           /// <summary>
           /// Desc:099政治面貌
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZMM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XW {get;set;}

           /// <summary>
           /// Desc:099参加工作时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJGZSJ {get;set;}

           /// <summary>
           /// Desc:099毕业时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZH {get;set;}

           /// <summary>
           /// Desc:教师资格证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGZH {get;set;}

           /// <summary>
           /// Desc:主讲教师资格证号 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJJSZGRZH {get;set;}

           /// <summary>
           /// Desc:099籍贯
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJLY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXMPY {get;set;}

           /// <summary>
           /// Desc:099指导学生毕业设计资格 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDBYZG {get;set;}

           /// <summary>
           /// Desc:099排课备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKBZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCZCBJ {get;set;}

           /// <summary>
           /// Desc:099社会服务方向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHFWFX {get;set;}

           /// <summary>
           /// Desc:099教龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JL {get;set;}

           /// <summary>
           /// Desc:099校龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXXL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWJSXM {get;set;}

           /// <summary>
           /// Desc:教师使用标记 T 启用,F 停用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSYBJ {get;set;}

           /// <summary>
           /// Desc:099学缘结构
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYJG {get;set;}

           /// <summary>
           /// Desc:099教师任职期限时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSRZQXSJ {get;set;}

           /// <summary>
           /// Desc:099坐车校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZCXQ {get;set;}

           /// <summary>
           /// Desc:099是否允许借教室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYXJJS {get;set;}

           /// <summary>
           /// Desc:来校时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXSJ {get;set;}

           /// <summary>
           /// Desc:099是否浙大外聘
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZDWP {get;set;}

    }
}

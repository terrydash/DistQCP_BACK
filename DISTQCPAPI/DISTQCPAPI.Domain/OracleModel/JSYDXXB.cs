﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSYDXXB
    {
           public JSYDXXB(){


           }
           /// <summary>
           /// Desc:N99异动号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YDH {get;set;}

           /// <summary>
           /// Desc:N99学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:N99学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:N99职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:N99姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:N99异动前部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQBM {get;set;}

           /// <summary>
           /// Desc:N99异动前科室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQKS {get;set;}

           /// <summary>
           /// Desc:N99异动前职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQZC {get;set;}

           /// <summary>
           /// Desc:N99异动前学位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQXW {get;set;}

           /// <summary>
           /// Desc:N99异动前毕业证书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQBYZS {get;set;}

           /// <summary>
           /// Desc:N99异动后部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHBM {get;set;}

           /// <summary>
           /// Desc:N99异动后科室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHKS {get;set;}

           /// <summary>
           /// Desc:N99异动后职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHZC {get;set;}

           /// <summary>
           /// Desc:N99异动后学位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHXW {get;set;}

           /// <summary>
           /// Desc:N99异动后毕业证书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHBYZS {get;set;}

           /// <summary>
           /// Desc:N99异动时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDSJ {get;set;}

           /// <summary>
           /// Desc:N99异动处理否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDCLF {get;set;}

    }
}

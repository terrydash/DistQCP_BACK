﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSZCB
    {
           public JSZCB(){


           }
           /// <summary>
           /// Desc:099职称代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZCDM {get;set;}

           /// <summary>
           /// Desc:099职称名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZCMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZLXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JDYL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZCLB {get;set;}

           /// <summary>
           /// Desc:带学生数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXSS {get;set;}

           /// <summary>
           /// Desc:标准工作量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZGZL {get;set;}

           /// <summary>
           /// Desc:标准工作量课筹
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZGZLKC {get;set;}

           /// <summary>
           /// Desc:超标准工作量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBZGZL {get;set;}

           /// <summary>
           /// Desc:超工作量课筹
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBZGZLKC {get;set;}

           /// <summary>
           /// Desc:超上限课筹
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSXKC {get;set;}

    }
}

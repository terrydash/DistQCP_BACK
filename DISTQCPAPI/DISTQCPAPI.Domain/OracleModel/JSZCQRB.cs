﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JSZCQRB
    {
           public JSZCQRB(){


           }
           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099职称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:099确认时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QRSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQR {get;set;}

    }
}

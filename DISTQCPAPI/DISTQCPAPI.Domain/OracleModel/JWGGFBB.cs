﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JWGGFBB
    {
           public JWGGFBB(){


           }
           /// <summary>
           /// Desc:099公告标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGBT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGZW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FBDW {get;set;}

           /// <summary>
           /// Desc:099发布单位
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FBSJ {get;set;}

           /// <summary>
           /// Desc:099发布时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YXQX {get;set;}

           /// <summary>
           /// Desc:099期限
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDDX {get;set;}

           /// <summary>
           /// Desc:面向大对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXXDX {get;set;}

           /// <summary>
           /// Desc:面向小对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FBNR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SCIP {get;set;}

           /// <summary>
           /// Desc:099公告正文
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FBR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGLBDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGLBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGSM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGSM2 {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXBCJXXB
    {
           public JXBCJXXB(){


           }
           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099及格平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JGPJF {get;set;}

           /// <summary>
           /// Desc:099及格标准差
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JGBZC {get;set;}

           /// <summary>
           /// Desc:099绩点计算方法
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDJSFF {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXBXKBLSZB
    {
           public JXBXKBLSZB(){


           }
           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099学院代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XYDM {get;set;}

           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:099比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BL {get;set;}

    }
}

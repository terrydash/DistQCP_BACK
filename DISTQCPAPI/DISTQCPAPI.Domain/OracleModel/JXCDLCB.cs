﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXCDLCB
    {
           public JXCDLCB(){


           }
           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSBH6 {get;set;}

           /// <summary>
           /// Desc:099教室个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSGS1 {get;set;}

           /// <summary>
           /// Desc:099教室个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSGS2 {get;set;}

           /// <summary>
           /// Desc:099教室个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSGS3 {get;set;}

           /// <summary>
           /// Desc:099教室个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSGS4 {get;set;}

           /// <summary>
           /// Desc:099座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZWS1 {get;set;}

           /// <summary>
           /// Desc:099座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZWS2 {get;set;}

           /// <summary>
           /// Desc:099座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZWS3 {get;set;}

           /// <summary>
           /// Desc:099座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZWS4 {get;set;}

           /// <summary>
           /// Desc:099总个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZGS1 {get;set;}

           /// <summary>
           /// Desc:099总个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZGS2 {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXCDSYB
    {
           public JXCDSYB(){


           }
           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ1 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ2 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ3 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ4 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ5 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ6 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ7 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ8 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ9 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ10 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ11 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ12 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ13 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ14 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ15 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ16 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ17 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ18 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ19 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ20 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ21 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ22 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ23 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ24 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ25 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ26 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ27 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ28 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ29 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ30 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ31 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ32 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ33 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ34 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ35 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ36 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ37 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ38 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ39 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ40 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ41 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ42 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ43 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ44 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ45 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ46 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ47 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ48 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ49 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ50 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ51 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ52 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ53 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ54 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ55 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ56 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ57 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ58 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ59 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ60 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ61 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ62 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ63 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ64 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ65 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ66 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ67 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ68 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ69 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ70 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ71 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ72 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ73 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ74 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ75 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ76 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ77 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ78 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ79 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ80 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ81 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ82 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ83 {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ84 {get;set;}

    }
}

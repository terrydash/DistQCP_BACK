﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXCDXSFPB
    {
           public JXCDXSFPB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:003星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQJ {get;set;}

           /// <summary>
           /// Desc:004起始时间段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short QSSJD {get;set;}

           /// <summary>
           /// Desc:005第几节
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJJ {get;set;}

           /// <summary>
           /// Desc:006任务总学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWZXS {get;set;}

           /// <summary>
           /// Desc:007教室类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSLB {get;set;}

           /// <summary>
           /// Desc:008开课学院
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:009百分比
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BFB {get;set;}

           /// <summary>
           /// Desc:010上限总学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXCDXXB
    {
           public JXCDXXB(){


           }
           /// <summary>
           /// Desc:001教室编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:002教室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMC {get;set;}

           /// <summary>
           /// Desc:003教室类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLB {get;set;}

           /// <summary>
           /// Desc:004座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZWS {get;set;}

           /// <summary>
           /// Desc:005建筑面积
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? JZMJ {get;set;}

           /// <summary>
           /// Desc:006校区代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQDM {get;set;}

           /// <summary>
           /// Desc:007备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099考试座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? KSZWS {get;set;}

           /// <summary>
           /// Desc:099可用否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KYF {get;set;}

           /// <summary>
           /// Desc:099优先级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXJ {get;set;}

           /// <summary>
           /// Desc:099实验标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYBJ {get;set;}

           /// <summary>
           /// Desc:099使用部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYBM {get;set;}

           /// <summary>
           /// Desc:考试可用否T可用F不可用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSKYF {get;set;}

           /// <summary>
           /// Desc:099楼号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYKYXQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYJSLB {get;set;}

           /// <summary>
           /// Desc:099  派监考学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJKXY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSMBLBDM {get;set;}

           /// <summary>
           /// Desc:099 考试教室优先级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSJSYXJ {get;set;}

           /// <summary>
           /// Desc:002教室简称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJC {get;set;}

           /// <summary>
           /// Desc:教室web是否可借用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KYF_WEB {get;set;}

           /// <summary>
           /// Desc:099POS编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string POS_ID {get;set;}

           /// <summary>
           /// Desc:099考试使用部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSYBM {get;set;}

           /// <summary>
           /// Desc:001实验室仪器设备（自动排课实验课考虑仪器是否够用）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSYQSB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NFBFYY {get;set;}

           /// <summary>
           /// Desc:标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS {get;set;}

    }
}

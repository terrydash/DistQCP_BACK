﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXCDYYB
    {
           public JXCDYYB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099教室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMC {get;set;}

           /// <summary>
           /// Desc:099开始周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short KSZ {get;set;}

           /// <summary>
           /// Desc:099结束周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short JSZ {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQJ {get;set;}

           /// <summary>
           /// Desc:099时间段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short SJD {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:099借用单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYDW {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DH {get;set;}

           /// <summary>
           /// Desc:099借用人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJGR {get;set;}

           /// <summary>
           /// Desc:099个人电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GRDH {get;set;}

           /// <summary>
           /// Desc:099借用理由
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYLY {get;set;}

           /// <summary>
           /// Desc:099借用教室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYJS {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099考试日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSRQ {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSRQ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099考试座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSZWS {get;set;}

           /// <summary>
           /// Desc:099上课长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SKCD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JYSJ {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZWSJD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? XUH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSBH1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMC1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YYLB {get;set;}

           /// <summary>
           /// Desc:099教室借用的具体时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYJTSJ {get;set;}

           /// <summary>
           /// Desc:099借用的教室是否要用设备
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSYSB {get;set;}

           /// <summary>
           /// Desc:099实际起始时间段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SJQSSJD {get;set;}

           /// <summary>
           /// Desc:099实际上课长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SJSKCD {get;set;}

           /// <summary>
           /// Desc:099教室借用的审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXDAB
    {
           public JXDAB(){


           }
           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:版本号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:适用专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZY {get;set;}

           /// <summary>
           /// Desc:单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DW {get;set;}

           /// <summary>
           /// Desc:大纲类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DGLX {get;set;}

           /// <summary>
           /// Desc:英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWMC {get;set;}

           /// <summary>
           /// Desc:英文摘要
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWZY {get;set;}

           /// <summary>
           /// Desc:参考书目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKSM {get;set;}

           /// <summary>
           /// Desc:是否审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSH {get;set;}

           /// <summary>
           /// Desc:是否批准
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFPZ {get;set;}

           /// <summary>
           /// Desc:使用说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSM {get;set;}

           /// <summary>
           /// Desc:审核人职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

    }
}

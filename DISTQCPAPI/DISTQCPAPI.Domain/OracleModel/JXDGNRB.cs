﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXDGNRB
    {
           public JXDGNRB(){


           }
           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:版本号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:第几部分
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BF {get;set;}

           /// <summary>
           /// Desc:总学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:讲课学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKXS {get;set;}

           /// <summary>
           /// Desc:实验学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXS {get;set;}

           /// <summary>
           /// Desc:上机学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXS {get;set;}

           /// <summary>
           /// Desc:标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BT {get;set;}

           /// <summary>
           /// Desc:具体内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTNR {get;set;}

           /// <summary>
           /// Desc:重点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZD {get;set;}

           /// <summary>
           /// Desc:难点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ND {get;set;}

           /// <summary>
           /// Desc:习题内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTNR {get;set;}

           /// <summary>
           /// Desc:实验上机内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSJ {get;set;}

    }
}

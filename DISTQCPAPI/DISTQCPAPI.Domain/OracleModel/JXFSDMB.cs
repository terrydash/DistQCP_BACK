﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXFSDMB
    {
           public JXFSDMB(){


           }
           /// <summary>
           /// Desc:099进行方式名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXFSMC {get;set;}

           /// <summary>
           /// Desc:099进行方式符号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXFSFH {get;set;}

           /// <summary>
           /// Desc:099快捷键
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KJJ {get;set;}

    }
}

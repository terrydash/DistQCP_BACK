﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXJCDMB
    {
           public JXJCDMB(){


           }
           /// <summary>
           /// Desc:099教学进程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJCDM {get;set;}

           /// <summary>
           /// Desc:099教学进程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXJCMC {get;set;}

           /// <summary>
           /// Desc:099教学进程符号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXJCFH {get;set;}

    }
}

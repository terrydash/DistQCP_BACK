﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXJCWHB
    {
           public JXJCWHB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099考试起止周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSQZZ {get;set;}

           /// <summary>
           /// Desc:099假期起止周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JQQZZ {get;set;}

           /// <summary>
           /// Desc:099教学起止周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXQZZ {get;set;}

    }
}

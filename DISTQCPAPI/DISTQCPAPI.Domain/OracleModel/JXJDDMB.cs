﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXJDDMB
    {
           public JXJDDMB(){


           }
           /// <summary>
           /// Desc:099进度类型代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LXDM {get;set;}

           /// <summary>
           /// Desc:099进度类型名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXMC {get;set;}

           /// <summary>
           /// Desc:099类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LBMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXJDJB
    {
           public JXJDJB(){


           }
           /// <summary>
           /// Desc:099奖学金等级代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJDJDM {get;set;}

           /// <summary>
           /// Desc:099奖学金等级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXJDJMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JE {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SSJXJLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYRYCH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXJHBYSJTMXXB
    {
           public JXJHBYSJTMXXB(){


           }
           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BYSJTMDM {get;set;}

           /// <summary>
           /// Desc:099毕业设计题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTM {get;set;}

           /// <summary>
           /// Desc:099题目来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMLY {get;set;}

           /// <summary>
           /// Desc:099题目类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMLX {get;set;}

           /// <summary>
           /// Desc:099题目性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMXZ {get;set;}

           /// <summary>
           /// Desc:099建议修读学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short JYXDXQ {get;set;}

           /// <summary>
           /// Desc:099起始结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSJSZ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099实习单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXDW {get;set;}

    }
}

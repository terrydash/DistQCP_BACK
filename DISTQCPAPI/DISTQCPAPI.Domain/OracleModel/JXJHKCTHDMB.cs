﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXJHKCTHDMB
    {
           public JXJHKCTHDMB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THKCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYFX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSH {get;set;}

           /// <summary>
           /// Desc:099操作时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZSJ {get;set;}

           /// <summary>
           /// Desc:099学生学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXYSHR {get;set;}

           /// <summary>
           /// Desc:099学生学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXYSHSJ {get;set;}

           /// <summary>
           /// Desc:099开课学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYSHR {get;set;}

           /// <summary>
           /// Desc:099开课学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYSHSJ {get;set;}

           /// <summary>
           /// Desc:申请替换学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQTHXN {get;set;}

           /// <summary>
           /// Desc:申请替换学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQTHXQ {get;set;}

    }
}

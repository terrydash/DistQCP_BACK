﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXJHKCXXLSB
    {
           public JXJHKCXXLSB(){


           }
           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:099课程性质
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:099课程类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLB {get;set;}

           /// <summary>
           /// Desc:099课程标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCBS {get;set;}

           /// <summary>
           /// Desc:099考核方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHFS {get;set;}

           /// <summary>
           /// Desc:099起始周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QSZ {get;set;}

           /// <summary>
           /// Desc:099结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSZ {get;set;}

           /// <summary>
           /// Desc:099开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:099开课系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKX {get;set;}

           /// <summary>
           /// Desc:099专业方向
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYFX {get;set;}

           /// <summary>
           /// Desc:099模块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFXMK {get;set;}

           /// <summary>
           /// Desc:099组号名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MKZH {get;set;}

           /// <summary>
           /// Desc:099建议修读学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short JYXDXQ {get;set;}

           /// <summary>
           /// Desc:099辅修标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? FXBS {get;set;}

           /// <summary>
           /// Desc:099课程内部编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCNBBH {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS {get;set;}

           /// <summary>
           /// Desc:099总学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXS {get;set;}

           /// <summary>
           /// Desc:099讲课学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKXS {get;set;}

           /// <summary>
           /// Desc:099实验学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXS {get;set;}

           /// <summary>
           /// Desc:099第二学位标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DEXWBS {get;set;}

           /// <summary>
           /// Desc:099起始结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSJSZ {get;set;}

           /// <summary>
           /// Desc:099课程群名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCQMC {get;set;}

           /// <summary>
           /// Desc:099课程号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCH {get;set;}

           /// <summary>
           /// Desc:099授课方式名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKFSMC {get;set;}

           /// <summary>
           /// Desc:099权重系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZXS {get;set;}

           /// <summary>
           /// Desc:099组号代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MKZHDM {get;set;}

           /// <summary>
           /// Desc:099模块名称代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFXMKDM {get;set;}

           /// <summary>
           /// Desc:099每学分费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZCFY {get;set;}

           /// <summary>
           /// Desc:099重修每学分费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CXFY {get;set;}

           /// <summary>
           /// Desc:099是否学位课
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXWK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSH {get;set;}

           /// <summary>
           /// Desc:011课程实践学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCSJXS {get;set;}

           /// <summary>
           /// Desc:011习题课学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTKXS {get;set;}

           /// <summary>
           /// Desc:011课外上机学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSJXS {get;set;}

           /// <summary>
           /// Desc:099课外上机学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSJXS {get;set;}

           /// <summary>
           /// Desc:011允许开课学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKKXQ {get;set;}

           /// <summary>
           /// Desc:099是否短学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDXQ {get;set;}

           /// <summary>
           /// Desc:099考试方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSFS {get;set;}

           /// <summary>
           /// Desc:099课外学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWXS {get;set;}

           /// <summary>
           /// Desc:099实践践码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJJM {get;set;}

           /// <summary>
           /// Desc:099教学场所
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXCS {get;set;}

           /// <summary>
           /// Desc:099素质资源类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZZYLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZFXWKC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFMFXWKC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSYSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJQSJSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJZZS {get;set;}

           /// <summary>
           /// Desc:099考试形式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYQSJSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXHBBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYPK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMCPY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFXXX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYKKX {get;set;}

           /// <summary>
           /// Desc:099是否毕业设计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBYSJ {get;set;}

           /// <summary>
           /// Desc:教学分段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXFD {get;set;}

           /// <summary>
           /// Desc:是否双语课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSYKC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YYS_BZ {get;set;}

           /// <summary>
           /// Desc:099工作量课程类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZLKCLX {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXJHSYXMKCXXB
    {
           public JXJHSYXMKCXXB(){


           }
           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099实验项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYXMDM {get;set;}

           /// <summary>
           /// Desc:099实验项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMMC {get;set;}

           /// <summary>
           /// Desc:099实验模块代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSMKDM {get;set;}

           /// <summary>
           /// Desc:099实验模块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSMKMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:099项目性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXZ {get;set;}

           /// <summary>
           /// Desc:099项目学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXF {get;set;}

           /// <summary>
           /// Desc:099开课学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short JYXDXQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSZ {get;set;}

           /// <summary>
           /// Desc:099起止周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSJSZ {get;set;}

           /// <summary>
           /// Desc:099开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:099实验中心
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKSYZX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZXYZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHFS {get;set;}

           /// <summary>
           /// Desc:099台套数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TTS {get;set;}

           /// <summary>
           /// Desc:099每台套学生数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MTTXSS {get;set;}

           /// <summary>
           /// Desc:099每组人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MZRS {get;set;}

           /// <summary>
           /// Desc:099实验项目确认:1表示确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMQR {get;set;}

           /// <summary>
           /// Desc:099项目成绩权重
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJQZ {get;set;}

    }
}

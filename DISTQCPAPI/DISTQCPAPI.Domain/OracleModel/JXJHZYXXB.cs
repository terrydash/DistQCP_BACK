﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXJHZYXXB
    {
           public JXJHZYXXB(){


           }
           /// <summary>
           /// Desc:001教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:002专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:003专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:004年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:005最低毕业学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZDBYXF {get;set;}

           /// <summary>
           /// Desc:006学分要求一
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ1 {get;set;}

           /// <summary>
           /// Desc:007学分要求二
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ2 {get;set;}

           /// <summary>
           /// Desc:008学分要求三
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ3 {get;set;}

           /// <summary>
           /// Desc:009学分要求四
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ4 {get;set;}

           /// <summary>
           /// Desc:099板块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMC {get;set;}

           /// <summary>
           /// Desc:010学分要求五
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ5 {get;set;}

           /// <summary>
           /// Desc:099校区标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQBS {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? RS {get;set;}

           /// <summary>
           /// Desc:010专业培养目标
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYPYMB {get;set;}

           /// <summary>
           /// Desc:010专业培养要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYPYYQ {get;set;}

           /// <summary>
           /// Desc:010主要课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYKC {get;set;}

           /// <summary>
           /// Desc:010特色课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TSKC {get;set;}

           /// <summary>
           /// Desc:010专业主干学科
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZGXK {get;set;}

           /// <summary>
           /// Desc:099学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYDM {get;set;}

           /// <summary>
           /// Desc:099任选学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RXXF {get;set;}

           /// <summary>
           /// Desc:099公选学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXF {get;set;}

           /// <summary>
           /// Desc:099第二学位学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DEXWXF {get;set;}

           /// <summary>
           /// Desc:010毕业标准
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYBZ {get;set;}

           /// <summary>
           /// Desc:010学位授予标准
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWSYBZ {get;set;}

           /// <summary>
           /// Desc:010是否热门专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFRMZY {get;set;}

           /// <summary>
           /// Desc:010下任务方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RW {get;set;}

           /// <summary>
           /// Desc:006学分要求六
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ6 {get;set;}

           /// <summary>
           /// Desc:007学分要求七
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ7 {get;set;}

           /// <summary>
           /// Desc:008学分要求八
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ8 {get;set;}

           /// <summary>
           /// Desc:009学分要求九
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ9 {get;set;}

           /// <summary>
           /// Desc:010学分要求十
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ10 {get;set;}

           /// <summary>
           /// Desc:006学分要求11
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ11 {get;set;}

           /// <summary>
           /// Desc:007学分要求12
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ12 {get;set;}

           /// <summary>
           /// Desc:010辅修学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXXF {get;set;}

           /// <summary>
           /// Desc:010基础课教学要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCXJX {get;set;}

           /// <summary>
           /// Desc:010主要实践教学
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYSJJX {get;set;}

           /// <summary>
           /// Desc:008学分要求13
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ13 {get;set;}

           /// <summary>
           /// Desc:009学分要求14
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ14 {get;set;}

           /// <summary>
           /// Desc:010学分要求15
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ15 {get;set;}

           /// <summary>
           /// Desc:006学分要求16
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ16 {get;set;}

           /// <summary>
           /// Desc:007学分要求17
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ17 {get;set;}

           /// <summary>
           /// Desc:008学分要求18
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ18 {get;set;}

           /// <summary>
           /// Desc:009学分要求19
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ19 {get;set;}

           /// <summary>
           /// Desc:010学分要求20
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ20 {get;set;}

           /// <summary>
           /// Desc:010第二专业学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DEZYXF {get;set;}

           /// <summary>
           /// Desc:010公选学分要求1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ1 {get;set;}

           /// <summary>
           /// Desc:010公选学分要求2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ2 {get;set;}

           /// <summary>
           /// Desc:010公选学分要求3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ3 {get;set;}

           /// <summary>
           /// Desc:010公选学分要求4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ4 {get;set;}

           /// <summary>
           /// Desc:010公选学分要求5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ5 {get;set;}

           /// <summary>
           /// Desc:010公选学分要求6
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ6 {get;set;}

           /// <summary>
           /// Desc:010公选学分要求7
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ7 {get;set;}

           /// <summary>
           /// Desc:010公选学分要求8
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ8 {get;set;}

           /// <summary>
           /// Desc:010公选学分要求9
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ9 {get;set;}

           /// <summary>
           /// Desc:010公选学分要求10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ10 {get;set;}

           /// <summary>
           /// Desc:010公选学分要求11
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ11 {get;set;}

           /// <summary>
           /// Desc:010公选学分要求12
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ12 {get;set;}

           /// <summary>
           /// Desc:010辅修年限
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXNX {get;set;}

           /// <summary>
           /// Desc:010备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZCFY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXFY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXFY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RCXQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYNX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ16 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ17 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ18 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ19 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ20 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ21 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ22 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ23 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ24 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZSYXW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GLXQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ25 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXFYQ26 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CQZS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHLJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KXG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ12 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ16 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ17 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ18 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ19 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? FXXFYQ20 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ12 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ16 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ17 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ18 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ19 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ20 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ21 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ22 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ23 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ24 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ25 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEZYXFYQ26 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z12 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Z14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ21 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ22 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ23 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ24 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ25 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ26 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ27 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ28 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GJZYDM {get;set;}

           /// <summary>
           /// Desc:099制订人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDR {get;set;}

           /// <summary>
           /// Desc:099审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

           /// <summary>
           /// Desc:099专业任选课学分要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYRXKXFYQ {get;set;}

           /// <summary>
           /// Desc:009学期选修课学分要求一
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQXF1 {get;set;}

           /// <summary>
           /// Desc:009学期选修课学分要求二
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQXF2 {get;set;}

           /// <summary>
           /// Desc:009学期选修课学分要求三
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQXF3 {get;set;}

           /// <summary>
           /// Desc:009学期选修课学分要求四
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQXF4 {get;set;}

           /// <summary>
           /// Desc:009学期选修课学分要求五
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQXF5 {get;set;}

           /// <summary>
           /// Desc:009学期选修课学分要求六
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQXF6 {get;set;}

           /// <summary>
           /// Desc:009学期选修课学分要求七
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQXF7 {get;set;}

           /// <summary>
           /// Desc:009学期选修课学分要求八
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQXF8 {get;set;}

           /// <summary>
           /// Desc:009学期选修课学分要求九
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQXF9 {get;set;}

           /// <summary>
           /// Desc:009学期选修课学分要求十
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQXF10 {get;set;}

           /// <summary>
           /// Desc:099二专业抵免学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EZYDMXF {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ1 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ2 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ3 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ4 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ5 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求6
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ6 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求7
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ7 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求8
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ8 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求9
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ9 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ10 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求11
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ11 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求12
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ12 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求13
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ13 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求14
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ14 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求15
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ15 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求16
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ16 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求17
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ17 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求18
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ18 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求19
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ19 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求20
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ20 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求21
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ21 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求22
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ22 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求23
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ23 {get;set;}

           /// <summary>
           /// Desc:099第二学位学分要求24
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DEXWXFYQ24 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XFYQ {get;set;}

           /// <summary>
           /// Desc:099英语类学分要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YYLXFYQ {get;set;}

           /// <summary>
           /// Desc:099学分要求29
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ29 {get;set;}

           /// <summary>
           /// Desc:099学分要求30
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ30 {get;set;}

           /// <summary>
           /// Desc:099学分要求31
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ31 {get;set;}

           /// <summary>
           /// Desc:099学分要求32
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ32 {get;set;}

           /// <summary>
           /// Desc:099学分要求33
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ33 {get;set;}

           /// <summary>
           /// Desc:099新生研讨课程要求最多学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSYTKC {get;set;}

           /// <summary>
           /// Desc:099通识选修+新生研讨课程要求学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TSXSXFYQ {get;set;}

    }
}

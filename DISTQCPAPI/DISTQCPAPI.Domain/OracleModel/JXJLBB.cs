﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXJLBB
    {
           public JXJLBB(){


           }
           /// <summary>
           /// Desc:099奖学金类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJLBDM {get;set;}

           /// <summary>
           /// Desc:099奖学金类别名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXJLBMC {get;set;}

    }
}

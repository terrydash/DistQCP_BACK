﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXXXYFKB
    {
           public JXXXYFKB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:003周次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:004选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:005学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099课堂纪律
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KTJL {get;set;}

           /// <summary>
           /// Desc:099基本意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JBYJ {get;set;}

           /// <summary>
           /// Desc:099习题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XT {get;set;}

           /// <summary>
           /// Desc:099实验报告
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYBG {get;set;}

           /// <summary>
           /// Desc:099预习报告
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXBG {get;set;}

           /// <summary>
           /// Desc:099分析报告
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXBG {get;set;}

           /// <summary>
           /// Desc:099作业批改情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYPGQK {get;set;}

           /// <summary>
           /// Desc:099完成时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WCSJ {get;set;}

           /// <summary>
           /// Desc:099复习时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXSJ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099旷课次数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? KKCS {get;set;}

           /// <summary>
           /// Desc:099迟到次数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? CDCS {get;set;}

    }
}

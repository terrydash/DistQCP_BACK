﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXXXYGGB
    {
           public JXXXYGGB(){


           }
           /// <summary>
           /// Desc:099公告标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGBT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGZW {get;set;}

           /// <summary>
           /// Desc:099发布单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FBDW {get;set;}

           /// <summary>
           /// Desc:099发布时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FBSJ {get;set;}

           /// <summary>
           /// Desc:099期限
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YXQX {get;set;}

           /// <summary>
           /// Desc:099公告正文
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGSM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SCIP {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FBR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZD {get;set;}

    }
}

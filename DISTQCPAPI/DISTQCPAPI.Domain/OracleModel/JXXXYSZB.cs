﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXXXYSZB
    {
           public JXXXYSZB(){


           }
           /// <summary>
           /// Desc:N01用户代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHDM {get;set;}

           /// <summary>
           /// Desc:N01用户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YHMC {get;set;}

           /// <summary>
           /// Desc:N01备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:N01用户编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YHBH {get;set;}

           /// <summary>
           /// Desc:N01聘用起始学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSXN {get;set;}

           /// <summary>
           /// Desc:N01聘用起始学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSXQ {get;set;}

           /// <summary>
           /// Desc:N01聘用结束学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXN {get;set;}

           /// <summary>
           /// Desc:N01聘用结束学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXQ {get;set;}

           /// <summary>
           /// Desc:N01是否在岗
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZG {get;set;}

    }
}

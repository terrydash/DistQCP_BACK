﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXYJBAB
    {
           public JXYJBAB(){


           }
           /// <summary>
           /// Desc:001教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:002业绩名类
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YJML {get;set;}

           /// <summary>
           /// Desc:003业绩内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJNR {get;set;}

           /// <summary>
           /// Desc:004业绩备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJBZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HTDRSJ {get;set;}

           /// <summary>
           /// Desc:006后台修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HTXGSJ {get;set;}

           /// <summary>
           /// Desc:007后台修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HTXGR {get;set;}

           /// <summary>
           /// Desc:008网页教师确认时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSQRSJ {get;set;}

           /// <summary>
           /// Desc:008网页教师查看状态，默认为0（未查看）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSCKZT {get;set;}

           /// <summary>
           /// Desc:008网页教师确认状态，默认为0（未确认）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSQRZT {get;set;}

    }
}

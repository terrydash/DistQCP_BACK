﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JXZLPJZZKHB
    {
           public JXZLPJZZKHB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099学生评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSPF {get;set;}

           /// <summary>
           /// Desc:099学院考核分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYKHF {get;set;}

           /// <summary>
           /// Desc:099考核总成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHZCJ {get;set;}

           /// <summary>
           /// Desc:099考核等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHDJ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

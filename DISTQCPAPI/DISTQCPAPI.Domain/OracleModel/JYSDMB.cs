﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JYSDMB
    {
           public JYSDMB(){


           }
           /// <summary>
           /// Desc:099教研室代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JYSDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYSMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXY {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class JZGLBB
    {
           public JZGLBB(){


           }
           /// <summary>
           /// Desc:职工类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGLBDM {get;set;}

           /// <summary>
           /// Desc:职工类别名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZGLBMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KBTABLE
    {
           public KBTABLE(){


           }
           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XQJ {get;set;}

           /// <summary>
           /// Desc:099起始时间段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal QSSJD {get;set;}

           /// <summary>
           /// Desc:099上课长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SKCD {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:099起始周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSZ {get;set;}

           /// <summary>
           /// Desc:099结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

    }
}

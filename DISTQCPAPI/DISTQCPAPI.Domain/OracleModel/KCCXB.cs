﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCCXB
    {
           public KCCXB(){


           }
           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQJ {get;set;}

           /// <summary>
           /// Desc:099起始时间段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string QSSJD {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? RS {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099教室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMC {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099对照信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DZXX {get;set;}

    }
}

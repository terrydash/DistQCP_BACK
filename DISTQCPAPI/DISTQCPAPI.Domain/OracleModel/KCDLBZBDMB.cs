﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCDLBZBDMB
    {
           public KCDLBZBDMB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DLDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BZBRS {get;set;}

           /// <summary>
           /// Desc:099浮动人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDRS {get;set;}

           /// <summary>
           /// Desc:099超过人数系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CGXS {get;set;}

    }
}

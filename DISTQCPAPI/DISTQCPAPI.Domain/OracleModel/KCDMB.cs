﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCDMB
    {
           public KCDMB(){


           }
           /// <summary>
           /// Desc:001课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:002课程名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCZWMC {get;set;}

           /// <summary>
           /// Desc:003课程英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCYWMC {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:005周学时/周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:099周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZS {get;set;}

           /// <summary>
           /// Desc:006预修要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXYQ {get;set;}

           /// <summary>
           /// Desc:007课程简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCJJ {get;set;}

           /// <summary>
           /// Desc:099标识语种
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXDG {get;set;}

           /// <summary>
           /// Desc:099标识1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS1 {get;set;}

           /// <summary>
           /// Desc:099标识2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS2 {get;set;}

           /// <summary>
           /// Desc:099期中学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZXS {get;set;}

           /// <summary>
           /// Desc:099考试内容及标准
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSNRJBZ {get;set;}

           /// <summary>
           /// Desc:099是否外语版
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFWYB {get;set;}

           /// <summary>
           /// Desc:099最低开课人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZDKKRS {get;set;}

           /// <summary>
           /// Desc:099课程类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLB {get;set;}

           /// <summary>
           /// Desc:099优先级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXJ {get;set;}

           /// <summary>
           /// Desc:099排课时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKSJ {get;set;}

           /// <summary>
           /// Desc:099排课要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKYQ {get;set;}

           /// <summary>
           /// Desc:099开课部门代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKBMDM {get;set;}

           /// <summary>
           /// Desc:099总学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXS {get;set;}

           /// <summary>
           /// Desc:099学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS {get;set;}

           /// <summary>
           /// Desc:099课程主要作用及目的
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCZYZYJMD {get;set;}

           /// <summary>
           /// Desc:099主要参考书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYCKS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KTHKCDM {get;set;}

           /// <summary>
           /// Desc:099层次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XLCC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZLXS {get;set;}

           /// <summary>
           /// Desc:099考核方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCYS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKBJ {get;set;}

           /// <summary>
           /// Desc:099理论学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? LLXS {get;set;}

           /// <summary>
           /// Desc:099实验学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? SYXS {get;set;}

           /// <summary>
           /// Desc:099上机学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? SJXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SYXMSYQ {get;set;}

           /// <summary>
           /// Desc:099课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? ZCFY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? CXFY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FXFY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCGS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKFSMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AXBXRW {get;set;}

           /// <summary>
           /// Desc:008教学大纲
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYPK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYKKBMDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BSFBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TEMP1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TEMP2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TEMP3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TEMP4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TEMP5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TEMP6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TEMP7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TEMP8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TEMP9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TEMP10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXFYQ {get;set;}

           /// <summary>
           /// Desc:099开课系代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXDM {get;set;}

           /// <summary>
           /// Desc:008教学大纲
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKFL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCSJXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTKXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSJXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSJXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YTXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SCJSSJXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SXXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KSXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BSXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SHDCXS {get;set;}

           /// <summary>
           /// Desc:099教研室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTYKW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCJC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKDX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XFJS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXSJS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKXSJS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXSJS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXSJS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXSSY {get;set;}

           /// <summary>
           /// Desc:099 标识4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMCPY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCJSZTDW {get;set;}

           /// <summary>
           /// Desc:099课程群名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCQMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSXSMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBYSJKC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099成绩录入人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLRR {get;set;}

           /// <summary>
           /// Desc:099学生选课是否提示必选
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTSBX {get;set;}

           /// <summary>
           /// Desc:教学大纲地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXDGDZ {get;set;}

           /// <summary>
           /// Desc:099课程分类
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCFL {get;set;}

           /// <summary>
           /// Desc:099课程英文简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCYWJJ {get;set;}

           /// <summary>
           /// Desc:099试卷录入职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJLRZGH {get;set;}

           /// <summary>
           /// Desc:课程简介地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCJJDZ {get;set;}

           /// <summary>
           /// Desc:001实验仪器代码（自动排课实验课考虑仪器是否够用）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQDM {get;set;}

           /// <summary>
           /// Desc:002实验仪器名称（自动排课实验课考虑仪器是否够用）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQMC {get;set;}

           /// <summary>
           /// Desc:099标识语种
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BSYZ {get;set;}

           /// <summary>
           /// Desc:是否主要课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZYKC {get;set;}

           /// <summary>
           /// Desc:099多学期课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXQTSKC {get;set;}

           /// <summary>
           /// Desc:099课程标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCBJ {get;set;}

           /// <summary>
           /// Desc:课程网页
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCWY {get;set;}

    }
}

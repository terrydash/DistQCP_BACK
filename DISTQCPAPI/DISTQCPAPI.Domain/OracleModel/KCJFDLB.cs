﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCJFDLB
    {
           public KCJFDLB(){


           }
           /// <summary>
           /// Desc:加分大类名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JFDLDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFDLMC {get;set;}

           /// <summary>
           /// Desc:是否体育系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTYXS {get;set;}

           /// <summary>
           /// Desc:是否体育系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYXS {get;set;}

    }
}

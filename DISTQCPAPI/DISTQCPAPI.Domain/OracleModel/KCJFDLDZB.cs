﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCJFDLDZB
    {
           public KCJFDLDZB(){


           }
           /// <summary>
           /// Desc:加分大类代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JFDLDM {get;set;}

           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

    }
}

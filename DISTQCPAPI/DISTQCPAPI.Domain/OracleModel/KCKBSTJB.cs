﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCKBSTJB
    {
           public KCKBSTJB(){


           }
           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程课表数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal KCKBS {get;set;}

           /// <summary>
           /// Desc:099周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH1 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH2 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH3 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH4 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH5 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH6 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH7 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH8 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH9 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH10 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH11 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH12 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH13 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH14 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH15 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH16 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH17 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH18 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH19 {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH20 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH21 {get;set;}

           /// <summary>
           /// Desc:099周学时倒
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ZXSN {get;set;}

           /// <summary>
           /// Desc:099上课内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SKNR {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYFX {get;set;}

    }
}

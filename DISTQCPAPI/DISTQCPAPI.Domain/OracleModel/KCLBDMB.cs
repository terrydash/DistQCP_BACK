﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCLBDMB
    {
           public KCLBDMB(){


           }
           /// <summary>
           /// Desc:001课程类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCLBDM {get;set;}

           /// <summary>
           /// Desc:002课程类别名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCLBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QX {get;set;}

           /// <summary>
           /// Desc:099必修课标识符
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BXKBSF {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCLBKKSJ
    {
           public KCLBKKSJ(){


           }
           /// <summary>
           /// Desc:099课程类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCLB {get;set;}

           /// <summary>
           /// Desc:099时间段序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJDXH {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ1 {get;set;}

    }
}

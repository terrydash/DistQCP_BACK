﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCLJJDLSB
    {
           public KCLJJDLSB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099课程累计绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? KCLJJD {get;set;}

           /// <summary>
           /// Desc:099名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? MC {get;set;}

           /// <summary>
           /// Desc:099是否控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SFKZ {get;set;}

    }
}

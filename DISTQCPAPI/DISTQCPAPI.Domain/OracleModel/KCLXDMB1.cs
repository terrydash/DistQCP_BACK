﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCLXDMB1
    {
           public KCLXDMB1(){


           }
           /// <summary>
           /// Desc:099课程类型代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCLXDM {get;set;}

           /// <summary>
           /// Desc:099课程类型名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCLXMC {get;set;}

    }
}

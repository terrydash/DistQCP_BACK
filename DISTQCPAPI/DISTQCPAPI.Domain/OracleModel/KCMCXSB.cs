﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCMCXSB
    {
           public KCMCXSB(){


           }
           /// <summary>
           /// Desc:099门次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MC {get;set;}

           /// <summary>
           /// Desc:099门次系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MCXS {get;set;}

    }
}

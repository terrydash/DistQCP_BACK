﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCPJHZB
    {
           public KCPJHZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099选课人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? XKRS {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099对课程评价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DKCPJ {get;set;}

           /// <summary>
           /// Desc:099对教材评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DJCPJ {get;set;}

           /// <summary>
           /// Desc:099学生对教师评价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? XSDJSPJ {get;set;}

           /// <summary>
           /// Desc:099学院对教师评价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? XYDJSPJ {get;set;}

           /// <summary>
           /// Desc:099对教师评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DJSPJ {get;set;}

           /// <summary>
           /// Desc:099评定信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PDXX {get;set;}

           /// <summary>
           /// Desc:099评定类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PDLX {get;set;}

           /// <summary>
           /// Desc:099等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJ {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BM {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HSDJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BSX {get;set;}

    }
}

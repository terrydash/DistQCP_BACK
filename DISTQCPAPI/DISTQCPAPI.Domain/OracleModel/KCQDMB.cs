﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCQDMB
    {
           public KCQDMB(){


           }
           /// <summary>
           /// Desc:099课程群代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCQDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCQMC {get;set;}

    }
}

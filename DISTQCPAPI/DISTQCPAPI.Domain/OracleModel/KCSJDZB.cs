﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCSJDZB
    {
           public KCSJDZB(){


           }
           /// <summary>
           /// Desc:099职务
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZW {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ {get;set;}

           /// <summary>
           /// Desc:099时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ0 {get;set;}

           /// <summary>
           /// Desc:099时间段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJD {get;set;}

           /// <summary>
           /// Desc:099具体时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

    }
}

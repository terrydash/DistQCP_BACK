﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCXZDMB
    {
           public KCXZDMB(){


           }
           /// <summary>
           /// Desc:001课程性质代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCXZDM {get;set;}

           /// <summary>
           /// Desc:002课程性质名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCXZMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZJC {get;set;}

           /// <summary>
           /// Desc:web对应， 限、校、院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEBDY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XBX {get;set;}

           /// <summary>
           /// Desc:??????,1:????????;0:??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXTJKZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZGXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCQZXS {get;set;}

           /// <summary>
           /// Desc:学生跨专业选修该性质的课程时算作哪类性质的学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DTKCXZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWJD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KZYSFXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZDZ {get;set;}

           /// <summary>
           /// Desc:099是否实践性教学环节课程。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXJXHJKC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NDXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCYD {get;set;}

           /// <summary>
           /// Desc:099打印显示顺序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? BBDYSX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWKCXZMC {get;set;}

           /// <summary>
           /// Desc:099是否可申请免试、免修
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKSQMSMX {get;set;}

    }
}

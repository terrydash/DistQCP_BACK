﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCXZSHB
    {
           public KCXZSHB(){


           }
           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099课程性质名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZMC {get;set;}

           /// <summary>
           /// Desc:099最低学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZDXF {get;set;}

           /// <summary>
           /// Desc:099专业方向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFX {get;set;}

    }
}

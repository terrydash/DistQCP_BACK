﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCZGPFDZB
    {
           public KCZGPFDZB(){


           }
           /// <summary>
           /// Desc:099主管类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGLX {get;set;}

           /// <summary>
           /// Desc:099评分
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PF {get;set;}

           /// <summary>
           /// Desc:099对应分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DYF {get;set;}

    }
}

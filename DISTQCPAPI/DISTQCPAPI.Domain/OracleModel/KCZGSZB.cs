﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KCZGSZB
    {
           public KCZGSZB(){


           }
           /// <summary>
           /// Desc:099主管类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGLX {get;set;}

           /// <summary>
           /// Desc:099主管单位
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGDW {get;set;}

           /// <summary>
           /// Desc:099主管教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099主管教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

    }
}

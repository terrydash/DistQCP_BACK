﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KFSYHCB
    {
           public KFSYHCB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string IDHC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCSL {get;set;}

           /// <summary>
           /// Desc:099规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GG {get;set;}

           /// <summary>
           /// Desc:099型号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099品牌
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PP {get;set;}

    }
}

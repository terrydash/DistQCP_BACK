﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KFSYSXMSQB
    {
           public KFSYSXMSQB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099实验项目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMDM {get;set;}

           /// <summary>
           /// Desc:099实验项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMMC {get;set;}

           /// <summary>
           /// Desc:099所属实验室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSSYSBH {get;set;}

           /// <summary>
           /// Desc:099所属实验室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSSYSMC {get;set;}

           /// <summary>
           /// Desc:099所属学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXYMC {get;set;}

           /// <summary>
           /// Desc:099实验项目类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMLX {get;set;}

           /// <summary>
           /// Desc:099指导教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099指导教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZC {get;set;}

           /// <summary>
           /// Desc:099实验时数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXS {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXF {get;set;}

           /// <summary>
           /// Desc:099指导教师工作量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSGZL {get;set;}

           /// <summary>
           /// Desc:099所需经费预算
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFYS {get;set;}

           /// <summary>
           /// Desc:099招收对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKDX {get;set;}

           /// <summary>
           /// Desc:099招收人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKRS {get;set;}

           /// <summary>
           /// Desc:099研究时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ {get;set;}

           /// <summary>
           /// Desc:099成果形式及考核办法
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CGXS {get;set;}

           /// <summary>
           /// Desc:099项目负责人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMFZR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZRZGH {get;set;}

           /// <summary>
           /// Desc:099联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZRDH {get;set;}

           /// <summary>
           /// Desc:099仪器设备名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQSBMC {get;set;}

           /// <summary>
           /// Desc:099场地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYCD {get;set;}

           /// <summary>
           /// Desc:099实验耗材
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCMC {get;set;}

           /// <summary>
           /// Desc:099实验耗材
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCSL {get;set;}

           /// <summary>
           /// Desc:099项目研究的意义、作用及预期达到的效果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMYY {get;set;}

           /// <summary>
           /// Desc:099项目主要内容、难点、创新点及突破方案设想
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMNR {get;set;}

           /// <summary>
           /// Desc:099实验室意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSYJ {get;set;}

           /// <summary>
           /// Desc:099学院意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYYJ {get;set;}

           /// <summary>
           /// Desc:099教务处意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCYJ {get;set;}

           /// <summary>
           /// Desc:099审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHLG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ1 {get;set;}

           /// <summary>
           /// Desc:099审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJG {get;set;}

    }
}

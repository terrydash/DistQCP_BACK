﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KHAPBDBJ2
    {
           public KHAPBDBJ2(){


           }
           /// <summary>
           /// Desc:099时间段序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short SJDXH {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:099班级信息对照
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJXXZD {get;set;}

           /// <summary>
           /// Desc:099班级信息对照2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJXXZD2 {get;set;}

    }
}

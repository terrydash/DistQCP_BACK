﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KHCDLSB
    {
           public KHCDLSB(){


           }
           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short RS {get;set;}

    }
}

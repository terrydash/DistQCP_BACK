﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KHDMB
    {
           public KHDMB(){


           }
           /// <summary>
           /// Desc:099课表数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal KBS {get;set;}

           /// <summary>
           /// Desc:099周学时
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099任课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKJS {get;set;}

           /// <summary>
           /// Desc:099课程类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLB {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? RS {get;set;}

           /// <summary>
           /// Desc:099安排标志
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string APBZ {get;set;}

           /// <summary>
           /// Desc:099校区要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQYQ {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FWYQ {get;set;}

           /// <summary>
           /// Desc:099教室类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLBYQ {get;set;}

    }
}

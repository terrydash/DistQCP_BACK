﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KHXSRSB
    {
           public KHXSRSB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099考试时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short RS {get;set;}

           /// <summary>
           /// Desc:099课程人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? KCRS {get;set;}

           /// <summary>
           /// Desc:099总课程人数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ZKCRS {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KHXXDZB
    {
           public KHXXDZB(){


           }
           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099对照信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DZXX {get;set;}

           /// <summary>
           /// Desc:099上课内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SKNR {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? RS {get;set;}

           /// <summary>
           /// Desc:099周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:099场地标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CDBS {get;set;}

           /// <summary>
           /// Desc:099院系选修课
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXXXK {get;set;}

           /// <summary>
           /// Desc:099实验
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYYCSK {get;set;}

           /// <summary>
           /// Desc:099起始周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QSZ {get;set;}

           /// <summary>
           /// Desc:099结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KKYZSDB
    {
           public KKYZSDB(){


           }
           /// <summary>
           /// Desc:099容量下限
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short RLXX {get;set;}

           /// <summary>
           /// Desc:099容量上限
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short RLSX {get;set;}

           /// <summary>
           /// Desc:099最低开课人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZDKKRS {get;set;}

    }
}

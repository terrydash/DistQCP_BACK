﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KQLXCSDYSZB
    {
           public KQLXCSDYSZB(){


           }
           /// <summary>
           /// Desc:099考勤类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KQLX {get;set;}

           /// <summary>
           /// Desc:099次数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CS {get;set;}

           /// <summary>
           /// Desc:099对应考勤类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYKQLX {get;set;}

    }
}

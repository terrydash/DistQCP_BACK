﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSAPQRD
    {
           public KSAPQRD(){


           }
           /// <summary>
           /// Desc:选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:课程期末是否需要教务处印刷试卷
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJWCYS {get;set;}

           /// <summary>
           /// Desc:课程期末是否需要教务处印刷试卷教师最后确认时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCYSQRSJ {get;set;}

           /// <summary>
           /// Desc:课程期末是否统一安排考试时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTIAPKSSJ {get;set;}

           /// <summary>
           /// Desc:课程期末是否统一安排考试时间教师最后确认时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TIAPKSQRSJ {get;set;}

    }
}

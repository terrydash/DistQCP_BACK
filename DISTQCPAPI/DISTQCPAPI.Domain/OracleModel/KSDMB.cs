﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSDMB
    {
           public KSDMB(){


           }
           /// <summary>
           /// Desc:科室代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KSDM {get;set;}

           /// <summary>
           /// Desc:科室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSMC {get;set;}

    }
}

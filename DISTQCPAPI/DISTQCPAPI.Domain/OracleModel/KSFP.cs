﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSFP
    {
           public KSFP(){


           }
           /// <summary>
           /// Desc:099周数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZS {get;set;}

           /// <summary>
           /// Desc:099学位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XW {get;set;}

           /// <summary>
           /// Desc:099位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

    }
}

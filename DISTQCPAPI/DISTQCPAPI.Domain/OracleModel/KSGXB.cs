﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSGXB
    {
           public KSGXB(){


           }
           /// <summary>
           /// Desc:001专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:002专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:003所属学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXYDM {get;set;}

           /// <summary>
           /// Desc:004所属学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXYMC {get;set;}

           /// <summary>
           /// Desc:005实验科室类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYLB {get;set;}

    }
}

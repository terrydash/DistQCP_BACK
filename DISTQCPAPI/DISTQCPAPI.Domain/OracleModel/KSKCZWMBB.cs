﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSKCZWMBB
    {
           public KSKCZWMBB(){


           }
           /// <summary>
           /// Desc:N099 模板代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MBDM {get;set;}

           /// <summary>
           /// Desc:N099 行号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string HH {get;set;}

           /// <summary>
           /// Desc:N099 列号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LH {get;set;}

           /// <summary>
           /// Desc:N099 是否排考
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFPK {get;set;}

           /// <summary>
           /// Desc:N099 是否显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXS {get;set;}

           /// <summary>
           /// Desc:N099 座位号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZWH {get;set;}

    }
}

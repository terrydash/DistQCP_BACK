﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSKCZWMBDMB
    {
           public KSKCZWMBDMB(){


           }
           /// <summary>
           /// Desc:N099 模板代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MBDM {get;set;}

           /// <summary>
           /// Desc:N099 模板名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MBMC {get;set;}

    }
}

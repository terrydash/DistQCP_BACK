﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSMSB
    {
           public KSMSB(){


           }
           /// <summary>
           /// Desc:考试模式代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KSMSDM {get;set;}

           /// <summary>
           /// Desc:考试模式名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KSMSMC {get;set;}

           /// <summary>
           /// Desc:学时范围
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XSFW {get;set;}

    }
}

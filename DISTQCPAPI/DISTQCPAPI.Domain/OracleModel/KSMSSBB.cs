﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSMSSBB
    {
           public KSMSSBB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:教师工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:考试模式代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSMSDM {get;set;}

           /// <summary>
           /// Desc:平时成绩项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PSCJXMDM {get;set;}

           /// <summary>
           /// Desc:考试周次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSZC {get;set;}

           /// <summary>
           /// Desc:考试日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSRQ {get;set;}

           /// <summary>
           /// Desc:考试开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSKSSJ {get;set;}

           /// <summary>
           /// Desc:考试结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSJSSJ {get;set;}

           /// <summary>
           /// Desc:提交状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TJZT {get;set;}

           /// <summary>
           /// Desc:提交时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJSJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSSJDDAPB
    {
           public KSSJDDAPB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099上课教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKJSZGH {get;set;}

           /// <summary>
           /// Desc:099上课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKJSXM {get;set;}

           /// <summary>
           /// Desc:099考试时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:099考试具体时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSJTSJ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099考试座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSZWS {get;set;}

           /// <summary>
           /// Desc:099教室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMC {get;set;}

           /// <summary>
           /// Desc:099课程人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? KCRS {get;set;}

           /// <summary>
           /// Desc:099总课程人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZKCRS {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? RS {get;set;}

           /// <summary>
           /// Desc:099实际人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SJRS {get;set;}

           /// <summary>
           /// Desc:099监考教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKJSZGH {get;set;}

           /// <summary>
           /// Desc:099监考教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKJSXM {get;set;}

           /// <summary>
           /// Desc:099教学场地
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal JXCD {get;set;}

           /// <summary>
           /// Desc:099开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:099监考时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKJS1 {get;set;}

           /// <summary>
           /// Desc:099监考时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKJS2 {get;set;}

           /// <summary>
           /// Desc:099监考时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKJS3 {get;set;}

           /// <summary>
           /// Desc:099监考时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKJS4 {get;set;}

           /// <summary>
           /// Desc:099合班标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? HBBZ {get;set;}

           /// <summary>
           /// Desc:099乘车地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCDD {get;set;}

           /// <summary>
           /// Desc:099监考
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JK {get;set;}

           /// <summary>
           /// Desc:099乘车地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCDD1 {get;set;}

           /// <summary>
           /// Desc:099乘车地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCDD2 {get;set;}

           /// <summary>
           /// Desc:099乘车地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCDD3 {get;set;}

           /// <summary>
           /// Desc:099乘车地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCDD4 {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH1 {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH2 {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH3 {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CDAPBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQYQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PZFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSCC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXKJSZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXKJSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXKJSZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXKJSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CDAPBJ2 {get;set;}

           /// <summary>
           /// Desc:099 派副监考学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDPJK {get;set;}

           /// <summary>
           /// Desc:099考试评卷编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXH {get;set;}

           /// <summary>
           /// Desc:099考试取卷地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSQJDD {get;set;}

           /// <summary>
           /// Desc:099考试负责人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSFZR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSJTSJ2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JKBJ {get;set;}

    }
}

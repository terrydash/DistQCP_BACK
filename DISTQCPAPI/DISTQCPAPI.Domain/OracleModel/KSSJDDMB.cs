﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSSJDDMB
    {
           public KSSJDDMB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KSSJD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSFKP {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJBHBJ {get;set;}

           /// <summary>
           /// Desc:009学生该时间段是否可查考试时间地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEB_XSSFKC {get;set;}

           /// <summary>
           /// Desc:009教师该时间段是否可查考试时间地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEB_JSSFKC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSFKPJS {get;set;}

    }
}

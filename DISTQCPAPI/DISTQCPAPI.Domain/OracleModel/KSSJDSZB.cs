﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSSJDSZB
    {
           public KSSJDSZB(){


           }
           /// <summary>
           /// Desc:099考试时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short KSSJ {get;set;}

           /// <summary>
           /// Desc:099考试时间显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSJTSJ {get;set;}

    }
}

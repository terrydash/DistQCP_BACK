﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSSJJSB
    {
           public KSSJJSB(){


           }
           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099考试时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? KSSJ {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099试卷编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDPK {get;set;}

    }
}

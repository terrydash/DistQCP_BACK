﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class KSSJZWS
    {
           public KSSJZWS(){


           }
           /// <summary>
           /// Desc:099第几周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DJZ {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQJ {get;set;}

           /// <summary>
           /// Desc:099时间段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short SJD {get;set;}

           /// <summary>
           /// Desc:099考试座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? KSZWS {get;set;}

           /// <summary>
           /// Desc:099顺序位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXW {get;set;}

    }
}

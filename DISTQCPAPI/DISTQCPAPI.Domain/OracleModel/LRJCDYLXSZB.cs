﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class LRJCDYLXSZB
    {
           public LRJCDYLXSZB(){


           }
           /// <summary>
           /// Desc:099考勤类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KQLX {get;set;}

           /// <summary>
           /// Desc:099是否对应节次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDYJC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class LSJFQKB
    {
           public LSJFQKB(){


           }
           /// <summary>
           /// Desc:001学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:003学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:004年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:005专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:006班级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:007交费标志
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFBZ {get;set;}

    }
}

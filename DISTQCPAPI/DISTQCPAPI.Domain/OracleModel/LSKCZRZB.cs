﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class LSKCZRZB
    {
           public LSKCZRZB(){


           }
           /// <summary>
           /// Desc:099序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XH {get;set;}

           /// <summary>
           /// Desc:099用户名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHM {get;set;}

           /// <summary>
           /// Desc:099用户操作
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YHCZ {get;set;}

           /// <summary>
           /// Desc:099操作对象模块
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZDXMK {get;set;}

           /// <summary>
           /// Desc:099登入时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DRSJ {get;set;}

           /// <summary>
           /// Desc:099退出时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TCSJ {get;set;}

           /// <summary>
           /// Desc:099操作时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZSJ {get;set;}

           /// <summary>
           /// Desc:099IP地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string IPDZ {get;set;}

    }
}

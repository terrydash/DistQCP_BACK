﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class LSKJSXXB
    {
           public LSKJSXXB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXDH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EMLDZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JZGLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXZLPJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXYJFX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSFB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYYX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSRM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKYQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWJSZG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RSZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TELNUMBER {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TELLX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSYSRY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFWP {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRXY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSKYF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JSXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRBKXY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MTZDKPKS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZCSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZCSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KJSH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DMTCZZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QDJSZGSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZMM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJGZSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJJSZGRZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXMPY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDBYZG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJLY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYSMC {get;set;}

           /// <summary>
           /// Desc:099现学历毕业时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XLBYSJ {get;set;}

           /// <summary>
           /// Desc:099现学历毕业专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XLBYZY {get;set;}

           /// <summary>
           /// Desc:099现学历毕业学校
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XLBYXX {get;set;}

           /// <summary>
           /// Desc:099现学位毕业时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWBYSJ {get;set;}

           /// <summary>
           /// Desc:099现学位毕业专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWBYZY {get;set;}

           /// <summary>
           /// Desc:099现学位毕业学校
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWBYXX {get;set;}

           /// <summary>
           /// Desc:099上一级学历
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXL {get;set;}

           /// <summary>
           /// Desc:099上一级学历毕业时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXLBYSJ {get;set;}

           /// <summary>
           /// Desc:099上一级学历毕业专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXLBYZY {get;set;}

           /// <summary>
           /// Desc:099上一级学历毕业学校
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXLBYXX {get;set;}

           /// <summary>
           /// Desc:099上一级学位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXW {get;set;}

           /// <summary>
           /// Desc:099上一级学位毕业时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXWBYSJ {get;set;}

           /// <summary>
           /// Desc:099上一级学位毕业专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXWBYZY {get;set;}

           /// <summary>
           /// Desc:099上一级学位毕业学校
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXWBYXX {get;set;}

           /// <summary>
           /// Desc:099本学期教学任务
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQJXRW {get;set;}

           /// <summary>
           /// Desc:099是否专任教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZR {get;set;}

           /// <summary>
           /// Desc:099是否主讲教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZJ {get;set;}

           /// <summary>
           /// Desc:099有无主讲教师资格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWZJJSZG {get;set;}

           /// <summary>
           /// Desc:099本学期在岗教授副教授是否给本科生授课
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBKSSK {get;set;}

           /// <summary>
           /// Desc:099通过课件审核课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TGKCDM {get;set;}

           /// <summary>
           /// Desc:099教师进修情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJXQK {get;set;}

           /// <summary>
           /// Desc:099上一级职称聘任时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SPRSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWJSXM {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class LSSYXK
    {
           public LSSYXK(){


           }
           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099实验选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXKKH {get;set;}

           /// <summary>
           /// Desc:099上课时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ {get;set;}

           /// <summary>
           /// Desc:099上课地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKDD {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? RS {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQJ {get;set;}

           /// <summary>
           /// Desc:099第几节
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DJJ {get;set;}

           /// <summary>
           /// Desc:099上课长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SKCD {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BZ {get;set;}

           /// <summary>
           /// Desc:099推荐课表代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJKBDM {get;set;}

    }
}

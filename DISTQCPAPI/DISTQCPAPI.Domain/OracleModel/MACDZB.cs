﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class MACDZB
    {
           public MACDZB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int XH {get;set;}

           /// <summary>
           /// Desc:099MAC地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MACDZ {get;set;}

    }
}

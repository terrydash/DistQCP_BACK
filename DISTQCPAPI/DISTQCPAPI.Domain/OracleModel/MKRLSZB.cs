﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class MKRLSZB
    {
           public MKRLSZB(){


           }
           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MKMC {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RS {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class MKZHB
    {
           public MKZHB(){


           }
           /// <summary>
           /// Desc:099模块组号代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MKZHDM {get;set;}

           /// <summary>
           /// Desc:099模块组号名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MKZHMC {get;set;}

           /// <summary>
           /// Desc:099要求获得学分
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YQDDXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LBNGX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LBXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFMKMS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GWQTZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWMKZHMC {get;set;}

           /// <summary>
           /// Desc:选课限制学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKXZXF {get;set;}

           /// <summary>
           /// Desc:099学生类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSLB {get;set;}

    }
}

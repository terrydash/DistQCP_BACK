﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class MMFSB
    {
           public MMFSB(){


           }
           /// <summary>
           /// Desc:099用户名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHM {get;set;}

           /// <summary>
           /// Desc:099smtp
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SMTP {get;set;}

           /// <summary>
           /// Desc:099发信人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXR {get;set;}

           /// <summary>
           /// Desc:099密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MM {get;set;}

           /// <summary>
           /// Desc:099发信人地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXRDZ {get;set;}

           /// <summary>
           /// Desc:099端口号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DKH {get;set;}

           /// <summary>
           /// Desc:099教学场地演示网址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HTTP {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class MYDDCDJB
    {
           public MYDDCDJB(){


           }
           /// <summary>
           /// Desc:099评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJH {get;set;}

           /// <summary>
           /// Desc:099等级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJ {get;set;}

           /// <summary>
           /// Desc:排序序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? XH {get;set;}

           /// <summary>
           /// Desc:099满意度评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? PF {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class MYDDCZBXXB
    {
           public MYDDCZBXXB(){


           }
           /// <summary>
           /// Desc:099课程类型代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCLXDM {get;set;}

           /// <summary>
           /// Desc:099评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJH {get;set;}

           /// <summary>
           /// Desc:099评价内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNR {get;set;}

           /// <summary>
           /// Desc:099调查内容简称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNRJC {get;set;}

    }
}

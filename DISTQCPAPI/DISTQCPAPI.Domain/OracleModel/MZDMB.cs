﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class MZDMB
    {
           public MZDMB(){


           }
           /// <summary>
           /// Desc:099民族代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MZDM {get;set;}

           /// <summary>
           /// Desc:099民族名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MZMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MZYWMC {get;set;}

    }
}

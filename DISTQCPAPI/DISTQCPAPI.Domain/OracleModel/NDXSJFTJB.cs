﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class NDXSJFTJB
    {
           public NDXSJFTJB(){


           }
           /// <summary>
           /// Desc:099年度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ND {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:099应交学费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? YJXF {get;set;}

           /// <summary>
           /// Desc:099应交住宿费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? YJZSF {get;set;}

           /// <summary>
           /// Desc:099应交选课费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? YJXKF {get;set;}

           /// <summary>
           /// Desc:099应交代管费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? YJDGF {get;set;}

           /// <summary>
           /// Desc:099应交数小计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? YJSXJ {get;set;}

           /// <summary>
           /// Desc:099应交人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? YJRS {get;set;}

           /// <summary>
           /// Desc:099实交学费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SJXF {get;set;}

           /// <summary>
           /// Desc:099实交注册费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SJZSF {get;set;}

           /// <summary>
           /// Desc:099实交选课费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SJXKF {get;set;}

           /// <summary>
           /// Desc:099实交代管费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SJDGF {get;set;}

           /// <summary>
           /// Desc:099实交数小计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SJSXJ {get;set;}

           /// <summary>
           /// Desc:099实际比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? SJBL {get;set;}

           /// <summary>
           /// Desc:099实交人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SJRS {get;set;}

           /// <summary>
           /// Desc:099未交学费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? WJXF {get;set;}

           /// <summary>
           /// Desc:099未交住宿费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? WJZSF {get;set;}

           /// <summary>
           /// Desc:099未交选课费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? WJXKF {get;set;}

           /// <summary>
           /// Desc:099未交代管费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? WJDGF {get;set;}

           /// <summary>
           /// Desc:099未交数小计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? WJSXJ {get;set;}

           /// <summary>
           /// Desc:099未交比率
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? WJBL {get;set;}

           /// <summary>
           /// Desc:099未交人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? WJRS {get;set;}

    }
}

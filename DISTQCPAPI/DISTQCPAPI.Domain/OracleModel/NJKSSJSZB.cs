﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class NJKSSJSZB
    {
           public NJKSSJSZB(){


           }
           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short NJ {get;set;}

           /// <summary>
           /// Desc:099时间段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJD {get;set;}

           /// <summary>
           /// Desc:099具体时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTSJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PDXDMB
    {
           public PDXDMB(){


           }
           /// <summary>
           /// Desc:099编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short BH {get;set;}

           /// <summary>
           /// Desc:099代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DM {get;set;}

           /// <summary>
           /// Desc:099评定项名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PDXMC {get;set;}

    }
}

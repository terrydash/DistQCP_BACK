﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PDXMBLSZB
    {
           public PDXMBLSZB(){


           }
           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099项目一比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XM1BL {get;set;}

           /// <summary>
           /// Desc:099项目二比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XM2BL {get;set;}

           /// <summary>
           /// Desc:099项目三比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XM3BL {get;set;}

           /// <summary>
           /// Desc:099项目四比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XM4BL {get;set;}

           /// <summary>
           /// Desc:099项目五比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XM5BL {get;set;}

           /// <summary>
           /// Desc:099项目六比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XM6BL {get;set;}

           /// <summary>
           /// Desc:099开设选修课门数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KSXXKMS {get;set;}

    }
}

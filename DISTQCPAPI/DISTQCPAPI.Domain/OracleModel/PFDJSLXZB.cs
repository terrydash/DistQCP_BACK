﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PFDJSLXZB
    {
           public PFDJSLXZB(){


           }
           /// <summary>
           /// Desc:099序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099评分等级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJ {get;set;}

           /// <summary>
           /// Desc:099数量限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SL {get;set;}

    }
}

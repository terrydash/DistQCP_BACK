﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PGZXXGPYB
    {
           public PGZXXGPYB(){


           }
           /// <summary>
           /// Desc:N99学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:N99学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:N99学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:N99选课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:N99评价周次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJZC {get;set;}

           /// <summary>
           /// Desc:N99评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJH {get;set;}

           /// <summary>
           /// Desc:N99修改次数（修改一次增加1）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int XGCS {get;set;}

           /// <summary>
           /// Desc:N99职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:N99课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:N99原评价建议
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YPJJY {get;set;}

           /// <summary>
           /// Desc:N99现评价建议
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XPJJY {get;set;}

           /// <summary>
           /// Desc:N99修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGSJ {get;set;}

           /// <summary>
           /// Desc:N99修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGR {get;set;}

    }
}

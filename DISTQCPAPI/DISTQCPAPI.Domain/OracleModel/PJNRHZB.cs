﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PJNRHZB
    {
           public PJNRHZB(){


           }
           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJH {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXDM {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? RS {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DF {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BSX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZTPF {get;set;}

    }
}

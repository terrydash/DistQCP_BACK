﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PJSRHDSZB
    {
           public PJSRHDSZB(){


           }
           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHM {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal QSBH {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal JSBH {get;set;}

           /// <summary>
           /// Desc:099领票日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LPRQ {get;set;}

    }
}

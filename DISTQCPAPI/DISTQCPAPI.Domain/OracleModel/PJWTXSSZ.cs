﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PJWTXSSZ
    {
           public PJWTXSSZ(){


           }
           /// <summary>
           /// Desc:099编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BH {get;set;}

           /// <summary>
           /// Desc:099等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJ {get;set;}

           /// <summary>
           /// Desc:099系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS {get;set;}

           /// <summary>
           /// Desc:099问题名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string WTMC {get;set;}

    }
}

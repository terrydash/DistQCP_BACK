﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PJXQCXSZB
    {
           public PJXQCXSZB(){


           }
           /// <summary>
           /// Desc:099评价学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099评价学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099查询开关，默认不可查
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short CXKG {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PJYHSZDXB
    {
           public PJYHSZDXB(){


           }
           /// <summary>
           /// Desc:099用户名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTC {get;set;}

           /// <summary>
           /// Desc:099参评学院对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CPXYDX {get;set;}

    }
}

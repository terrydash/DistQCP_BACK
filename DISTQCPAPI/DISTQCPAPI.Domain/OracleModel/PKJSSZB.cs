﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PKJSSZB
    {
           public PKJSSZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099一
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ONE {get;set;}

           /// <summary>
           /// Desc:0992
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TWO {get;set;}

           /// <summary>
           /// Desc:0993
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THREE {get;set;}

           /// <summary>
           /// Desc:099起始时间段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FOUR {get;set;}

           /// <summary>
           /// Desc:099学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XS {get;set;}

           /// <summary>
           /// Desc:099容量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SF_XYCZXQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SF_PKQBTZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PK_KSSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PK_JSSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KPKXY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AKKXYP {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SF_SYBMXSXY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXYZDPK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYZDPK {get;set;}

           /// <summary>
           /// Desc:099教务处落实的任务与学院落实的任务是否分开排课
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWC_XYFKPK {get;set;}

           /// <summary>
           /// Desc:099是否正在自动排课标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZZZDPKBJ {get;set;}

           /// <summary>
           /// Desc:Nxx是否正在排课数据初始化
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_PKSJCSH {get;set;}

           /// <summary>
           /// Desc:099排课初始化控制信息开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKKZQSSJ {get;set;}

           /// <summary>
           /// Desc:099排课初始化控制信息开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKKZJSSJ {get;set;}

           /// <summary>
           /// Desc:099排课初始化控制提示信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKKZTSXX {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PKJTSJB
    {
           public PKJTSJB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099第几节
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal DJJ {get;set;}

           /// <summary>
           /// Desc:099上课具体时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XH {get;set;}

           /// <summary>
           /// Desc:099禁止排课校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JZPKXQ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PKKB
    {
           public PKKB(){


           }
           /// <summary>
           /// Desc:序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XH {get;set;}

           /// <summary>
           /// Desc:起始时间段，控制人机交互排课的控件，不能修改
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? QSSJD {get;set;}

           /// <summary>
           /// Desc:课表长度，控制人机交互排课的控件显示，根据学校要求设定
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KBCD {get;set;}

           /// <summary>
           /// Desc:第几节，根据学校要求设定
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DJJ {get;set;}

           /// <summary>
           /// Desc:校区，控制跨校区的，相邻两条件记录相同，不能跨校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KH {get;set;}

           /// <summary>
           /// Desc:上下午标识。1：上午，2：下午，3：晚上
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ1 {get;set;}

           /// <summary>
           /// Desc:099课表显示在那个具体序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KBJCXH {get;set;}

    }
}

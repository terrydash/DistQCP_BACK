﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PKSJCTB
    {
           public PKSJCTB(){


           }
           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQJ {get;set;}

           /// <summary>
           /// Desc:099起始时间段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short QSSJD {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSZ {get;set;}

    }
}

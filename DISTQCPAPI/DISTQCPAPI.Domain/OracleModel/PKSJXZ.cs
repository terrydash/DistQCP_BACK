﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PKSJXZ
    {
           public PKSJXZ(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LB {get;set;}

           /// <summary>
           /// Desc:099名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MC {get;set;}

           /// <summary>
           /// Desc:099代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DM {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQJ {get;set;}

           /// <summary>
           /// Desc:099起始时间段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short QSSJD {get;set;}

           /// <summary>
           /// Desc:099上课长度
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short SKCD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal QSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal JSZ {get;set;}

           /// <summary>
           /// Desc:099资源来源名称，节假日等
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYMC {get;set;}

    }
}

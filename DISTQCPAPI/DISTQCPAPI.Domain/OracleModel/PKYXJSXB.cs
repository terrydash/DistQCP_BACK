﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PKYXJSXB
    {
           public PKYXJSXB(){


           }
           /// <summary>
           /// Desc:001 类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LX {get;set;}

           /// <summary>
           /// Desc:002 序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XUH {get;set;}

           /// <summary>
           /// Desc:003 优先级内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXJNR {get;set;}

           /// <summary>
           /// Desc:003 优先级顺序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YXJSX {get;set;}

           /// <summary>
           /// Desc:004 是否有效
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYX {get;set;}

    }
}

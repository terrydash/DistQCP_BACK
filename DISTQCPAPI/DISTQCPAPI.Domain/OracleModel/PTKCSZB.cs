﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PTKCSZB
    {
           public PTKCSZB(){


           }
           /// <summary>
           /// Desc:001学科类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKLB {get;set;}

           /// <summary>
           /// Desc:002开课学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short KKXQ {get;set;}

           /// <summary>
           /// Desc:003课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:004课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:007考核方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHFS {get;set;}

           /// <summary>
           /// Desc:008周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:009学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099学期长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQCD {get;set;}

           /// <summary>
           /// Desc:099分组标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZBS {get;set;}

           /// <summary>
           /// Desc:099学分要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? XFYQ {get;set;}

    }
}

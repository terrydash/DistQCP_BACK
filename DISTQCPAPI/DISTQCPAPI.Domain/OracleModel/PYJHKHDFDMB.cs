﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PYJHKHDFDMB
    {
           public PYJHKHDFDMB(){


           }
           /// <summary>
           /// Desc:099考核打分代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KHDFDM {get;set;}

           /// <summary>
           /// Desc:099考核打分名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHDFMC {get;set;}

           /// <summary>
           /// Desc:099考核内容代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KHNRDM {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

    }
}

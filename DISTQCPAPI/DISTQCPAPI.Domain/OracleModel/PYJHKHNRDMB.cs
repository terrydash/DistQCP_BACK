﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PYJHKHNRDMB
    {
           public PYJHKHNRDMB(){


           }
           /// <summary>
           /// Desc:099考核内容代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KHNRDM {get;set;}

           /// <summary>
           /// Desc:099考核内容名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHNRMC {get;set;}

           /// <summary>
           /// Desc:099项目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMDM {get;set;}

    }
}

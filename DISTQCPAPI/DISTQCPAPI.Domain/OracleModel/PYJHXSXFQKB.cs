﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class PYJHXSXFQKB
    {
           public PYJHXSXFQKB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMDM {get;set;}

           /// <summary>
           /// Desc:099项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMMC {get;set;}

           /// <summary>
           /// Desc:099考核内容代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KHNRDM {get;set;}

           /// <summary>
           /// Desc:099考核内容名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHNRMC {get;set;}

           /// <summary>
           /// Desc:099考核打分代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KHDFDM {get;set;}

           /// <summary>
           /// Desc:099考核打分名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHDFMC {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099项目类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMLB {get;set;}

           /// <summary>
           /// Desc:099负责单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZDW {get;set;}

           /// <summary>
           /// Desc:099学院团委审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYTWSH {get;set;}

           /// <summary>
           /// Desc:099学院审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSH {get;set;}

           /// <summary>
           /// Desc:099是否提交(0:保存、1:提交)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTJ {get;set;}

    }
}

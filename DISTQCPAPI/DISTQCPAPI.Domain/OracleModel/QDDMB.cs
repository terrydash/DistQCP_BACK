﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class QDDMB
    {
           public QDDMB(){


           }
           /// <summary>
           /// Desc:099区队代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string QDDM {get;set;}

           /// <summary>
           /// Desc:099区队名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QDMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class QTKKSB
    {
           public QTKKSB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099考试时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQJ {get;set;}

           /// <summary>
           /// Desc:099起始时间段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSSJD {get;set;}

           /// <summary>
           /// Desc:099开始周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSZ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099考试座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSZWS {get;set;}

           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099教室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMC {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? RS {get;set;}

           /// <summary>
           /// Desc:099实际人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SJRS {get;set;}

           /// <summary>
           /// Desc:099上课时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ1 {get;set;}

           /// <summary>
           /// Desc:099上课时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ2 {get;set;}

           /// <summary>
           /// Desc:099教室个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSGS {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099考核方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHFS {get;set;}

           /// <summary>
           /// Desc:099教学场地
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXCD {get;set;}

           /// <summary>
           /// Desc:099课程人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? KCRS {get;set;}

           /// <summary>
           /// Desc:099总课程人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZKCRS {get;set;}

           /// <summary>
           /// Desc:099开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:099监考时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKJS1 {get;set;}

           /// <summary>
           /// Desc:099监考时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKJS2 {get;set;}

           /// <summary>
           /// Desc:099监考时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKJS3 {get;set;}

           /// <summary>
           /// Desc:099监考时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKJS4 {get;set;}

           /// <summary>
           /// Desc:099乘车地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCDD {get;set;}

           /// <summary>
           /// Desc:099监考
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JK {get;set;}

           /// <summary>
           /// Desc:099乘车地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCDD1 {get;set;}

           /// <summary>
           /// Desc:099乘车地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCDD2 {get;set;}

           /// <summary>
           /// Desc:099乘车地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCDD3 {get;set;}

           /// <summary>
           /// Desc:099乘车地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCDD4 {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH1 {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH2 {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH3 {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQYQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXKJSZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXKJSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXKJSZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXKJSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFFH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CDAPBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSH1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? HBBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSQJDD {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class QTSYSJD
    {
           public QTSYSJD(){


           }
           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short QSSJD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKCD {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NFSY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTSJ {get;set;}

           /// <summary>
           /// Desc:判断两场考试是否相邻
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXL {get;set;}

    }
}

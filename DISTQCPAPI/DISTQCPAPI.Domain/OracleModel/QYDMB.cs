﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class QYDMB
    {
           public QYDMB(){


           }
           /// <summary>
           /// Desc:001企业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string QYDM {get;set;}

           /// <summary>
           /// Desc:002企业名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string QYMC {get;set;}

           /// <summary>
           /// Desc:003企业地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYDZ {get;set;}

           /// <summary>
           /// Desc:004企业联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYLXDH {get;set;}

           /// <summary>
           /// Desc:005企业联系人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYLXR {get;set;}

           /// <summary>
           /// Desc:006企业合作学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYHZXY {get;set;}

           /// <summary>
           /// Desc:007企业合作系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYHZX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYHZZY {get;set;}

           /// <summary>
           /// Desc:008联系教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:009联系教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:009备注信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZXX {get;set;}

    }
}

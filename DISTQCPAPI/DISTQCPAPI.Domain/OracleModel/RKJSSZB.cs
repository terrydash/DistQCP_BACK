﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class RKJSSZB
    {
           public RKJSSZB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099教学班
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXB {get;set;}

           /// <summary>
           /// Desc:099职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

    }
}

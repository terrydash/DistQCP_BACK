﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class RQKSZB
    {
           public RQKSZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short N {get;set;}

           /// <summary>
           /// Desc:099年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short Y {get;set;}

           /// <summary>
           /// Desc:099日
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short R {get;set;}

           /// <summary>
           /// Desc:099第几周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short DJZ {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQJ {get;set;}

           /// <summary>
           /// Desc:099年月日
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NYR {get;set;}

           /// <summary>
           /// Desc:099是否第一次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDYC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZC1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DJZ1 {get;set;}

           /// <summary>
           /// Desc:099节假名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JJMC {get;set;}

    }
}

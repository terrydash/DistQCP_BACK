﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class RSXSDMB
    {
           public RSXSDMB(){


           }
           /// <summary>
           /// Desc:系数类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XSLX {get;set;}

           /// <summary>
           /// Desc:人数区间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RSQJ {get;set;}

           /// <summary>
           /// Desc:系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS {get;set;}

           /// <summary>
           /// Desc:上限系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXXS {get;set;}

           /// <summary>
           /// Desc:下限系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXXS {get;set;}

           /// <summary>
           /// Desc:系数增减量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSZJL {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

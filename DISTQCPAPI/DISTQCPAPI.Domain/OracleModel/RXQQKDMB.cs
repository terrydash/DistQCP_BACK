﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class RXQQKDMB
    {
           public RXQQKDMB(){


           }
           /// <summary>
           /// Desc:099入学前情况代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RXQQKDM {get;set;}

           /// <summary>
           /// Desc:099入学前情况名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RXQQKMC {get;set;}

    }
}

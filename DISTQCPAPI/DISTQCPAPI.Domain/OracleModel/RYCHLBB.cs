﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class RYCHLBB
    {
           public RYCHLBB(){


           }
           /// <summary>
           /// Desc:099荣誉称号类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RYCHLBDM {get;set;}

           /// <summary>
           /// Desc:099荣誉称号类别名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RYCHLBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JB {get;set;}

    }
}

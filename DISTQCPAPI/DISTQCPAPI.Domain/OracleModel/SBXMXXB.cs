﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SBXMXXB
    {
           public SBXMXXB(){


           }
           /// <summary>
           /// Desc:申报项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMDM {get;set;}

           /// <summary>
           /// Desc:申报项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMMC {get;set;}

           /// <summary>
           /// Desc:是否可申报
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKSB {get;set;}

    }
}

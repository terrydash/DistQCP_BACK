﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SFBZDLB
    {
           public SFBZDLB(){


           }
           /// <summary>
           /// Desc:099大类代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DLDM {get;set;}

           /// <summary>
           /// Desc:099导论名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLMC {get;set;}

           /// <summary>
           /// Desc:099专业注册费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZYZCF {get;set;}

    }
}

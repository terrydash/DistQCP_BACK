﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SFJGHHB
    {
           public SFJGHHB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099是否注册
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFYF {get;set;}

           /// <summary>
           /// Desc:099是否贷款或者缓交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDKHJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SFQKTJB
    {
           public SFQKTJB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD6 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD7 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD8 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD9 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD10 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD11 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD12 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD13 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD14 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD15 {get;set;}

           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD16 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD17 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD18 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD19 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD20 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD21 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD22 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD23 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD24 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD25 {get;set;}

           /// <summary>
           /// Desc:099学生类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSLB {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD26 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD27 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD28 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD29 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD30 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD31 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD32 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD33 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD34 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD35 {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD36 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD37 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD38 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD39 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD40 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD41 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD42 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD43 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD44 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD45 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD1 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD46 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD47 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD48 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD49 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD50 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD51 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD52 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD53 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD54 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD55 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD2 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD56 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD57 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD58 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD59 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD60 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD61 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD62 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD63 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD64 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD65 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD3 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD66 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD67 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD68 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD69 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD70 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD71 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD72 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD73 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD74 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD75 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD4 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD76 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD77 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD78 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD79 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD80 {get;set;}

           /// <summary>
           /// Desc:099字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZD5 {get;set;}

    }
}

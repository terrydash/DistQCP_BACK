﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SFXMDMB
    {
           public SFXMDMB(){


           }
           /// <summary>
           /// Desc:099收费项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SFXMDM {get;set;}

           /// <summary>
           /// Desc:099收费项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXMMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSBZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYXJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXMBM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFRMEZY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYBBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZSF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJCF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYJXM {get;set;}

           /// <summary>
           /// Desc:099是否选课费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXKF {get;set;}

           /// <summary>
           /// Desc:099欠费是否统计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QFSFTJ {get;set;}

           /// <summary>
           /// Desc:099计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLDW {get;set;}

           /// <summary>
           /// Desc:是否打印
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDY {get;set;}

           /// <summary>
           /// Desc:收费标准
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBZ {get;set;}

           /// <summary>
           /// Desc:是否允许前台修改
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYXQTXG {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SFZJDMB
    {
           public SFZJDMB(){


           }
           /// <summary>
           /// Desc:099身份证件代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SFZJDM {get;set;}

           /// <summary>
           /// Desc:099身份证件名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SFZJMC {get;set;}

    }
}

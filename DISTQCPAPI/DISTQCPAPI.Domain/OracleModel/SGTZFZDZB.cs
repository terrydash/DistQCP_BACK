﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SGTZFZDZB
    {
           public SGTZFZDZB(){


           }
           /// <summary>
           /// Desc:099性别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:099身高段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SG {get;set;}

           /// <summary>
           /// Desc:099序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XH {get;set;}

           /// <summary>
           /// Desc:099体重段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TZ {get;set;}

           /// <summary>
           /// Desc:099分值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NJ {get;set;}

    }
}

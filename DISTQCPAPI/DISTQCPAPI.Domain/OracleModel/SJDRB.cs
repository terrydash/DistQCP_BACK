﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SJDRB
    {
           public SJDRB(){


           }
           /// <summary>
           /// Desc:099表序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BXH {get;set;}

           /// <summary>
           /// Desc:099表名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BMC {get;set;}

           /// <summary>
           /// Desc:099表中文名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BZWMC {get;set;}

    }
}

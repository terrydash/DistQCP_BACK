﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SJDRZDB
    {
           public SJDRZDB(){


           }
           /// <summary>
           /// Desc:099表序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BXH {get;set;}

           /// <summary>
           /// Desc:099字段序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZDXH {get;set;}

           /// <summary>
           /// Desc:099字段名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZDMC {get;set;}

           /// <summary>
           /// Desc:099字段中文名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZDZWMC {get;set;}

    }
}

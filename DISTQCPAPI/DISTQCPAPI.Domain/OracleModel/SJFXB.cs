﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SJFXB
    {
           public SJFXB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ID {get;set;}

           /// <summary>
           /// Desc:分析情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXQK {get;set;}

           /// <summary>
           /// Desc:改进措施
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GJCS {get;set;}

           /// <summary>
           /// Desc:考试形式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TXSJ {get;set;}

    }
}

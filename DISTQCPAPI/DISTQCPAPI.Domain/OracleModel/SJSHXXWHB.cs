﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SJSHXXWHB
    {
           public SJSHXXWHB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099毕业所需要学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSXXF {get;set;}

           /// <summary>
           /// Desc:099毕业允许未修学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYYXWXXF {get;set;}

           /// <summary>
           /// Desc:099毕业所需要门数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSXMS {get;set;}

           /// <summary>
           /// Desc:099毕业允许未修门数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYYXWXMS {get;set;}

    }
}

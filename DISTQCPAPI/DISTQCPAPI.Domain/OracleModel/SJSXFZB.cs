﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SJSXFZB
    {
           public SJSXFZB(){


           }
           /// <summary>
           /// Desc:099实践实习编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJSXBH {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099题目类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TMLX {get;set;}

           /// <summary>
           /// Desc:099进行方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXFS {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? ZS {get;set;}

           /// <summary>
           /// Desc:099课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:099实习基地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJD {get;set;}

           /// <summary>
           /// Desc:099指导教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJS {get;set;}

    }
}

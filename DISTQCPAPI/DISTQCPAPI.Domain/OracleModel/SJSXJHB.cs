﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SJSXJHB
    {
           public SJSXJHB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099实习金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJE {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099产品1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CP1 {get;set;}

           /// <summary>
           /// Desc:099数量1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SL1 {get;set;}

           /// <summary>
           /// Desc:099产品2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CP2 {get;set;}

           /// <summary>
           /// Desc:099数量2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SL2 {get;set;}

           /// <summary>
           /// Desc:099产品3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CP3 {get;set;}

           /// <summary>
           /// Desc:099数量3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SL3 {get;set;}

           /// <summary>
           /// Desc:099产品4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CP4 {get;set;}

           /// <summary>
           /// Desc:099数量4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SL4 {get;set;}

           /// <summary>
           /// Desc:099产品5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CP5 {get;set;}

           /// <summary>
           /// Desc:099数量5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SL5 {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SJSXXSAPB
    {
           public SJSXXSAPB(){


           }
           /// <summary>
           /// Desc:099实践实习编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJSXBH {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099实习基地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJD {get;set;}

           /// <summary>
           /// Desc:099实习内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXNR {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SJSYSJKZB
    {
           public SJSYSJKZB(){


           }
           /// <summary>
           /// Desc:N099序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XH {get;set;}

           /// <summary>
           /// Desc:N099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:N099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:N099说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SM {get;set;}

           /// <summary>
           /// Desc:N099是否安排
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFAP {get;set;}

    }
}

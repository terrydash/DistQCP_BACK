﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SJXMDMB
    {
           public SJXMDMB(){


           }
           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJXMDM {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXMMC {get;set;}

           /// <summary>
           /// Desc:099????????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXMYWMC {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXF {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXXMDM {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZXDM {get;set;}

           /// <summary>
           /// Desc:099所属课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSKCDM {get;set;}

           /// <summary>
           /// Desc:099实践项目性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXMXZ {get;set;}

           /// <summary>
           /// Desc:099实践项目类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXMLB {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS11 {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SJXMFZB
    {
           public SJXMFZB(){


           }
           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJXMDM {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXMMC {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? RS {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZH {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKZH {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZS {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSJSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SJYSB
    {
           public SJYSB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:单位bm
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DW {get;set;}

           /// <summary>
           /// Desc:教研室ks
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYS {get;set;}

           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:考试/考查:A/B卷
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSKC {get;set;}

           /// <summary>
           /// Desc:机动
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JD {get;set;}

           /// <summary>
           /// Desc:考试人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KSRS {get;set;}

           /// <summary>
           /// Desc:考试日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSRQ {get;set;}

           /// <summary>
           /// Desc:送卷人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJR {get;set;}

           /// <summary>
           /// Desc:送卷日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJRQ {get;set;}

           /// <summary>
           /// Desc:领卷人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJR {get;set;}

           /// <summary>
           /// Desc:领卷日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJRQ {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:印刷厂审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSCSHR {get;set;}

           /// <summary>
           /// Desc:印刷厂审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSCSHSJ {get;set;}

           /// <summary>
           /// Desc:教务处审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHR {get;set;}

           /// <summary>
           /// Desc:教务处审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJNX {get;set;}

           /// <summary>
           /// Desc:序列号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ID {get;set;}

    }
}

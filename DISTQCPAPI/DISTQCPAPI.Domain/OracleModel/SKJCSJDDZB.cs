﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SKJCSJDDZB
    {
           public SKJCSJDDZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099第几节
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJJ {get;set;}

           /// <summary>
           /// Desc:099起始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSSJ {get;set;}

           /// <summary>
           /// Desc:099结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

    }
}

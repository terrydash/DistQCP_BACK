﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SMBZB
    {
           public SMBZB(){


           }
           /// <summary>
           /// Desc:说明备注名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KEY {get;set;}

           /// <summary>
           /// Desc:说明备注内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VALUE {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SQSHSJB
    {
           public SQSHSJB(){


           }
           /// <summary>
           /// Desc:申请开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SQKSSJ {get;set;}

           /// <summary>
           /// Desc:申请结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SQJSSJ {get;set;}

           /// <summary>
           /// Desc:审核开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SHKSSJ {get;set;}

           /// <summary>
           /// Desc:审核结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SHJSSJ {get;set;}

    }
}

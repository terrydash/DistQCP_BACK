﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SRTPXMB
    {
           public SRTPXMB(){


           }
           /// <summary>
           /// Desc:099项目编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMBH {get;set;}

           /// <summary>
           /// Desc:099项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMMC {get;set;}

           /// <summary>
           /// Desc:099联系人类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXRLB {get;set;}

           /// <summary>
           /// Desc:099研究形式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJXS {get;set;}

           /// <summary>
           /// Desc:099答辩成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBCJ {get;set;}

           /// <summary>
           /// Desc:099表中文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CGFB {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

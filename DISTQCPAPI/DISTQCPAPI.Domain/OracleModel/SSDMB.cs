﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SSDMB
    {
           public SSDMB(){


           }
           /// <summary>
           /// Desc:099楼号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LH {get;set;}

           /// <summary>
           /// Desc:099宿舍号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SSH {get;set;}

           /// <summary>
           /// Desc:099宿舍描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSMS {get;set;}

           /// <summary>
           /// Desc:099收费标准
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFBZ {get;set;}

    }
}

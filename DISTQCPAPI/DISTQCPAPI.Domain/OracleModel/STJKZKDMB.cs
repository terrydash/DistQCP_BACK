﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class STJKZKDMB
    {
           public STJKZKDMB(){


           }
           /// <summary>
           /// Desc:099身体健康状况代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string STJKZKDM {get;set;}

           /// <summary>
           /// Desc:099身体健康状况名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string STJKZKMC {get;set;}

    }
}

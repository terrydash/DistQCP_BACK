﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXDWDMB
    {
           public SXDWDMB(){


           }
           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXDWDM {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXDWMC {get;set;}

           /// <summary>
           /// Desc:099????????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXDWYWMC {get;set;}

           /// <summary>
           /// Desc:099???????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLLXSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LB {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXDWFLDMB
    {
           public SXDWFLDMB(){


           }
           /// <summary>
           /// Desc:099实习单位分类代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXDWFLDM {get;set;}

           /// <summary>
           /// Desc:099实习单位分类名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXDWFLMC {get;set;}

    }
}

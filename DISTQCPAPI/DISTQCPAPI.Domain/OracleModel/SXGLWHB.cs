﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXGLWHB
    {
           public SXGLWHB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099实习类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXLB {get;set;}

           /// <summary>
           /// Desc:099实习单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXDW {get;set;}

    }
}

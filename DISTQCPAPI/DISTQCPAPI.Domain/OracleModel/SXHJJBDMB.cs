﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXHJJBDMB
    {
           public SXHJJBDMB(){


           }
           /// <summary>
           /// Desc:099实习获奖级别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXHJJBDM {get;set;}

           /// <summary>
           /// Desc:099实习获奖级别名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXHJJBMC {get;set;}

    }
}

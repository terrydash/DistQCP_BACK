﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXJDDMB
    {
           public SXJDDMB(){


           }
           /// <summary>
           /// Desc:099实习基地代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXJDDM {get;set;}

           /// <summary>
           /// Desc:099实习基地名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXJDMC {get;set;}

           /// <summary>
           /// Desc:099实习基地邮编
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJDYB {get;set;}

           /// <summary>
           /// Desc:099实习基地地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJDDZ {get;set;}

           /// <summary>
           /// Desc:099联系人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXR {get;set;}

           /// <summary>
           /// Desc:099联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXDH {get;set;}

    }
}

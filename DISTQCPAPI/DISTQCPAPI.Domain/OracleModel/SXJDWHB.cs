﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXJDWHB
    {
           public SXJDWHB(){


           }
           /// <summary>
           /// Desc:099实习基地名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXJDMC {get;set;}

           /// <summary>
           /// Desc:099实习基地性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJDXZ {get;set;}

           /// <summary>
           /// Desc:099实习基地类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJDLB {get;set;}

           /// <summary>
           /// Desc:099适用学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXY {get;set;}

           /// <summary>
           /// Desc:099适用专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZY {get;set;}

           /// <summary>
           /// Desc:099所在地区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZDQ {get;set;}

           /// <summary>
           /// Desc:099通讯地质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TXDZ {get;set;}

           /// <summary>
           /// Desc:099负责人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZR {get;set;}

           /// <summary>
           /// Desc:099联系方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXFS {get;set;}

           /// <summary>
           /// Desc:099启用时间年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYSJN {get;set;}

           /// <summary>
           /// Desc:099启用时间月
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYSJY {get;set;}

           /// <summary>
           /// Desc:099签约时间年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYUESJN {get;set;}

           /// <summary>
           /// Desc:099签约时间月
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QYUESJY {get;set;}

           /// <summary>
           /// Desc:099创建单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDW {get;set;}

    }
}

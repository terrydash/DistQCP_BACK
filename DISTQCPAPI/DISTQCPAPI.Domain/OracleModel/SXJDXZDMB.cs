﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXJDXZDMB
    {
           public SXJDXZDMB(){


           }
           /// <summary>
           /// Desc:099基地性质代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JDXZDM {get;set;}

           /// <summary>
           /// Desc:099基地性质名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDXZMC {get;set;}

    }
}

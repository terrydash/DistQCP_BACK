﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXJHBB
    {
           public SXJHBB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099教师数目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSM {get;set;}

           /// <summary>
           /// Desc:099实习分类
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXFL {get;set;}

           /// <summary>
           /// Desc:099实习内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXNR {get;set;}

           /// <summary>
           /// Desc:099周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZS {get;set;}

           /// <summary>
           /// Desc:099实习起止时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXQZSJ {get;set;}

           /// <summary>
           /// Desc:099实习形式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXXS {get;set;}

           /// <summary>
           /// Desc:099实习基地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJD {get;set;}

           /// <summary>
           /// Desc:099实习参观单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXCGDW {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:099何时与实习基地建立关系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLGX {get;set;}

           /// <summary>
           /// Desc:099是否有协议
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYXY {get;set;}

           /// <summary>
           /// Desc:099实习共几次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJC {get;set;}

           /// <summary>
           /// Desc:099第几次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJC {get;set;}

           /// <summary>
           /// Desc:099专业方向
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYFX {get;set;}

    }
}

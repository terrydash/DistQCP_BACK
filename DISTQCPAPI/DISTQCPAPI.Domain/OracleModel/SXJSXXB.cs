﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXJSXXB
    {
           public SXJSXXB(){


           }
           /// <summary>
           /// Desc:N009职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:N009姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:N009教学工作
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXGZ {get;set;}

           /// <summary>
           /// Desc:N009专长
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXJXQKB
    {
           public SXJXQKB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:099实习类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXLX {get;set;}

           /// <summary>
           /// Desc:099实习个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SXGS {get;set;}

           /// <summary>
           /// Desc:099实习学时数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SXXSS {get;set;}

           /// <summary>
           /// Desc:099实习人时数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BDRSS {get;set;}

           /// <summary>
           /// Desc:099本地实习数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BDSXS {get;set;}

           /// <summary>
           /// Desc:099本地实习人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BDSXRS {get;set;}

           /// <summary>
           /// Desc:099人均实习经费（本地）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? RJSXJFBD {get;set;}

           /// <summary>
           /// Desc:099外地实习数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? WDSXS {get;set;}

           /// <summary>
           /// Desc:099外地实习人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? WDSXRS {get;set;}

           /// <summary>
           /// Desc:099人均实习经费（外地）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? RJSXJFWD {get;set;}

           /// <summary>
           /// Desc:099实习总人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SXZRS {get;set;}

           /// <summary>
           /// Desc:099总实习费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? ZSXFY {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXLBDMB
    {
           public SXLBDMB(){


           }
           /// <summary>
           /// Desc:099实习类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXLBDM {get;set;}

           /// <summary>
           /// Desc:099实习类别名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXLBMC {get;set;}

    }
}

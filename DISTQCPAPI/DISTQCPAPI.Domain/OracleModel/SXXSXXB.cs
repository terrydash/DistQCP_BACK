﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXXSXXB
    {
           public SXXSXXB(){


           }
           /// <summary>
           /// Desc:N009学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:N009姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:N009见习周次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXZS {get;set;}

           /// <summary>
           /// Desc:N009见习成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXCJ {get;set;}

           /// <summary>
           /// Desc:N009实习方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXFS {get;set;}

           /// <summary>
           /// Desc:N009实习性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXXZ {get;set;}

    }
}

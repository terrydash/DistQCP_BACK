﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SXXZDMB
    {
           public SXXZDMB(){


           }
           /// <summary>
           /// Desc:001实习性质代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXXZDM {get;set;}

           /// <summary>
           /// Desc:002实习性质名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXXZMC {get;set;}

    }
}

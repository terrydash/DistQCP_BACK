﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYHCB
    {
           public SYHCB(){


           }
           /// <summary>
           /// Desc:099耗材代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string HCDM {get;set;}

           /// <summary>
           /// Desc:099耗材名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCMC {get;set;}

           /// <summary>
           /// Desc:099耗材系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCXS {get;set;}

           /// <summary>
           /// Desc:099规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GG {get;set;}

           /// <summary>
           /// Desc:099型号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099品牌
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PP {get;set;}

    }
}

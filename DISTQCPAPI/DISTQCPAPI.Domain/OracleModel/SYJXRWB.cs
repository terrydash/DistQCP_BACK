﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYJXRWB
    {
           public SYJXRWB(){


           }
           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMDM {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMMC {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYMKDM {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYMKMC {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXF {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? RS {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKSYZX {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSZ {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSJSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDDH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYXJC {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQYQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CDBS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKDD {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZDX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? TKBS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSZWS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSDD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? RL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YXRS {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKZT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKCD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSSJD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXXMDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRSZ {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZSYZ {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFFZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDZS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099教学班名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXBMC {get;set;}

    }
}

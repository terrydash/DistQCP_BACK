﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYKKZXB
    {
           public SYKKZXB(){


           }
           /// <summary>
           /// Desc:099实验中心代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYZXDM {get;set;}

           /// <summary>
           /// Desc:099实验中心
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZX {get;set;}

           /// <summary>
           /// Desc:099实验中心英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZXYWMC {get;set;}

           /// <summary>
           /// Desc:099校区代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQDM {get;set;}

           /// <summary>
           /// Desc:099所属学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXYDM {get;set;}

    }
}

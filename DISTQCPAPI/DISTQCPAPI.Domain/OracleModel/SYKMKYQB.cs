﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYKMKYQB
    {
           public SYKMKYQB(){


           }
           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099实验模块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYMKDM {get;set;}

           /// <summary>
           /// Desc:099实验模块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYMKMC {get;set;}

           /// <summary>
           /// Desc:099门数要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MSYQ {get;set;}

           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099学分要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XFYQ {get;set;}

    }
}

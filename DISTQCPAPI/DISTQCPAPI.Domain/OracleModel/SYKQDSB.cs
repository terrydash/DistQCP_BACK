﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYKQDSB
    {
           public SYKQDSB(){


           }
           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099区队数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QDS {get;set;}

    }
}

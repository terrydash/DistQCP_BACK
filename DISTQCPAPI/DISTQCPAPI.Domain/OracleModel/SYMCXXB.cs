﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYMCXXB
    {
           public SYMCXXB(){


           }
           /// <summary>
           /// Desc:实验代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYDM {get;set;}

           /// <summary>
           /// Desc:实验名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYMC {get;set;}

           /// <summary>
           /// Desc:实验类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYLB {get;set;}

    }
}

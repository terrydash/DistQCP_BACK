﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYMKDMB
    {
           public SYMKDMB(){


           }
           /// <summary>
           /// Desc:099实验模块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYMKDM {get;set;}

           /// <summary>
           /// Desc:099实验模块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYMKMC {get;set;}

           /// <summary>
           /// Desc:099实验中心代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZXDM {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYPHXSSZB
    {
           public SYPHXSSZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099人数段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RSD {get;set;}

           /// <summary>
           /// Desc:099平行系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PHXS {get;set;}

           /// <summary>
           /// Desc:099不平行系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BPHXS {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYRWB
    {
           public SYRWB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:实验学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? SYXS {get;set;}

           /// <summary>
           /// Desc:校区代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQDM {get;set;}

           /// <summary>
           /// Desc:选课人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XKRS {get;set;}

           /// <summary>
           /// Desc:人时数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? RSS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? SJXS {get;set;}

    }
}

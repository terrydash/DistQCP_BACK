﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSCDXXB
    {
           public SYSCDXXB(){


           }
           /// <summary>
           /// Desc:N99实验室编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYSBH {get;set;}

           /// <summary>
           /// Desc:N99实验室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSMC {get;set;}

           /// <summary>
           /// Desc:N99实验中心
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZX {get;set;}

           /// <summary>
           /// Desc:N99座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZWS {get;set;}

           /// <summary>
           /// Desc:N99实验室类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSLB {get;set;}

           /// <summary>
           /// Desc:N99校区代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQDM {get;set;}

           /// <summary>
           /// Desc:N99使用班级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYBJ {get;set;}

           /// <summary>
           /// Desc:N99使用部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYBM {get;set;}

           /// <summary>
           /// Desc:N99使用专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZY {get;set;}

           /// <summary>
           /// Desc:N99楼号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LH {get;set;}

           /// <summary>
           /// Desc:N99层号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CH {get;set;}

           /// <summary>
           /// Desc:N99房号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FH {get;set;}

           /// <summary>
           /// Desc:N99备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099建立年份
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLNF {get;set;}

           /// <summary>
           /// Desc:099使用面积
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYMJ {get;set;}

           /// <summary>
           /// Desc:099管理级别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GLJB {get;set;}

           /// <summary>
           /// Desc:099实验室支出
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSZC {get;set;}

           /// <summary>
           /// Desc:099实验室投入
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSTR {get;set;}

           /// <summary>
           /// Desc:099仪器设备额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQSBE {get;set;}

           /// <summary>
           /// Desc:099其他设备额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QTSBE {get;set;}

           /// <summary>
           /// Desc:099人员总数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RYZS {get;set;}

           /// <summary>
           /// Desc:099科研项目数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KYXMS {get;set;}

           /// <summary>
           /// Desc:099获奖等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJJB {get;set;}

           /// <summary>
           /// Desc:099实验室主任
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSZR {get;set;}

           /// <summary>
           /// Desc:099开放类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KFLX {get;set;}

           /// <summary>
           /// Desc:099开放时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KFSJ {get;set;}

           /// <summary>
           /// Desc:099开放对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KFDX {get;set;}

           /// <summary>
           /// Desc:099指导教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099指导教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099开放目的和范围
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MDFW {get;set;}

           /// <summary>
           /// Desc:099实验室主任工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSZRZGH {get;set;}

           /// <summary>
           /// Desc:099审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYJSBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYSYXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SBTS {get;set;}

           /// <summary>
           /// Desc:099实验科室代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYKSDM {get;set;}

           /// <summary>
           /// Desc:099实验科室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYKSMC {get;set;}

    }
}

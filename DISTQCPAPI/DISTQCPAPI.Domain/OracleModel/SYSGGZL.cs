﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSGGZL
    {
           public SYSGGZL(){


           }
           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYS {get;set;}

           /// <summary>
           /// Desc:099G1??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string G1XS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZXS {get;set;}

           /// <summary>
           /// Desc:099G2??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string G2XS {get;set;}

           /// <summary>
           /// Desc:099G21??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string G21XS {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SBLYL {get;set;}

           /// <summary>
           /// Desc:099????????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WJSZDZTS {get;set;}

           /// <summary>
           /// Desc:099G3??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string G3XS {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WJWHL {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSJGZL
    {
           public SYSJGZL(){


           }
           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYS {get;set;}

           /// <summary>
           /// Desc:099????????????/??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSJSGZLXS {get;set;}

           /// <summary>
           /// Desc:099??????????(??)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXKYPTJSJF {get;set;}

           /// <summary>
           /// Desc:099?????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NXS {get;set;}

           /// <summary>
           /// Desc:099J2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string J2 {get;set;}

           /// <summary>
           /// Desc:099J3??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string J3XS {get;set;}

           /// <summary>
           /// Desc:099J4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string J4 {get;set;}

           /// <summary>
           /// Desc:099J5????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string J5BXZS {get;set;}

           /// <summary>
           /// Desc:099J5??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string J5XS {get;set;}

           /// <summary>
           /// Desc:099J6????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string J6BXZS {get;set;}

           /// <summary>
           /// Desc:099J6??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string J6XS {get;set;}

    }
}

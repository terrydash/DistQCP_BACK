﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSJRSB
    {
           public SYSJRSB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程中文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCZWMC {get;set;}

           /// <summary>
           /// Desc:099实验室代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SYSDM {get;set;}

           /// <summary>
           /// Desc:099实验室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSMC {get;set;}

           /// <summary>
           /// Desc:099可用人数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short KYRS {get;set;}

           /// <summary>
           /// Desc:099实验容量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYRL {get;set;}

           /// <summary>
           /// Desc:099最多实验室个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSGS {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSJXPXB
    {
           public SYSJXPXB(){


           }
           /// <summary>
           /// Desc:N99序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XH {get;set;}

           /// <summary>
           /// Desc:N99编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BH {get;set;}

           /// <summary>
           /// Desc:N99姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:N99类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LX {get;set;}

           /// <summary>
           /// Desc:N99开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:N99结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:N99地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DD {get;set;}

           /// <summary>
           /// Desc:N99经费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JF {get;set;}

           /// <summary>
           /// Desc:N99经费来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFLY {get;set;}

           /// <summary>
           /// Desc:N99经费用途
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFYT {get;set;}

           /// <summary>
           /// Desc:N99内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NR {get;set;}

           /// <summary>
           /// Desc:N99目标
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MB {get;set;}

           /// <summary>
           /// Desc:N99备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

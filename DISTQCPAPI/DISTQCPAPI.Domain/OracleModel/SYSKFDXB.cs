﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSKFDXB
    {
           public SYSKFDXB(){


           }
           /// <summary>
           /// Desc:099开放对象代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KFDXDM {get;set;}

           /// <summary>
           /// Desc:099开放对象名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KFDXMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSKFLXB
    {
           public SYSKFLXB(){


           }
           /// <summary>
           /// Desc:099开放类型代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KFLXDM {get;set;}

           /// <summary>
           /// Desc:099开放类型名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KFLXMC {get;set;}

    }
}

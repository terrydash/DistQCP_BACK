﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSKFSJB
    {
           public SYSKFSJB(){


           }
           /// <summary>
           /// Desc:099开放时间代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KFSJDM {get;set;}

           /// <summary>
           /// Desc:099开放时间名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KFSJMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSRYB
    {
           public SYSRYB(){


           }
           /// <summary>
           /// Desc:N99编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BH {get;set;}

           /// <summary>
           /// Desc:N99姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:N99性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:N99出生日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSRQ {get;set;}

           /// <summary>
           /// Desc:N99部门(实验中心)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BM {get;set;}

           /// <summary>
           /// Desc:N99文化程度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WHCD {get;set;}

           /// <summary>
           /// Desc:N99毕业时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJ {get;set;}

           /// <summary>
           /// Desc:N99所学专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXZY {get;set;}

           /// <summary>
           /// Desc:N99实验室工龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SYSGL {get;set;}

           /// <summary>
           /// Desc:N99学历
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XL {get;set;}

           /// <summary>
           /// Desc:N99职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:N99评职时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PZSJ {get;set;}

           /// <summary>
           /// Desc:N99外语语种
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WYYZ {get;set;}

           /// <summary>
           /// Desc:N99外语水平
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WYSP {get;set;}

           /// <summary>
           /// Desc:N99成果奖励
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CGJL {get;set;}

           /// <summary>
           /// Desc:N99论文数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? LWSL {get;set;}

           /// <summary>
           /// Desc:N99论文级别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LWJB {get;set;}

           /// <summary>
           /// Desc:N99著作数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZZSL {get;set;}

           /// <summary>
           /// Desc:N99著作级别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZJB {get;set;}

           /// <summary>
           /// Desc:N99简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JJ {get;set;}

           /// <summary>
           /// Desc:N99岗位名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GWMC {get;set;}

           /// <summary>
           /// Desc:N99岗位职责
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GWZZ {get;set;}

           /// <summary>
           /// Desc:N99工作时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZSJ {get;set;}

           /// <summary>
           /// Desc:N99业务专长
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWZC {get;set;}

           /// <summary>
           /// Desc:N99聘任情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PRQK {get;set;}

           /// <summary>
           /// Desc:N99人员类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RYLB {get;set;}

           /// <summary>
           /// Desc:N99主要工作
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYGZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSLB {get;set;}

           /// <summary>
           /// Desc:消耗品申请审批人标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SPRBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSKS {get;set;}

           /// <summary>
           /// Desc:N99是否专职实验人员
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZZSYRY {get;set;}

           /// <summary>
           /// Desc:N99实验室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSBH {get;set;}

           /// <summary>
           /// Desc:N99实验室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSMC {get;set;}

           /// <summary>
           /// Desc:N99实验室职务
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSZW {get;set;}

           /// <summary>
           /// Desc:N99实验室对应专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSDYZYDM {get;set;}

           /// <summary>
           /// Desc:N99实验室对应专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSDYZYMC {get;set;}

           /// <summary>
           /// Desc:N99所属一级学科编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSYJXKBH {get;set;}

           /// <summary>
           /// Desc:N99所属一级学科名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSYJXKMC {get;set;}

           /// <summary>
           /// Desc:N99所属二级学科编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSEJXKBH {get;set;}

           /// <summary>
           /// Desc:N99所属二级学科名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSEJXKMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSRYJBXXB
    {
           public SYSRYJBXXB(){


           }
           /// <summary>
           /// Desc:011用户名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHM {get;set;}

           /// <summary>
           /// Desc:011姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:011性别及比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:011所在单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZDW {get;set;}

           /// <summary>
           /// Desc:011出生年月
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSNY {get;set;}

           /// <summary>
           /// Desc:011文化程度及比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WHCD {get;set;}

           /// <summary>
           /// Desc:011所学专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXZY {get;set;}

           /// <summary>
           /// Desc:011毕业时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJ {get;set;}

           /// <summary>
           /// Desc:011实验室工龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSGL {get;set;}

           /// <summary>
           /// Desc:011专业职称及比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZC {get;set;}

           /// <summary>
           /// Desc:011评职时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PZSJ {get;set;}

           /// <summary>
           /// Desc:011主要工作
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYGZ {get;set;}

           /// <summary>
           /// Desc:011人员类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RYLB {get;set;}

           /// <summary>
           /// Desc:011论文级别数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LWJB {get;set;}

           /// <summary>
           /// Desc:011获奖情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LWSL {get;set;}

    }
}

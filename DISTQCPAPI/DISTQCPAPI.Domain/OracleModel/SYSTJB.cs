﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSTJB
    {
           public SYSTJB(){


           }
           /// <summary>
           /// Desc:011学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:011学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:011实验室
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYS {get;set;}

           /// <summary>
           /// Desc:011实验室面积
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSMJ {get;set;}

           /// <summary>
           /// Desc:011生均面积
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJMJ {get;set;}

           /// <summary>
           /// Desc:011设备总量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SBZL {get;set;}

           /// <summary>
           /// Desc:011可用设备数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KYSBS {get;set;}

           /// <summary>
           /// Desc:011完好率
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WHL {get;set;}

           /// <summary>
           /// Desc:011利用率
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYL {get;set;}

           /// <summary>
           /// Desc:011实验设备总数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSBZS {get;set;}

           /// <summary>
           /// Desc:011生均设备值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJSBZ {get;set;}

           /// <summary>
           /// Desc:011开课门数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKMS {get;set;}

           /// <summary>
           /// Desc:011实验项目数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMS {get;set;}

           /// <summary>
           /// Desc:011开出率
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCL {get;set;}

    }
}

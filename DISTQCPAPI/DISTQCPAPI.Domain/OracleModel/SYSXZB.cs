﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSXZB
    {
           public SYSXZB(){


           }
           /// <summary>
           /// Desc:099实验性质代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYXZDM {get;set;}

           /// <summary>
           /// Desc:099实验性质名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXZMC {get;set;}

    }
}

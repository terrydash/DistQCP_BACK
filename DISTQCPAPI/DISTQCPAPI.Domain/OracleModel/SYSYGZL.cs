﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSYGZL
    {
           public SYSYGZL(){


           }
           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:099???
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099???????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYXMDM {get;set;}

           /// <summary>
           /// Desc:099???????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMMC {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHXS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDZS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EDZS {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYLXXS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SJRS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SJSS {get;set;}

           /// <summary>
           /// Desc:099Y2??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Y2XS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Y2JHXS {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJHXS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYRS {get;set;}

           /// <summary>
           /// Desc:099???????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EDSYRS {get;set;}

           /// <summary>
           /// Desc:099???????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYKLXXS {get;set;}

           /// <summary>
           /// Desc:099Y3??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Y3XS {get;set;}

           /// <summary>
           /// Desc:099y4?? 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Y4XS {get;set;}

           /// <summary>
           /// Desc:099???? 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHZS {get;set;}

           /// <summary>
           /// Desc:099???? 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSBJS {get;set;}

           /// <summary>
           /// Desc:099y5?? 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Y5XS {get;set;}

           /// <summary>
           /// Desc:099y5j???? 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Y5JHZS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSRS {get;set;}

           /// <summary>
           /// Desc:099??????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJRS {get;set;}

           /// <summary>
           /// Desc:099????????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJSJSS {get;set;}

           /// <summary>
           /// Desc:099y6??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Y6XS {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CFCS {get;set;}

           /// <summary>
           /// Desc:?????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYLX {get;set;}

           /// <summary>
           /// Desc:????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFWX {get;set;}

           /// <summary>
           /// Desc:099???????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXSYXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYYQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDTT {get;set;}

           /// <summary>
           /// Desc:099项目系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXS {get;set;}

           /// <summary>
           /// Desc:099课程系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXS {get;set;}

           /// <summary>
           /// Desc:099已选人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXRS {get;set;}

           /// <summary>
           /// Desc:099选课班名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKBMC {get;set;}

           /// <summary>
           /// Desc:099实验系数（不平行）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYBPHXS {get;set;}

           /// <summary>
           /// Desc:099实验系数（平行）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYPHXS {get;set;}

    }
}

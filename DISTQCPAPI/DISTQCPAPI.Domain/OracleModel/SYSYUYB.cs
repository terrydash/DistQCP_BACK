﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYSYUYB
    {
           public SYSYUYB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YYBH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099原起止周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string QZZ {get;set;}

           /// <summary>
           /// Desc:099原结束周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZ {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQJ {get;set;}

           /// <summary>
           /// Desc:099时间段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJD {get;set;}

           /// <summary>
           /// Desc:099中文时间段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZWSJD {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:099上课长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKCD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教室类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLB {get;set;}

           /// <summary>
           /// Desc:099参加实验人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJSYRS {get;set;}

           /// <summary>
           /// Desc:099实验室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSMC {get;set;}

           /// <summary>
           /// Desc:099实验室归属学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSGSXY {get;set;}

           /// <summary>
           /// Desc:099项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMMC {get;set;}

           /// <summary>
           /// Desc:099申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:099申请人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQR {get;set;}

           /// <summary>
           /// Desc:099审核标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHBJ {get;set;}

           /// <summary>
           /// Desc:099审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

           /// <summary>
           /// Desc:099审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

    }
}

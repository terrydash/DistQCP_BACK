﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYXMDMB
    {
           public SYXMDMB(){


           }
           /// <summary>
           /// Desc:099实验项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYXMDM {get;set;}

           /// <summary>
           /// Desc:099实验项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMMC {get;set;}

           /// <summary>
           /// Desc:099实验项目英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMYWMC {get;set;}

           /// <summary>
           /// Desc:099实验模块代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYMKDM {get;set;}

           /// <summary>
           /// Desc:099学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS {get;set;}

           /// <summary>
           /// Desc:099先修项目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXXM {get;set;}

           /// <summary>
           /// Desc:099简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JJ {get;set;}

           /// <summary>
           /// Desc:099项目性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZXYZ {get;set;}

           /// <summary>
           /// Desc:099项目学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXF {get;set;}

           /// <summary>
           /// Desc:099实验中心代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZXDM {get;set;}

           /// <summary>
           /// Desc:099项目类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMLB {get;set;}

           /// <summary>
           /// Desc:099项目要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMYQ {get;set;}

           /// <summary>
           /// Desc:099仪器设备名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SBMC {get;set;}

           /// <summary>
           /// Desc:099仪器设备规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SBGG {get;set;}

           /// <summary>
           /// Desc:099仪器设备数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQSL {get;set;}

           /// <summary>
           /// Desc:099耗材名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCMC {get;set;}

           /// <summary>
           /// Desc:099耗材数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCSL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMLX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZRS {get;set;}

           /// <summary>
           /// Desc:099实验室类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSDM {get;set;}

           /// <summary>
           /// Desc:是否独立开课
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDLKK {get;set;}

           /// <summary>
           /// Desc:适用专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZY {get;set;}

           /// <summary>
           /// Desc:目的任务
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MDRW {get;set;}

           /// <summary>
           /// Desc:内容安排
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NRAP {get;set;}

           /// <summary>
           /// Desc:考核要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHYQ {get;set;}

           /// <summary>
           /// Desc:099仪器代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQDM {get;set;}

           /// <summary>
           /// Desc:099耗材代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCDM {get;set;}

           /// <summary>
           /// Desc:099所属专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSZYDM {get;set;}

           /// <summary>
           /// Desc:099仪器系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQXS {get;set;}

           /// <summary>
           /// Desc:099耗材料系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKBJ {get;set;}

           /// <summary>
           /// Desc:099项目成绩比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMCJBL {get;set;}

           /// <summary>
           /// Desc:099项目方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMFS {get;set;}

           /// <summary>
           /// Desc:099起止周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZZ {get;set;}

           /// <summary>
           /// Desc:099台套数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TTS {get;set;}

           /// <summary>
           /// Desc:099每台套学生数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MTTXSS {get;set;}

           /// <summary>
           /// Desc:099每组人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MZRS {get;set;}

           /// <summary>
           /// Desc:099是否实训项目0表示否，1表示是
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSXXM {get;set;}

    }
}

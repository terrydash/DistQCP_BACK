﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYXMFSB
    {
           public SYXMFSB(){


           }
           /// <summary>
           /// Desc:099方式代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FSDM {get;set;}

           /// <summary>
           /// Desc:099方式名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSMC {get;set;}

    }
}

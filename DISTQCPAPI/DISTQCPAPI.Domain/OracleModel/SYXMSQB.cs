﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYXMSQB
    {
           public SYXMSQB(){


           }
           /// <summary>
           /// Desc:099周次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQJ {get;set;}

           /// <summary>
           /// Desc:099年月日
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NYR {get;set;}

           /// <summary>
           /// Desc:099节次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JC {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099实验者对象
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYZDX {get;set;}

           /// <summary>
           /// Desc:099实验者类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZLB {get;set;}

           /// <summary>
           /// Desc:099实验者人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZRS {get;set;}

           /// <summary>
           /// Desc:099分组
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ {get;set;}

           /// <summary>
           /// Desc:099每组人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MZRS {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099实验项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYXMDM {get;set;}

           /// <summary>
           /// Desc:099实验项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMMC {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTJ {get;set;}

           /// <summary>
           /// Desc:原排课节次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCCONST {get;set;}

           /// <summary>
           /// Desc:099实验学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS {get;set;}

           /// <summary>
           /// Desc:099项目性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZXYZ {get;set;}

           /// <summary>
           /// Desc:099项目类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMLB {get;set;}

           /// <summary>
           /// Desc:099项目要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMYQ {get;set;}

           /// <summary>
           /// Desc:099设备名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SBMC {get;set;}

           /// <summary>
           /// Desc:099设备规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SBGG {get;set;}

           /// <summary>
           /// Desc:099仪器设备数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQSL {get;set;}

           /// <summary>
           /// Desc:099耗材名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCMC {get;set;}

           /// <summary>
           /// Desc:099耗材数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCSL {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:实验老师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYJSZGH {get;set;}

           /// <summary>
           /// Desc:实验老师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYJSXM {get;set;}

           /// <summary>
           /// Desc:实验室代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSDM {get;set;}

           /// <summary>
           /// Desc:实验室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSMC {get;set;}

           /// <summary>
           /// Desc:起始节次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSJC {get;set;}

           /// <summary>
           /// Desc:教师申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSTJSJ {get;set;}

           /// <summary>
           /// Desc:099实验室类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSLB {get;set;}

           /// <summary>
           /// Desc:099分组序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal FZXH {get;set;}

           /// <summary>
           /// Desc:099申请时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SBHCID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKFSY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BDYY {get;set;}

           /// <summary>
           /// Desc:099任课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKJSXM {get;set;}

           /// <summary>
           /// Desc:099教务处审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSH {get;set;}

           /// <summary>
           /// Desc:099上课状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKZT {get;set;}

           /// <summary>
           /// Desc:审核备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHBZ {get;set;}

    }
}

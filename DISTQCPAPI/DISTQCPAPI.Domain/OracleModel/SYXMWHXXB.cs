﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYXMWHXXB
    {
           public SYXMWHXXB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099实验项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYXMDM {get;set;}

           /// <summary>
           /// Desc:099实验项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMMC {get;set;}

           /// <summary>
           /// Desc:099项目学生人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMXSRS {get;set;}

           /// <summary>
           /// Desc:099实验项目系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMXS {get;set;}

           /// <summary>
           /// Desc:099项目批次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMPC {get;set;}

           /// <summary>
           /// Desc:099开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:099学科组织
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKX {get;set;}

           /// <summary>
           /// Desc:099课程类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLB {get;set;}

    }
}

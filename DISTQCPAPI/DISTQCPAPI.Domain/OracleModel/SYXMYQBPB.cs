﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYXMYQBPB
    {
           public SYXMYQBPB(){


           }
           /// <summary>
           /// Desc:N99实验项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYXMDM {get;set;}

           /// <summary>
           /// Desc:N99实验项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMMC {get;set;}

           /// <summary>
           /// Desc:N99仪器编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YQBH {get;set;}

           /// <summary>
           /// Desc:N99仪器名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQMC {get;set;}

           /// <summary>
           /// Desc:N99数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SL {get;set;}

    }
}

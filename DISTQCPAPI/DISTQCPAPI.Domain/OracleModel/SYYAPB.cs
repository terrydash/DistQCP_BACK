﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SYYAPB
    {
           public SYYAPB(){


           }
           /// <summary>
           /// Desc:实验选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYXKKH {get;set;}

           /// <summary>
           /// Desc:实验员工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SYYZGH {get;set;}

           /// <summary>
           /// Desc:实验准备学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZBXS {get;set;}

           /// <summary>
           /// Desc:实验组数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZS {get;set;}

           /// <summary>
           /// Desc:实验准备总学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYZHXS {get;set;}

    }
}

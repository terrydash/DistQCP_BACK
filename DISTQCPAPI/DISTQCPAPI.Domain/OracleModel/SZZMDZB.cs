﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SZZMDZB
    {
           public SZZMDZB(){


           }
           /// <summary>
           /// Desc:099数字
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SZ {get;set;}

           /// <summary>
           /// Desc:099字母
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZM {get;set;}

    }
}

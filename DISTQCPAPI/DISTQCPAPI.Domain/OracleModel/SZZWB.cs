﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class SZZWB
    {
           public SZZWB(){


           }
           /// <summary>
           /// Desc:099数字
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short SZ {get;set;}

           /// <summary>
           /// Desc:099职务
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWY {get;set;}

    }
}

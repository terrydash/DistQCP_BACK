﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class THPJDFB
    {
           public THPJDFB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:003教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:006同行对教师评价分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? THPJF {get;set;}

           /// <summary>
           /// Desc:007总评价分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? ZDF {get;set;}

           /// <summary>
           /// Desc:008审核状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHZT {get;set;}

           /// <summary>
           /// Desc:009考核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHR {get;set;}

           /// <summary>
           /// Desc:010考核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHSJ {get;set;}

           /// <summary>
           /// Desc:010审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

           /// <summary>
           /// Desc:011审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

    }
}

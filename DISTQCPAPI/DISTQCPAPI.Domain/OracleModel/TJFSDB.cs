﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TJFSDB
    {
           public TJFSDB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FSD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FBFZFSD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JGF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FBFZJGF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FBFZMF {get;set;}

           /// <summary>
           /// Desc:099百分制良好分数线
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? LHF {get;set;}

           /// <summary>
           /// Desc:099百分制优秀分数线
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? YXF {get;set;}

           /// <summary>
           /// Desc:099非百分制良好分数线
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FBFZLHF {get;set;}

           /// <summary>
           /// Desc:099非百分制优秀分数线
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FBFZYXF {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TJGYSYQCS
    {
           public TJGYSYQCS(){


           }
           /// <summary>
           /// Desc:099教材名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:099单价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:099教材作者
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JCZZ {get;set;}

           /// <summary>
           /// Desc:099版本号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BBH {get;set;}

           /// <summary>
           /// Desc:099出版社
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:099书架号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SJH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GYSCS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQCS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZK {get;set;}

           /// <summary>
           /// Desc:099供应商
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GYS {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KTCS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZSH {get;set;}

    }
}

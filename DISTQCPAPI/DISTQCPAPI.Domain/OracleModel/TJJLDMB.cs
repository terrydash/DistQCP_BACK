﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TJJLDMB
    {
           public TJJLDMB(){


           }
           /// <summary>
           /// Desc:011体检结论代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJJLDM {get;set;}

           /// <summary>
           /// Desc:011体检结论名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJJLMC {get;set;}

    }
}

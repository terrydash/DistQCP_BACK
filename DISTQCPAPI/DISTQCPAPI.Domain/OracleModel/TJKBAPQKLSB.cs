﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TJKBAPQKLSB
    {
           public TJKBAPQKLSB(){


           }
           /// <summary>
           /// Desc:099推荐课表代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJKBDM {get;set;}

           /// <summary>
           /// Desc:099时间段序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SJDXH {get;set;}

           /// <summary>
           /// Desc:099起始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal QSSJ {get;set;}

           /// <summary>
           /// Desc:099结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal JSSJ {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:099课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZGH {get;set;}

    }
}

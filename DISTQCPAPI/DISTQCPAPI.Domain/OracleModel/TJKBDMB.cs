﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TJKBDMB
    {
           public TJKBDMB(){


           }
           /// <summary>
           /// Desc:099推荐课表代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJKBDM {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal XH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099上课内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SKNR {get;set;}

           /// <summary>
           /// Desc:099推荐课表代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJKBMC {get;set;}

           /// <summary>
           /// Desc:099快速选课标志
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSXKBZ {get;set;}

           /// <summary>
           /// Desc:099院系选修课
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXXXK {get;set;}

           /// <summary>
           /// Desc:099平行班标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PXBBS {get;set;}

           /// <summary>
           /// Desc:099合班标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? HBBJ {get;set;}

           /// <summary>
           /// Desc:099累计否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJF {get;set;}

           /// <summary>
           /// Desc:099课表人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KBRS {get;set;}

           /// <summary>
           /// Desc:099起始周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QSZ {get;set;}

           /// <summary>
           /// Desc:099结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSZ {get;set;}

           /// <summary>
           /// Desc:099排课单双周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKDSZ {get;set;}

    }
}

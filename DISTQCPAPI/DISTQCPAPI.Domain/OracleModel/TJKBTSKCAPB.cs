﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TJKBTSKCAPB
    {
           public TJKBTSKCAPB(){


           }
           /// <summary>
           /// Desc:099推荐课表代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJKBDM {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQJ {get;set;}

           /// <summary>
           /// Desc:099时间段序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short SJDXH {get;set;}

           /// <summary>
           /// Desc:099上课长度
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short SKCD {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099起始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short QSSJ {get;set;}

           /// <summary>
           /// Desc:099结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short JSSJ {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZWS {get;set;}

           /// <summary>
           /// Desc:099上课内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SKNR {get;set;}

           /// <summary>
           /// Desc:099起始时间段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short QSSJD {get;set;}

           /// <summary>
           /// Desc:099序号，对于同一个教学班级 同一时间序号是相同的
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKXH {get;set;}

    }
}

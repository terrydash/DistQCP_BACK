﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TJXXB
    {
           public TJXXB(){


           }
           /// <summary>
           /// Desc:099表名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BMC {get;set;}

           /// <summary>
           /// Desc:099字段名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZDMC {get;set;}

           /// <summary>
           /// Desc:099字段中文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDZWMC {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XH {get;set;}

    }
}

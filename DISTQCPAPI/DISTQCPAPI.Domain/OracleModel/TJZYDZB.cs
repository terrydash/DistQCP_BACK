﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TJZYDZB
    {
           public TJZYDZB(){


           }
           /// <summary>
           /// Desc:099现专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XZYDM {get;set;}

           /// <summary>
           /// Desc:099现专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZYMC {get;set;}

           /// <summary>
           /// Desc:099推荐专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJZYDM {get;set;}

           /// <summary>
           /// Desc:099推荐专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJZYMC {get;set;}

           /// <summary>
           /// Desc:099学科
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XK {get;set;}

           /// <summary>
           /// Desc:099是否外语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFWY {get;set;}

           /// <summary>
           /// Desc:099是否师范
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSF {get;set;}

           /// <summary>
           /// Desc:099是否二学位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFEXW {get;set;}

    }
}

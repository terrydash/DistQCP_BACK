﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TPSZB
    {
           public TPSZB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:投票人数限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TPRSXZ {get;set;}

           /// <summary>
           /// Desc:有效起始学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXQSXN {get;set;}

           /// <summary>
           /// Desc:有效起始学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXQSXQ {get;set;}

           /// <summary>
           /// Desc:有效终止学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXZZXN {get;set;}

           /// <summary>
           /// Desc:有效终止学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXZZXQ {get;set;}

           /// <summary>
           /// Desc:结果是否可查 0 不可查 1 可查
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JGSFKC {get;set;}

           /// <summary>
           /// Desc:网上查阅范围 0 全部参选 1 获奖人员
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSCYFW {get;set;}

           /// <summary>
           /// Desc:面向对象 1 学生 2 教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDX {get;set;}

           /// <summary>
           /// Desc:投票开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:投票结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:099评选项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PXXMDM {get;set;}

           /// <summary>
           /// Desc:099评选项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PXXMMC {get;set;}

           /// <summary>
           /// Desc:099评选项目面向对象（进入系统可评对象用,隔开）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PXXMMXDX {get;set;}

           /// <summary>
           /// Desc:099网页申请控制是否可查看pdf文件（是；否）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSCPDF {get;set;}

           /// <summary>
           /// Desc:099网页申请控制是否可查看视频文件（是；否）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSCSP {get;set;}

           /// <summary>
           /// Desc:099网页申请控制可否选投其他人（是；否）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKQTR {get;set;}

    }
}

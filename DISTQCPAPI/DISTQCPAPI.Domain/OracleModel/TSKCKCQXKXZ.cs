﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TSKCKCQXKXZ
    {
           public TSKCKCQXKXZ(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099门次限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKXZ {get;set;}

           /// <summary>
           /// Desc:099学分限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZXF {get;set;}

           /// <summary>
           /// Desc:099课程群名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCQMC {get;set;}

           /// <summary>
           /// Desc:099课程类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCLX {get;set;}

    }
}

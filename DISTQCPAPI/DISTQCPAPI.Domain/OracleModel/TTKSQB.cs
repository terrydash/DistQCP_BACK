﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TTKSQB
    {
           public TTKSQB(){


           }
           /// <summary>
           /// Desc:099变动类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BDLB {get;set;}

           /// <summary>
           /// Desc:099变动编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BDBH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099原教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJSZGH {get;set;}

           /// <summary>
           /// Desc:099原起始周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQSZ {get;set;}

           /// <summary>
           /// Desc:099原结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJSZ {get;set;}

           /// <summary>
           /// Desc:099原星期几
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXQJ {get;set;}

           /// <summary>
           /// Desc:099原时间段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSJD {get;set;}

           /// <summary>
           /// Desc:099原单双周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDSZ {get;set;}

           /// <summary>
           /// Desc:099原上课长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSKCD {get;set;}

           /// <summary>
           /// Desc:099原教室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJSBH {get;set;}

           /// <summary>
           /// Desc:099现教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJSZGH {get;set;}

           /// <summary>
           /// Desc:099现起始周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQSZ {get;set;}

           /// <summary>
           /// Desc:099现结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJSZ {get;set;}

           /// <summary>
           /// Desc:099现星期几
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXQJ {get;set;}

           /// <summary>
           /// Desc:099现时间段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSJD {get;set;}

           /// <summary>
           /// Desc:099现单双周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XDSZ {get;set;}

           /// <summary>
           /// Desc:099现上课长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSKCD {get;set;}

           /// <summary>
           /// Desc:099现教室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJSBH {get;set;}

           /// <summary>
           /// Desc:099允许变教室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSFYXBD {get;set;}

           /// <summary>
           /// Desc:099教室类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLB {get;set;}

           /// <summary>
           /// Desc:099座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZWS {get;set;}

           /// <summary>
           /// Desc:099校区要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQYQ {get;set;}

           /// <summary>
           /// Desc:099变动原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BDYY {get;set;}

           /// <summary>
           /// Desc:099申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:099申请人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQR {get;set;}

           /// <summary>
           /// Desc:099审核标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHBJ {get;set;}

           /// <summary>
           /// Desc:099审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

           /// <summary>
           /// Desc:099审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCJG {get;set;}

           /// <summary>
           /// Desc:099标志
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QBQZZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YYLB {get;set;}

           /// <summary>
           /// Desc:099是否记录调停课总学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJLZHXS {get;set;}

           /// <summary>
           /// Desc:099学院审核标记 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHBJ {get;set;}

           /// <summary>
           /// Desc:099学院审核时间 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHSJ {get;set;}

           /// <summary>
           /// Desc:099学院审核人 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJSZGH_DG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJSZGH_DG {get;set;}

           /// <summary>
           /// Desc:099冲突判断信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJG {get;set;}

           /// <summary>
           /// Desc:099学院反馈意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYFKYJ {get;set;}

           /// <summary>
           /// Desc:099教务处反馈意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCFKYJ {get;set;}

           /// <summary>
           /// Desc:序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XUH {get;set;}

           /// <summary>
           /// Desc:调课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKJS {get;set;}

           /// <summary>
           /// Desc:N099 是否代课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDK {get;set;}

           /// <summary>
           /// Desc:现起始结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQSJSZ {get;set;}

           /// <summary>
           /// Desc:原起始结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQSJSZ {get;set;}

    }
}

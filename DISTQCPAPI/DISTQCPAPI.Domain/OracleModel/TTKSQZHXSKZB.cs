﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TTKSQZHXSKZB
    {
           public TTKSQZHXSKZB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:099总的上课学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXS {get;set;}

           /// <summary>
           /// Desc:099可调整的上课百分比
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BFB {get;set;}

           /// <summary>
           /// Desc:099可调整的上课学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKZHXS {get;set;}

    }
}

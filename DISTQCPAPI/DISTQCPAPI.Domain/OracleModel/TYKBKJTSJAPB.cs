﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TYKBKJTSJAPB
    {
           public TYKBKJTSJAPB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short NJ {get;set;}

           /// <summary>
           /// Desc:099板块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKMC {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SJDXH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short QSSJD {get;set;}

           /// <summary>
           /// Desc:099上课长度
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short SKCD {get;set;}

    }
}

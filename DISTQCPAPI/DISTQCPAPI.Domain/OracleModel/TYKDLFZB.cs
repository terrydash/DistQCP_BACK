﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TYKDLFZB
    {
           public TYKDLFZB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:003课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:004课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:005学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:006测试日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSRQ {get;set;}

           /// <summary>
           /// Desc:007学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:008身高
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SG {get;set;}

           /// <summary>
           /// Desc:009体重
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TZ {get;set;}

           /// <summary>
           /// Desc:010锻炼次数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DLCS {get;set;}

           /// <summary>
           /// Desc:011锻炼成绩1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM1 {get;set;}

           /// <summary>
           /// Desc:012锻炼成绩2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM2 {get;set;}

           /// <summary>
           /// Desc:013锻炼成绩3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM3 {get;set;}

           /// <summary>
           /// Desc:014锻炼成绩4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM4 {get;set;}

           /// <summary>
           /// Desc:015锻炼成绩5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM5 {get;set;}

           /// <summary>
           /// Desc:016锻炼成绩6
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM6 {get;set;}

           /// <summary>
           /// Desc:017锻炼成绩7
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM7 {get;set;}

           /// <summary>
           /// Desc:018锻炼成绩8
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM8 {get;set;}

           /// <summary>
           /// Desc:019锻炼成绩9
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM9 {get;set;}

           /// <summary>
           /// Desc:020锻炼成绩10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM10 {get;set;}

           /// <summary>
           /// Desc:021锻炼成绩11
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM11 {get;set;}

           /// <summary>
           /// Desc:022锻炼成绩12
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM12 {get;set;}

           /// <summary>
           /// Desc:023锻炼成绩13
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM13 {get;set;}

           /// <summary>
           /// Desc:024锻炼成绩14
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM14 {get;set;}

           /// <summary>
           /// Desc:025锻炼成绩15
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ12 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ13 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ14 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ15 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTZFZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FHLTZZS {get;set;}

           /// <summary>
           /// Desc:011备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

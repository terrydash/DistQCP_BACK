﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class TYKKCDMB
    {
           public TYKKCDMB(){


           }
           /// <summary>
           /// Desc:001课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:002课程中文名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCZWMC {get;set;}

           /// <summary>
           /// Desc:003课程英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCYWMC {get;set;}

           /// <summary>
           /// Desc:005学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:006周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:007周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZS {get;set;}

           /// <summary>
           /// Desc:010预修要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXYQ {get;set;}

           /// <summary>
           /// Desc:011课程简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCJJ {get;set;}

           /// <summary>
           /// Desc:012教学大纲
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXDG {get;set;}

           /// <summary>
           /// Desc:008标识1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS1 {get;set;}

           /// <summary>
           /// Desc:009标识2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS2 {get;set;}

           /// <summary>
           /// Desc:004权重系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? QZXS {get;set;}

           /// <summary>
           /// Desc:013考试内容及标准
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSNRJBZ {get;set;}

           /// <summary>
           /// Desc:099对应课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYKCDM {get;set;}

           /// <summary>
           /// Desc:099对应课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYKCMC {get;set;}

           /// <summary>
           /// Desc:099所属板块课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSBKKCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? ZCFY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? CXFY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FXFY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KTHKCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKBMDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSKCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHFS {get;set;}

           /// <summary>
           /// Desc:099 标识4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMCPY {get;set;}

           /// <summary>
           /// Desc:099课程标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCBJ {get;set;}

    }
}

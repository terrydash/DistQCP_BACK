﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class WEBCDGNB
    {
           public WEBCDGNB(){


           }
           /// <summary>
           /// Desc:001  功能模块界面
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GNMKWJM {get;set;}

           /// <summary>
           /// Desc:002  功能模块名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GNMKMC {get;set;}

           /// <summary>
           /// Desc:003  功能模块说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

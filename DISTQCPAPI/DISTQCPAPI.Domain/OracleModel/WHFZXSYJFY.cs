﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class WHFZXSYJFY
    {
           public WHFZXSYJFY(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:099专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DQSZJ {get;set;}

           /// <summary>
           /// Desc:099班级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:099预缴金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? YJFY {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class WJZBLSZB2
    {
           public WJZBLSZB2(){


           }
           /// <summary>
           /// Desc:099院系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? YX {get;set;}

           /// <summary>
           /// Desc:099楼号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? LH {get;set;}

           /// <summary>
           /// Desc:099合格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? HG {get;set;}

           /// <summary>
           /// Desc:099编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? BHG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? ZD {get;set;}

           /// <summary>
           /// Desc:099学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYDM {get;set;}

    }
}

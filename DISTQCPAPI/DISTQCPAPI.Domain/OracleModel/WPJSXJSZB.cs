﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class WPJSXJSZB
    {
           public WPJSXJSZB(){


           }
           /// <summary>
           /// Desc:001教师职称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZC {get;set;}

           /// <summary>
           /// Desc:002人数段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RSD {get;set;}

           /// <summary>
           /// Desc:003薪金
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XJ {get;set;}

    }
}

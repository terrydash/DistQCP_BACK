﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XBDMB
    {
           public XBDMB(){


           }
           /// <summary>
           /// Desc:099性别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XBDM {get;set;}

           /// <summary>
           /// Desc:099性别名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XBMC {get;set;}

    }
}

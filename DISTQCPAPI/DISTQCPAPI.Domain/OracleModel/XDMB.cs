﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XDMB
    {
           public XDMB(){


           }
           /// <summary>
           /// Desc:001系代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XDM {get;set;}

           /// <summary>
           /// Desc:002系名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMC {get;set;}

           /// <summary>
           /// Desc:099系英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYWMC {get;set;}

           /// <summary>
           /// Desc:099托管学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XFZR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XCLSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKYJYJFX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KYJJXCG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099所属学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXSXYDM {get;set;}

    }
}

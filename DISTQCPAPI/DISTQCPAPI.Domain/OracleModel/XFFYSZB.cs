﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XFFYSZB
    {
           public XFFYSZB(){


           }
           /// <summary>
           /// Desc:099是否优秀教材
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? ZCXFFY {get;set;}

           /// <summary>
           /// Desc:099是否实验班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? CXXFFY {get;set;}

           /// <summary>
           /// Desc:099进修学分费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JXXFFY1 {get;set;}

           /// <summary>
           /// Desc:099进修学分费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JXXFFY2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FXXFFY {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XFXSFPB
    {
           public XFXSFPB(){


           }
           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099教学周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXZS {get;set;}

           /// <summary>
           /// Desc:099公共必修课学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF1 {get;set;}

           /// <summary>
           /// Desc:099专业必修课学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF2 {get;set;}

           /// <summary>
           /// Desc:099专业任选课学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF3 {get;set;}

           /// <summary>
           /// Desc:099集中实践类环节学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF4 {get;set;}

           /// <summary>
           /// Desc:099公共必修课学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS1 {get;set;}

           /// <summary>
           /// Desc:099专业必修课学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS2 {get;set;}

           /// <summary>
           /// Desc:099专业任选课学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS3 {get;set;}

           /// <summary>
           /// Desc:099单独实践类教学周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DDSJJXZS {get;set;}

           /// <summary>
           /// Desc:099考试周数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSZS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYFX {get;set;}

    }
}

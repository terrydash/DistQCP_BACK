﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XHPWXJLB
    {
           public XHPWXJLB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ID {get;set;}

           /// <summary>
           /// Desc:099费用类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FYLX {get;set;}

           /// <summary>
           /// Desc:099费用发生时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FYFSSJ {get;set;}

           /// <summary>
           /// Desc:099保修时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BXSJ {get;set;}

           /// <summary>
           /// Desc:099实验室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSMC {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JE {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JBRZGH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JBR {get;set;}

    }
}

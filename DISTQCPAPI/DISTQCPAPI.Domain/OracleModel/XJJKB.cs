﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XJJKB
    {
           public XJJKB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099当前所在级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DQSZJ {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099获得学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? HDXF {get;set;}

           /// <summary>
           /// Desc:099不及格课程及学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJGKCJXF {get;set;}

           /// <summary>
           /// Desc:099是否警告
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJG {get;set;}

           /// <summary>
           /// Desc:099未通过学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WTGXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSJG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? XNHDXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XNBJGKCJXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XNWTGXF {get;set;}

           /// <summary>
           /// Desc:099平均学分绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJXFJD {get;set;}

           /// <summary>
           /// Desc:099警告类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JGLX {get;set;}

           /// <summary>
           /// Desc:099选课学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XKXF {get;set;}

           /// <summary>
           /// Desc:099计划内平均学分绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHNPJXFJD {get;set;}

           /// <summary>
           /// Desc:099计划内选课学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JHNXKXF {get;set;}

           /// <summary>
           /// Desc:099累计未获得学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? LJWHDXF {get;set;}

           /// <summary>
           /// Desc:099必修课累计未获得学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BXKLJWHDXF {get;set;}

           /// <summary>
           /// Desc:099等级考试未通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKSQK {get;set;}

           /// <summary>
           /// Desc:不及格门次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJGMC {get;set;}

           /// <summary>
           /// Desc:099任意选修课获得学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? RYXXKHDXF {get;set;}

           /// <summary>
           /// Desc:099未修课程及学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXKCJXF {get;set;}

           /// <summary>
           /// Desc:099计划外不及格课程及学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHWBJGKCJXF {get;set;}

    }
}

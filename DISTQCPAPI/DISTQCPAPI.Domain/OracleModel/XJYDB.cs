﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XJYDB
    {
           public XJYDB(){


           }
           /// <summary>
           /// Desc:099异动学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal YDXH {get;set;}

           /// <summary>
           /// Desc:001学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:003性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:004处理文号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLWH {get;set;}

           /// <summary>
           /// Desc:005异动类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDLB {get;set;}

           /// <summary>
           /// Desc:006异动原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDYY {get;set;}

           /// <summary>
           /// Desc:007异动时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDSJ {get;set;}

           /// <summary>
           /// Desc:008异动说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDSM {get;set;}

           /// <summary>
           /// Desc:009异动前学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQXY {get;set;}

           /// <summary>
           /// Desc:010异动前系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQXI {get;set;}

           /// <summary>
           /// Desc:011异动前专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQZY {get;set;}

           /// <summary>
           /// Desc:012异动前学制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YDQXZ {get;set;}

           /// <summary>
           /// Desc:013异动前专业方向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQZYFX {get;set;}

           /// <summary>
           /// Desc:014异动前培养方向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQPYFX {get;set;}

           /// <summary>
           /// Desc:015异动前行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQXZB {get;set;}

           /// <summary>
           /// Desc:016异动前学籍状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQXJZT {get;set;}

           /// <summary>
           /// Desc:017异动后学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHXY {get;set;}

           /// <summary>
           /// Desc:018异动后系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHXI {get;set;}

           /// <summary>
           /// Desc:019异动后专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHZY {get;set;}

           /// <summary>
           /// Desc:020异动后学制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YDHXZ {get;set;}

           /// <summary>
           /// Desc:021异动后专业方向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHZYFX {get;set;}

           /// <summary>
           /// Desc:022异动后培养方向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHPYFX {get;set;}

           /// <summary>
           /// Desc:023异动后行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHXZB {get;set;}

           /// <summary>
           /// Desc:024异动后学籍状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHXJZT {get;set;}

           /// <summary>
           /// Desc:099异动前所在年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YDQSZNJ {get;set;}

           /// <summary>
           /// Desc:099异动后所在年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YDHSZNJ {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099操作人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZR {get;set;}

           /// <summary>
           /// Desc:099操作日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? XKF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZCF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQSFZX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHSFZX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDZZSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYDCLKB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQSFZC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHSFZC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQXSCBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHXSCBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLCG {get;set;}

           /// <summary>
           /// Desc:099是否给予退学处理
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGYTXCL {get;set;}

           /// <summary>
           /// Desc:099试读结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SDJG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099异动结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDJG {get;set;}

           /// <summary>
           /// Desc:099异动前在校状况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQZXZK {get;set;}

           /// <summary>
           /// Desc:099异动后在校状况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHZXZK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQCC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHCC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQZYLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHZYLB {get;set;}

           /// <summary>
           /// Desc:099转学前学校名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXQXXMC {get;set;}

           /// <summary>
           /// Desc:099转学前所在年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXQNJ {get;set;}

           /// <summary>
           /// Desc:099转学前所在专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXQZYMC {get;set;}

           /// <summary>
           /// Desc:099转学后学校名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXHXXMC {get;set;}

           /// <summary>
           /// Desc:099转学后所在年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXHNJ {get;set;}

           /// <summary>
           /// Desc:099转学后所在专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXHZYMC {get;set;}

           /// <summary>
           /// Desc:099异动前托管学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQTGXY {get;set;}

           /// <summary>
           /// Desc:099异动后托管学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHTGXY {get;set;}

           /// <summary>
           /// Desc:异动前毕业年份
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQBYNF {get;set;}

           /// <summary>
           /// Desc:异动后毕业年份
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHBYNF {get;set;}

           /// <summary>
           /// Desc:099是否生成选课记录
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSCXK {get;set;}

           /// <summary>
           /// Desc:099异动后学生类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQXSLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHXSLB {get;set;}

           /// <summary>
           /// Desc:099异动后身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQSFZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHSFZH {get;set;}

           /// <summary>
           /// Desc:异动前国家学籍
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDQGJXJ {get;set;}

           /// <summary>
           /// Desc:异动后国家学籍
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHGJXJ {get;set;}

    }
}

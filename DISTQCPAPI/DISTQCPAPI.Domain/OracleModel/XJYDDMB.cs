﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XJYDDMB
    {
           public XJYDDMB(){


           }
           /// <summary>
           /// Desc:001异动代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XJYDDM {get;set;}

           /// <summary>
           /// Desc:002异动名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XJYDMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKBD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFCG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFHG {get;set;}

           /// <summary>
           /// Desc:099转学类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJYDYWMC {get;set;}

           /// <summary>
           /// Desc:是否可操作
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKCZ {get;set;}

           /// <summary>
           /// Desc:099是否生成选课记录
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSCXK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZFDXC_DMK_DM {get;set;}

           /// <summary>
           /// Desc:099web是否可申请
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEBSFXS {get;set;}

           /// <summary>
           /// Desc:099是否在校
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZX {get;set;}

           /// <summary>
           /// Desc:099是否注册
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZC {get;set;}

           /// <summary>
           /// Desc:099学籍状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJZT {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XJYDJKB
    {
           public XJYDJKB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ21 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ31 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ41 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ1XF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ2XF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ3XF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ4XF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ5XF1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ5XF2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ5XF3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ61 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ6XF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ7XF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ8XF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQHDXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQXKXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJHDXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJXKXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ91 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ9XF {get;set;}

           /// <summary>
           /// Desc:099操作信息记录
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZXXJL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ101 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ10 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ10XF {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ1 {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ2 {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ3 {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ4 {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ5 {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因6
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ6 {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因7
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ7 {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因8
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ8 {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因9
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ9 {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ10 {get;set;}

           /// <summary>
           /// Desc:099是否警告
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ1A {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ1A1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ1AXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ1B {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ1B1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ1BXF {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因11
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ1A {get;set;}

           /// <summary>
           /// Desc:099手工设置警告原因12
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SGTJ1B {get;set;}

           /// <summary>
           /// Desc:099条件13
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ13 {get;set;}

    }
}

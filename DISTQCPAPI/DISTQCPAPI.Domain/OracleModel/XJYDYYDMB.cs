﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XJYDYYDMB
    {
           public XJYDYYDMB(){


           }
           /// <summary>
           /// Desc:099学籍异动原因代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XJYDYYDM {get;set;}

           /// <summary>
           /// Desc:099学籍异动原因名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJYDYYMC {get;set;}

    }
}

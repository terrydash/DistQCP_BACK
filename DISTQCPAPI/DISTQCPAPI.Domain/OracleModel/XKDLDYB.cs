﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XKDLDYB
    {
           public XKDLDYB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XS {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XKDLMXB
    {
           public XKDLMXB(){


           }
           /// <summary>
           /// Desc:099学科导论名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKDLMC {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XH {get;set;}

           /// <summary>
           /// Desc:099主要部分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYBF {get;set;}

           /// <summary>
           /// Desc:099任课教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RKJS {get;set;}

           /// <summary>
           /// Desc:099职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:099学时安排
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSAP {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099开课标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKBJ {get;set;}

           /// <summary>
           /// Desc:099上课时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ {get;set;}

           /// <summary>
           /// Desc:099上课地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKDD {get;set;}

           /// <summary>
           /// Desc:099选修人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? XXRS {get;set;}

           /// <summary>
           /// Desc:099预选人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? YXRS {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQJ {get;set;}

           /// <summary>
           /// Desc:099起始时间段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QSSJD {get;set;}

           /// <summary>
           /// Desc:099上课长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SKCD {get;set;}

           /// <summary>
           /// Desc:099座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZWS {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XKJFB
    {
           public XKJFB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099选课经费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XKJF {get;set;}

           /// <summary>
           /// Desc:099实缴学费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SJXF {get;set;}

           /// <summary>
           /// Desc:099学费减免
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFJM {get;set;}

           /// <summary>
           /// Desc:099预增选课费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YZXKF {get;set;}

           /// <summary>
           /// Desc:099专业调增经费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZYTZJF {get;set;}

           /// <summary>
           /// Desc:099奖贷学金预支
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JDXJYZ {get;set;}

           /// <summary>
           /// Desc:099缓交学费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? HJXF {get;set;}

           /// <summary>
           /// Desc:099已用选课经费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YYXKJF {get;set;}

           /// <summary>
           /// Desc:099选课经费余额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XKJFYE {get;set;}

           /// <summary>
           /// Desc:099欠缴金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? QJJE {get;set;}

    }
}

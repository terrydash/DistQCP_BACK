﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XKJSDMB
    {
           public XKJSDMB(){


           }
           /// <summary>
           /// Desc:001学科竞赛代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKJSDM {get;set;}

           /// <summary>
           /// Desc:002学科竞赛名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKJSMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

    }
}

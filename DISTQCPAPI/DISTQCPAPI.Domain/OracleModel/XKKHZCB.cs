﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XKKHZCB
    {
           public XKKHZCB(){


           }
           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099学年学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XNXQ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099学期课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQKC {get;set;}

           /// <summary>
           /// Desc:099字段序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDXH {get;set;}

           /// <summary>
           /// Desc:099课程长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCCD {get;set;}

           /// <summary>
           /// Desc:099教师长度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSCD {get;set;}

    }
}

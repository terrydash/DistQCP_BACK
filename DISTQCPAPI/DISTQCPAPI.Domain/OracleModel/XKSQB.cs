﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XKSQB
    {
           public XKSQB(){


           }
           /// <summary>
           /// Desc:099类别(=0) 2：补选  3：退选
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LB {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXDH {get;set;}

           /// <summary>
           /// Desc:申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:申请理由
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:申请确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SQQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBZY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCJG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYXKKH {get;set;}

           /// <summary>
           /// Desc:教材预定 1：预定
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCYD {get;set;}

           /// <summary>
           /// Desc:教师意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSYJ {get;set;}

           /// <summary>
           /// Desc:教师审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSHSJ {get;set;}

           /// <summary>
           /// Desc:学院意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYYJ {get;set;}

           /// <summary>
           /// Desc:学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHSJ {get;set;}

           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:是否重修
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFCX {get;set;}

           /// <summary>
           /// Desc:学院审核否  0：待审 1：通过 2：不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHF {get;set;}

           /// <summary>
           /// Desc:学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZRBJ {get;set;}

           /// <summary>
           /// Desc:099教务处意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCYJ {get;set;}

           /// <summary>
           /// Desc:开课学院意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYYJ {get;set;}

           /// <summary>
           /// Desc:开课学院审核结果 0：待审 1：通过 2：不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYSHJG {get;set;}

           /// <summary>
           /// Desc:开课学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYSHSJ {get;set;}

           /// <summary>
           /// Desc:开课学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYSHR {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XKTZB
    {
           public XKTZB(){


           }
           /// <summary>
           /// Desc:099学年学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XNXQ {get;set;}

           /// <summary>
           /// Desc:099通知标题
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TZBT {get;set;}

           /// <summary>
           /// Desc:099通知正文
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TZZW {get;set;}

    }
}

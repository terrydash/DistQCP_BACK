﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XKZZGZL
    {
           public XKZZGZL(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099开课学院
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:099学科组织
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKX {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099课程类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLB {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099总学时(学分)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXS {get;set;}

           /// <summary>
           /// Desc:099工作量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZL {get;set;}

           /// <summary>
           /// Desc:099所属课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSKCXZ {get;set;}

           /// <summary>
           /// Desc:099学院审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSH {get;set;}

           /// <summary>
           /// Desc:099教务处审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RS {get;set;}

           /// <summary>
           /// Desc:099是否本学院 1： 是 0 ：否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBXY {get;set;}

           /// <summary>
           /// Desc:099是否本学科组织 1： 是 0 ：否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBXKZZ {get;set;}

    }
}

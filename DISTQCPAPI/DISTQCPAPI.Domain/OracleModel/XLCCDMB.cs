﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XLCCDMB
    {
           public XLCCDMB(){


           }
           /// <summary>
           /// Desc:099学历层次代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XLCCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XLCCMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYXW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZSB {get;set;}

           /// <summary>
           /// Desc:英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XLCCYWMC {get;set;}

           /// <summary>
           /// Desc:001预警提示信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TSXX {get;set;}

    }
}

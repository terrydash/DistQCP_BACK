﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XMPJXQSZB
    {
           public XMPJXQSZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099评价开关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? PJKG {get;set;}

           /// <summary>
           /// Desc:099查询开关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? CXKG {get;set;}

    }
}

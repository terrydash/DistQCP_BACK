﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XMPJZBXXB
    {
           public XMPJZBXXB(){


           }
           /// <summary>
           /// Desc:099课程类型代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLXDM {get;set;}

           /// <summary>
           /// Desc:099对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DX {get;set;}

           /// <summary>
           /// Desc:099评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJH {get;set;}

           /// <summary>
           /// Desc:099评价内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNR {get;set;}

           /// <summary>
           /// Desc:099权重
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? QZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJZBMC {get;set;}

           /// <summary>
           /// Desc:099参评对象
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CPDX {get;set;}

    }
}

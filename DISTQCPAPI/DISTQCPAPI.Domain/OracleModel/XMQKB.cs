﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XMQKB
    {
           public XMQKB(){


           }
           /// <summary>
           /// Desc:项目流水号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMLSH {get;set;}

           /// <summary>
           /// Desc:项目编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMBH {get;set;}

           /// <summary>
           /// Desc:项目简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMJJ {get;set;}

           /// <summary>
           /// Desc:交流期限
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLQX {get;set;}

           /// <summary>
           /// Desc:交流国家
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLGJ {get;set;}

           /// <summary>
           /// Desc:交流学校
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLXX {get;set;}

           /// <summary>
           /// Desc:面向对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDX {get;set;}

           /// <summary>
           /// Desc:申请学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQXY {get;set;}

           /// <summary>
           /// Desc:申请人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQR {get;set;}

           /// <summary>
           /// Desc:申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

           /// <summary>
           /// Desc:审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

           /// <summary>
           /// Desc:审核标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHBJ {get;set;}

           /// <summary>
           /// Desc:项目开关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMKG {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XMSQQXB
    {
           public XMSQQXB(){


           }
           /// <summary>
           /// Desc:职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:是否有中期检查权限：0-无，1-有
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZQJCQX {get;set;}

           /// <summary>
           /// Desc:是否有项目验收权限：0-无，1-有
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMYSQX {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XMXSPFTJB
    {
           public XMXSPFTJB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099对教师评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DJSPF {get;set;}

           /// <summary>
           /// Desc:099对课程评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DKCPF {get;set;}

           /// <summary>
           /// Desc:099对教材评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DJCPF {get;set;}

           /// <summary>
           /// Desc:099对象代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXQ {get;set;}

           /// <summary>
           /// Desc:099换算等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HSDJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XMXSPJTJB1
    {
           public XMXSPJTJB1(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJH {get;set;}

           /// <summary>
           /// Desc:099评价内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNR {get;set;}

           /// <summary>
           /// Desc:099单项均值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? DXJZ {get;set;}

           /// <summary>
           /// Desc:099课程均值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? KCJZ {get;set;}

           /// <summary>
           /// Desc:099非常满意数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? FCMYS {get;set;}

           /// <summary>
           /// Desc:099比较满意数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? BJMYS {get;set;}

           /// <summary>
           /// Desc:099基本满意数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JBMYS {get;set;}

           /// <summary>
           /// Desc:099不满意数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? BMYS {get;set;}

           /// <summary>
           /// Desc:099非常不满意数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? FCBMYS {get;set;}

           /// <summary>
           /// Desc:099对象代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXQ {get;set;}

    }
}

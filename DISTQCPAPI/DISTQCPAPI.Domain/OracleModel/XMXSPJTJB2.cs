﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XMXSPJTJB2
    {
           public XMXSPJTJB2(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NL {get;set;}

           /// <summary>
           /// Desc:099性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:099教师院系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSYX {get;set;}

           /// <summary>
           /// Desc:099对教师评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DJSPF {get;set;}

           /// <summary>
           /// Desc:099对课程评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DKCPF {get;set;}

           /// <summary>
           /// Desc:099对教材评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DJCPF {get;set;}

           /// <summary>
           /// Desc:099选课人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XKRS {get;set;}

           /// <summary>
           /// Desc:099计分人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JFRS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? PF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JSZF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JSPJF {get;set;}

           /// <summary>
           /// Desc:099对象代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXQ {get;set;}

           /// <summary>
           /// Desc:099评价内容得分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? PJNRDF {get;set;}

           /// <summary>
           /// Desc:099方差
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FC {get;set;}

           /// <summary>
           /// Desc:099标准分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? BZF {get;set;}

           /// <summary>
           /// Desc:099督导组评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DDZPF {get;set;}

           /// <summary>
           /// Desc:099院系评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? YXPF {get;set;}

    }
}

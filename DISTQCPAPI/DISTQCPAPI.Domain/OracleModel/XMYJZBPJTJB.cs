﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XMYJZBPJTJB
    {
           public XMYJZBPJTJB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJH {get;set;}

           /// <summary>
           /// Desc:099对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DX {get;set;}

           /// <summary>
           /// Desc:099一级指标
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJZBMC {get;set;}

           /// <summary>
           /// Desc:099评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? PF {get;set;}

           /// <summary>
           /// Desc:099对象代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXDM {get;set;}

           /// <summary>
           /// Desc:099参评人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CPRS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XQDMB
    {
           public XQDMB(){


           }
           /// <summary>
           /// Desc:001校区代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQDM {get;set;}

           /// <summary>
           /// Desc:002校区名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQJP {get;set;}

           /// <summary>
           /// Desc:可选哪些校区的课程（如校区代码为1、2，则输入12）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KXKCXQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KXKCXQ_XXK {get;set;}

           /// <summary>
           /// Desc:099排课教师可跨校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKJSKPXQ {get;set;}

           /// <summary>
           /// Desc:N099选课校区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKX {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XQSKJH
    {
           public XQSKJH(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:教师职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZC {get;set;}

           /// <summary>
           /// Desc:班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:讲课授课时数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKSKSS {get;set;}

           /// <summary>
           /// Desc:实践授课时数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJSKSS {get;set;}

           /// <summary>
           /// Desc:习题课授课时数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTKSKSS {get;set;}

           /// <summary>
           /// Desc:多媒体教学学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DMTJXXS {get;set;}

           /// <summary>
           /// Desc:授课批次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKPC {get;set;}

           /// <summary>
           /// Desc:周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:考试形式（考试/考查）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSXS {get;set;}

           /// <summary>
           /// Desc:考试方式（开卷/闭卷）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSFS {get;set;}

           /// <summary>
           /// Desc:平时成绩比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PS {get;set;}

           /// <summary>
           /// Desc:时间成绩比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJ {get;set;}

           /// <summary>
           /// Desc:期末成绩比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QM {get;set;}

           /// <summary>
           /// Desc:答疑安排
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYAP {get;set;}

    }
}

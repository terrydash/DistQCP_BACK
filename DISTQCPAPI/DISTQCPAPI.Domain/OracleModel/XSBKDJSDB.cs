﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSBKDJSDB
    {
           public XSBKDJSDB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:003板块课程名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKKCMC {get;set;}

           /// <summary>
           /// Desc:004板块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKDM {get;set;}

           /// <summary>
           /// Desc:005板块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMC {get;set;}

           /// <summary>
           /// Desc:006等级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJ {get;set;}

           /// <summary>
           /// Desc:007取值方向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZFX {get;set;}

           /// <summary>
           /// Desc:008人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? RS {get;set;}

           /// <summary>
           /// Desc:009极限分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXF {get;set;}

    }
}

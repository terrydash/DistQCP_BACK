﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSBMQKB
    {
           public XSBMQKB(){


           }
           /// <summary>
           /// Desc:099名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MC {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZH {get;set;}

           /// <summary>
           /// Desc:是否交费。是：已交费；否(或is null)：未交费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDJC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDDG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BMSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:N099 保留成绩准考证号 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BLCJZKZH {get;set;}

           /// <summary>
           /// Desc:N099 保留成绩类型 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BLCJLX {get;set;}

           /// <summary>
           /// Desc:099报名号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BMH {get;set;}

           /// <summary>
           /// Desc:099准考证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZKZH {get;set;}

           /// <summary>
           /// Desc:099考试地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSDD {get;set;}

           /// <summary>
           /// Desc:099考试时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:N099证件类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJLX {get;set;}

           /// <summary>
           /// Desc:银行卡号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YHKH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZKZH1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBLCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXDH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KFJE {get;set;}

           /// <summary>
           /// Desc:是否确认诚信承诺书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQRCNS {get;set;}

           /// <summary>
           /// Desc:是否确认个人信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQRGRXX {get;set;}

           /// <summary>
           /// Desc:099 保留成绩分数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BLCJFS {get;set;}

           /// <summary>
           /// Desc:是否确认完成
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQRWC {get;set;}

           /// <summary>
           /// Desc:099考场号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCH {get;set;}

           /// <summary>
           /// Desc:099考试座位号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSZWH {get;set;}

    }
}

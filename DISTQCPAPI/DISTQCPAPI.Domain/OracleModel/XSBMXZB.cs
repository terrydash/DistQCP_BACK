﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSBMXZB
    {
           public XSBMXZB(){


           }
           /// <summary>
           /// Desc:099 报名学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099 报名学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099限制报名的项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BMXM {get;set;}

    }
}

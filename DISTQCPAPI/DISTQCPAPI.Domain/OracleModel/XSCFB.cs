﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSCFB
    {
           public XSCFB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:003学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:004姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:005性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:006学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:007系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XI {get;set;}

           /// <summary>
           /// Desc:008专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:009行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:010学籍状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJZT {get;set;}

           /// <summary>
           /// Desc:011处分结果
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CFJG {get;set;}

           /// <summary>
           /// Desc:099处分原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CFYY {get;set;}

           /// <summary>
           /// Desc:099处分日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CFRQ {get;set;}

           /// <summary>
           /// Desc:014处分文件号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CFWJH {get;set;}

           /// <summary>
           /// Desc:015给予处分单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GYCFDW {get;set;}

           /// <summary>
           /// Desc:016是否评议
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFPY {get;set;}

           /// <summary>
           /// Desc:017解除处分日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCCFRQ {get;set;}

           /// <summary>
           /// Desc:018备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZYH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CFYYLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCWJH {get;set;}

           /// <summary>
           /// Desc:099处分文件名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CFWJM {get;set;}

    }
}

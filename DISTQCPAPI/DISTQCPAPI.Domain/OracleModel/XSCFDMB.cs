﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSCFDMB
    {
           public XSCFDMB(){


           }
           /// <summary>
           /// Desc:001处分代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XSCFDM {get;set;}

           /// <summary>
           /// Desc:002处分名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XSCFMC {get;set;}

    }
}

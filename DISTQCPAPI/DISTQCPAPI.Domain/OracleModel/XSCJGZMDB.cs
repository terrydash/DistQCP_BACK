﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSCJGZMDB
    {
           public XSCJGZMDB(){


           }
           /// <summary>
           /// Desc:ID 主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ID {get;set;}

           /// <summary>
           /// Desc:申请ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQID {get;set;}

           /// <summary>
           /// Desc:专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:总评成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:补考成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKCJ {get;set;}

           /// <summary>
           /// Desc:毕业补考成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYBKCJ {get;set;}

           /// <summary>
           /// Desc:更正原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZYY {get;set;}

           /// <summary>
           /// Desc:教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:开课学年学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXNXQ {get;set;}

           /// <summary>
           /// Desc:选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:平时成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSCJ {get;set;}

           /// <summary>
           /// Desc:更正后平时成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZHPSCJ {get;set;}

           /// <summary>
           /// Desc:期中成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZCJ {get;set;}

           /// <summary>
           /// Desc:更正后期中成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZHQZCJ {get;set;}

           /// <summary>
           /// Desc:期末成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QMCJ {get;set;}

           /// <summary>
           /// Desc:更正后期末成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZHQMCJ {get;set;}

           /// <summary>
           /// Desc:实验成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYCJ {get;set;}

           /// <summary>
           /// Desc:更正后实验成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZHSYCJ {get;set;}

           /// <summary>
           /// Desc:更正后总评成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZHCJ {get;set;}

           /// <summary>
           /// Desc:更正后补考成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZHBKCJ {get;set;}

           /// <summary>
           /// Desc:更正后毕业补考成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZHBYBKCJ {get;set;}

           /// <summary>
           /// Desc:更正原因选择其他是须输入更正原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QTGZYY {get;set;}

    }
}

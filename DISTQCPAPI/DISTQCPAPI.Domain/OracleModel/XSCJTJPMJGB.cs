﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSCJTJPMJGB
    {
           public XSCJTJPMJGB(){


           }
           /// <summary>
           /// Desc:009学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:009学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:009学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:009姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:009学历层次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XLCC {get;set;}

           /// <summary>
           /// Desc:009总分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZF {get;set;}

           /// <summary>
           /// Desc:009门数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MS {get;set;}

           /// <summary>
           /// Desc:009总学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXF {get;set;}

           /// <summary>
           /// Desc:009获得学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HDXF {get;set;}

           /// <summary>
           /// Desc:009算数平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSPJF {get;set;}

           /// <summary>
           /// Desc:009学分加权平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XFJQPJF {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSCXXFB
    {
           public XSCXXFB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:003学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:004创新内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CXNR {get;set;}

           /// <summary>
           /// Desc:004创新学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXXF {get;set;}

           /// <summary>
           /// Desc:004平均学分绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJXFJD {get;set;}

           /// <summary>
           /// Desc:004课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:011类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LX {get;set;}

           /// <summary>
           /// Desc:011成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:011公选课类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXKLB {get;set;}

           /// <summary>
           /// Desc:011创新内容代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXNRDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QDSJ {get;set;}

           /// <summary>
           /// Desc:099次数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CS {get;set;}

           /// <summary>
           /// Desc:099是否已转入成绩表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYZR {get;set;}

           /// <summary>
           /// Desc:099课程类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLB {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSDCBKCJB
    {
           public XSDCBKCJB(){


           }
           /// <summary>
           /// Desc:099补考学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKXN {get;set;}

           /// <summary>
           /// Desc:099补考学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short BKXQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099补考成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKCJ {get;set;}

           /// <summary>
           /// Desc:099是否毕业补考
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SFBYBK {get;set;}

           /// <summary>
           /// Desc:099重修标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? CXBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJCBK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QMCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYCJ {get;set;}

           /// <summary>
           /// Desc:099成绩最后一次修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJXGR {get;set;}

           /// <summary>
           /// Desc:099成绩最后一次修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJXGSJ {get;set;}

    }
}

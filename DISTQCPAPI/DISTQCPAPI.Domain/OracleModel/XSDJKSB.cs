﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSDJKSB
    {
           public XSDJKSB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:003学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:004姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:005等级考试名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DJKSMC {get;set;}

           /// <summary>
           /// Desc:006考试日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSRQ {get;set;}

           /// <summary>
           /// Desc:007成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSQMX {get;set;}

           /// <summary>
           /// Desc:099身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZH {get;set;}

           /// <summary>
           /// Desc:099准考证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZKZH {get;set;}

           /// <summary>
           /// Desc:099听力成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TLCJ {get;set;}

           /// <summary>
           /// Desc:099阅读成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDCJ {get;set;}

           /// <summary>
           /// Desc:099写作成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZCJ {get;set;}

           /// <summary>
           /// Desc:099综合成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHCJ {get;set;}

           /// <summary>
           /// Desc:099普通话等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PTHDJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PTHPC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ2 {get;set;}

           /// <summary>
           /// Desc:099折合成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHHCJ {get;set;}

           /// <summary>
           /// Desc:099是否通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTG {get;set;}

           /// <summary>
           /// Desc:099通过时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TGSJ {get;set;}

    }
}

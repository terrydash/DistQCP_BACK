﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSFXZCB
    {
           public XSFXZCB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:003学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:004姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:005报名类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BMLB {get;set;}

           /// <summary>
           /// Desc:006教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:007注册方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZCFS {get;set;}

           /// <summary>
           /// Desc:008注册时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZCSJ {get;set;}

           /// <summary>
           /// Desc:009操作人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZR {get;set;}

    }
}

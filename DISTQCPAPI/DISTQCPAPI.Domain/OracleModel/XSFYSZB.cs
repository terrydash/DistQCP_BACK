﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSFYSZB
    {
           public XSFYSZB(){


           }
           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:099专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099宿舍号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSH {get;set;}

           /// <summary>
           /// Desc:099专业基本费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZYJBF {get;set;}

           /// <summary>
           /// Desc:099总收费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? ZSF {get;set;}

           /// <summary>
           /// Desc:099代管费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DGF {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

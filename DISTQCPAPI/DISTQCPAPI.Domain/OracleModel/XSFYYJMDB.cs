﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSFYYJMDB
    {
           public XSFYYJMDB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099学费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM1 {get;set;}

           /// <summary>
           /// Desc:099代管费（教材费）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM2 {get;set;}

           /// <summary>
           /// Desc:099缴费标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

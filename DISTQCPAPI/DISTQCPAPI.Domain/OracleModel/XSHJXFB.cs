﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSHJXFB
    {
           public XSHJXFB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099缓交金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JE {get;set;}

           /// <summary>
           /// Desc:099缓交原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJYY {get;set;}

           /// <summary>
           /// Desc:099缓交日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJRQ {get;set;}

    }
}

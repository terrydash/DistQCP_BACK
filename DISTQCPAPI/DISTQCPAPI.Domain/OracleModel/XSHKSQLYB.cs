﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSHKSQLYB
    {
           public XSHKSQLYB(){


           }
           /// <summary>
           /// Desc:申请理由编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SQLYBH {get;set;}

           /// <summary>
           /// Desc:申请理由
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SQLY {get;set;}

           /// <summary>
           /// Desc:是否限制分数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SFXZFS {get;set;}

           /// <summary>
           /// Desc:限制分数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZFS {get;set;}

           /// <summary>
           /// Desc:类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LB {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSJBJSQTXXB
    {
           public XSJBJSQTXXB(){


           }
           /// <summary>
           /// Desc:001学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:009所在院校
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:009所在系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XI {get;set;}

           /// <summary>
           /// Desc:009专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:009年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:009班级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:009学籍表号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJBH {get;set;}

           /// <summary>
           /// Desc:009入伍年月
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWNY {get;set;}

           /// <summary>
           /// Desc:009原单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDW {get;set;}

           /// <summary>
           /// Desc:009入伍地区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWDQ {get;set;}

           /// <summary>
           /// Desc:009学生来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSLY {get;set;}

           /// <summary>
           /// Desc:009学员类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYLB {get;set;}

           /// <summary>
           /// Desc:009分配类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FPLB {get;set;}

           /// <summary>
           /// Desc:009分配部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FPBM {get;set;}

           /// <summary>
           /// Desc:009分配单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FPDW {get;set;}

           /// <summary>
           /// Desc:009单位类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWLB {get;set;}

           /// <summary>
           /// Desc:009入学批准书号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXPZSH {get;set;}

           /// <summary>
           /// Desc:009军事复试
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSFS {get;set;}

           /// <summary>
           /// Desc:009文化复试
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WHFS {get;set;}

           /// <summary>
           /// Desc:009体格检查
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJJC {get;set;}

           /// <summary>
           /// Desc:009军衔文职
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXWZ {get;set;}

           /// <summary>
           /// Desc:009现职时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZSJ {get;set;}

           /// <summary>
           /// Desc:009所属军区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSJQ {get;set;}

           /// <summary>
           /// Desc:009专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:009性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:009文化程度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WHCD {get;set;}

           /// <summary>
           /// Desc:009单位驻地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DWZD {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSJBXXLSB
    {
           public XSJBXXLSB(){


           }
           /// <summary>
           /// Desc:001学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:003性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:004出生日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSRQ {get;set;}

           /// <summary>
           /// Desc:005政治面貌
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZMM {get;set;}

           /// <summary>
           /// Desc:006民族
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MZ {get;set;}

           /// <summary>
           /// Desc:007籍贯
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JG {get;set;}

           /// <summary>
           /// Desc:008来源地区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYDQ {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:010系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XI {get;set;}

           /// <summary>
           /// Desc:011专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:012行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:013学制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XZ {get;set;}

           /// <summary>
           /// Desc:014学习年限
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XXNX {get;set;}

           /// <summary>
           /// Desc:015学籍状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJZT {get;set;}

           /// <summary>
           /// Desc:016当前所在级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DQSZJ {get;set;}

           /// <summary>
           /// Desc:017培养方向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYFX {get;set;}

           /// <summary>
           /// Desc:018专业方向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFX {get;set;}

           /// <summary>
           /// Desc:019专业类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYLB {get;set;}

           /// <summary>
           /// Desc:020入学日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RXRQ {get;set;}

           /// <summary>
           /// Desc:021毕业中学
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYZX {get;set;}

           /// <summary>
           /// Desc:022宿舍号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSH {get;set;}

           /// <summary>
           /// Desc:023电子邮箱地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DZYXDZ {get;set;}

           /// <summary>
           /// Desc:024联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXDH {get;set;}

           /// <summary>
           /// Desc:025准考证号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZKZH {get;set;}

           /// <summary>
           /// Desc:026身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZH {get;set;}

           /// <summary>
           /// Desc:027港澳台码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GATM {get;set;}

           /// <summary>
           /// Desc:028健康状况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKZK {get;set;}

           /// <summary>
           /// Desc:099英文姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWXW {get;set;}

           /// <summary>
           /// Desc:029备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MM {get;set;}

           /// <summary>
           /// Desc:030报到号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BDH {get;set;}

           /// <summary>
           /// Desc:099英语等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YYDJ {get;set;}

           /// <summary>
           /// Desc:099卡号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KH {get;set;}

           /// <summary>
           /// Desc:031招生时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSSJ {get;set;}

           /// <summary>
           /// Desc:011入学总分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RXZF {get;set;}

           /// <summary>
           /// Desc:035是否优秀学生
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYXXS {get;set;}

           /// <summary>
           /// Desc:036学生类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSLB {get;set;}

           /// <summary>
           /// Desc:099姓名拼音
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMPY {get;set;}

           /// <summary>
           /// Desc:011专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:011招生专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSZYDM {get;set;}

           /// <summary>
           /// Desc:011校区名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQMC {get;set;}

           /// <summary>
           /// Desc:011录检表页码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJBYM {get;set;}

           /// <summary>
           /// Desc:011收费来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFLY {get;set;}

           /// <summary>
           /// Desc:011是否可注册
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKZC {get;set;}

           /// <summary>
           /// Desc:011层次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CC {get;set;}

           /// <summary>
           /// Desc:011邮政编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YZBM {get;set;}

           /// <summary>
           /// Desc:011家庭地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTDZ {get;set;}

           /// <summary>
           /// Desc:011英语成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YYCJ {get;set;}

           /// <summary>
           /// Desc:011是否注册
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZC {get;set;}

           /// <summary>
           /// Desc:011收费标准类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBZLB {get;set;}

           /// <summary>
           /// Desc:011考生号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSH {get;set;}

           /// <summary>
           /// Desc:011银行帐号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YHZH {get;set;}

           /// <summary>
           /// Desc:011入党时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RDSJ {get;set;}

           /// <summary>
           /// Desc:011入党时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LQMCJYH {get;set;}

           /// <summary>
           /// Desc:011是否在校
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZX {get;set;}

           /// <summary>
           /// Desc:011考生类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSLB {get;set;}

           /// <summary>
           /// Desc:011曾用名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYM {get;set;}

           /// <summary>
           /// Desc:011出生地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSD {get;set;}

           /// <summary>
           /// Desc:011录检表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJB {get;set;}

           /// <summary>
           /// Desc:011录检表册
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJBC {get;set;}

           /// <summary>
           /// Desc:011录检表行
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJBH {get;set;}

           /// <summary>
           /// Desc:011电子注册号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DZZCH {get;set;}

           /// <summary>
           /// Desc:011乘车区间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCQJ {get;set;}

           /// <summary>
           /// Desc:011大类名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLMC {get;set;}

           /// <summary>
           /// Desc:011考生特征
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSTZ {get;set;}

           /// <summary>
           /// Desc:011特长
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TC {get;set;}

           /// <summary>
           /// Desc:011入学方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RXFS {get;set;}

           /// <summary>
           /// Desc:011是否走读生
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZDS {get;set;}

           /// <summary>
           /// Desc:011办学形式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BXXS {get;set;}

           /// <summary>
           /// Desc:011办学类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BXLX {get;set;}

           /// <summary>
           /// Desc:011学习形式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXXS {get;set;}

           /// <summary>
           /// Desc:011招生季节
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSJJ {get;set;}

           /// <summary>
           /// Desc:011主修外语语种
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXWYYZ {get;set;}

           /// <summary>
           /// Desc:011主修外语级别名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXWYJBMC {get;set;}

           /// <summary>
           /// Desc:011家庭所在地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTSZD {get;set;}

           /// <summary>
           /// Desc:011来源省
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYS {get;set;}

           /// <summary>
           /// Desc:011是否留学生
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFLXS {get;set;}

           /// <summary>
           /// Desc:011电话号码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TELNUMBER {get;set;}

           /// <summary>
           /// Desc:011电话类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TELLX {get;set;}

           /// <summary>
           /// Desc:011科类
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KL {get;set;}

           /// <summary>
           /// Desc:011科目1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM1 {get;set;}

           /// <summary>
           /// Desc:011科目2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM2 {get;set;}

           /// <summary>
           /// Desc:011科目3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM3 {get;set;}

           /// <summary>
           /// Desc:011科目4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM4 {get;set;}

           /// <summary>
           /// Desc:011科目5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM5 {get;set;}

           /// <summary>
           /// Desc:011科目6
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM6 {get;set;}

           /// <summary>
           /// Desc:011家庭邮编
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTYB {get;set;}

           /// <summary>
           /// Desc:011家庭电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTDH {get;set;}

           /// <summary>
           /// Desc:011父亲姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQXM {get;set;}

           /// <summary>
           /// Desc:011父亲单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQDW {get;set;}

           /// <summary>
           /// Desc:011父亲单位电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQDWDH {get;set;}

           /// <summary>
           /// Desc:011父亲单位邮编
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQDWYB {get;set;}

           /// <summary>
           /// Desc:011母亲姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQXM {get;set;}

           /// <summary>
           /// Desc:011母亲单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQDW {get;set;}

           /// <summary>
           /// Desc:011母亲单位电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQDWDH {get;set;}

           /// <summary>
           /// Desc:011母亲单位邮编
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQDWYB {get;set;}

           /// <summary>
           /// Desc:011父亲EMAIL
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQEMAIL {get;set;}

           /// <summary>
           /// Desc:011母亲EMAIL
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQEMAIL {get;set;}

           /// <summary>
           /// Desc:011父亲出生日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQCSRQ {get;set;}

           /// <summary>
           /// Desc:011母亲出生日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQCSRQ {get;set;}

           /// <summary>
           /// Desc:011父亲政治面貌
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQZZMM {get;set;}

           /// <summary>
           /// Desc:011母亲政治面貌
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQZZMM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YKTID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TSFBBJ {get;set;}

           /// <summary>
           /// Desc:011录取批次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LQPC {get;set;}

           /// <summary>
           /// Desc:011考生奖惩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSJC {get;set;}

           /// <summary>
           /// Desc:011原毕业专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YBYZY {get;set;}

           /// <summary>
           /// Desc:011录取志愿
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LQZY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKPJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFPDG {get;set;}

           /// <summary>
           /// Desc:099招生类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSLB {get;set;}

           /// <summary>
           /// Desc:011录取号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LQH {get;set;}

           /// <summary>
           /// Desc:011身高
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SG {get;set;}

           /// <summary>
           /// Desc:011体重
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSZYMC {get;set;}

           /// <summary>
           /// Desc:099教师评语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSPY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WPKH {get;set;}

           /// <summary>
           /// Desc:004入录帐号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RRZH {get;set;}

           /// <summary>
           /// Desc:099学员队
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYD {get;set;}

           /// <summary>
           /// Desc:099托管学院 默认要与学院相同
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY_XSXY {get;set;}

           /// <summary>
           /// Desc:099文化程度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WHCD {get;set;}

           /// <summary>
           /// Desc:099入伍年月
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWSJ {get;set;}

           /// <summary>
           /// Desc:099入伍地区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWDQ {get;set;}

           /// <summary>
           /// Desc:099所属军区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSJQ {get;set;}

           /// <summary>
           /// Desc:099原单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDW {get;set;}

           /// <summary>
           /// Desc:099学籍表号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJBH {get;set;}

           /// <summary>
           /// Desc:099入学批准书号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RXPZSH {get;set;}

           /// <summary>
           /// Desc:099学员类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYLB {get;set;}

           /// <summary>
           /// Desc:099寄投地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TZSJTDZ {get;set;}

           /// <summary>
           /// Desc:099通知书EMS号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EMS {get;set;}

           /// <summary>
           /// Desc:099退档
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TDBJ {get;set;}

           /// <summary>
           /// Desc:002转入标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZRBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFBZ {get;set;}

           /// <summary>
           /// Desc:培养方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJLX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WPDW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHFSTNME {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBBXSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBXSZSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HCPYHZT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYSFDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYSDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYXDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WPDWDQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJZG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLMMM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EJXK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EJXKSSYY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXPCKZXX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXXXGYH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFNZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HKXZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJKYPCKZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QFBS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ENSTUNUM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHFMYNME {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWCSRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWBYRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWRXRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZDD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QFQK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TDZY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HKSZD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZR2LXFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZR1LXFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DS2LXFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DS1LXFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZRXM2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZRXM1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSXM2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DSXM1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDJS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YZYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFCGLX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSCBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQXY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSMM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSHBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string USERID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGSPYDY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YHKH {get;set;}

           /// <summary>
           /// Desc:099满意度是否可 调查(“是”为可调查)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKDC {get;set;}

    }
}

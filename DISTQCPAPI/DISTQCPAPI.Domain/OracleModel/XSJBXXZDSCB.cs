﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSJBXXZDSCB
    {
           public XSJBXXZDSCB(){


           }
           /// <summary>
           /// Desc:099用户名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHM {get;set;}

           /// <summary>
           /// Desc:099字段名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZDMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDZWMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGYC {get;set;}

    }
}

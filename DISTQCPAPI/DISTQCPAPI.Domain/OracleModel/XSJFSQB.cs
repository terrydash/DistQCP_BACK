﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSJFSQB
    {
           public XSJFSQB(){


           }
           /// <summary>
           /// Desc:申请编号  PK
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SQID {get;set;}

           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:班级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:训练内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XLNR {get;set;}

           /// <summary>
           /// Desc:教务处审核结果 0：待审 1：通过 2：不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHJG {get;set;}

           /// <summary>
           /// Desc:教务处审核意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHYJ {get;set;}

           /// <summary>
           /// Desc:教务处审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHR {get;set;}

           /// <summary>
           /// Desc:教务处审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHSJ {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? SQSJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSJLB
    {
           public XSJLB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:003学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:004姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:005性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:006学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:007系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XI {get;set;}

           /// <summary>
           /// Desc:008专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:009行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:010奖励类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JLMC {get;set;}

           /// <summary>
           /// Desc:011奖励日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JLRQ {get;set;}

           /// <summary>
           /// Desc:012奖励级别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLJB {get;set;}

           /// <summary>
           /// Desc:013奖励金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JE {get;set;}

           /// <summary>
           /// Desc:014奖励方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLFS {get;set;}

           /// <summary>
           /// Desc:015奖励单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLDW {get;set;}

           /// <summary>
           /// Desc:016奖励文号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLWH {get;set;}

           /// <summary>
           /// Desc:017备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099奖励文件名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLWJM {get;set;}

           /// <summary>
           /// Desc:002考生号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSH {get;set;}

    }
}

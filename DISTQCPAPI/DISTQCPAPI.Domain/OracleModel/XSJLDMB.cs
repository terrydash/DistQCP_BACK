﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSJLDMB
    {
           public XSJLDMB(){


           }
           /// <summary>
           /// Desc:001奖励代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XSJLDM {get;set;}

           /// <summary>
           /// Desc:002奖励名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XSJLMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSJMB
    {
           public XSJMB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDR {get;set;}

           /// <summary>
           /// Desc:009????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYJE {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:003??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:004??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:005????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:006????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XFJM {get;set;}

           /// <summary>
           /// Desc:007????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GFBT {get;set;}

           /// <summary>
           /// Desc:008????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SDJE {get;set;}

           /// <summary>
           /// Desc:011导入时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DRSJ {get;set;}

    }
}

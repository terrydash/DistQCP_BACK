﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSJQPJFXXB
    {
           public XSJQPJFXXB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099分类1加权平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JQPJF_FL1 {get;set;}

           /// <summary>
           /// Desc:099分类2加权平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JQPJF_FL2 {get;set;}

           /// <summary>
           /// Desc:099分类3加权平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JQPJF_FL3 {get;set;}

           /// <summary>
           /// Desc:099分类4加权平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JQPJF_FL4 {get;set;}

           /// <summary>
           /// Desc:099分类5加权平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JQPJF_FL5 {get;set;}

           /// <summary>
           /// Desc:099分类6加权平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JQPJF_FL6 {get;set;}

           /// <summary>
           /// Desc:099分类7加权平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JQPJF_FL7 {get;set;}

           /// <summary>
           /// Desc:099分类8加权平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JQPJF_FL8 {get;set;}

           /// <summary>
           /// Desc:099算术平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JQPJF {get;set;}

    }
}

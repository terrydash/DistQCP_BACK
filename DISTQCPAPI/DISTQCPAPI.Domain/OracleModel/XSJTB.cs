﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSJTB
    {
           public XSJTB(){


           }
           /// <summary>
           /// Desc:001学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:003家庭地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTDZ {get;set;}

           /// <summary>
           /// Desc:004家庭邮编
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTYB {get;set;}

           /// <summary>
           /// Desc:005家庭电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTDH {get;set;}

           /// <summary>
           /// Desc:006父亲姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQXM {get;set;}

           /// <summary>
           /// Desc:007父亲单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQDW {get;set;}

           /// <summary>
           /// Desc:008父亲单位电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQDWDH {get;set;}

           /// <summary>
           /// Desc:009父亲单位邮编
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQDWYB {get;set;}

           /// <summary>
           /// Desc:010母亲姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQXM {get;set;}

           /// <summary>
           /// Desc:011母亲单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQDW {get;set;}

           /// <summary>
           /// Desc:012母亲单位电话或手机
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQDWDH {get;set;}

           /// <summary>
           /// Desc:013母亲单位邮编
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQDWYB {get;set;}

           /// <summary>
           /// Desc:014备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099家庭所在地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTSZD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQEMAIL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQEMAIL {get;set;}

           /// <summary>
           /// Desc:N099 父亲出生日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQCSRQ {get;set;}

           /// <summary>
           /// Desc:N099 母亲出生日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQCSRQ {get;set;}

           /// <summary>
           /// Desc:N099 父亲政治面貌
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQZZMM {get;set;}

           /// <summary>
           /// Desc:N099 母亲政治面貌
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQZZMM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JZZY {get;set;}

           /// <summary>
           /// Desc:N099父亲个人电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FQGRDH {get;set;}

           /// <summary>
           /// Desc:N099母亲个人电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MQGRDH {get;set;}

    }
}

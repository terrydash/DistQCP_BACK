﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSKCB
    {
           public XSKCB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQJ {get;set;}

           /// <summary>
           /// Desc:099第几节
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short DJJ {get;set;}

           /// <summary>
           /// Desc:099上课长度
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SKCD {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DSZ {get;set;}

           /// <summary>
           /// Desc:099起始周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string QSZ {get;set;}

           /// <summary>
           /// Desc:099结束周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZ {get;set;}

           /// <summary>
           /// Desc:099课程表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCB {get;set;}

           /// <summary>
           /// Desc:099选课参数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKCS {get;set;}

           /// <summary>
           /// Desc:099选上否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSF {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSKCMCTHB
    {
           public XSKCMCTHB(){


           }
           /// <summary>
           /// Desc:099班级代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099替换前课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THQKCMC {get;set;}

           /// <summary>
           /// Desc:099替换后课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THHKCMC {get;set;}

    }
}

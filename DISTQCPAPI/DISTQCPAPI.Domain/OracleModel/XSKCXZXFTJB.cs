﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSKCXZXFTJB
    {
           public XSKCXZXFTJB(){


           }
           /// <summary>
           /// Desc:学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:课程性质
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCXZMC {get;set;}

           /// <summary>
           /// Desc:学分要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XFYQ {get;set;}

           /// <summary>
           /// Desc:获得学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HDXF {get;set;}

           /// <summary>
           /// Desc:未通过学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WTGXF {get;set;}

           /// <summary>
           /// Desc:还需学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HXXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJSJ {get;set;}

           /// <summary>
           /// Desc:获得计划外课程学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHWKCXF {get;set;}

           /// <summary>
           /// Desc:获得计划内课程学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHNKCXF {get;set;}

    }
}

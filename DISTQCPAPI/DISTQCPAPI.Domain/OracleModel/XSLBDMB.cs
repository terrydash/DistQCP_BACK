﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSLBDMB
    {
           public XSLBDMB(){


           }
           /// <summary>
           /// Desc:099学生类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XSLBDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSLBMC {get;set;}

           /// <summary>
           /// Desc:099每学分收费金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJE {get;set;}

    }
}

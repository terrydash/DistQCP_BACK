﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSPFB
    {
           public XSPFB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJH {get;set;}

           /// <summary>
           /// Desc:099评价内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNR {get;set;}

           /// <summary>
           /// Desc:099评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? PF {get;set;}

           /// <summary>
           /// Desc:099是否交费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SFJF {get;set;}

           /// <summary>
           /// Desc:099评价时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJSJ {get;set;}

           /// <summary>
           /// Desc:099??
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJ {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? PF2 {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXDM {get;set;}

           /// <summary>
           /// Desc:099????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJXX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BS11 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXSPY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BSX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHDJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBTJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PFFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZTPF {get;set;}

           /// <summary>
           /// Desc:099评分对照
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PFDZB {get;set;}

    }
}

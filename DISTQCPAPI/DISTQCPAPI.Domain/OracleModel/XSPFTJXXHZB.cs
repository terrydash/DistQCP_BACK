﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSPFTJXXHZB
    {
           public XSPFTJXXHZB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:课程分类
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCFL {get;set;}

           /// <summary>
           /// Desc:教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:对教师评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DJSPF {get;set;}

           /// <summary>
           /// Desc:对课程评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DKCPF {get;set;}

           /// <summary>
           /// Desc:对教材评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DJCPF {get;set;}

           /// <summary>
           /// Desc:标识
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BSX {get;set;}

           /// <summary>
           /// Desc:对象代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DXDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYPM {get;set;}

           /// <summary>
           /// Desc:全校平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QXPJF {get;set;}

           /// <summary>
           /// Desc:学院平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYPJF {get;set;}

           /// <summary>
           /// Desc:全校同类平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QXTLPJF {get;set;}

           /// <summary>
           /// Desc:学院同类平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYTLPJF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

    }
}

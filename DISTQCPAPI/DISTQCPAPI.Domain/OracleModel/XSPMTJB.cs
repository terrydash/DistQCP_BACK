﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSPMTJB
    {
           public XSPMTJB(){


           }
           /// <summary>
           /// Desc:099专业
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short NJ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099平均学分绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? PJXFJD {get;set;}

           /// <summary>
           /// Desc:099学分绩点总和
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? XFJDZH {get;set;}

           /// <summary>
           /// Desc:099名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? MC {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSPYXZB
    {
           public XSPYXZB(){


           }
           /// <summary>
           /// Desc:099流水号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LSH {get;set;}

           /// <summary>
           /// Desc:099评语限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYXZ {get;set;}

    }
}

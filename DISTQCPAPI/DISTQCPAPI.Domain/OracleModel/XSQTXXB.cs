﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSQTXXB
    {
           public XSQTXXB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSSZS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSSZCS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSSZX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJZS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJZCS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJZX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XBMC {get;set;}

           /// <summary>
           /// Desc:099入学前情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RXQQK {get;set;}

           /// <summary>
           /// Desc:099是否华侨
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFHQ {get;set;}

           /// <summary>
           /// Desc:099户口性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HKXZ {get;set;}

           /// <summary>
           /// Desc:099就读方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDFS {get;set;}

    }
}

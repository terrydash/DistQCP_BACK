﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSRSDXSSZB
    {
           public XSRSDXSSZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099大类代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DLDM {get;set;}

           /// <summary>
           /// Desc:099大类名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLMC {get;set;}

           /// <summary>
           /// Desc:099人数系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RSXS {get;set;}

           /// <summary>
           /// Desc:099人数段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RSD {get;set;}

           /// <summary>
           /// Desc:099增减人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJRS {get;set;}

           /// <summary>
           /// Desc:099每增加人数系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MZJRSXS {get;set;}

    }
}

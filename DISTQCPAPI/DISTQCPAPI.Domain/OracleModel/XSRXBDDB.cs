﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSRXBDDB
    {
           public XSRXBDDB(){


           }
           /// <summary>
           /// Desc:099标题
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BT {get;set;}

           /// <summary>
           /// Desc:099正文序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ZWXH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSRXCJB
    {
           public XSRXCJB(){


           }
           /// <summary>
           /// Desc:001学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:004入学总分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? RXZF {get;set;}

           /// <summary>
           /// Desc:003考生类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSLB {get;set;}

           /// <summary>
           /// Desc:005科目1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM1 {get;set;}

           /// <summary>
           /// Desc:006科目2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM2 {get;set;}

           /// <summary>
           /// Desc:007科目3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM3 {get;set;}

           /// <summary>
           /// Desc:008科目4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM4 {get;set;}

           /// <summary>
           /// Desc:009科目5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM5 {get;set;}

           /// <summary>
           /// Desc:010外语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WY {get;set;}

           /// <summary>
           /// Desc:011外语成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WYCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KM6 {get;set;}

           /// <summary>
           /// Desc:099投档成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TDCJ {get;set;}

           /// <summary>
           /// Desc:099特征成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TZCJ {get;set;}

           /// <summary>
           /// Desc:099艺体专业成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSZYCJ {get;set;}

           /// <summary>
           /// Desc:099报考专业志愿1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKZYZY1 {get;set;}

           /// <summary>
           /// Desc:099报考专业志愿2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKZYZY2 {get;set;}

           /// <summary>
           /// Desc:099报考专业志愿3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKZYZY3 {get;set;}

    }
}

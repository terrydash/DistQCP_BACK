﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSRYCHB
    {
           public XSRYCHB(){


           }
           /// <summary>
           /// Desc:001学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:003姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:004性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:005学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:006系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XI {get;set;}

           /// <summary>
           /// Desc:007专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:008行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:009荣誉称号名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RYCHMC {get;set;}

           /// <summary>
           /// Desc:010金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JE {get;set;}

           /// <summary>
           /// Desc:011奖励日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JLRQ {get;set;}

           /// <summary>
           /// Desc:012奖励文号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLWH {get;set;}

           /// <summary>
           /// Desc:013备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYDW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RYJB {get;set;}

           /// <summary>
           /// Desc:099荣誉文件名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RYWJM {get;set;}

    }
}

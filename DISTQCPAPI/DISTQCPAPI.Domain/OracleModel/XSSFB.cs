﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSSFB
    {
           public XSSFB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM01 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM02 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM03 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM04 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM05 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM06 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM07 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM08 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM09 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM10 {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM11 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM12 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM13 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM14 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM15 {get;set;}

           /// <summary>
           /// Desc:099原始合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? YSHJ {get;set;}

           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:099是否欠费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSLB {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZKZH {get;set;}

           /// <summary>
           /// Desc:099性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZH {get;set;}

           /// <summary>
           /// Desc:099银行帐号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YHZH {get;set;}

           /// <summary>
           /// Desc:099是否作废
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZF {get;set;}

           /// <summary>
           /// Desc:099修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGR {get;set;}

           /// <summary>
           /// Desc:099修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGSJ {get;set;}

           /// <summary>
           /// Desc:099原应收合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YYSHJ {get;set;}

           /// <summary>
           /// Desc:099修改原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGYY {get;set;}

    }
}

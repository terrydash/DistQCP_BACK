﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSSHGZB
    {
           public XSSHGZB(){


           }
           /// <summary>
           /// Desc:001学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:003性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:004学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:005系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XI {get;set;}

           /// <summary>
           /// Desc:006专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:007行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:099担任何种社会工作
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DRHZSHGZ {get;set;}

           /// <summary>
           /// Desc:008奖励名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLMC {get;set;}

           /// <summary>
           /// Desc:009奖励日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLRQ {get;set;}

           /// <summary>
           /// Desc:010奖励级别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JLJB {get;set;}

           /// <summary>
           /// Desc:011奖励单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLDW {get;set;}

           /// <summary>
           /// Desc:012奖励文号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLWH {get;set;}

           /// <summary>
           /// Desc:013备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RZBM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XNKPDJ {get;set;}

    }
}

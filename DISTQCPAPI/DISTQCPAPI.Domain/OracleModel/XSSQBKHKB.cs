﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSSQBKHKB
    {
           public XSSQBKHKB(){


           }
           /// <summary>
           /// Desc:099补考学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKXN {get;set;}

           /// <summary>
           /// Desc:099补考学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKXQ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

           /// <summary>
           /// Desc:099审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

           /// <summary>
           /// Desc:099申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:099审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJG {get;set;}

           /// <summary>
           /// Desc:099学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHR {get;set;}

           /// <summary>
           /// Desc:099学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHSJ {get;set;}

           /// <summary>
           /// Desc:099学院审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHJG {get;set;}

           /// <summary>
           /// Desc:申请教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQJSZGH {get;set;}

           /// <summary>
           /// Desc:申请原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQYY {get;set;}

           /// <summary>
           /// Desc:099是否打印
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDY {get;set;}

           /// <summary>
           /// Desc:099打印流水
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? DYLS {get;set;}

    }
}

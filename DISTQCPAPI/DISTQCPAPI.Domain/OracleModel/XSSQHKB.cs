﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSSQHKB
    {
           public XSSQHKB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJG {get;set;}

           /// <summary>
           /// Desc:申请原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQYY {get;set;}

           /// <summary>
           /// Desc:099学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHR {get;set;}

           /// <summary>
           /// Desc:099学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHSJ {get;set;}

           /// <summary>
           /// Desc:099学生学院审核结果 0：待审 1：通过  2：不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHJG {get;set;}

           /// <summary>
           /// Desc:申请教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQJSZGH {get;set;}

           /// <summary>
           /// Desc:099是否打印
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDY {get;set;}

           /// <summary>
           /// Desc:099打印流水
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? DYLS {get;set;}

           /// <summary>
           /// Desc:开课学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYSHR {get;set;}

           /// <summary>
           /// Desc:开课学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYSHSJ {get;set;}

           /// <summary>
           /// Desc:开课学院审核结果  0：待审 1：通过  2：不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYSHJG {get;set;}

           /// <summary>
           /// Desc:学生学院审核意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHYJ {get;set;}

           /// <summary>
           /// Desc:开课学院审核意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYSHYJ {get;set;}

           /// <summary>
           /// Desc:教师审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSHR {get;set;}

           /// <summary>
           /// Desc:教师审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSHSJ {get;set;}

           /// <summary>
           /// Desc:教师审核结果   0：待审 1：通过 2：不通过
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSHJG {get;set;}

           /// <summary>
           /// Desc:教师审核意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSHYJ {get;set;}

           /// <summary>
           /// Desc:教务处审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHR {get;set;}

           /// <summary>
           /// Desc:教务处审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHSJ {get;set;}

           /// <summary>
           /// Desc:教务处审核结果   0：待审 1：通过 2：不通过 3：转入
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHJG {get;set;}

           /// <summary>
           /// Desc:教务处审核意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHYJ {get;set;}

           /// <summary>
           /// Desc:学籍异动类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJYDLX {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSSQJFB
    {
           public XSSQJFB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099加分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JF {get;set;}

           /// <summary>
           /// Desc:099申请人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQR {get;set;}

           /// <summary>
           /// Desc:099申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:099学院审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHJG {get;set;}

           /// <summary>
           /// Desc:099学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHR {get;set;}

           /// <summary>
           /// Desc:099学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHSJ {get;set;}

           /// <summary>
           /// Desc:099教务处审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHJG {get;set;}

           /// <summary>
           /// Desc:099教务处审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHR {get;set;}

           /// <summary>
           /// Desc:099教务处审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSHSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQJFYY {get;set;}

           /// <summary>
           /// Desc:099是否处理
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFCL {get;set;}

           /// <summary>
           /// Desc:099处理时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLSJ {get;set;}

           /// <summary>
           /// Desc:099加分申请人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFCLR {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSWSJFSQKCLB
    {
           public XSWSJFSQKCLB(){


           }
           /// <summary>
           /// Desc:申请课程列表ID  PK
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SQKCLBID {get;set;}

           /// <summary>
           /// Desc:申请ID  FK
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQID {get;set;}

           /// <summary>
           /// Desc:课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:原始成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSCJ {get;set;}

           /// <summary>
           /// Desc:参加比赛类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBSLX {get;set;}

           /// <summary>
           /// Desc:加分值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFZ {get;set;}

           /// <summary>
           /// Desc:加分后成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFHCJ {get;set;}

           /// <summary>
           /// Desc:比赛类型代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BSLXDM {get;set;}

    }
}

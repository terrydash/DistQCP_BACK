﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSXFJDTJB
    {
           public XSXFJDTJB(){


           }
           /// <summary>
           /// Desc:099 学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099 学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099 学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099 绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JD {get;set;}

           /// <summary>
           /// Desc:099 学分要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSG {get;set;}

           /// <summary>
           /// Desc:099专业可选课的选课门次,即专业课程里是否选课为“是”的课程的可选门次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZYKCMS {get;set;}

    }
}

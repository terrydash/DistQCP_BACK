﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSXKBO
    {
           public XSXKBO(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099选上否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSF {get;set;}

           /// <summary>
           /// Desc:099流水号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? LSH {get;set;}

           /// <summary>
           /// Desc:099教材预订
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCYD {get;set;}

           /// <summary>
           /// Desc:099考试时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099重修标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? CXBJ {get;set;}

           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099教室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMC {get;set;}

           /// <summary>
           /// Desc:099缓考标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? HKBJ {get;set;}

           /// <summary>
           /// Desc:099备注2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? BZ2 {get;set;}

           /// <summary>
           /// Desc:099学科类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKLB {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099上课时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ {get;set;}

           /// <summary>
           /// Desc:099选课时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZWH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? QZD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZXX {get;set;}

           /// <summary>
           /// Desc:N99 提前选修标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TQXXBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJBH {get;set;}

           /// <summary>
           /// Desc:N10可报名的轮次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKP {get;set;}

           /// <summary>
           /// Desc:N99补修标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BCXXBJ {get;set;}

           /// <summary>
           /// Desc:099是否可查
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKC {get;set;}

           /// <summary>
           /// Desc:099是否任意选修课
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFRYXXK {get;set;}

           /// <summary>
           /// Desc:N099是否自学重修
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZXCX {get;set;}

           /// <summary>
           /// Desc:N099是否重组班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFCZB {get;set;}

           /// <summary>
           /// Desc:099重考标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKBJ {get;set;}

           /// <summary>
           /// Desc:099志愿
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:099刷选标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXBJ {get;set;}

           /// <summary>
           /// Desc:099第几次重修报名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJCCX {get;set;}

           /// <summary>
           /// Desc:录入人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ1 {get;set;}

           /// <summary>
           /// Desc:099身份备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ_QZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSBH_QZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMC_QZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZWH_QZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? BZ2_QZ {get;set;}

           /// <summary>
           /// Desc:学业预警选课标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYYJBS {get;set;}

           /// <summary>
           /// Desc:选课年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKNJ {get;set;}

           /// <summary>
           /// Desc:选课专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKZYDM {get;set;}

    }
}

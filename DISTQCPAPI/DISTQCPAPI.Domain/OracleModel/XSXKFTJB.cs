﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSXKFTJB
    {
           public XSXKFTJB(){


           }
           /// <summary>
           /// Desc:099年度
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short ND {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FY1 {get;set;}

           /// <summary>
           /// Desc:099费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FY2 {get;set;}

           /// <summary>
           /// Desc:099费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FY3 {get;set;}

           /// <summary>
           /// Desc:099累计缴费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? LJJE {get;set;}

    }
}

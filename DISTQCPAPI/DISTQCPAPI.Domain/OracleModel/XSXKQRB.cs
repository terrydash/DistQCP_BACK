﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSXKQRB
    {
           public XSXKQRB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QRSJ {get;set;}

           /// <summary>
           /// Desc:结束学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXN {get;set;}

           /// <summary>
           /// Desc:结束学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXQ {get;set;}

           /// <summary>
           /// Desc:反馈意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FKYJ {get;set;}

           /// <summary>
           /// Desc:审核意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHYJ {get;set;}

           /// <summary>
           /// Desc:审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJ {get;set;}

           /// <summary>
           /// Desc:是否有疑问
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYYW {get;set;}

    }
}

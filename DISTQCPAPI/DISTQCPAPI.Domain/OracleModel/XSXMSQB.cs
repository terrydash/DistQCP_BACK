﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSXMSQB
    {
           public XSXMSQB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:项目课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMKH {get;set;}

           /// <summary>
           /// Desc:学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:申请理由
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQLY {get;set;}

           /// <summary>
           /// Desc:教师意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSYJ {get;set;}

           /// <summary>
           /// Desc:是否接受
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJS {get;set;}

           /// <summary>
           /// Desc:申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQRQ {get;set;}

           /// <summary>
           /// Desc:成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ {get;set;}

    }
}

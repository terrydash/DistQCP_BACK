﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSXQPMJGB
    {
           public XSXQPMJGB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099平均学分绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? PJXFJD {get;set;}

           /// <summary>
           /// Desc:099名次1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? MC1 {get;set;}

           /// <summary>
           /// Desc:099学分绩点总和
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? XFJDZH {get;set;}

           /// <summary>
           /// Desc:099名次2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? MC2 {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099总名次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? ZMC2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XFJQPJF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? MC3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PMFS {get;set;}

           /// <summary>
           /// Desc:099不及格学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BJGXF {get;set;}

           /// <summary>
           /// Desc:099学习质量分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XXZLF {get;set;}

           /// <summary>
           /// Desc:099学习质量分排名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XXZLFMC {get;set;}

           /// <summary>
           /// Desc:099课程总绩点数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? KCZJDS {get;set;}

           /// <summary>
           /// Desc:099课程总绩点数名次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? KCZJDSMC {get;set;}

           /// <summary>
           /// Desc:099加权平均分班级排名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JQPJFBJPM {get;set;}

           /// <summary>
           /// Desc:099统计时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJSJ {get;set;}

           /// <summary>
           /// Desc:099统计人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJR {get;set;}

           /// <summary>
           /// Desc:099平均学分绩点（出国用）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? PJXFJDCGY {get;set;}

           /// <summary>
           /// Desc:099名次1（出国用）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? CGYMC1 {get;set;}

           /// <summary>
           /// Desc:099百分制平均学分绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BFZPJXFJD {get;set;}

           /// <summary>
           /// Desc:算数平均分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSPJF {get;set;}

           /// <summary>
           /// Desc:算数平均分专业排名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSPJFZYPM {get;set;}

           /// <summary>
           /// Desc:算数平均分班级排名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSPJFBJPM {get;set;}

    }
}

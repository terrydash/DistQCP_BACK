﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSXTZGXXB
    {
           public XSXTZGXXB(){


           }
           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:辅修专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXZY {get;set;}

           /// <summary>
           /// Desc:辅修学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXXY {get;set;}

           /// <summary>
           /// Desc:学院审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSH {get;set;}

           /// <summary>
           /// Desc:学校审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXSH {get;set;}

           /// <summary>
           /// Desc:学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:面象年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short MXNJ {get;set;}

           /// <summary>
           /// Desc:标识
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BS {get;set;}

    }
}

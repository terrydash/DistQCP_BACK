﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSXWKCB
    {
           public XSXWKCB(){


           }
           /// <summary>
           /// Desc:099学号 
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ {get;set;}

           /// <summary>
           /// Desc:099补考成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKCJ {get;set;}

           /// <summary>
           /// Desc:099重修成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXCJ {get;set;}

           /// <summary>
           /// Desc:099组合代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MKZHDM {get;set;}

           /// <summary>
           /// Desc:099组合名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MKZHMC {get;set;}

           /// <summary>
           /// Desc:099辅修标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXBJ {get;set;}

    }
}

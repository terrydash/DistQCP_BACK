﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSYJSZTJB
    {
           public XSYJSZTJB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099预警类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YJLB {get;set;}

           /// <summary>
           /// Desc:099条件类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJLX {get;set;}

           /// <summary>
           /// Desc:099条件内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJNR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJGX {get;set;}

           /// <summary>
           /// Desc:099条件级别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJJB {get;set;}

           /// <summary>
           /// Desc:099是否统计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTJ {get;set;}

    }
}

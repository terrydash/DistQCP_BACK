﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSYJTJB
    {
           public XSYJTJB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099总学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXF {get;set;}

           /// <summary>
           /// Desc:099不及格学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJGXF {get;set;}

           /// <summary>
           /// Desc:099获得学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HDXF {get;set;}

           /// <summary>
           /// Desc:099预警类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YJLX {get;set;}

           /// <summary>
           /// Desc:099统计类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJLX {get;set;}

           /// <summary>
           /// Desc:099统计级别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJJB {get;set;}

           /// <summary>
           /// Desc:099是否满足条件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFMZTJ {get;set;}

    }
}

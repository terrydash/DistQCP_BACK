﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSYXPJBLSZB
    {
           public XSYXPJBLSZB(){


           }
           /// <summary>
           /// Desc:099比率
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XSBL {get;set;}

           /// <summary>
           /// Desc:099优秀比率
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YXBL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? THBL {get;set;}

    }
}

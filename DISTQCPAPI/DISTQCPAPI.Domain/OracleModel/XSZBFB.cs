﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSZBFB
    {
           public XSZBFB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal BBXH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBHXSZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBQXSZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099教务处确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCQR {get;set;}

           /// <summary>
           /// Desc:099学院确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYQR {get;set;}

           /// <summary>
           /// Desc:099发证日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZRQ {get;set;}

           /// <summary>
           /// Desc:099打印学生证的备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSZBZ {get;set;}

    }
}

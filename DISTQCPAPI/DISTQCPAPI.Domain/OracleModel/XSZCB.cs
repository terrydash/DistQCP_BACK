﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSZCB
    {
           public XSZCB(){


           }
           /// <summary>
           /// Desc:001学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:002姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:003行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:004专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:005年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:006学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:007学年学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XNXQ {get;set;}

           /// <summary>
           /// Desc:008是否注册
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZC {get;set;}

           /// <summary>
           /// Desc:099注册时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZCSJ {get;set;}

           /// <summary>
           /// Desc:099是否报到
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBD {get;set;}

           /// <summary>
           /// Desc:099缴费情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFQK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH1 {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH2 {get;set;}

           /// <summary>
           /// Desc:099报到情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BDQK {get;set;}

           /// <summary>
           /// Desc:099备注信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZXX {get;set;}

           /// <summary>
           /// Desc:099是否缴费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJF {get;set;}

           /// <summary>
           /// Desc:099是否有绿色通道
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFLSTD {get;set;}

           /// <summary>
           /// Desc:099学生档案是否接受
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSDASFJS {get;set;}

           /// <summary>
           /// Desc:099政治面貌代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZMM {get;set;}

           /// <summary>
           /// Desc:099是否户口迁移
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HKSFQY {get;set;}

           /// <summary>
           /// Desc:099操作人及时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZR {get;set;}

    }
}

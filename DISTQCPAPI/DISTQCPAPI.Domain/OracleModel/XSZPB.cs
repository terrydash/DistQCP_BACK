﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSZPB
    {
           public XSZPB(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099照片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public byte[] ZP {get;set;}

    }
}

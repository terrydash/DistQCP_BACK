﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSZSBSXB
    {
           public XSZSBSXB(){


           }
           /// <summary>
           /// Desc:001学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:002专升本学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZSB {get;set;}

    }
}

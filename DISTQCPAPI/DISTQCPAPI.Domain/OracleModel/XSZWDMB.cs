﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XSZWDMB
    {
           public XSZWDMB(){


           }
           /// <summary>
           /// Desc:001职务代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XSZWDM {get;set;}

           /// <summary>
           /// Desc:002职务名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XSZWMC {get;set;}

    }
}

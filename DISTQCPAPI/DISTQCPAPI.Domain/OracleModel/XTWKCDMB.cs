﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XTWKCDMB
    {
           public XTWKCDMB(){


           }
           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:课程英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCYWMC {get;set;}

    }
}

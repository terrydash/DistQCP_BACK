﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XTWSFXMDMB
    {
           public XTWSFXMDMB(){


           }
           /// <summary>
           /// Desc:099收费项目代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SFXMDM {get;set;}

           /// <summary>
           /// Desc:099收费项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXMMC {get;set;}

           /// <summary>
           /// Desc:099收费项目编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXMBM {get;set;}

           /// <summary>
           /// Desc:099收费项目描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXMMS {get;set;}

           /// <summary>
           /// Desc:099收费项目标准
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXMBZ {get;set;}

           /// <summary>
           /// Desc:099计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLDW {get;set;}

    }
}

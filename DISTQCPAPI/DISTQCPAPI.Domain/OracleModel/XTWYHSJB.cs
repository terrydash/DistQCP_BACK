﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XTWYHSJB
    {
           public XTWYHSJB(){


           }
           /// <summary>
           /// Desc:09流水号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LSH {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099收费项目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXMDM {get;set;}

           /// <summary>
           /// Desc:099收费金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SFJE {get;set;}

           /// <summary>
           /// Desc:099收费日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFRQ {get;set;}

           /// <summary>
           /// Desc:099操作人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZR {get;set;}

           /// <summary>
           /// Desc:099缴费编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFBM {get;set;}

           /// <summary>
           /// Desc:099缴款人形式；其值为“集体”或“个人”。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKRXS {get;set;}

           /// <summary>
           /// Desc:099数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SL {get;set;}

           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099班级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:099缴款人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKR {get;set;}

           /// <summary>
           /// Desc:099收费类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFLX {get;set;}

           /// <summary>
           /// Desc:099收费日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZSFRQ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099是否打印
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDY {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XWDMB
    {
           public XWDMB(){


           }
           /// <summary>
           /// Desc:学位代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XWDM {get;set;}

           /// <summary>
           /// Desc:学位名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWMC {get;set;}

    }
}

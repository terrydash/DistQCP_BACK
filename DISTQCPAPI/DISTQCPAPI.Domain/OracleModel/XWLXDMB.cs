﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XWLXDMB
    {
           public XWLXDMB(){


           }
           /// <summary>
           /// Desc:001学位类型代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XWLXDM {get;set;}

           /// <summary>
           /// Desc:002学位类型名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWLXMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWYWMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XWSHZYFXSZB
    {
           public XWSHZYFXSZB(){


           }
           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099专业方向代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYFXDM {get;set;}

           /// <summary>
           /// Desc:099课程类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLX {get;set;}

           /// <summary>
           /// Desc:099计算结果种类
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ1 {get;set;}

           /// <summary>
           /// Desc:099分值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJ1FS {get;set;}

           /// <summary>
           /// Desc:099成绩要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJYQ {get;set;}

           /// <summary>
           /// Desc:099课程性质1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ1 {get;set;}

           /// <summary>
           /// Desc:099学分1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF1 {get;set;}

           /// <summary>
           /// Desc:099课程性质2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ2 {get;set;}

           /// <summary>
           /// Desc:099学分2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF2 {get;set;}

           /// <summary>
           /// Desc:099课程类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLB {get;set;}

           /// <summary>
           /// Desc:099学分3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF3 {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099不及格不计算
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJGBJS {get;set;}

    }
}

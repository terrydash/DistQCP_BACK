﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XXKXZ
    {
           public XXKXZ(){


           }
           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099班级代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJDM {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099选课性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKXZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZXF {get;set;}

           /// <summary>
           /// Desc:新生研讨课程限制门次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKXZ_XSYT {get;set;}

           /// <summary>
           /// Desc:通识选修课程限制门次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKXZ_TSXX {get;set;}

           /// <summary>
           /// Desc:新生研讨课程选课学分限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZXF_XSYT {get;set;}

           /// <summary>
           /// Desc:通识选修课程选课学分限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZXF_TSXX {get;set;}

    }
}

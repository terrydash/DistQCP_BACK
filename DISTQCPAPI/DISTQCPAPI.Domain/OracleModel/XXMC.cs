﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XXMC
    {
           public XXMC(){


           }
           /// <summary>
           /// Desc:N01  学校名称 
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string xxmc {get;set;}

           /// <summary>
           /// Desc:N01  选课学年 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:N01  选课学期 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:Nxy  容量/计划比 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZXRS {get;set;}

           /// <summary>
           /// Desc:Nxy  开课最低 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KKZD {get;set;}

           /// <summary>
           /// Desc:Nxy  最低选课学分 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZDXKXF {get;set;}

           /// <summary>
           /// Desc:Nxy  最低获得学分 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZDHDXF {get;set;}

           /// <summary>
           /// Desc:N06  成绩录入学年 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJXN {get;set;}

           /// <summary>
           /// Desc:N06  成绩录入学期 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJXQ {get;set;}

           /// <summary>
           /// Desc:N07  教材使用部门 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXXX {get;set;}

           /// <summary>
           /// Desc:N01  当前学年 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DQXN {get;set;}

           /// <summary>
           /// Desc:N01  当前学期 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DQXQ {get;set;}

           /// <summary>
           /// Desc:Nxx  机房容量 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JFRL {get;set;}

           /// <summary>
           /// Desc:Nxy  评价信息 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJXX {get;set;}

           /// <summary>
           /// Desc:Nxx  导论课开关 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DLKKG {get;set;}

           /// <summary>
           /// Desc:Nxy  余量控制，0不显示，1显示 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YLKZ {get;set;}

           /// <summary>
           /// Desc:Nxx  预选时间1 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXXKSJ1 {get;set;}

           /// <summary>
           /// Desc:Nxx  预选时间2 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXXKSJ2 {get;set;}

           /// <summary>
           /// Desc:Nxx  在线人数控制 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZXRSKZ {get;set;}

           /// <summary>
           /// Desc:Nxx  内部人数控制 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? NBRSKZ {get;set;}

           /// <summary>
           /// Desc:Nxx  统计开关 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? TJKG {get;set;}

           /// <summary>
           /// Desc:Nxx  开课部门，按系1，反之 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKBM {get;set;}

           /// <summary>
           /// Desc:N05  选课类别，1全部可选，0只选校选课 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKLB {get;set;}

           /// <summary>
           /// Desc:Nxx  节次，上午节次，4或5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? JC {get;set;}

           /// <summary>
           /// Desc:Nxy
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJYXQSSJ {get;set;}

           /// <summary>
           /// Desc:Nxy
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJYXJSSJ {get;set;}

           /// <summary>
           /// Desc:N01  起始周 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QSZ {get;set;}

           /// <summary>
           /// Desc:N01  结束周 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSZ {get;set;}

           /// <summary>
           /// Desc:N04  课表备注，1显示，0不显示 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KBBZ {get;set;}

           /// <summary>
           /// Desc:N01  学校注册码，需正方软件股份有限公司提供 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SN {get;set;}

           /// <summary>
           /// Desc:N01  数据库用户名 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YHM {get;set;}

           /// <summary>
           /// Desc:N01  英文简称 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWJC {get;set;}

           /// <summary>
           /// Desc:Nxx  课表类别，0，1 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? KBLB {get;set;}

           /// <summary>
           /// Desc:Nxx  可以使用功能，1显示，0不显示 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KYSYGN {get;set;}

           /// <summary>
           /// Desc:Nxx  任务，任务下到班级bj,任务下到专业zy 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RW {get;set;}

           /// <summary>
           /// Desc:N06  绩点增量。如0,0.5等 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JDZL {get;set;}

           /// <summary>
           /// Desc:N06  成绩类别，1只录总评成绩，0全部可以录 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLB {get;set;}

           /// <summary>
           /// Desc:N06  网上成绩输出时统计成绩，按总评成绩设为zscj，按期末成绩qmcj 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJCJ {get;set;}

           /// <summary>
           /// Desc:N04  智能排课-》板块课程安排-》各板块教师 教室安排：板块中有多个时间段时是否分时间段排场地，如果要分时间段排场地使用xqj，反之空 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKSJ {get;set;}

           /// <summary>
           /// Desc:Nxy  
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXJSKG {get;set;}

           /// <summary>
           /// Desc:N03  落实任务时教师从jsprb（师资管理-》教师信息管理-》教师资格聘任）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLS {get;set;}

           /// <summary>
           /// Desc:N03  落实任务时教师最大周学时。如：5。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? JSMZZDXS {get;set;}

           /// <summary>
           /// Desc:N03  落实任务时教师最多可任课的门数。如：5。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSZDRKMS {get;set;}

           /// <summary>
           /// Desc:Nxy
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXXMQSSJ {get;set;}

           /// <summary>
           /// Desc:Nxy
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXXMJSSJ {get;set;}

           /// <summary>
           /// Desc:N03  计划任务下达中拆合教学班可用，教学处进行合班操作。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JWCLS {get;set;}

           /// <summary>
           /// Desc:N09  设置每学分费用方式,1:按课程库学分设置；2：按年级专业课程学分设置；3：按年级专业设置；其他统一设置学分 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XFTJ {get;set;}

           /// <summary>
           /// Desc:N04  课表是否显示具体节次 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCBZ {get;set;}

           /// <summary>
           /// Desc:N06  成绩管理-》查询、打印：学院用户能否查询其他学院学生的成绩。1：不能；0：能。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJCXXYKZ {get;set;}

           /// <summary>
           /// Desc:N09  收费控制，用在web系统登录。根据"学生收费表"和"银行实交表"的 应收费用 和 实收费用 的差来判断。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKZ {get;set;}

           /// <summary>
           /// Desc:Nxx
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYJHBZ {get;set;}

           /// <summary>
           /// Desc:Nxy  最高选课学分 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZGXKXF {get;set;}

           /// <summary>
           /// Desc:N04  推荐课表查询打印时，板块课程的教室、教师是否显示 。是：显示；否：不显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKJS {get;set;}

           /// <summary>
           /// Desc:N04  课表字体大小，值越大字体越大 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KBFONTSIZE {get;set;}

           /// <summary>
           /// Desc:N06  当学生重修不跟任何教学班时，重修记载次数。1：只计最高一次，0：计历次（针对功能:成绩综合处理-〉重修） 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXJZFS {get;set;}

           /// <summary>
           /// Desc:N03  公选课通用名 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXKTYM {get;set;}

           /// <summary>
           /// Desc:Nxx  财务控制注册学籍修改。1为可修改 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CWZCKZXGXJ {get;set;}

           /// <summary>
           /// Desc:N10  评价学生超过班级人数的百分之几时不再控制选课及查询。如80%则填80 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJBL {get;set;}

           /// <summary>
           /// Desc:N06  高水平运动员成绩比例设置。例如：50,40,10 表示课程卷面成绩占50% ,比赛成绩占40%,训练成绩占10%
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GSPYDYCJSZ {get;set;}

           /// <summary>
           /// Desc:N03  计划安排时是否可修改计划继承的信息。1：表示不可修改 ；0：表示可修改
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXGJHXX {get;set;}

           /// <summary>
           /// Desc:N05  公选课任务安排控制。1：表示学院只录入老师；0：学院什么也不能做
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXKKZ {get;set;}

           /// <summary>
           /// Desc:N05  学院用户能否在Cs端增加网上报名记录。1：可以，0：不可以。针对功能：选课管理-〉学生网上报名 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSBMZJ {get;set;}

           /// <summary>
           /// Desc:N03  预选教师时，安排预选教师如果需要教师申请，教师想上课申请学期。例如：2004-200511（学年学期开关状态） 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXSKXQ {get;set;}

           /// <summary>
           /// Desc:N03  板块课安排时1表示体育板块按性别显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJXS {get;set;}

           /// <summary>
           /// Desc:Nxx  计划  任务安排时,1表示 周学时为x-y只需安排x部份
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFAPSYK {get;set;}

           /// <summary>
           /// Desc:N04  打印教室借用单时，1表示别一种格式：显示具体时间;2:表示具体时间和周次均显示。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJYD {get;set;}

           /// <summary>
           /// Desc:N12  网上校选课申请学期。例如：2004-200511（第1-9位：学年，第10位：学期，第11位：开关状态） 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXKSQXQ {get;set;}

           /// <summary>
           /// Desc:N05  是否处于公选课预选状态。1：是；0：否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXKYX {get;set;}

           /// <summary>
           /// Desc:Nxy  实验选课时显示的起始页面的格式。1：需要选具体的项目时间，0：不需要选具体的项目。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXKGS {get;set;}

           /// <summary>
           /// Desc:N12  web页中是否控制半小时必须重新登陆。1：需要，0：不需要。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEBBXSKZ {get;set;}

           /// <summary>
           /// Desc:N12  Web登录时是否检测服务器Ip。0：不检测，1：检测。（默认检测） 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEBIPKZ {get;set;}

           /// <summary>
           /// Desc:N12 考试查询开关，第1位表示学生集中考试，第2位表示学生补考,第3位表示学生分散考试;第4位表示老师集中考试,第5位表示老师分散考试。1：可以查询，0：不可以。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSCXKG {get;set;}

           /// <summary>
           /// Desc:N05  板块选课设定，两位表示。第一位为0：不可以跨板块，为1：可以；第二位为0：不能跨等级，为1：高等级可以选低等级,2:低等级可以选高等级,3:任意选 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKXKSD {get;set;}

           /// <summary>
           /// Desc:N12  教师修改个人信息的开关：1:可以修改，0：不能修改。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXGXXKZ {get;set;}

           /// <summary>
           /// Desc:N04  后台打印课表上是否打印课程性质：1:打印。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KB_DYKCXZ {get;set;}

           /// <summary>
           /// Desc:N05  自选课程本专业、跨专业选课、跨年级开关(安徽师大专用)。四位。第1位，1:本专业可选，0：不可选；第2位，1：跨专业可选，0：不可选；第3位，1：高年级选低年级可选，0：不可选；第4位，1：低年级选高年级可选，0：不可选 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KG_KNJXK {get;set;}

           /// <summary>
           /// Desc:N06  保存时计算总评条件：1:每次保存时不管总评是否为空都要重新计算，0：保存时总评有值则不重新总评。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KG_ZPJSTJ {get;set;}

           /// <summary>
           /// Desc:N12  xsxk.aspx进入时默认显示页面：1:显示已选课程，0：显示本专业选课。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJ_XSXKJM {get;set;}

           /// <summary>
           /// Desc:N04  1：表示晚上显示的格子大点；2：为A3格式；3：晚上没有显示；4：上课时间为5段，每段均匀；5：B5，上部分为课表，下部分为任务 ；6：左边部分为课表，右边部分为任务;7：左边部分为课表，右边部分为上课具体时间;9：A4，上部分为课表，下部分为任务;
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KBGS {get;set;}

           /// <summary>
           /// Desc:N04  排课时是否判断实践课的冲突。1：判断，0：不判断。必须先定实践课的起止周、教师、教室，否则冲突判断无效 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KG_PDSJCT {get;set;}

           /// <summary>
           /// Desc:N05  网上xsxk.aspx中，1：表示可跨年级选课，0：不能 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNJXK {get;set;}

           /// <summary>
           /// Desc:N05  网上xsxk.aspx中，1：表示可跨专业选课，0：不能 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KZYXK {get;set;}

           /// <summary>
           /// Desc:N04  输入系统默认允许冲突的教师职工号，多个时用分开，如：123456,123457 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCTJS {get;set;}

           /// <summary>
           /// Desc:Nxx  实验课（0-y的课程）是否按理论排，1：按理论排，0：不按理论排 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYKSFALLP {get;set;}

           /// <summary>
           /// Desc:N04  课表备注内容，换行分隔符#13 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KBBZNR {get;set;}

           /// <summary>
           /// Desc:N10  多个教师是否可以只评一个教师。1：可以，0：不可以。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJ_DGJS {get;set;}

           /// <summary>
           /// Desc:N04  选课管理-》查询打印-》点名册打印：点名册备注 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBCBZ {get;set;}

           /// <summary>
           /// Desc:N04  是否单节排课 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJPK {get;set;}

           /// <summary>
           /// Desc:N07  教材从任务转入计划密码 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCGXMM {get;set;}

           /// <summary>
           /// Desc:N04  课表输出到excel的格式。默认为现有格式；1：为中国矿业大学提供格式；2：为云南师范大学提供的推荐课表格式；3：为河南大学提供的场地课表格式；4：吉林医药提供的推荐课表;7：军事交通学院推荐课表;8:江西中医学院课表;9:成都信息工程学院银杏酒店管理学院;14：中国地质大学推荐课表;
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TOEXCELGS {get;set;}

           /// <summary>
           /// Desc:N04  换教师是否允许冲突。1:允许冲突 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJSYXCT {get;set;}

           /// <summary>
           /// Desc:N06  试卷分析统计成绩，按总评成绩设为zscj，按期末成绩qmcj 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJCJ_SJFX {get;set;}

           /// <summary>
           /// Desc:N05  网上选课时学生可选任务。0：该课程任务全部可选；1：只能选所在专业（班级）合班组成专业（班级）的任务 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XK_KXRW {get;set;}

           /// <summary>
           /// Desc:N12  网上重修报名可报课程控制。0：全部可报；1：只能报计划中的课程；2：只能报计划中有并且任务中有的课程；3：只能报计划中有至今为通过的课程（学分必修大于原来的学分，考核方式必须一样）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBM_KBKC {get;set;}

           /// <summary>
           /// Desc:N03  学院可否修改教学计划。0：不可以；1：可以 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXJH_XYXGJH {get;set;}

           /// <summary>
           /// Desc:N06  成绩管理-》成绩录入-》选课成绩录入：成绩是否需要正反各录一次。1：需要；0：不需要 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJSFLDC {get;set;}

           /// <summary>
           /// Desc:N12  网上教师查看选课情况时是否可以增加删除名单（针对js_xkqk_gcxy.aspx）。0：不可以；1：可以 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KG_WSJSJMD {get;set;}

           /// <summary>
           /// Desc:N12  网上是否打印期中成绩。0：不打印；1：打印 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KG_WSDYQZ {get;set;}

           /// <summary>
           /// Desc:N06  cs端成绩录入是否控制学年学期（同网上成绩录入学期）。0：不控制；1：控制 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KG_CSCJKZ {get;set;}

           /// <summary>
           /// Desc:N06成绩单确认学院用户查询 1：学生所在学院 2：教师所在学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDQR {get;set;}

           /// <summary>
           /// Desc:N06  学院录入成绩。1：学生所在学院；2：教师所在学院 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYRLCJ {get;set;}

           /// <summary>
           /// Desc:Nxx  排课时实验部分是否按班级分成多个教学班（选教室时人数按"单个实验室容量"计）1：是0：否 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PK_SYSFKZRL {get;set;}

           /// <summary>
           /// Desc:N04  打印或输出课表时是否显示五天（不显示星期六、星期天）1：是0：否 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KB_SFWT {get;set;}

           /// <summary>
           /// Desc:Nxy  重修报名起始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBMQSSJ {get;set;}

           /// <summary>
           /// Desc:Nxy  重修报名结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBMJSSJ {get;set;}

           /// <summary>
           /// Desc:N05  重修选课控制，2位。第1位：是否允许冲突；第2位：容量已满时是否允许选课。1：允许0：不允许，默认不允许 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBM_SFYXCT {get;set;}

           /// <summary>
           /// Desc:N12  网上选课名单、成绩录入名单的排序字段。1：xh0：系统默认值(zymc,xh或xzb,xh) 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSMD_PXZD {get;set;}

           /// <summary>
           /// Desc:N06  缓考成绩 1：表示进期末成绩否则进总评成绩 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HKJLZT {get;set;}

           /// <summary>
           /// Desc:N12  全校性选修课（连接xf_xsqxxxk.aspx时）"上课时间"下拉框中的显示信息。1：显示星期几；0：上课时间。默认同0 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXKXQJ {get;set;}

           /// <summary>
           /// Desc:N06  等级考试免修标准。1：表示成绩大于等于60为免修；否则不控制 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKSMXSZ {get;set;}

           /// <summary>
           /// Desc:N14  1：同一教师同一课程不同教学班的教学日历不同，0：反之 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXRL_XKKH {get;set;}

           /// <summary>
           /// Desc:N06  缓考成绩由谁录入。1：开课学院；0：学生所在学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HKCJSLKZ {get;set;}

           /// <summary>
           /// Desc:N06  成绩管理-》成绩综合处理-》补考：1表示补考成绩由开课学院输入，否则由学生所在学院录入 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKCJSLKZ {get;set;}

           /// <summary>
           /// Desc:N06  重修成绩由谁输入。1：开课学院；0：学生所在学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXCJSLKZ {get;set;}

           /// <summary>
           /// Desc:N06  单个学生成绩修改由谁录入。1：开课学院；0：学生所在学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DGXSCJSLKZ {get;set;}

           /// <summary>
           /// Desc:N05  点名册打印控制。1：表示教师所在学院打印，否则由开课学院打印。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DMCDYKZ {get;set;}

           /// <summary>
           /// Desc:N12  重修报名时，选择课程时默认弹出窗口。1:跟班重修2:单开版重修，0：不弹出 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBM_MRXZ {get;set;}

           /// <summary>
           /// Desc:Nxx  教室上限，根据放大系数后计算 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSX {get;set;}

           /// <summary>
           /// Desc:N04  调（停）课是否必须与教学任务关联。0：可以不关联，1：必须关联 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TTKGL {get;set;}

           /// <summary>
           /// Desc:N06  例如：*1.2+5。表示 课程成绩*1.2+系数 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GSPYDYSZ {get;set;}

           /// <summary>
           /// Desc:N10  学生教学评价方式，0：评价开关有条件放开，并且学生所上课程成绩已提交；1：评价开关无条件放开 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSJXPJFS {get;set;}

           /// <summary>
           /// Desc:N05  预选教师时，安排预选教师是否需要教师申请。1:表示不需要；0：需要 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLJSXSKXXB {get;set;}

           /// <summary>
           /// Desc:N06  成绩管理-》查询、打印-》个人学年成绩单：1:表示补考过了绩点按六十分计算，2：表示绩点分按补考与正考成绩最大的成绩计算 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKSFYJD {get;set;}

           /// <summary>
           /// Desc:N07  1:表示下任务一定要填教材 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XRWSFYJC {get;set;}

           /// <summary>
           /// Desc:N12  网上成绩录入控制项，五位。第1位为1:有空成绩时不能提交；第2位为1：后台“学生免缓缺处理”的信息网上不能修改；第3位为1：网上教师不能录缓考，第4位为1：录了期末才能录总评，第五位为1：比例非零的列有空值不能提交。为0时反之 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WS_CJLRKZ {get;set;}

           /// <summary>
           /// Desc:N14  教师信息维护：1:表示学院可增加教师信息 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXSFKZJJSXX {get;set;}

           /// <summary>
           /// Desc:N12  学生当前状态为未注册，能否进行的3种网上操作。第1位：网上登陆；第2位：网上选课；第3位：网上查成绩。1能，0不能 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKZ_SFZC {get;set;}

           /// <summary>
           /// Desc:Nxx  
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSXS {get;set;}

           /// <summary>
           /// Desc:Nxy 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? MZJJK {get;set;}

           /// <summary>
           /// Desc:N06  重修次数。在 成绩管理-》功能重修成绩报名统计 中 重修超过这个次数后不能再重修。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CXCS {get;set;}

           /// <summary>
           /// Desc:N12  网上重修报名。1：表示重修报名取重修报名统计表 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBMSZ {get;set;}

           /// <summary>
           /// Desc:N05  重修报名统计设置。1：表示补考后才可重修 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBMTJSZ {get;set;}

           /// <summary>
           /// Desc:N13  毕业生管理-》毕业成绩总表：打印时补考名称需要显示成什么就输入什么 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMCXS {get;set;}

           /// <summary>
           /// Desc:N03  学院用户能否选其他学院的教师，1:可以，0：不可以 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KG_XYXJS {get;set;}

           /// <summary>
           /// Desc:N03  教学计划管理-》课程库管理：学院是否可增加课程。1：表示可以增加 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSFKZJ_KCDM {get;set;}

           /// <summary>
           /// Desc:N04  "换教师、起止周、周次"功能中授权后学院拥有功能设置。1：表示各项皆可操作，0：表示只能查询及换教师 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJS_XYGN {get;set;}

           /// <summary>
           /// Desc:N03  学院是否可增加公选课任务。1：表示可以增加 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYKZJ_GXKRW {get;set;}

           /// <summary>
           /// Desc:Nxy  开课学院教材修改。1--可改0--不可改 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXYJCXG {get;set;}

           /// <summary>
           /// Desc:N07  审核教材 1：表示审核通过的才转入教材计划 0：所有有教材的任务都转入教材计划 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJC {get;set;}

           /// <summary>
           /// Desc:N12  "学生选课"界面：查看课表时是否提示应选而未选的课程。1：提示，0：不提示。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TS_XSKB {get;set;}

           /// <summary>
           /// Desc:N13  毕业生管理：毕业审核是否结束，如果结束了学院将不能审核。1：结束；0：反之 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSHJS {get;set;}

           /// <summary>
           /// Desc:N13  毕业生管理：学位审核是否结束，如果结束了学院将不能审核。1：结束；0：反之 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWSHJS {get;set;}

           /// <summary>
           /// Desc:N05  "学生选课"：任务取自jxrwb还是jxrwb_xyview1。0：jxrwb；1：jxrwb_xyview1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XK_XSRW {get;set;}

           /// <summary>
           /// Desc:N04  当人数大于某个值时的放大系数。例如：120,1.02 表示任务人数超过120时自动排课放大系数变为1.02
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FDXS {get;set;}

           /// <summary>
           /// Desc:N01  查询部门密码的权限。设置为1：只有jwc01可查询；否则有该功能的用户都可以查
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YHSFYXMMCX {get;set;}

           /// <summary>
           /// Desc:N03  是否要自动计算学时（计划，原始任务，教学任务）：1：表示不计算；0：表示计算 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYZDJSXS {get;set;}

           /// <summary>
           /// Desc:N12  Rlymsq.aspx（容量已满申请）中学生可以选择哪些课程性质的课程,格式如：必修课,专业选修课 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RLYMSQ_KCXZ {get;set;}

           /// <summary>
           /// Desc:N04  后台打印课表时是否显示专业方向,1：显示，0：不显示。默认不显示 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KB_ZYFX {get;set;}

           /// <summary>
           /// Desc:N12  教师是否可查询其他教师课表。1：不可查；0：可查
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSKBCXXZ {get;set;}

           /// <summary>
           /// Desc:N12  教师网上录入成绩是否显示期中。1：不显示；0：显示 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLRSFXSQZ {get;set;}

           /// <summary>
           /// Desc:N04  打印教师课表时。1：按开课学院打印；0：教师所在学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DY_JSKBXZ {get;set;}

           /// <summary>
           /// Desc:N12  网上教师是否可查其他教师教学任务。1：不可查；0：可查 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSCKJXRWXZ {get;set;}

           /// <summary>
           /// Desc:N01  每学年开课的学期数。3：3学期，默认为2学期。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXNXQS {get;set;}

           /// <summary>
           /// Desc:N07  教材数量不足时是否虚出库；1:表示是；0：表示不是 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXCK {get;set;}

           /// <summary>
           /// Desc:N03  1:表示可以按班级，专业，教学班打印；0：只能按专业打印 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWDY_GS {get;set;}

           /// <summary>
           /// Desc:N09票据格式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJGS {get;set;}

           /// <summary>
           /// Desc:N02  值为1显示根据专业加学生(学号自动生成) 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GJZYJXS {get;set;}

           /// <summary>
           /// Desc:N09  教学计划管理-》专业计划管理：学院可否修改学分费用。1：可以；0：不可以
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSFKXGFY {get;set;}

           /// <summary>
           /// Desc:N09  收费时是否要更新学生信息"是否注册"字段。1：表示更新 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSGXSFZC {get;set;}

           /// <summary>
           /// Desc:N06  逗号前面表示低于此分数为不及格，后面表示不及格率,不符合该条件（大于该不及格率）将不能提交。例如：60,100表示分数低于60的为不及格，不及格率为100% 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJGL {get;set;}

           /// <summary>
           /// Desc:N06  逗号前面表示高于此分数为优秀，后面表示优秀率，不符合该条件（大于该优秀率）将不能提交。例如：85,100表示分数高于85为优秀，优秀率为100% 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXL {get;set;}

           /// <summary>
           /// Desc:Nxy  学生成绩加分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFCJ {get;set;}

           /// <summary>
           /// Desc:N12  根据每学期的注册情况来控制学生网上查成绩（没注册的学期的成绩不能查询）。1能，0不能 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQZC_KZX {get;set;}

           /// <summary>
           /// Desc:N09  1：表示同一专业有不同的收费标准 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYZYBTSFBZ {get;set;}

           /// <summary>
           /// Desc:N04  后台打印教室借用单时晚上显示的具体时间，如：18:30-21:00。可以为空 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJY_WSSJ {get;set;}

           /// <summary>
           /// Desc:N12  学生选课是否允许冲突。1：允许0：不允许，默认不允许 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXK_SFYXCT {get;set;}

           /// <summary>
           /// Desc:Nxx  网上是否单独录缓考（不录补考），1：是，0：否。设为1时，在补考考试-》补考名单生成中生成缓考学生名单，再在成绩综合处理-》生成补考录入名单中分配录入教师 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_BKHK {get;set;}

           /// <summary>
           /// Desc:N12  网上报名时学生是否能修改照片，1：能，0：不能。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSBM_XGZP {get;set;}

           /// <summary>
           /// Desc:N02  1：宁波诺丁汉大学2005级新生注册流程表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSRXBDD_GS {get;set;}

           /// <summary>
           /// Desc:N03  专业标记打上后是否按任务人数限定选课人数。1：是，4：否 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXK_ZYBJ {get;set;}

           /// <summary>
           /// Desc:N05  学籍异动课程调整时，1可以显示学生自己选课的课程 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJYD_XSXKKC {get;set;}

           /// <summary>
           /// Desc:N06  打印个人学年学期成绩单时成绩的取值。1为总评成绩与补考成绩中最大的一个；0：总评成绩 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYGRXNXQCJD {get;set;}

           /// <summary>
           /// Desc:N06  专业排名统计时，设置为1时成绩取总评成绩与补考成绩最大的一个 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PMTJSZ {get;set;}

           /// <summary>
           /// Desc:N03  删除正式教学计划时，设置为1时：删除正式计划时，会自动删除原始教学任务、教学任务； 0：不会删除 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SCJXRW {get;set;}

           /// <summary>
           /// Desc:N03  设置为1时系统自动计算学分，总学时应为8的倍数 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJS_XF {get;set;}

           /// <summary>
           /// Desc:N02  学籍异动监控设置格式。1：为又一种格式 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJJK_GS {get;set;}

           /// <summary>
           /// Desc:N06  选课管理-》选课课程调整-》学籍异动学生课程调整：留级处理时课程成绩最低免修分数线，低于该分数线的课程当前学期不处理掉 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJCLKC {get;set;}

           /// <summary>
           /// Desc:N12  网上用户登陆是否使用登陆名。2位，第1位控制学生，第2为控制教师。1：使用；0：不使用。默认不使用 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSMMXG_DLM {get;set;}

           /// <summary>
           /// Desc:N05  系统设置：更新重修标记。1：按选课课号中的课程代码区分；0：按选课课号中的课程代码+课程名称区分 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DCXBJ_FZZD {get;set;}

           /// <summary>
           /// Desc:Nxy  学生等级考试成绩分数对应，在等级考试成绩中可设置 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSDJ_DYZ {get;set;}

           /// <summary>
           /// Desc:N06  成绩管理-》成绩综合处理：补考、缓考、重修成绩、重修补考录入设置，5位表示。第一位为补考，第二位缓考，第三位重修，第四位补考录入后生成名单是否显示,第五位重修补考。1：可录入或可显示 0反之 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKHKCXLRSZ {get;set;}

           /// <summary>
           /// Desc:N03  下任务实践是否要分开下，1：表示分开下 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XRW_SYRWFKX {get;set;}

           /// <summary>
           /// Desc:N06  补考成绩是否进重修。1:表示进入重修 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKTOCX {get;set;}

           /// <summary>
           /// Desc:N03  更新课程库时同时更新计划，原始任务，任务 第1位课程名称，2:学分,3:总学时,4:讲课学时,5:实验学时,6:上机学时,7:课程性质,8: 开课学院,9:周学时,10:开课系 ,11:考核方式,12:课程实践学时,13:课外学时,14:习题课学时 ,15:课内实践学时,16:课外实践学时 为1修改
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXJHXX {get;set;}

           /// <summary>
           /// Desc:N06  英语4、6等级考试成绩实行710分制的开始学年学期 如2004-20052
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSDJKS_XNXQ {get;set;}

           /// <summary>
           /// Desc:N06  打印学生个人学年学期成绩单时是否要打印重修成绩。 1：表示打印；0：不打印
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYCXCJ_GRXNXQ {get;set;}

           /// <summary>
           /// Desc:N03  下任务时是否要判断总学时，如周学时*起止周<总学时就不能保存 1:表示要判断
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XRW_SFYPDXS {get;set;}

           /// <summary>
           /// Desc:N02  学生修改个人信息，设置年级格式如"2004,2005"意思为2004，2005年级可修改信息，空为当前在校学生
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGXSXX_NJ {get;set;}

           /// <summary>
           /// Desc:N07  教材盘库开关设置，1：为关闭盘库 0：开始盘库
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCPKKGSZ {get;set;}

           /// <summary>
           /// Desc:N07  学生网上单独预订教材的学年学期开关。格式如：2005-200611。前9位是学年，第10位是学期，第11位是开放开关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXJC {get;set;}

           /// <summary>
           /// Desc:N07  学生网上单独预订教材时显示的教材信息。4位分别表示教材名称、作者、出版社、版别。1：显示；0：不不显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXJC_XSZD {get;set;}

           /// <summary>
           /// Desc:N12  网上校选课申请时，课程性质、课程归属、周学时、学分（继承课程库的信息）教师能否修改。1：能；0：不能
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXKSQ_XXJC {get;set;}

           /// <summary>
           /// Desc:N15  考试地点安排时，自动排考场的方法。0：默认；1：ysdx提出，按教室顺序排；2：bjlgdx提出，按楼号优先级、距离等信息排。注：自动排考试时间地点时使用的时方法2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_ZDPKSDD {get;set;}

           /// <summary>
           /// Desc:N14  工作量计算方式 设置为1另一种格式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZL_GS {get;set;}

           /// <summary>
           /// Desc:N14  工作量计算方式 每课时费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? MKSFY {get;set;}

           /// <summary>
           /// Desc:N15  考试返回地点信息以生成考试名单时，是否生成未注册学生的名单。1：生成；0：不生成
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_FHXX {get;set;}

           /// <summary>
           /// Desc:N15  分散考试需要耗的节次，如是2节还是3节，则需填入数字2或3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_FSKSCD {get;set;}

           /// <summary>
           /// Desc:Nxx  课程归属转公选模块代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCGS_ZMKZH {get;set;}

           /// <summary>
           /// Desc:N14  实验室工作量系数设置如：60,300,3,180,5,60,60,60,80,6第一位 y2系数,2:y3系数,3:y4系数,4:y6系数,5:j5系数,6:j6系数,7:G1系数,8:G2系数,9:G21系数,10:G3系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYSGZLXS {get;set;}

           /// <summary>
           /// Desc:Nxx
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJCJSJD {get;set;}

           /// <summary>
           /// Desc:N12  网上预约教室的时间限制，比如规定距离开始使用教室时间7天之内才可提出申请，则填入7。默认无限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXJSJY_RQXZ {get;set;}

           /// <summary>
           /// Desc:N12  网上录入成绩时是否需要有分班级（专业）选择的功能。1：有；0：没有
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_FBJXZ {get;set;}

           /// <summary>
           /// Desc:N15  分散考试安排时间，保存时是否判断考试时间冲突。1：判断；0：不判断。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSKSAP_JCCT {get;set;}

           /// <summary>
           /// Desc:N06  n，成绩录入时间限制，课程考试结束（已系统排定的考试时间为准）->结束后n天。如n为空则表示不根据考试时间来限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_TS {get;set;}

           /// <summary>
           /// Desc:N06  x，网上成绩录入时，学生如果无考试时间地点，将成绩自动记载为x（x请不要超过8个字符，并将该成绩在系统维护-》代码维护-》成绩子系统代码：的对照表中加入）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_WKSDD {get;set;}

           /// <summary>
           /// Desc:N06  网上课程选课和预选时，学生是否只能选本方向的课程。1：是；0：否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSYX_ZYFX {get;set;}

           /// <summary>
           /// Desc:N03  学院录入教学计划时最高学分不能超过学分数 如：170
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KYXSZGXF {get;set;}

           /// <summary>
           /// Desc:Nxx  英语四级考试格式如：50,350 更新学生成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKS_CJFSSD {get;set;}

           /// <summary>
           /// Desc:N12  web系统登陆时，学生因未注册而不能登陆时的提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEBDLTSY_SFZC {get;set;}

           /// <summary>
           /// Desc:Nxx  学生教室预约开关，1为开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSKYYKG {get;set;}

           /// <summary>
           /// Desc:N02  学生信息删除时，设置为1时：删除学生信息时，自动会删除相关学生其他信息； 0：不会删除
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SCXSXX {get;set;}

           /// <summary>
           /// Desc:N09  收费管理-》收费代码维护-》收费结果设置：时是否要加入学生注册表 格式2004-200521 最后一位1为要加入
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZC_XNXQ {get;set;}

           /// <summary>
           /// Desc:N06  学生免缓缺处理学年学期设置开关 格式2004-200521 最后一位1为放开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MHQCL_XNXQ {get;set;}

           /// <summary>
           /// Desc:N06  打印班级成绩单表头
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYCJTT {get;set;}

           /// <summary>
           /// Desc:N15  集中或分散考试安排监考教师时是否按"开课学院安排主监考，学生学院安排副监考"的原则安排。1：是；0：否。如果为9，则由"学生学院安排监考"。注意：如果不是0，则不需要设置"派	监考学院"。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSKS_JKAP {get;set;}

           /// <summary>
           /// Desc:N15  n，分散考试安排监考教师时，考试人数大于n时提示需要安排3个监考
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSKS_JKAPRS {get;set;}

           /// <summary>
           /// Desc:N12  网上成绩输出打印时，是否直接打印: 1（输出excel后直接转成网页）,3(直接输出到网页html打印，格式为默认),4(输出到excel无进程滞留，格式默认)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJZJDY {get;set;}

           /// <summary>
           /// Desc:N03  下任务时学生所在学院下班级，开课学院下老师 1:表示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XRWSFFKX {get;set;}

           /// <summary>
           /// Desc:N01  Cs用户登入时系统提示语 第一位为开关 1：表示开 后面为提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSYHDL_TSY {get;set;}

           /// <summary>
           /// Desc:N12  教师查询课表控制。最后1位为1则实行控制，该字符串前12位为不可查询的学年学期，格式如：2004-2005-1-1，否则不控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSCXKBKZ {get;set;}

           /// <summary>
           /// Desc:N04  "教务处"角色的用户是否能使用"使用部门"不是用户单位的教室。1：能；0：不能。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXCDSYBM_JWC {get;set;}

           /// <summary>
           /// Desc:N03  学生所在专业下任务开关 1：表示开放
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSZXY_XRWKG {get;set;}

           /// <summary>
           /// Desc:N03学院用户可否增加预选课，任务显示不显示设置。格式如:2005-20061000,2005-20062000 前面是学年，学期，预选开关，选课开关 1：表示开，0：表示关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXKSZ_SFYXXKSZ {get;set;}

           /// <summary>
           /// Desc:N12  实验课选课开关1为开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYKXKKG {get;set;}

           /// <summary>
           /// Desc:N12  网上打印成绩格式为2时是否不插入行政班、单独增加行政班一列，主要用于《校选课》成绩单的打印，1为开！
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WS_CJDY_XSBJ {get;set;}

           /// <summary>
           /// Desc:N12  网上打印成绩格式为2 且《行政班》单独占一列时是否显示性别，主要用于《有需要的体育课》成绩单的打印，1为开！
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WS_CJDY_XSXB {get;set;}

           /// <summary>
           /// Desc:N12  网上录入成绩格式时是否显示性别，主要用于《有需要的体育课》成绩的录入，1为开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WS_CJLR_AXBLCJ {get;set;}

           /// <summary>
           /// Desc:N12  网上学生课表查询限制，格式为12005-20061，即开关加学年学期，1为控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WS_XSKBCXXZ {get;set;}

           /// <summary>
           /// Desc:N02  学生分方向时如果一个班有多个方向（不重新组班），关闭分班后班级 设置为1时：关闭
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFFX_KZ {get;set;}

           /// <summary>
           /// Desc:N12  xf_xsqxxxk.aspx中课程归属是否默认为空（显示全部课程归属的）。1：是；0：否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XK_XSKCGS {get;set;}

           /// <summary>
           /// Desc:N12  学生教学评价针对一个教师的各项指标评价全部相同是否允许保存，默认不允许，为1时允许
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSPJXT {get;set;}

           /// <summary>
           /// Desc:N13  打印毕业成绩平均绩点 1：取自统计 成绩管理-》统计分析-》专业排名统计 的结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYPJJD {get;set;}

           /// <summary>
           /// Desc:N03  打印教师个人任务书鼓励语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DY_JSGRRWS {get;set;}

           /// <summary>
           /// Desc:N12  成绩单格式为2时候是否分2排显示，1为开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LLXS {get;set;}

           /// <summary>
           /// Desc:N06  按专业录入成绩时 1：直接进正式库；0：先进临时库
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJJCJB {get;set;}

           /// <summary>
           /// Desc:N06  成绩管理-》查询、打印-》班级成绩登记表1：选中格式为"成绩登记表"时附注的内容值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJFZ {get;set;}

           /// <summary>
           /// Desc:N02  学生信息默认显示 格式如11 第一位是在校，第二位学籍状态。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MRXSXXXX {get;set;}

           /// <summary>
           /// Desc:N02  是否要生成学生密码的明码。1：要，2：不要
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSMM_XSMM {get;set;}

           /// <summary>
           /// Desc:N12  用来确定成绩录入处输出打印的四种格式的选择，8位每种格式占2位，01010101表示格式一、格式二、格式三（试卷分析）、格式四分别选择的是01（即格式一）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GS_CJDY {get;set;}

           /// <summary>
           /// Desc:N12  网上成绩录入处输出打印的格式表头是否需要增加选课课号一栏
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDY_XKKH {get;set;}

           /// <summary>
           /// Desc:N04  打印教师课表时的内容格式。0：系统默认；1：北京理工提出
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DY_JSKBNR {get;set;}

           /// <summary>
           /// Desc:N01  系统的版本日期是否取程序文件的最后修改日期。1：是；0：否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBRQ_XGSJ {get;set;}

           /// <summary>
           /// Desc:N07  设置为1时以教材编号为准 否则以教材名称，作者，出版社，版本为准
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSZJCBH {get;set;}

           /// <summary>
           /// Desc:N07  设置为1时按签协议的学生发书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSSZ {get;set;}

           /// <summary>
           /// Desc:Nxy  实验选项目的轮次和可否改退选，2位。第1位：轮次；第2位：可否改退选。格式如：21，表示第2轮可改退选
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXXMLCTX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYXSSZ {get;set;}

           /// <summary>
           /// Desc:N05 xsxk.aspx时候，是否允许改选（本轮的也不允许改选）。1：是；0：否。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXK_SFYXGX {get;set;}

           /// <summary>
           /// Desc:N07教材管理->教材征订修改 其他校区征订数量发生变化时,该校区征订数量自动增减。输入校区简拼
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJXQ {get;set;}

           /// <summary>
           /// Desc:N02学生报道注册时，更新学生信息中的是否在校，是否注册。为1更新
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXXSXX {get;set;}

           /// <summary>
           /// Desc:N02学生报道注册时，自动分配宿舍号。为1时要分配宿舍号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSZC_SSH {get;set;}

           /// <summary>
           /// Desc:N12  web学生用户在教室预约（xxjsjy.aspx）时，是否只能查看空教室而不能预约。1：是；0：否。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS_JSYUY {get;set;}

           /// <summary>
           /// Desc:N06等级考试成绩非百分制的名称 如：英语四级,英语六级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKSMC_FBFZ {get;set;}

           /// <summary>
           /// Desc:N07  教材管理-》数据统计打印：教材发放名细 的备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JC_JCBZ {get;set;}

           /// <summary>
           /// Desc:N12  网上成绩录入 保存就可打印成绩单开关，1为开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WS_CJ_BCJDY {get;set;}

           /// <summary>
           /// Desc:N12  网上成绩录入 保存后显示成绩核对单单开关，1为开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WS_CJHDD {get;set;}

           /// <summary>
           /// Desc:N12  重修报名页面是否作直考报名用。 1为是；0为否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBM_ZK {get;set;}

           /// <summary>
           /// Desc:N12  转专业申请是否要判断学生成绩排名 1不判断
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZY_SFPDPM {get;set;}

           /// <summary>
           /// Desc:N12  全部选课xsxk.aspx按钮控制 1：表示可点,0 不可点 第一位 快速选课, 2:本专业选课 3:选修课程 4:跨专业选课 5:特殊课程 6:选体育课 7:清空选课 8:已选课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKBTN_KZ {get;set;}

           /// <summary>
           /// Desc:N12  网上xsxk.aspx中，跨专业选课进入时提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KZY_TSY {get;set;}

           /// <summary>
           /// Desc:N12  学生网上查成绩时是否要显示所选学分，获得学分，重修学分。 1：不显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYXSXFTJ {get;set;}

           /// <summary>
           /// Desc:N01  学生管理-》查询统计打印-》打印学生证：学校所在地火车站名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CCQJ {get;set;}

           /// <summary>
           /// Desc:N01  学校所在地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXSZD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRBKXNXQ {get;set;}

           /// <summary>
           /// Desc:N12补考成绩最大值，当补考录入成绩超过bkzdcj时，最大为bkzdcj
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKZDCJ {get;set;}

           /// <summary>
           /// Desc:N02学院房间设置中是否到专业 1：到专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FJTOZY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YXRSB {get;set;}

           /// <summary>
           /// Desc:Nxx  网上重修报名选课时的按扭显示，4位，1表示显示，0不显示。第1位：跟班重修选课；第2位：单开班重修选课；第3位：我要报名；第4位：体育项目重修报名。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBM_ANXS {get;set;}

           /// <summary>
           /// Desc:N13学生所学计划外课程的课程性质归类为 如："校选修课"
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHWXZ {get;set;}

           /// <summary>
           /// Desc:N15  学生网上直考报名时可报的课程。0：全部已排考试的课程；1：学生不及格的计划内课程，并且该课程在学生有空的时间开考。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZKBM_KBKC {get;set;}

           /// <summary>
           /// Desc:N12补考名单生成后，哪些性质可由学生自愿确认是否要参加补考
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMD_KBXZ {get;set;}

           /// <summary>
           /// Desc:N04 角色为"电教中心"的用户在"教学场地调度"中可以借用的教室的"教室类别"。格式如：多媒体,普通教室。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXCDYY_DJZX {get;set;}

           /// <summary>
           /// Desc:N12 学生确认提交个人信息后，提示给学生的信息语。空时不提示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSQR_TS {get;set;}

           /// <summary>
           /// Desc:N06 总评是否要精确到。0：四舍五入（不保留小数点）； 1：精确到0.5； 2：精确到0.1（保留一位小数）。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZPCJ_JQ {get;set;}

           /// <summary>
           /// Desc:N06显示重修成绩找不到正考成绩的成绩 设置为1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXS_CXCJ {get;set;}

           /// <summary>
           /// Desc:N15 天数n。分散考试监考安排时，学院在考试时间的前n天不能修改监考老师。如n为1时，考试时间为25日，表示23日能修改，而24日不能修改。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSKS_JSXGSJ {get;set;}

           /// <summary>
           /// Desc:N06 学生网上报名是否上传照片 1 为要上传，0 为不要上传
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSWSBMZP {get;set;}

           /// <summary>
           /// Desc:N15 Cs_0，KSGL_ZDPKSDD为2时或自动排考试时间地点的找教室方法。有效值为1或2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_CS0 {get;set;}

           /// <summary>
           /// Desc:N12学生毕业可确认的年级 如：2002,2003
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYQR_NJ {get;set;}

           /// <summary>
           /// Desc:N15 学院是否可以安排巡考。0：否；1：是
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_XYXKAP {get;set;}

           /// <summary>
           /// Desc:N04 全校总课表中的简易教室课表的格式。0：起止周+教师姓名；1：起止周；2：周次+节次+单双周+上课人数;3：上课班级+节次+起止周+单双周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_JYJSKB {get;set;}

           /// <summary>
           /// Desc:N11锻炼次数最多记次数 如：90
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLCS {get;set;}

           /// <summary>
           /// Desc:N06 该设置方式共十七种 格式如："1.0/1.0//////////0.5/1/1/1/1/0.05/" 第1位 补考及格后绩点为 如:"1"。第2位 重修及格后绩点为 如:"1"。第3位 绩点按正考与补考成绩最大值计算绩点，设置为1时是该算法。第4位 绩点按该门课程最大值计算绩点。第5位 绩点如果该门课程补考通过的绩点-0.5否则按该门课程最大值计算绩点，设置为1时是该算法。第6位 设置为1时考查课无绩点；设置为2时选修课无绩点；设置为3时公选课无绩点；设置为4时选修课及格时绩点为计算值，不及格无绩点；设置为“4-1.0”时选修课及格时绩点为1.0，不及格时无绩点；设置为5时课程性质是否有绩点取自kcxzdmb；为6时公选课不及格无绩点；为7时课程性质为实践类的课程无绩点。第7位 设置为1.2时表示补考及格后绩点为“1.2”，重修及格正考及格按最大来计算绩点。第8位 1时同一门课程取平均。第9位 1时表示只有学位课程有绩点。第10位 1绩点计算如：成绩*学分*K/100。第11位 重修补考与补考通过为1.5，重修及格正考及格按最大来计算绩点。第12位 经过两次及两次以上考试才通过的，绩点以通过考试的成绩计算绩点再乘以0.5计算(第12位输入的值只能是数字)。第13位 设置为1时，非百分制绩点取自表fbfzjdszb（非百分制绩点设置表）。第14位 设置为1时，按照学生最后一次成绩计算绩点。第15位 设置为1时，正考及格绩点直接取分数，补考、重修通过则绩点为60，如果都不及格则绩点为0。第16位 设置为1时，不同的年级有不同的绩点计算方式，具体的需分年级设置绩点对照表。第17位 设置不为空，则绩点为课程成绩×设置的值。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDMS {get;set;}

           /// <summary>
           /// Desc:N13毕业审核等级考试成绩设置格式"55,375/60","/" 前代表英语四级，后面代表计算机
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSH_DJKSSZ {get;set;}

           /// <summary>
           /// Desc:N13学位审核等级考试成绩设置格式"60,425/60","/" 前代表英语四级，后面代表计算机
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWSH_DJKSSZ {get;set;}

           /// <summary>
           /// Desc:N13毕业审核事组号多余学分当作什么性质学分 如："专业任选课"
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXKDYDZXF {get;set;}

           /// <summary>
           /// Desc:N03学院预选设置 1：按开课学院 否则按学生学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXSZ_BMTJ {get;set;}

           /// <summary>
           /// Desc:N15 二级管理时，集中（第1位表示）、补考（第2位表示）、分散（第3位表示）安排某考场的主监考、副监考的学院是否一致。0：不一致；1：一致。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_PJKXY {get;set;}

           /// <summary>
           /// Desc:N03下任务起止周开关 1：表示关，其他为开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XRWQSZKG {get;set;}

           /// <summary>
           /// Desc:N13当替代课程学分不足或超过由什么性质替换 如："其他校选课"
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TDXFBXZ {get;set;}

           /// <summary>
           /// Desc:N07教材入库模式方式 设置为1：另一模式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JC_RKFS {get;set;}

           /// <summary>
           /// Desc:Nxx n。某门课程如若需要的考场数小于 n 必须排在同一栋楼
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_TLCS {get;set;}

           /// <summary>
           /// Desc:N01 考试学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSXN {get;set;}

           /// <summary>
           /// Desc:N01 考试学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSXQ {get;set;}

           /// <summary>
           /// Desc:N06打印成绩时打印格式是否可选，1 为只打印格式二，0 为可以选择 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDYGSKZ {get;set;}

           /// <summary>
           /// Desc:N03系统自动生成课程代码 1:表示自动生成 0：表示不自动生成
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XT_ZDSCKCDM {get;set;}

           /// <summary>
           /// Desc:N01  学校英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXYWMC {get;set;}

           /// <summary>
           /// Desc:N03上机学时显示名称 如"实习学时" 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXS_XS {get;set;}

           /// <summary>
           /// Desc:N12学生网上报名学年学期设置 格式如："2005-200621" 最后一位为开关 1：表示开 0：表示关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BM_XNXQ {get;set;}

           /// <summary>
           /// Desc:N14可在工作量统计中设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZLXS {get;set;}

           /// <summary>
           /// Desc:N12??????????????????,????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSKSCXKZ {get;set;}

           /// <summary>
           /// Desc:N12??????????????????????,???
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSKBCXKZ {get;set;}

           /// <summary>
           /// Desc:N13学业成绩表A4打印是否显示标题；‘0’：显示；‘1’：不显示，默认为‘0’
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYCJBTXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSCJLR_DYKZ {get;set;}

           /// <summary>
           /// Desc:N07教材发书时 设置为1时表示该学年已缴费的学生可发书 设置为0时都可发
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CW_CK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXJKYH {get;set;}

           /// <summary>
           /// Desc:N12教师网上申请公选课是否可以申请时间‘0’可以，‘1’不可以
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXKSQ_SKSJKZ {get;set;}

           /// <summary>
           /// Desc:N12教师录入成绩时是否允许录入总评成绩 1：不能录入 0：可以
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LR_ZPCJ {get;set;}

           /// <summary>
           /// Desc:N12教师录入成绩时级制根据任务中的考核方式设置如 考试->百分制,考查->五级制 设置为1：按照此方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SD_KHFS {get;set;}

           /// <summary>
           /// Desc:N13毕业审核日期输出名单时要用 格式：2006-05-02
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSHRQ {get;set;}

           /// <summary>
           /// Desc:N02根据学生申请退辅修信息 1：要申请 0:不要申请
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GJSQTFX {get;set;}

           /// <summary>
           /// Desc:N01 web登陆时，连接的公告的地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HTTP_GG {get;set;}

           /// <summary>
           /// Desc:N13全程成绩打印备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QCCJDYBZ {get;set;}

           /// <summary>
           /// Desc:N12???????????????,????
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXK_XFYQXX {get;set;}

           /// <summary>
           /// Desc:单个学生按标准课程修改 1可以修改，否则不能修改
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DGXSABZKCXG {get;set;}

           /// <summary>
           /// Desc:N12 web查成绩 是否是 "成绩单回收确认"后 学生才能查。1:是；0：否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEB_TJQRHCCJ {get;set;}

           /// <summary>
           /// Desc:N12体育成绩录入时平时成绩对应信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJXSXS {get;set;}

           /// <summary>
           /// Desc:099毕业审核时体育是否走项目0：走项目1：不走项目2：体育和分项目同时审核（西安理工）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSH_TYBZXM {get;set;}

           /// <summary>
           /// Desc:N06计算单门课程绩点时，绩点保留位数。默认保留到小数点后面两位小数。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BLWS_PJJD {get;set;}

           /// <summary>
           /// Desc:012 网上选体育项目时，单学期允许选的项目数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEB_XQTYXMS {get;set;}

           /// <summary>
           /// Desc:012 网上重修报名时，本学期已选课程能否报名。如果不能，请设置为空；否则请填入*
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBM_XQYXKC {get;set;}

           /// <summary>
           /// Desc:N06学生网上查询成绩时是否显示平时，期中，期末，实验 ‘1’显示，‘0’不显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJCX_XS {get;set;}

           /// <summary>
           /// Desc:N02综合素质测评汇总比例设置：格式为：30,50,20 第一个为学生基本素质比例,第二个为课程成绩,第三个创新能力
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZPSZCP_BL {get;set;}

           /// <summary>
           /// Desc:N12学生网上报考二次考试的分数要求 前面是期末最低分限制,后面是总评最高分限制中间用"|" 例 “30|75”
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ECKS_FSYQ {get;set;}

           /// <summary>
           /// Desc:N02学院申请岗位数的开关设置 如：2005-200611 最后一位为1：放开，0:关闭
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY_SQGWS {get;set;}

           /// <summary>
           /// Desc:N13打印毕业生成绩时是否要考虑课程替换 1：考虑 0：不考虑
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYBYSCJ {get;set;}

           /// <summary>
           /// Desc:N04排课数据恢复，是否能够按年级恢复。1：能；0：不能
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PKSJHFANJ {get;set;}

           /// <summary>
           /// Desc:N02学生困难补助等是否只允许贫困生申请 1只允许 0：都可以
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFPD_PKS {get;set;}

           /// <summary>
           /// Desc:N02学生申请岗位信息维护 格式：2005-200611 为学年+学期+开关 最后一位为1时是开，0为关闭
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQGW_SJ {get;set;}

           /// <summary>
           /// Desc:N06 教师网上打印是否在本页显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYCJFS {get;set;}

           /// <summary>
           /// Desc:N06 教师网上打印是否行政班是否选, ‘0’ 可选，‘1’ 不可选
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDYBJXX {get;set;}

           /// <summary>
           /// Desc:N06 新生网上注册是否打印报到单, ‘1’ 打印，‘0’ 不打印
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSZC_SFDY {get;set;}

           /// <summary>
           /// Desc:N04 人机交互排课中的"周次范围"的应用程度。0："周次范围"仅对界面中的"时间教师安排"的显示有效；1：不仅显示有效，而且界面排课时也有效
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_ZCFW {get;set;}

           /// <summary>
           /// Desc:N12 教学日历的录入结束时间，时间格式为"2006-01-01-01-01"
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXRL_JSSJ {get;set;}

           /// <summary>
           /// Desc:N12 教学日历的录入起始时间，时间格式为"2006-01-01-01-01"
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXRL_QSSJ {get;set;}

           /// <summary>
           /// Desc:N14工作量固定值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZ_GDZ {get;set;}

           /// <summary>
           /// Desc:N10教学质量评价理论带实验课程评价指标是否一致，默认0为一致
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJBS {get;set;}

           /// <summary>
           /// Desc:N02学生报道注册是否注册取自 设置为1时取 收费结果返回表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSZC_QZSFJGHHB {get;set;}

           /// <summary>
           /// Desc:N12 辅修选课的起始时间，时间格式为"2006-01-01-01-01"
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXXKQSSJ {get;set;}

           /// <summary>
           /// Desc:N12 辅修选课的结束时间，时间格式为"2006-01-01-01-01"
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXXKJSSJ {get;set;}

           /// <summary>
           /// Desc:N12 辅修选课的类别 1显示单开班课程，2，只显示跟班选课的课程，0 两个都显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXXKXKLB {get;set;}

           /// <summary>
           /// Desc:N12 成绩保存时百分比是否都设成100% ，1 设成，0 不设成
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_SFZS {get;set;}

           /// <summary>
           /// Desc:N12 学生选课时候教学班显示是否控制年级 0 控制，1 不控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXK_NJKZ {get;set;}

           /// <summary>
           /// Desc:N06补考教师分配方式，设置为1：开课学院 0：学生学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKJSFPFS {get;set;}

           /// <summary>
           /// Desc:N06学生缓考跟下一年考试 设置为1跟下一年考试
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HK_GXNJ {get;set;}

           /// <summary>
           /// Desc:N12 学生直考选课的门数限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZKXK_MSXZ {get;set;}

           /// <summary>
           /// Desc:N13毕业审核时体育门数修过几门才能通过 如4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSHB_TYMSSZ {get;set;}

           /// <summary>
           /// Desc:N10是否需要多次评价，默认为否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXYDCPJ {get;set;}

           /// <summary>
           /// Desc:N03计划任务安排时是否显示实践和存实验的 设置为1：不显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XRWSFX_SJSY {get;set;}

           /// <summary>
           /// Desc:N02学生辅修报名审核设置共两位 第一位为学生学院 第二位为辅修学院 设置为1时有效 设置为0无效
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXSHSZ {get;set;}

           /// <summary>
           /// Desc:N12 学生提前选修课程的总学分限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TQXKXFZH {get;set;}

           /// <summary>
           /// Desc:N12 学生提前选课的最低的学分绩点要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TQXK_JDYQ {get;set;}

           /// <summary>
           /// Desc:N12 补修页面的设置 1 限制只能选补修，2 限制只能选提前选修，0 不限制 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSBKXZ {get;set;}

           /// <summary>
           /// Desc:学生注册打印 1：默认打印 0:默认不打印
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSZC_DY {get;set;}

           /// <summary>
           /// Desc:N06学生免缓缺处理 1：开课学院，0：学生学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MHQ_SZ {get;set;}

           /// <summary>
           /// Desc:N15 某补考学期，学生补考的时候是否要分多个补考段（如：期初补考、期中补考）。1：是；0：否。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BK_KSSJD {get;set;}

           /// <summary>
           /// Desc:N06 绩点乘系数。设置为“1”取自教学计划课程信息表；为“2”取自课程库；为“0”取自课程性质代码表。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDSFCXS {get;set;}

           /// <summary>
           /// Desc:N12 学生成绩查询显示设置： 0 显示所有成绩； 1 有补考，重修的话合成一条成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXCJCXXS {get;set;}

           /// <summary>
           /// Desc:N06成绩申请修改审核部门学籍科 设置成1要审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJSQXG_QRBM {get;set;}

           /// <summary>
           /// Desc:N02打印新生名单开始页码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DYYS {get;set;}

           /// <summary>
           /// Desc:N10 评价记录数过多的处理方式，方式1：按照类别分类建立评价表，0：各类别在一张评价表中
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNRGD {get;set;}

           /// <summary>
           /// Desc:N06 教师成绩录入密码的方式 1. 取自教学任务表，2.取自用户表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSLR_CJMM {get;set;}

           /// <summary>
           /// Desc:N15 两位表示。第1位表示补考名单生成时默认学生是否参加补考,1：参加；0：不参加。第2位表示开课学院是否有权确认某门课程不让补考,1：有权；0：无权。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BK_BKQR {get;set;}

           /// <summary>
           /// Desc:N03是否需要公选计划 1：需要，0：不需要
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXJH {get;set;}

           /// <summary>
           /// Desc:N05加选课名单时，可否增加控制条件 第一位代表成绩保存 1：不能增加 0：可以增加，第二位成绩提交 1：不能增加 0：可以增加，第三位学生考完试 1：不能增加 0：可以增加。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZ_XKMD {get;set;}

           /// <summary>
           /// Desc:N13毕业结论条件  可在毕业审核右键->设置->设置毕业结论条件中设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYJRTJ {get;set;}

           /// <summary>
           /// Desc:N06打印提交申请的间隔天数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYTJTS {get;set;}

           /// <summary>
           /// Desc:N02学生信息申请确认学年学期设置 格式:2006-200711 表示:学年学期 最后一位1:为可申 0:不可申请
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXX_QRXQ {get;set;}

           /// <summary>
           /// Desc:N06密码发送延时  秒 如10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MMFSJG {get;set;}

           /// <summary>
           /// Desc:N04 网上预约教室后，是否算教室已被使用。1：算已被使用，0：反之
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YUYJS {get;set;}

           /// <summary>
           /// Desc:N06学生成绩没有正考只有重修成绩时是否显示，1：表示不显示，否则显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXSFXS {get;set;}

           /// <summary>
           /// Desc:N06 网上使用ahnu_xstxkc.aspx页面退选课程时候，每学生允许退选的最大门数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSCXKMS {get;set;}

           /// <summary>
           /// Desc:N06 网上使用ahnu_xstxkc.aspx页面退选课程时候，教学班的学生数达到容量的百分之几的时候不能退选
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSSCXKBL {get;set;}

           /// <summary>
           /// Desc:N13转专业学生的转专业前的必修课是否需要审核 1：不审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSH_ZZYXS {get;set;}

           /// <summary>
           /// Desc:N02警告提示语 第一位为1或0其他可自由设置，如第一位为1：代表被警告的学生可看到提示语，否则看不到
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JGTSY {get;set;}

           /// <summary>
           /// Desc:N07 在教材管理班级代码维护里->刷新教材人数时更新教材人数。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSXJCRS {get;set;}

           /// <summary>
           /// Desc:N13延长学制处理学年学期 格式：2005-200621 最后一位为1：表示可操作，0：不可操作
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YCXZ_XNXQ {get;set;}

           /// <summary>
           /// Desc:N02专业分流学年学期设置格式：2006-20071
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFL_SZ {get;set;}

           /// <summary>
           /// Desc:N04 教室预约单打印是否需要审核 1：审核后才可以打印；2：不需要审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSYUYDDY {get;set;}

           /// <summary>
           /// Desc:N04 课表打印的时候,左边距的大小。默认左右都为10。左边距最大可以设为20。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KB_ZBJU {get;set;}

           /// <summary>
           /// Desc:N04 学生未注册被限制网上成绩查询提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSCJXZTS {get;set;}

           /// <summary>
           /// Desc:N04 学生未注册被限制网上选课提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXKXZTS {get;set;}

           /// <summary>
           /// Desc:N05 板块学生等级维护中，学生的分级方式。0：完全按学生的成绩来分；1：按学生所在板块的不同，各等级分的人数不尽相同，但有上下限的分数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKFJFS {get;set;}

           /// <summary>
           /// Desc:N06学生预警时间设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJSJSZ {get;set;}

           /// <summary>
           /// Desc:N06奖学金排名学年设置 格式：2006-20071 最后一位为1表示开放
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXJPM_XNXQ {get;set;}

           /// <summary>
           /// Desc:N06奖学金排名学生查看学年设置 格式：2006-2007
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXJPM_XSCKXNXQ {get;set;}

           /// <summary>
           /// Desc:N06保研排名学年设置 格式：2006-20071 最后一位为1表示开放
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYPM_XNXQ {get;set;}

           /// <summary>
           /// Desc:N06保研排名学生查看学年设置 格式：2006-2007
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYPM_XSCKXNXQ {get;set;}

           /// <summary>
           /// Desc:N06成绩申请学年学期设置 格式：2006-200711 最后一位为1表示开放 0:关闭
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJSQXNXQ_SZ {get;set;}

           /// <summary>
           /// Desc:N06优秀排名学年设置 格式：2006-20071 最后一位为1表示开放
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXS_XNXQ {get;set;}

           /// <summary>
           /// Desc:N06优秀排名学生查看学年设置 格式：2006-2007
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXS_XSCKXNXQ {get;set;}

           /// <summary>
           /// Desc:N06优秀生条件设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXS_TJSZ {get;set;}

           /// <summary>
           /// Desc:N06保研排名显示方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYPM_XSFS {get;set;}

           /// <summary>
           /// Desc:N15是否根据可安排监考教师库来选监考老师;1：是; 否则否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_GJKAPJKJSB {get;set;}

           /// <summary>
           /// Desc:N04学院人机交互排课时是否按开课学院排课。0：学生学院；1：开课学院。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_KKXYPK {get;set;}

           /// <summary>
           /// Desc:N15在考试监考教师安排时，学院用户能否选其他学院的老师，0：不可以，1则可以
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_XYXJS {get;set;}

           /// <summary>
           /// Desc:N02审核学生申请的信息 第1位为1学院审核 第二位为1教务处审核。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHXSXX_JB {get;set;}

           /// <summary>
           /// Desc:N07 WEB端教师填写教材学年学期设置 格式如：2006-200711 最后一位为1表示开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDJC_XNXQSZ {get;set;}

           /// <summary>
           /// Desc:N04 人机交互排课中的"允许冲突"。0：允许课表冲突及教师冲突；1：仅允许课表冲突。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_CT {get;set;}

           /// <summary>
           /// Desc:N04 学院操作"人机交互排课"时候的几项显示。共6位，第1位表示：允许冲突；第2位：单双教室；第3位：独立教室；第4位：多个教室；第5位：冲突提示；第6位：选择教室时候的重复使用等项	。1表示显示，反之不显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_XYXS {get;set;}

           /// <summary>
           /// Desc:N03教学任务打印备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWDY_BZ {get;set;}

           /// <summary>
           /// Desc:N13体育课外补考成绩录入学年学期设置 格式如：2006-200711 最后一为为1表示放开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYKWLBKCJ_XNXQ {get;set;}

           /// <summary>
           /// Desc:N13体育课外合格补考成绩录入学年学期设置 格式如：2006-200711 最后一为为1表示放开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYKWHGBKCJ_XNXQ {get;set;}

           /// <summary>
           /// Desc:N13体育课外成绩录入学年学期设置 格式如：2006-200711 最后一为为1表示放开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYKWLCJ_XNXQ {get;set;}

           /// <summary>
           /// Desc:N13体育课外合格成绩录入学年学期设置 格式如：2006-200711 最后一为为1表示放开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYKWHGCJ_XNXQ {get;set;}

           /// <summary>
           /// Desc:N11体育课外锻炼如上一学期分值成绩超过190奖励本学期课外锻炼次数10次 格式如190|10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXQ_CGFS {get;set;}

           /// <summary>
           /// Desc:N11体育课外锻炼补考总分小于200分时需要补考锻炼次数 如10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYFSBK_DLCS {get;set;}

           /// <summary>
           /// Desc:N06学院修改成绩中课程代码,学分,课程性质,课程归属 共四位 为1表示可修改 0不可修改 格式：1111
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY_GGCJ {get;set;}

           /// <summary>
           /// Desc:N06成绩申请类型共三位 第一位为增加，第二位修改，第三位删除设置成1可操作 格式如：111
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJSQLXSZ {get;set;}

           /// <summary>
           /// Desc:N04 课表中是否显示。第1位：起止周；第2位：教室类别;第3位：教师职称;第4位：总学时;第5位：学分;第6位：开课学院(hndx专用);第7位：专业年级(hndx专用)。1：显示；0：不显示。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KB_XSXX {get;set;}

           /// <summary>
           /// Desc:N10网上评价方式,0:评价指标纵向;1：评价指标横向
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSPJFS {get;set;}

           /// <summary>
           /// Desc:N06成绩登记表备注填写
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBZ {get;set;}

           /// <summary>
           /// Desc:N06邮件支持中文字符
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJZCZF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GRCJ_BZ {get;set;}

           /// <summary>
           /// Desc:N06单个学生成绩修改字段 第1位:学年,第2位:学期,第3位:课程名称,第4位:课程性质,第5位:学分,第6位:平时成绩,第7位:期中成绩,第8位:期末成绩,第9位:实验成绩,第10位:成绩,第11位:补考成绩,第12位:重修成绩,第13位:重修标记,第14位:课程归属,第15位:备注,第16位:辅修标记,第17位:增加按纽,第18位:删除按纽,第19位:保存按纽,第20位:补考成绩备注。 设置为1可修改 格式如：11111111111111111111
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJ_XGZDSZ {get;set;}

           /// <summary>
           /// Desc:N12公文附件上传的各WEB服务器的访问路径，每一地址用||隔开，地址为直接访问登录口的地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEBSERVER_IP {get;set;}

           /// <summary>
           /// Desc:N12 1 附件上传直接上传到第一台WEB服务器即(WEB.CONFIG中HTTP1中设定的服务器)，2 附件调用WEBSERVER上传到每台服务器，地址在系统设置WEBSERVER_IP中设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FJ_SCMS {get;set;}

           /// <summary>
           /// Desc:N04 学生预选门数限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXMSXZ {get;set;}

           /// <summary>
           /// Desc:N12打印成绩时默认选择在哪个格式,1,2,3,4分别代表格式1,格式2,格式3,格式4,否则不选择
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDY_MRSZ {get;set;}

           /// <summary>
           /// Desc:N08实践实验分组和安排是否进行学年学期控制，控制为是，反之为否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJSYSJKZ {get;set;}

           /// <summary>
           /// Desc:N05 实验选课是否可以跨年级 1 为可以跨年级， 0 为不可跨年级 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXK_SFKNJ {get;set;}

           /// <summary>
           /// Desc:N03教学计划更改申请表学院批注名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHSQB_XYHZ {get;set;}

           /// <summary>
           /// Desc:N03教学计划更改申请表教务处批注名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHSQB_JWCHZ {get;set;}

           /// <summary>
           /// Desc:N15  分散考试安排时间，保存时判断冲突后能否强行保存，1：不可以，0：可以
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSKSAP_JCCTSFKBC {get;set;}

           /// <summary>
           /// Desc:N05 当 xs_kxrw 为‘1’ 时，其他教学班是否显示，‘1’ 显示，‘0’ 不显示  
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS_KXRWXS {get;set;}

           /// <summary>
           /// Desc:N05 校公选课的课程信息中的课程简介取自 ‘1’取自 gxksqb（公选课申请表） ，‘0’课程代码表  
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXK_KCJJ {get;set;}

           /// <summary>
           /// Desc:N12毕业设计学生选课时是否允许跨专业选题,1为允许可以选择自己学院的选题,2为不允许,只能选择本专业的选题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KG_BYSJ_KZY {get;set;}

           /// <summary>
           /// Desc:N15 学生补考时候是否可以操作多个时间段，1：多个时间段，0：不是多个时间段；
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BK_DGKSSJD {get;set;}

           /// <summary>
           /// Desc:N02 是否要生成教师密码的明码。1：要，2：不要
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMM_JSMM {get;set;}

           /// <summary>
           /// Desc:N06学生申请缓考(第一位),放弃考试(第二位)资格时审核学院 1：开课学院 0：学生学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHDW {get;set;}

           /// <summary>
           /// Desc:N15 天数n。分散考试时间安排时，学院在考试时间的前n天内不能安排考试。如n为14时，你想安排的考试时间为15日，表示1日能安排15日考试，而2日不能安排15日考试。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSKS_KSAPZCKZ {get;set;}

           /// <summary>
           /// Desc:N09 统计收费查询时是否要生成到xssfb 1:表示生成 0：不生成
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SC_XSSFB {get;set;}

           /// <summary>
           /// Desc:N08实践管理分成课程设计管理和实习管理，1为是，0为否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJFLB {get;set;}

           /// <summary>
           /// Desc:N08课程设计选课开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCSJQSSJ {get;set;}

           /// <summary>
           /// Desc:N08课程设计选课结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCSJJSSJ {get;set;}

           /// <summary>
           /// Desc:N08实习选课开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXQSSJ {get;set;}

           /// <summary>
           /// Desc:N08实习选课结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SXJSSJ {get;set;}

           /// <summary>
           /// Desc:N05 实验选课跨专业控制：  “1” 可以选择辅修（二专业）专业的实验课程，“2” 可以跨专业选实验课  
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXKSFKZY {get;set;}

           /// <summary>
           /// Desc:N05 实践选课是否可以选择辅修（二专业）专业的实践课程 ,“1”为可以跨专业，“0” 为不可以 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXKSFKZY {get;set;}

           /// <summary>
           /// Desc:N05 公选课组号限制。0：本学期；1：历年 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXK_ZXZSZ {get;set;}

           /// <summary>
           /// Desc:N04 课表打印的时候,上边距的大小。默认上下都为10。上边距最大可以设为20。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KB_SBJU {get;set;}

           /// <summary>
           /// Desc:N03学院录入计划时学分和如超过此学分时审核不通过 如：200
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHSZXF {get;set;}

           /// <summary>
           /// Desc:N08毕业设计教师网上申报审核只要学院审核即可,无需教务处审核;默认为0:需教务处审核;1:不需教务处审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCWXSH {get;set;}

           /// <summary>
           /// Desc:N10按开课学院统计学生评价结果，默认为0，即按全校；1则按开课学院进行统计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AKKXYTJ {get;set;}

           /// <summary>
           /// Desc:N02学院用户报道注册时控制 格式为“11” 第一位报道，第二位 注册。为1可操作
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSBDZCKZ {get;set;}

           /// <summary>
           /// Desc:N46查看教学建设申请情况汇总表 格式：20071表示07年可查询
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXHZ_QK {get;set;}

           /// <summary>
           /// Desc:N46网上申报课程教学建设时是否只能报课程库里有的课程 1：只能 0：都可以
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZBKCK {get;set;}

           /// <summary>
           /// Desc:N10评价统计结果按用户管理的校区进行分类查询，分校区为1，反之为0
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AXQTJCX {get;set;}

           /// <summary>
           /// Desc:N06 学生网上重修报名重修报名可报课程为“4”时不可报课程性质，  例：“全校性选修课,必修课” 中间用英文状态下的 “,”隔开 ，课程性质取自课程代码表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSCX_KCXZ {get;set;}

           /// <summary>
           /// Desc:N06 学生查询培养计划的模式 “0” 显示通过情况，“1” 不显示通过情况 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYJHXSMS {get;set;}

           /// <summary>
           /// Desc:N06 选课说明按钮隐藏的时间控制,例 “30”单位为秒
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKSM_SJKZ {get;set;}

           /// <summary>
           /// Desc:N02数据中心用户
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJZXYH {get;set;}

           /// <summary>
           /// Desc:N06 学生全校性公选课的学分限制 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XXKZGXF {get;set;}

           /// <summary>
           /// Desc:N03是否新生下计划 设置为1表示是
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHLG_XSJH {get;set;}

           /// <summary>
           /// Desc:N06绩点设置 格式：2004-20052/1.0
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GS_JDSZ {get;set;}

           /// <summary>
           /// Desc:N04 第一位表示： “学院”角色的用户是否能使用“使用部门”不是用户单位的教室。1：能；0：不能。第二位表示：教室借用必须注明相关信息后才能提交（如必须填写借用人、借用原因、联系电话等）,1：必填；0：不必填。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXCDSYBM_XY {get;set;}

           /// <summary>
           /// Desc:N06 字段为 “1” 时，学生名单排序增加按年级排序（选课情况，成绩录入，成绩打印） 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? WSMD_PXZD1 {get;set;}

           /// <summary>
           /// Desc:N11体育课外类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYKWLB {get;set;}

           /// <summary>
           /// Desc:N06成绩申请表备注 换行用#13
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJSQBZ {get;set;}

           /// <summary>
           /// Desc:N13个人课程替换是否要审核 1：要审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GRKCTH_SFSH {get;set;}

           /// <summary>
           /// Desc:N02学籍警告异动类别 如：休学
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJJG_YDLBPC {get;set;}

           /// <summary>
           /// Desc:N06出国默认课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CG_MRKCXZ {get;set;}

           /// <summary>
           /// Desc:N06回国默认课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HG_MRKCXZ {get;set;}

           /// <summary>
           /// Desc:N08实验项目模式选择：0为按课程及项目安排和选课；1为按项目安排和选课,与课程脱离
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXMMS {get;set;}

           /// <summary>
           /// Desc:N06 体育课外成绩录入是否允许非体育部教师查看 “1” 不允许，“0” 允许 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYKW_SFXS {get;set;}

           /// <summary>
           /// Desc:N06 学生补考名单确认中，为‘1’时只显示毕业补考名单
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMD_ZXSBYBK {get;set;}

           /// <summary>
           /// Desc:N06 学生教学评价的保存的时间控制,例 “30”单位为秒
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKPJ_SJKZ {get;set;}

           /// <summary>
           /// Desc:N03计划说明部分 设置为1另一种模式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHSMBF {get;set;}

           /// <summary>
           /// Desc:N06 学生网上查询成绩时是否显示根据处理成绩 ‘0’显示，‘1’不显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJCX_SHLG {get;set;}

           /// <summary>
           /// Desc:N10  学生网上评价的方式,评价页面是（xsjxpj.aspx）为‘0’，评价页面是（xsjxpj2.aspx） 为 ‘1’
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSPJ_PJMS {get;set;}

           /// <summary>
           /// Desc:N13体育课外补考成绩录入由分配的教师录入还是上课教师录入 1：是分配的教师录入
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYKW_LBKCJ {get;set;}

           /// <summary>
           /// Desc:N02按条件查询学生信息默认值 如:sfzc,sfzx,xjzt
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ATJCXXS_MRZ {get;set;}

           /// <summary>
           /// Desc:N08学科竞赛参赛资格怔中的注意事项内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKJSBZ {get;set;}

           /// <summary>
           /// Desc:N06学生分级教学处理分数 如：80
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FJJX_CJ {get;set;}

           /// <summary>
           /// Desc:N13毕业审核按课程类别审核 1：按课程类别，0：按课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSH_AKCLB {get;set;}

           /// <summary>
           /// Desc:N13学生所学计划外课程的课程类别归类为 如：“公共基础课”
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHWKCLB {get;set;}

           /// <summary>
           /// Desc:N01 重修报名轮次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? CXBM_DJC {get;set;}

           /// <summary>
           /// Desc:Nxx重修报名门次控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? CXBM_MCKZ {get;set;}

           /// <summary>
           /// Desc:N13预审学期设置 如：7
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YSXQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXMC1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXMC2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SN1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SN2 {get;set;}

           /// <summary>
           /// Desc:N12  成绩录入时最高分数限制 例如‘150’
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_XZFS {get;set;}

           /// <summary>
           /// Desc:N12  成绩录入时最高分取自系统设置‘cjlr_xzfs’的学院，如果有多个学院可以用英文状态下的‘,’隔开， 例如‘计算机科学与技术学院,管理学院’
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_XZXY {get;set;}

           /// <summary>
           /// Desc:N12  学生选课时，判断所选门课程是否在xsxkb（学生选课表）和cjb（成绩表）中 存在则不允许选且提示 “需要到重修中去选”，‘0’ 不判断，‘1’ 判断
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXK_CXKZ {get;set;}

           /// <summary>
           /// Desc:N03教学计划执行报表打印备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHZXBB_BZ {get;set;}

           /// <summary>
           /// Desc:N12  教师修改教学日历时是否可以修改成绩比例 ‘0’ 可以修改，‘1’不可以修改 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXRL_BLXG {get;set;}

           /// <summary>
           /// Desc:N03院系教师任务安排表打印备注 如要换行请输入#13 如：一式三份#13备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXRWJSDY_BZ {get;set;}

           /// <summary>
           /// Desc:N12  班级选课的选课类别设置，‘1’为班级选课，‘0’ 为个人选课 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKLB_SHGC {get;set;}

           /// <summary>
           /// Desc:N12 在学生选择教学班的页面是容量已满是否隐藏 ，‘0’不隐藏，‘1’ 隐藏
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXJS_XSKZ {get;set;}

           /// <summary>
           /// Desc:N04 学院操作“人机交互排课(2)”时候的几项显示。共7位，第1位表示：课表冲突；第2位：教师冲突；第3位：教室冲突；第4位：跨校区；第5位：重复使用；第6位：不计座位数；第7位：不计教室类别等项。1表示显示，反之不显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK2_XYXS {get;set;}

           /// <summary>
           /// Desc:N15 是否以学生学院来排考试时间和地点;1:表示以学生学院来排考试时间和地点,否则以开课学院;
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_XSXY {get;set;}

           /// <summary>
           /// Desc:N01学校校长
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXXZ {get;set;}

           /// <summary>
           /// Desc:N01学校编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXBH {get;set;}

           /// <summary>
           /// Desc:N01系改名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGM {get;set;}

           /// <summary>
           /// Desc:N03计划任务刷新无方向课程不是预选人数刷新, 1：按班级人数刷新 0：学生信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JH_SXRS {get;set;}

           /// <summary>
           /// Desc:N13学位审核时，未毕业是否转入
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWSH_WBYSFZR {get;set;}

           /// <summary>
           /// Desc:N02新生报到单信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSBDD_BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYDD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCDW {get;set;}

           /// <summary>
           /// Desc:N06教师成绩录入锁定设置（0不锁定，1锁定），第一、二、三、四位分别为平时成绩，期中成绩，期末成绩和实验成绩，如 1000 时平时成绩录入锁定
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_SDSZ {get;set;}

           /// <summary>
           /// Desc:N13毕业审核计划课程取自学生信息 设置为1：取自 0：教学计划课程信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSH_QZXSXX {get;set;}

           /// <summary>
           /// Desc:N15 监考教师安排方式二,1:表示提供监考教师安排时学院根据下发名额来提供，0：则否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_JKJSANFS2 {get;set;}

           /// <summary>
           /// Desc:N04 智能排课-》排课数据初始化-》上课学时分配表，若为‘1’则学院根据开课学院,否则根据学生学院；
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_XSFPGJKKXY {get;set;}

           /// <summary>
           /// Desc:N12 如果 “xsjxpjfs”为 “0”时学生教学评价的方式 ，0： 课程考试时间已经返回，则必须进行教学质量评价，若未有返回考试时间，则根据成绩提交时间来进行教学质量评价；1：则根据成绩提交时间来进行教学质量评价 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSJXPJKSSJ {get;set;}

           /// <summary>
           /// Desc:N09是否有第二种住宿费。第一位设置为1时表示有，其后紧跟住宿费数目；否则为一种。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGL_ZSFSZ {get;set;}

           /// <summary>
           /// Desc:099 学生网上补考确认的门次控制;学生补考的门次将会小于等于ksgl_bkqrms
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_BKQRMS {get;set;}

           /// <summary>
           /// Desc:099 进入集中考试的每个专业的课程门数。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_KSKCZYMSKZ {get;set;}

           /// <summary>
           /// Desc:N06重修学分来源。1：各任务表；2：重修成绩处理－>重修成绩报名名单统计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CX_XFLY {get;set;}

           /// <summary>
           /// Desc:N03下任务时，选择上课老师时学院用户是否可以选择"有无教师资格"为“无”的教师,1:表示可以选择；0：则相反
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XRW_YWJSZG {get;set;}

           /// <summary>
           /// Desc:N04  课表中是否显示打印时间，1：表示显示，否则步显示。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KB_XSDYSJ {get;set;}

           /// <summary>
           /// Desc:N02 查询辅修课、第二专业任务安排时，是否按开课学院查询。1：按开课学院；否则按学生学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXGL_SFKKXY {get;set;}

           /// <summary>
           /// Desc:N06 网上多个学期录成绩。0：只能设置一个学期可录，否则多个学期可录
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJGL_WSDGXQLCJ {get;set;}

           /// <summary>
           /// Desc:N12 成绩录入的模式： 设置成 ‘1’ 则 成绩可  以提交多次 ； 设置成 ‘0’时 只可以提交一次  
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLRMS {get;set;}

           /// <summary>
           /// Desc:N12 学生网上报名是否判断所填身份证号是否与学生库中的相同 ‘1’ 判断，‘0’不判断 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSBMSFZH {get;set;}

           /// <summary>
           /// Desc:N14 框中填写外部视图，普通格式不用填写
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSGL {get;set;}

           /// <summary>
           /// Desc:N10 为1时学院用户进入之后只能查看本学院排名信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXZL_PMTJ {get;set;}

           /// <summary>
           /// Desc:N15分散考试排地点里是否对已经排了时间地点但不时分散考试的教学班级以及对考试方式是分散考试的但是没有排考试时间地点的进行提示;1:提示;0:不提示;
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSKS_PDDTS {get;set;}

           /// <summary>
           /// Desc:N02删除学生分方向表时，如果设置为1,则将学生基本信息表中的班级信息和专业方向信息更新为原来信息；否则手动修改学生基本信息表中的信息。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSFFX_GLXSXXB {get;set;}

           /// <summary>
           /// Desc:N10 评分结果查询中教师加权平均分计算方式,方式1：sum(计分人数*教师总分)/sum(计分人数),其他为sum(教师总分)/sum(计分人数)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJQPJF {get;set;}

           /// <summary>
           /// Desc:N12 学生还未评价是否允许网上选课和查成绩，2位表示（ ‘1’可以，‘0’ 不可以），如 ‘10’表示允许选课但不允许查成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HZDZPJKZ {get;set;}

           /// <summary>
           /// Desc:Nxx 调停课是否释放教室的资源。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TTKSFSFZY {get;set;}

           /// <summary>
           /// Desc:N12 web 子系统取系统设置的方式 ；‘1’ 取自 数据库，‘0’取自web 服务器上的 xml 文件 。默认为‘1’
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTSZFS {get;set;}

           /// <summary>
           /// Desc:N13成绩单打印平均学分绩点和平均成绩,1为考虑课程类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDDY_JD {get;set;}

           /// <summary>
           /// Desc:N12 教师网上成绩录入是否可以修改成绩比例 ‘1’可以修改，‘0’不可以修改 ；
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLRBLXG {get;set;}

           /// <summary>
           /// Desc:N12  学生选课允许跨专业的年级，默认为空。（注：格式为  2005;2006 中间用英文状态下的 ‘;’隔开 ）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KZYNJKZ {get;set;}

           /// <summary>
           /// Desc:N08 毕业设计成绩录入密码生成方式。1：按每个教师一个密码，默认为空：按每个选课课号一个密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJ_MMSC {get;set;}

           /// <summary>
           /// Desc:N12 同行（特权用户）评价范围：‘1’ 学院，‘2’ 系 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJXPJFW {get;set;}

           /// <summary>
           /// Desc:N12 公选课选课控制：如果已修改过该门课程并且已经通过是否允许再选，‘0’允许，‘1’不允许 （默认 为‘0’）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXK_XKKZ {get;set;}

           /// <summary>
           /// Desc:N08毕业设计学生选题是否按教师拟带人数控制，1：控制，0：不控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJXTKZ {get;set;}

           /// <summary>
           /// Desc:N12 网上选课‘课程信息’和‘教师信息’ 是否直接取自系统的静态页面：‘1’取自静态页面，‘0’不取自静态页面 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKLJKZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSYXJCKZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string THXYKEY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ANHUXKLC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JLXYKEY {get;set;}

           /// <summary>
           /// Desc:N12 修改静态变量的web服务器url 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JTBLURL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YCXYWEBZCM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YCXYWEBSERVICE {get;set;}

           /// <summary>
           /// Desc:N09当前学年缴费时，是否先缴往年欠费。设置为1：必须先缴清往年欠费才能进行本学年缴费；设置为其它值则不受往年欠费控制。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGL_SFXJQF {get;set;}

           /// <summary>
           /// Desc:N02学期注册时间。格式为：“注册学年学期*注册起始时间*注册结束时间”。其中注册起始时间和注册结束时间格式为“yyyy-mm-dd”格式。注册学年学期格式形如：“2006-2007-1”
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQZCSJ {get;set;}

           /// <summary>
           /// Desc:N02更新学生未注册时间。在学生信息维护界面，如果系统时间大于该时间才能将学生注册状态为“注册中..”的记录更新为“未注册”。格式为“yyyy-mm-dd”。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GXWZCSJ {get;set;}

           /// <summary>
           /// Desc:N02学期注册时，最低可欠费的限额。例如：5000
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDQFXE {get;set;}

           /// <summary>
           /// Desc:N03    已安排计划课程不能修改课程库课程信息和已落实任务不能修改计划课程信息 1:不能修改
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXG {get;set;}

           /// <summary>
           /// Desc:N10  教学质量评价打分类型，1：为直接打分，其他为等级制打分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DFLX {get;set;}

           /// <summary>
           /// Desc:N04在人机交互式排课里学院用户是否能强行排不是学校可排时间的时间段;1:表示不可以强制安排，否则可以
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_SFNPSYSJD {get;set;}

           /// <summary>
           /// Desc:N04 调停补课申请教室后，是否算教室已被使用。1：算已被使用，0：反之
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TTKSQ_YJS {get;set;}

           /// <summary>
           /// Desc:N08  选题是否面向多个学院，1为是，默认为否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDGXYXT {get;set;}

           /// <summary>
           /// Desc:N01学校教务部名称。例1：教务部；例2：教务处。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWBMC {get;set;}

           /// <summary>
           /// Desc:N02学生申请转专业界面，英语课程代码。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YYKCDM {get;set;}

           /// <summary>
           /// Desc:N06统计平均学分绩点时需保留的位数。默认为保留小数点后面两位。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BLWS_PJXFJD {get;set;}

           /// <summary>
           /// Desc:N04在人机交互式界面，当根据周次范围来排课时不考虑任务学时分配的起止周以及周学时,只根据人机交互式的周次范围;1:表示有效，否则无效
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_ZCFWSFGJRW {get;set;}

           /// <summary>
           /// Desc:N05网上报名可临时书写的提示内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BMXMBTSWZ {get;set;}

           /// <summary>
           /// Desc:N08 毕业设计成绩录入方式：1为分项目录入合成
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRFS {get;set;}

           /// <summary>
           /// Desc:N09收费票据打印项目设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFPJDYXMSZ {get;set;}

           /// <summary>
           /// Desc:N01  校长姓名拼音
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZXMPY {get;set;}

           /// <summary>
           /// Desc:n13 毕业审核不计重修标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJCXBJ {get;set;}

           /// <summary>
           /// Desc:n15 学院是否可排冲突，1：可以。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_XYYXPCT {get;set;}

           /// <summary>
           /// Desc:N05是否预定教材，1为预定，0不预定
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYDJC {get;set;}

           /// <summary>
           /// Desc:N04教室是否每次借用多个,1为是,0为每次借用一个
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJYDG {get;set;}

           /// <summary>
           /// Desc:N04教室是否可以借用连续周次同一时间段的教室,1为是,0为否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSJYLXZC {get;set;}

           /// <summary>
           /// Desc:N12网上教师登陆提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSJS_TSY {get;set;}

           /// <summary>
           /// Desc:N12网上学生登陆提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSXS_TSY {get;set;}

           /// <summary>
           /// Desc:Nxx是否根据标准考场来安排考试地点；安徽农业大学特殊
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_GJBZKCAPDD {get;set;}

           /// <summary>
           /// Desc:N08 毕业设计分学院控制:1,统一控制:0
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJFXYKZ {get;set;}

           /// <summary>
           /// Desc:N99  辅修每学年开课的学期数。3：3学期，默认为2学期。（山东经济） 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXMXNXQS {get;set;}

           /// <summary>
           /// Desc:Nxx是否根据试卷上课教师课程代码 来安排考试地点；安徽农业大学特殊
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_GJSKJSKCDM {get;set;}

           /// <summary>
           /// Desc:Nxx监考教师根据分组来安排；安徽农业大学特殊
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_JKJSFZAP {get;set;}

           /// <summary>
           /// Desc:N99修改任务人数：1：模式1(中国矿大), 2：模式2(中国地质大学)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGRWRS {get;set;}

           /// <summary>
           /// Desc:N08实践项目是否要分班级按项目来分组以及下任务，1：表示根据班级按项目来分组以及下任务，0:表示不是。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJGL_SJXMFBJ {get;set;}

           /// <summary>
           /// Desc:N13个人成绩替换多或少的学分归入哪个课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GRTH_KCXZ {get;set;}

           /// <summary>
           /// Desc:N13毕业证书打印日期中的0输出：1. 为○ 否则为零
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYZSDYRQ {get;set;}

           /// <summary>
           /// Desc:N04下任务的同时插入排课程前预排表，1：下任务同时直接插入到排课预排表,0:表示否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_XRWDKB {get;set;}

           /// <summary>
           /// Desc:N08实践项目任务来源，1：表示先在教学计划中安排实践课程信息，再在实践管理中安排具体项目，0:表示不是。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJGL_RWLY {get;set;}

           /// <summary>
           /// Desc:N04 人机交互式排课时是否根据计划课程分专业年级来控制，1：表示是，0：不是
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_TYPK {get;set;}

           /// <summary>
           /// Desc:N02  单个生成学号方式:0:按专业生成，1:按行政班生成
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SCXHFS {get;set;}

           /// <summary>
           /// Desc:Nxy  最高选课学分获取方式，是：表示根据xkzgxfxz取值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDXKXF_FS2 {get;set;}

           /// <summary>
           /// Desc:N02  学生报到注册输入学号后是否直接报道：1是，0不是。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKQR {get;set;}

           /// <summary>
           /// Desc:N10  评价时对应分按照升序则设置为1，默认为降序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJDYFPX {get;set;}

           /// <summary>
           /// Desc:N12  网上成绩打印不统计重修学生则设置为1，默认为统计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDY_BTJCX {get;set;}

           /// <summary>
           /// Desc:N12 学生网上报名是否可以删除 “1” 不可以删除，“0” 可以删除 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSBMSFSC {get;set;}

           /// <summary>
           /// Desc:N04人机交互式排课(2)里调整学时分配的学时方式，第1位表示是否可调整,1：可以调整，0：则不能；第2位,若为1表示学时分布均可修改,若为2表示只可改节假日周、无学时周和最后一周(最后第1周学时数小于等于最后第2周学时),其它则不能修改
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPKTZ2_GGXSPFFS {get;set;}

           /// <summary>
           /// Desc:N04人机交互式排课(2)里,第1位表示课表，第2位表示教师，第3位表示教室，第4位表示任务；1表示显示，否则不显示。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPKTZ2_PKKBKZ {get;set;}

           /// <summary>
           /// Desc:N12北京电子科技重考最大值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJDZCKZDZ {get;set;}

           /// <summary>
           /// Desc:N13学生体质健康标准测试名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSTZSZMC {get;set;}

           /// <summary>
           /// Desc:主修专业学院时间控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXZYXYSJKZ {get;set;}

           /// <summary>
           /// Desc:099学院用户查询课程是否可选：1、可选
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYYHSFKX {get;set;}

           /// <summary>
           /// Desc:N04教室预约模式开关。默认000，日期，单日，固定时间段；100，周次，单日，固定时间段；010，日期，多日（同周多日，多周一日），固定时间段。
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSYYMSKG {get;set;}

           /// <summary>
           /// Desc:N12网上报名说明后导向页面，0bmxmb.aspx，1xsgrxx.aspx
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BMBMBDXYM {get;set;}

           /// <summary>
           /// Desc:099设置为0时按成绩表中学位课计算学位课平均分绩点,为1时按教学计划中学位课计算
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWKPJFJD {get;set;}

           /// <summary>
           /// Desc:N02是否关联迎新数据子类：2是 先分班在迎新，1是 先迎新在分班，0不是。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS_YXSJZL {get;set;}

           /// <summary>
           /// Desc:N14教学日历生成方式：0  表示不考虑多个教师，1表示考虑多个教师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXRLSCFS {get;set;}

           /// <summary>
           /// Desc:N12学生选课专业课程分组学分门数控制，必须重启IIS，第一位门数控制，按学期已选门数控制为1；第二位学分控制，按学期已选学分控制为1，按历年修得学分控制为2；不控制为空或00
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MKZHKZ {get;set;}

           /// <summary>
           /// Desc:N01学校代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXDM {get;set;}

           /// <summary>
           /// Desc:N01校长名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZXM {get;set;}

           /// <summary>
           /// Desc:N09 自动填充应交项目金额。1：是，0：否。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDTCYJXMJE {get;set;}

           /// <summary>
           /// Desc:调停课是多个教师相同时间是否一起调；1:表示一起调；
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TTK_DJS {get;set;}

           /// <summary>
           /// Desc:第一位，老师查看所有留言：0表示老师只能看到学生给自己留的言，1表示老师可以看到跟自己有关的课程学生给所有老师留的言；第2位，学生查看所有留言：0 表示学生只能看到自己的对老师的留言   1表示学生可以看到跟自己有关课程的其他同学留言
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LYBKG {get;set;}

           /// <summary>
           /// Desc:调停课总次数限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZCSXZ {get;set;}

           /// <summary>
           /// Desc:调停课课程次数限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KCCSXZ {get;set;}

    }
}

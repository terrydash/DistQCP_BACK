﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XXMCSZB
    {
           public XXMCSZB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:Nxx重修学分控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBM_XFKZ {get;set;}

           /// <summary>
           /// Desc:教室借用控制 0--预约 1--借用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSJYKZ {get;set;}

           /// <summary>
           /// Desc:N04智能排课-》教学场地借用，学院用户是否可以先预约借用，1：则先预约借用，否则：直接借用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_SFZJJYJS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CDRS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JZJK {get;set;}

           /// <summary>
           /// Desc:是否按大一同样处理补考成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYGL_SFADYTYCL {get;set;}

           /// <summary>
           /// Desc:N09  收费管理-与其他系统接口-学生收费数据接收功能菜单是否显示。设置为"1"时显示，否则不显示。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGL_XSSFSJJS {get;set;}

           /// <summary>
           /// Desc:成绩打印格式显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDY_GSXS {get;set;}

           /// <summary>
           /// Desc:N13 做组合课程替换时，替换课程是否要求全部及格才能替换被替换课程。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYGL_ZHKCTHSFYQJG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYGL_SFKLZHKCTH {get;set;}

           /// <summary>
           /// Desc:N06 成绩单回收确认时，是否需要院系审核。若院系需审核，即院系审核通过之后，教务处才能进行审核；否则只要进行一次审核即可。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXSFQRCJDHS {get;set;}

           /// <summary>
           /// Desc:N06 学生成绩加分处理（正考）时，最高不得超过多少分；为空则按照不的高于该门课程最高成绩处理。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJJFCLZGF {get;set;}

           /// <summary>
           /// Desc:N09 收费票据左边距设置，单位为mm,仅当系统设置中的pjgs设置为格式9、13、2、14、16、17、18、20、21或23时有效。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZBJ {get;set;}

           /// <summary>
           /// Desc:N09 收费票据上边距设置，单位为mm,仅当系统设置中的pjgs设置为格式9、13、2、14、16、17、18、20、21或23时有效。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWGGYXQXSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYKFXZKC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJQBKZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXKQKXSKZ {get;set;}

           /// <summary>
           /// Desc:N01系统登陆次数,登陆时超过该次数则系统将不能登陆(该字段必须数字)。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTDLCS {get;set;}

           /// <summary>
           /// Desc:N15  考试中控制不可用的教室:集中(第一位),补考(第二位),分散(第三位):1控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKYJSKZ {get;set;}

           /// <summary>
           /// Desc:N08  毕业题目审批应设置1：按教师所在学院, 默认为按学生学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTMSPSZ {get;set;}

           /// <summary>
           /// Desc:099是否可查学院设置。共两位，前一位表示选课表，后一位表示成绩表；设置为1表示开课学院，设置为0表示学生学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKCXYSZ {get;set;}

           /// <summary>
           /// Desc:099 学生网上申请放弃考试资格门次控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQFQKSMS {get;set;}

           /// <summary>
           /// Desc:099 教师网上录补考，允许多次录补考，设置为1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDCBK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXSFSMXX {get;set;}

           /// <summary>
           /// Desc:N12成绩录入考查课是否显示加分 1显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCKSFJF {get;set;}

           /// <summary>
           /// Desc:N99公共选修课允许时间地点冲突。1为时间冲突，0为不允许冲突。默认为0
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXK_GXKCT {get;set;}

           /// <summary>
           /// Desc:099学生照片上传文件大小设置，大于此设置的将不能上传,单位k
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEB_ZPDXSZ {get;set;}

           /// <summary>
           /// Desc:099学生照片上传建议实际像素设置，如设置为144/172，即建议实际像素为144×172(宽×高),
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEB_ZPFBLSZ {get;set;}

           /// <summary>
           /// Desc:099设置为1则：补考成绩按比例（取原期末成绩比例），和平时，期中，实验按比例折算，原补考卷面成绩保留
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKCJZS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYXSZ_SBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYXSZ_ZBJ {get;set;}

           /// <summary>
           /// Desc:N99公共选修课选定的记录是否打重修标记,设置成 1 打重修标记 ,0 不打重修标记 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXK_GXCXBJ {get;set;}

           /// <summary>
           /// Desc:099学籍异动选课门数限制（沈阳化工专用） 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDXKMSXZ {get;set;}

           /// <summary>
           /// Desc:N09在“按专业初始化”界面生成学生应收费时，往年欠费是否加入今年应收费中。设置为“是”：计入今年应收费；否则不计入。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LNQFSFJRJN {get;set;}

           /// <summary>
           /// Desc:099学生照片上传最小分辨率设置，如设置为150，图片分辨率不低于150dpi,低于150ddpi的将不能上传
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEB_ZPFBL {get;set;}

           /// <summary>
           /// Desc:099学生照片格式限制，如BMP/JPG/JPEG
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEB_ZPGS {get;set;}

           /// <summary>
           /// Desc:N02学生证打印发证日期/有效日期:格式:2009年01月01日/2009年12月01日
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSZDYRQ {get;set;}

           /// <summary>
           /// Desc:N099 辅修选课允许冲突设置 “1“ 允许冲突，”0“ 不允许冲突 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXSFYXCT {get;set;}

           /// <summary>
           /// Desc:N099 网上调停课申请是否判断冲突设置 “1“ 判断冲突，”0“ 不判断冲突 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TTKPDCT {get;set;}

           /// <summary>
           /// Desc:N099 学生网上报名的身份证号和学生库中不一致的提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSBMTSY1 {get;set;}

           /// <summary>
           /// Desc:N099 学生网上报名的学生库身份证号为空的的提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSBMTSY2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSHPC {get;set;}

           /// <summary>
           /// Desc:099 课程教学小结课程类别设置,以课程类别代码表代码填充,多个以逗号区分.例：01,03,04
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKXJKCSXSZ {get;set;}

           /// <summary>
           /// Desc:099 excel加密密码设置，默认为 opendoor
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EXCELJM {get;set;}

           /// <summary>
           /// Desc:N099 体育课外成绩录入是否锁定学年学期 ‘1’ 锁定， ‘0’ 不锁定
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYKWXNXQKZ {get;set;}

           /// <summary>
           /// Desc:N99成绩打印班级设置，设置为1：打印班级取学生注册表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDYBJSZ {get;set;}

           /// <summary>
           /// Desc:N099 学生网上容量已满申请的最大门数 ，默认 2 门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BXSQZGMS {get;set;}

           /// <summary>
           /// Desc:N099 学生选课容量比例设置 默认为 ‘1’ 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKRLBL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYKCCJMM {get;set;}

           /// <summary>
           /// Desc:N99成绩录入默认为1显示班级，设置为0则显示专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_XSBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DY_GETCS {get;set;}

           /// <summary>
           /// Desc:099 诚信承诺书文件名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXCNS {get;set;}

           /// <summary>
           /// Desc:099 考试注意事项文件名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSZYSX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XYMHQCLSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XBJXRS {get;set;}

           /// <summary>
           /// Desc:N08实践选课是否可退：可退：是;不可退：否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJSFKT {get;set;}

           /// <summary>
           /// Desc:N08实践选课是否可选：可选：是;不可选：否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJSFKX {get;set;}

           /// <summary>
           /// Desc:099 学生登录判断设置 为"1"通过学生缴费信息判断学生是否允许登录 为"0"通过学生是否注册判断
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLPDSZ {get;set;}

           /// <summary>
           /// Desc:099 是否进行考试安排确认_统一排考，0否，1是
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSAPQR_YS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSAPQR_PK {get;set;}

           /// <summary>
           /// Desc:Nxy开放人数百分比
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KFRSBFB {get;set;}

           /// <summary>
           /// Desc:099 教师是否参与评价，0否，1是
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSFCYPJ {get;set;}

           /// <summary>
           /// Desc:099 教师是否参与评价查询学年学期，例：200820092
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSFCYPJXNXQ {get;set;}

           /// <summary>
           /// Desc:N04 当前可排上课内容 0-理论,1-实验,空的时候不控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_KPKSKNR {get;set;}

           /// <summary>
           /// Desc:N15 不能补考学生不及格门数控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMD_MSKZ {get;set;}

           /// <summary>
           /// Desc:N03 教学计划下达班级校区取班级校区代码:1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXJH_PKXQDM {get;set;}

           /// <summary>
           /// Desc:N12 webLog日志开关,1为记录所有日志内容,2为只记录错误日志内容(只能是网页使用)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEBLOGKG {get;set;}

           /// <summary>
           /// Desc:N12 毕业设计课题申请是否放开曾用题选择：1 放开；0 不放开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJFKZCYXZ {get;set;}

           /// <summary>
           /// Desc:N12欠费学生相关操作限制，共四位，格式为1111。第一位表示学生选课是否被限制；第二位表示网上报名是否可报；第三位表示教师网上可否打印成绩；第四位表示客户端成绩管理模块查询、统计、打印是否被限制。设置为“1”表示限制，设置为“0”表示不限制。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QFXZ {get;set;}

           /// <summary>
           /// Desc:N12 教师成绩打印试卷分析中统计重修学生开关,0默认统计重修学生,1不统计重修学生
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJFX_TJCXKG {get;set;}

           /// <summary>
           /// Desc:N12 学生选课无时间地点是否可选：1 不可选；0 可选
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXK_WSJDDXZ {get;set;}

           /// <summary>
           /// Desc:N06 设置共两位，格式如：00。第一位表示客户端和网页端录入正考成绩时，控制特殊学生的课程成绩是否屏蔽；第二位表示在对成绩进行查询、统计、打印时，控制被屏蔽的成绩是否被排除在操作之外。设置为“0”表示不控制，设置为“1”表示控制。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PCWXCJCLKZ {get;set;}

           /// <summary>
           /// Desc:N12 补考成绩打印时选择条件打印开关：1 启用；0 关闭
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKDY_ATJ {get;set;}

           /// <summary>
           /// Desc:N12 学生成绩查询使用临时表设置：0 不使用；1 使用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJCX_LSB {get;set;}

           /// <summary>
           /// Desc:N04  输入系统体育项目安排默认允许冲突的教学场地编号，多个时用,分开，如：1234,4578 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCTJSBH {get;set;}

           /// <summary>
           /// Desc:N06在同一个学年学期是否有多次重修段，1：是，否则为否
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJGL_DGCXSJD {get;set;}

           /// <summary>
           /// Desc:默认收费学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MRSFXN {get;set;}

           /// <summary>
           /// Desc:N99密码生成方式，0：生成可逆密码；1：生成不可逆密码。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MMSCFS {get;set;}

           /// <summary>
           /// Desc:N05选课说明开关，依次是：学生选课，网上报名，重修报名，全校性选课，体育选课。不使用说明，为0；每次访问页面都需要同意，为1（网上报名为1或2）；每次登录都需要同意，为a（网上报名为a或b）；只需要同意一次，为A（网上报名为A或B）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKSM_KG {get;set;}

           /// <summary>
           /// Desc:生成教学班关联的学生条件：1.是否在校 或 2.是否注册 或 3.是否在校,是否注册。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SCJXB_XSTJ {get;set;}

           /// <summary>
           /// Desc:099选课容量已满、冲突课程申请、退课申请开关：1表示放开申请，0关闭申请
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKCTSQ {get;set;}

           /// <summary>
           /// Desc:N05学生跨专业选课时是否可以跨学院跨专业选课 0：可以，1：不可以
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KZYXKSFKXY {get;set;}

           /// <summary>
           /// Desc:N05学生跨专业选课时是否显示计划外课程 0：显示，1：不显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KZYXKSFXSJHWKC {get;set;}

           /// <summary>
           /// Desc:099学院下达编辑开关：前面10位代表学年学期,最后一位1代表开0代表关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYXDBJKG {get;set;}

           /// <summary>
           /// Desc:099辅修预选设置开关:1为开0为关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FXYXSZKG {get;set;}

           /// <summary>
           /// Desc:N45学生辅修选课设置每学分应缴的费用（如：50表示每学分应缴50元）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? FXJFSZ {get;set;}

           /// <summary>
           /// Desc:N15 集中和补考考试排地点时不判断上课地点冲突(但是要判断教室借用的冲突以及考试的冲突) 1：为不判断上课地点冲突，0：判断上课地点冲突
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSGL_PKCTPD {get;set;}

           /// <summary>
           /// Desc:N12  网上班级课表查询限制，格式为12005-20061，即开关加学年学期，1为控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WS_TJKBCXXZ {get;set;}

           /// <summary>
           /// Desc:N12  学生登录提示还须获得学分开关：1为开；0为关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJXFKG {get;set;}

           /// <summary>
           /// Desc:N12  平时成绩比例范围限制，格式如：15-50；表示15%≤平时成绩比例≤50%
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSCJBLXZ {get;set;}

           /// <summary>
           /// Desc:N06 web上成绩录入是否放开导入成绩功能,1为是
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_SFKDR {get;set;}

           /// <summary>
           /// Desc:N01招生数据处理时，不同批次数据国编码是否对应不同学校内专业,1:表示对应学校内不同专业 （注意：在招生数据里录取批次，需要对应专业代码表内的国编序号;同一国编码，对应序号不能相同。）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GMMSFDYDG {get;set;}

           /// <summary>
           /// Desc:N15 网上考试名单输出是否显示照片,1为显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WSKSMDDYFS {get;set;}

           /// <summary>
           /// Desc:Nxx学生网上报名打印校对单说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDDSM {get;set;}

           /// <summary>
           /// Desc:099是否根据缴费结果来更新学生基本信息的注册状态 0：不更新，1：更新
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGSZCZT {get;set;}

           /// <summary>
           /// Desc:N01教务处角色;（注不能维护为空）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCJS {get;set;}

           /// <summary>
           /// Desc:N01 专业代码长度设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDMCD {get;set;}

           /// <summary>
           /// Desc:N01 学院代码长度设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYDMCD {get;set;}

           /// <summary>
           /// Desc:N12  学生教学评价时，评价与建议是否必填|必填多少字，如设置为1|20，则评价与建议必填且必须填20字以上；
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSJXPJ_PJXXKZ {get;set;}

           /// <summary>
           /// Desc:009重修报名模式1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBMMS1 {get;set;}

           /// <summary>
           /// Desc:009重修报名模式2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXBMMS2 {get;set;}

           /// <summary>
           /// Desc:N03 学生二专业报名是否需要审核，0：不需要
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSEZYBMSFYSH {get;set;}

           /// <summary>
           /// Desc:N06网页成绩专业排名统计查询显示设置。（"是"表示能查询；"否或者空"表示不能查询）。 
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PMTJCXSZ {get;set;}

           /// <summary>
           /// Desc:099文件服务器模式是否开启 0：不开启；1：开启
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WJFWQ {get;set;}

           /// <summary>
           /// Desc:N07教材出库状态设置为 "1",表示教材按照校区出库，否则不按照校区出库
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AXQCK {get;set;}

           /// <summary>
           /// Desc:N12  补考成绩录入控制项(仅当bkcjzs设置为1时有效)，第一位为1：补考成绩可录，第二位为1：比例不为空的不能保存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_BKKZ {get;set;}

           /// <summary>
           /// Desc:N12  成绩录入特殊计算设置(补考时仅当bkcjzs设置为1时有效,格式为a|b|c|d),期末成绩比例≥a%，期末成绩<b，总评成绩≥c的，总评成绩自动记为d
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_BKTSJS {get;set;}

           /// <summary>
           /// Desc:099票据缴款人信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJJKRXX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UNLOCKFXXK {get;set;}

           /// <summary>
           /// Desc:N12 教师网上调停课教室能否使用"使用部门" 不是用户单位的教室。1：能;0:不能
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXCDSYBM_JS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDZMBZ {get;set;}

           /// <summary>
           /// Desc:099设置为1时,二级学院安排院系选修课任务时,按教学计划来查 课程,否则按课程库查课程。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZKCLY {get;set;}

           /// <summary>
           /// Desc:099打印教材出库单备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JC_JCBZ2 {get;set;}

           /// <summary>
           /// Desc:N06  n，阶段成绩录入时间限制，课程阶段考试结束（教师考试模式申报时提交的时间为准）->结束后n天。如n为空则阶段成绩不可录入
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLR_TS_PS {get;set;}

           /// <summary>
           /// Desc:N12设置为1，则教师网页成绩录入期末比例不为0时，期末成绩不及格，则总评成绩等于期末成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEB_CJLR_QMZP {get;set;}

           /// <summary>
           /// Desc:预约打印类别限制数量（0:不做限制）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YYDY_LBXZ {get;set;}

           /// <summary>
           /// Desc:N12 教师允许调停课的比例系数，如：10%表示为10，默认为：10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSTKBL {get;set;}

           /// <summary>
           /// Desc:N12 教师调停课的次数提示设置，如设置为：2，则教师调停课的次数超过2次时，提示老师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YTKCS {get;set;}

           /// <summary>
           /// Desc: N09票据打印的备注信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJBZ {get;set;}

           /// <summary>
           /// Desc: N09票据打印的注释信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJZS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCBB {get;set;}

           /// <summary>
           /// Desc:N09生成缴费流水号，设置为1表示采用序列产生流水号，反之通过YHSJB取流水号最大值+1处理
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJLSH {get;set;}

           /// <summary>
           /// Desc:院系选修课学年学期开关设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXXXKSQXQ {get;set;}

           /// <summary>
           /// Desc:N099基本课程系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JBKCXS {get;set;}

           /// <summary>
           /// Desc:N099基本人数基本系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXBRSJBXS {get;set;}

           /// <summary>
           /// Desc:N04调停课申请通知单打印备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TTKDYBZ {get;set;}

           /// <summary>
           /// Desc:099阶段成绩录入最大比例（如：70，是N个阶段成绩之和加起来不能超过70%）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JDCJLRZDBL {get;set;}

           /// <summary>
           /// Desc:099网页端成绩更正提示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEB_CJGZTS {get;set;}

           /// <summary>
           /// Desc:11 第一位为开课学院审核，第二位为学生学院审核，不能同时为0或空
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHLBBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WEB_HKTS {get;set;}

           /// <summary>
           /// Desc:N12 学生个人信息修改设置两位：第1位设置为1则后台信息字段授权了学生个人信息也不能修改，否则根据字段授权可修改；第2位设置为1则学生个人信息页面只能修改学生基本信息表以外的表	的字段信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSGRXX_XGSZ {get;set;}

           /// <summary>
           /// Desc:N99教师允许调课系数（百分数表示；如：12.5%表示为12.5）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? JSTKXS {get;set;}

           /// <summary>
           /// Desc:099 最低平均学分绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? CJGL_ZDPJXFJD {get;set;}

           /// <summary>
           /// Desc:N08 II类学分项目申报审核控制（默认值为0）： 只需要学院审核设置为0 ，需要教务处审核设置为1     。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMSBSH {get;set;}

           /// <summary>
           /// Desc:099学生登录学业预警提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSDLXYYJTSY {get;set;}

           /// <summary>
           /// Desc:099学生登录学生欠费提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSDLXSQFJTSY {get;set;}

           /// <summary>
           /// Desc:N099 平时成绩评价分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSCJPJF {get;set;}

           /// <summary>
           /// Desc:N099 平时成绩标准差
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSCJBZC {get;set;}

           /// <summary>
           /// Desc:N099 平时成绩超出评价分和标准差范围提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSCJXZTSY {get;set;}

           /// <summary>
           /// Desc:099成绩打印格式名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDY_GSMC {get;set;}

           /// <summary>
           /// Desc:099成绩打印图片显示;1显示，0不显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDY_TPXS {get;set;}

           /// <summary>
           /// Desc:     密码有效期限,以天为单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MMYXQX {get;set;}

           /// <summary>
           /// Desc:     登陆密码过期强制修改 ‘0’为 否 ‘1’为是
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MMGQQZXG {get;set;}

           /// <summary>
           /// Desc:099教师点名多少天完成考勤录入设置单位（天）。为空不限制；为0必须当天录完；其他数字为考勤后多少天内必须完成录入。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? BLKQSJSZ {get;set;}

           /// <summary>
           /// Desc:N10满意度关联教学评价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MYDGLJXPJ {get;set;}

           /// <summary>
           /// Desc:N10满意度关联教学评价提示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MYDGLJXPJTS {get;set;}

           /// <summary>
           /// Desc:099理论学时系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LLXSXS {get;set;}

           /// <summary>
           /// Desc:099选课限制总学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XK_XZZXF {get;set;}

           /// <summary>
           /// Desc:099选课限制总门数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XK_XZZMS {get;set;}

           /// <summary>
           /// Desc:N14实践课每周课时数（用于计算实践课教师工作量）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJKMZKSS {get;set;}

           /// <summary>
           /// Desc:优秀比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKSM_YXBL {get;set;}

           /// <summary>
           /// Desc:评价等级为优秀+良好比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKSM_YXLHBL {get;set;}

           /// <summary>
           /// Desc:N099 短信接口服务器IP地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXJKURL {get;set;}

           /// <summary>
           /// Desc:N099 短信接口通讯密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DXJKKG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXJKMM {get;set;}

           /// <summary>
           /// Desc:违纪和作弊审核人以“,”号分开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WJZBSH {get;set;}

           /// <summary>
           /// Desc:099兰州交通大学少数民族学生（特殊学生）总评成绩计算公式，用|符号隔开分别表示正考和补考重修计算公式。即45-60表示正考成绩在这之间折算为60；40-60表示补考重修成绩在这之间折算为60
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LZJTDX_TSXSLBCJCL {get;set;}

           /// <summary>
           /// Desc:099网上投票教师是否可以申请多个评选项目；是：可以申请多个；否：只能申请一个
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKYSQDGPXXM {get;set;}

           /// <summary>
           /// Desc:099重修成绩最大值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXCJMAX {get;set;}

           /// <summary>
           /// Desc:099毕业设计学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJ_XN {get;set;}

           /// <summary>
           /// Desc:099毕业设计学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJ_XQ {get;set;}

           /// <summary>
           /// Desc:099 学生参照系统自愿提示评语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSJXPJLSBJ {get;set;}

           /// <summary>
           /// Desc:099满意度调查评价字数限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS_JSMYDPJ_PJCS {get;set;}

           /// <summary>
           /// Desc:099下载学生的开题报告及论文的网址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZDZ {get;set;}

           /// <summary>
           /// Desc:N04教务处控制二级学院是否可以在授课进度安排表中录入教师，格式为2005-200611，即学年学期加开关，开关为最后一位，1代表可以录入教师，否则不可以录入教师。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKJDKZ {get;set;}

           /// <summary>
           /// Desc:099论文下载密钥
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KEYWORD {get;set;}

           /// <summary>
           /// Desc:099中国矿业大学专用，没有参与问卷调查不可以选课开关控制设置及提示信息组成。格式：1|XXXX。1：表示控制。0：表示不控制。XXXX：表示控制以后学生没有参与调查进行提示信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WJDCKGTS {get;set;}

           /// <summary>
           /// Desc:099单开班重修学生选择优先控制开关。1：表示控制不及格学生	优选选择；0：不控制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DKBCXXSYXJKG {get;set;}

           /// <summary>
           /// Desc:N13打印成绩总表时，属于“其他获得学分”的课程归属名称，多个用逗号隔开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDY_QTXF_KCGS {get;set;}

           /// <summary>
           /// Desc:N13打印成绩总表时，“其他获得学分”一栏将显示的课程归属，多个用逗号隔开
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJDY_QTXF_XSKZ {get;set;}

           /// <summary>
           /// Desc:是否选择校区预约教室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXZXQYYJS {get;set;}

           /// <summary>
           /// Desc:A3成绩单标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string A3CJDBT {get;set;}

           /// <summary>
           /// Desc:A3成绩单签章栏
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string A3CJDQZ {get;set;}

           /// <summary>
           /// Desc:A4成绩单标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string A4CJDBT {get;set;}

           /// <summary>
           /// Desc:A4成绩单签章栏
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string A4CJDQZ {get;set;}

           /// <summary>
           /// Desc:N99  短信内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXNR {get;set;}

           /// <summary>
           /// Desc:N99  成绩密码发送邮件时连接失败是否重试,0为否,1为是
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJSBSFCS {get;set;}

           /// <summary>
           /// Desc:科研训练与素质拓展要求学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQXF_SZTZ {get;set;}

           /// <summary>
           /// Desc:099评价的课程数限制，大于等于这个设置值评价优的课程数设置才有用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJKCSXZ {get;set;}

           /// <summary>
           /// Desc:评优的百分比，评优的百分比不能超过这个值，维护成1-100的数字(40就是不能超过40%)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PYKCBFB {get;set;}

           /// <summary>
           /// Desc:099不及格率达到要求时，出现在网页的提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJGLTS {get;set;}

           /// <summary>
           /// Desc:099逗号前表示低于此分数为不及格，后面表示不及格率，不符合该条件（大于该不及格率）需要再次确认后才能提交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJGL1 {get;set;}

           /// <summary>
           /// Desc:099不及格率达到要求时，出现在网页的提示语，需要再次确认后才能提交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJGLTS1 {get;set;}

           /// <summary>
           /// Desc:099优秀率达到要求时，出现在网页的提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXLTS {get;set;}

           /// <summary>
           /// Desc:099设置为1：在人机交互排课2中多个教师可根据每个教师的具体周次排课
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZNPK_DJSZC {get;set;}

           /// <summary>
           /// Desc:099学籍监控结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJJSSJ {get;set;}

           /// <summary>
           /// Desc:099学籍监控开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XJQSSJ {get;set;}

           /// <summary>
           /// Desc:099教师录成绩注意事项
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HNLG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJGQMCJZSZPCJ {get;set;}

           /// <summary>
           /// Desc:科研项目控制是否必填,0不必填，1必填
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KYXMBT {get;set;}

           /// <summary>
           /// Desc:社会实践控制是否必填,0不必填，1必填
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHSJBT {get;set;}

    }
}

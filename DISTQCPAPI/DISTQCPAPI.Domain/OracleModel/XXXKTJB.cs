﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XXXKTJB
    {
           public XXXKTJB(){


           }
           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? LB1 {get;set;}

           /// <summary>
           /// Desc:099类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? LB2 {get;set;}

           /// <summary>
           /// Desc:099类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? LB3 {get;set;}

           /// <summary>
           /// Desc:099类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? LB4 {get;set;}

           /// <summary>
           /// Desc:099类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? LB5 {get;set;}

           /// <summary>
           /// Desc:099类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? LB6 {get;set;}

           /// <summary>
           /// Desc:099合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? HJ {get;set;}

    }
}

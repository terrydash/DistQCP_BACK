﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XXYCPYJB
    {
           public XXYCPYJB(){


           }
           /// <summary>
           /// Desc:N99学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:N99学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:N99学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:N99选课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:N99评价周次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJZC {get;set;}

           /// <summary>
           /// Desc:N99职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:N99总评得分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? ZPDF {get;set;}

           /// <summary>
           /// Desc:N99课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:N99是否提交（保存：0,提交:1）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SFTJ {get;set;}

           /// <summary>
           /// Desc:N99提交时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJSJ {get;set;}

           /// <summary>
           /// Desc:099信息员评价标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJBS {get;set;}

    }
}

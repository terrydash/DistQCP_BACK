﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XXYCPZBDFB
    {
           public XXYCPZBDFB(){


           }
           /// <summary>
           /// Desc:N99学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:N99学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:N99学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:N99选课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:N99评价周次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJZC {get;set;}

           /// <summary>
           /// Desc:N99职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:N99评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJH {get;set;}

           /// <summary>
           /// Desc:N99课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:N99学生评价建议
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSPJJY {get;set;}

           /// <summary>
           /// Desc:N99学院审核状态（通过,不通过）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHZT {get;set;}

           /// <summary>
           /// Desc:N99学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHR {get;set;}

           /// <summary>
           /// Desc:N99学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHSJ {get;set;}

           /// <summary>
           /// Desc:N99评估中心确认（通过,不通过）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PGZXQR {get;set;}

           /// <summary>
           /// Desc:N99评估中心确认人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PGZXQRR {get;set;}

           /// <summary>
           /// Desc:N99评估中心确认时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PGZXQRSJ {get;set;}

    }
}

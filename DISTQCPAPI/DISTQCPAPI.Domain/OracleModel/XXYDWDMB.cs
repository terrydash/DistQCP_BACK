﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XXYDWDMB
    {
           public XXYDWDMB(){


           }
           /// <summary>
           /// Desc:单位代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DWDM {get;set;}

           /// <summary>
           /// Desc:单位名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DWMC {get;set;}

    }
}

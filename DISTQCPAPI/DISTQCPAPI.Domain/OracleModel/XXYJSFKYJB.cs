﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XXYJSFKYJB
    {
           public XXYJSFKYJB(){


           }
           /// <summary>
           /// Desc:N99N99学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:N99N99学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:N99N99选课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:N99评价周次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJZC {get;set;}

           /// <summary>
           /// Desc:N99职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGH {get;set;}

           /// <summary>
           /// Desc:N99课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:N99评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJH {get;set;}

           /// <summary>
           /// Desc:N99反馈意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FKYJ {get;set;}

           /// <summary>
           /// Desc:N99反馈时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FKSJ {get;set;}

           /// <summary>
           /// Desc:N99是否提交（保存：0,提交:1）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SFTJ {get;set;}

           /// <summary>
           /// Desc:N99提交时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJSJ {get;set;}

           /// <summary>
           /// Desc:N99评估中心审核状态（通过,不通过）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PGZXSHZT {get;set;}

           /// <summary>
           /// Desc:N99评估中心审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PGZXSHR {get;set;}

           /// <summary>
           /// Desc:N99评估中心审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PGZXSHSJ {get;set;}

           /// <summary>
           /// Desc:099学院反馈意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYFKYJ {get;set;}

           /// <summary>
           /// Desc:099学院反馈者
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYFKR {get;set;}

    }
}

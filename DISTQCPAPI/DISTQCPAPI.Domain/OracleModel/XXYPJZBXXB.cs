﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XXYPJZBXXB
    {
           public XXYPJZBXXB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099对象
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short DX {get;set;}

           /// <summary>
           /// Desc:099一级指标代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJZBDM {get;set;}

           /// <summary>
           /// Desc:099评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJH {get;set;}

           /// <summary>
           /// Desc:099评价内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNR {get;set;}

           /// <summary>
           /// Desc:099打分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DF {get;set;}

           /// <summary>
           /// Desc:099是否提交（保存：0；提交：1）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTJ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XYCJBBZB
    {
           public XYCJBBZB(){


           }
           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYLW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKSXX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXTJ {get;set;}

           /// <summary>
           /// Desc:099下拉框选项
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XLKXX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZTZXX {get;set;}

           /// <summary>
           /// Desc:099打印日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYRQ {get;set;}

           /// <summary>
           /// Desc:099成绩打印条件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DYCJTJ {get;set;}

           /// <summary>
           /// Desc:099成绩条件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJTJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJKSTJ {get;set;}

           /// <summary>
           /// Desc:099备注设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZSZ {get;set;}

           /// <summary>
           /// Desc:099学年学期设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XNXQSZ {get;set;}

           /// <summary>
           /// Desc:099页脚备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJBZ {get;set;}

           /// <summary>
           /// Desc:099审核人中文姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHRZWXM {get;set;}

           /// <summary>
           /// Desc:099审核人英文姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHRYWXM {get;set;}

           /// <summary>
           /// Desc:099功能模块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GNMKDM {get;set;}

           /// <summary>
           /// Desc:学位课条件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XWKTJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCHG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLK {get;set;}

    }
}

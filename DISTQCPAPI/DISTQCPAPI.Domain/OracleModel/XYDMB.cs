﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XYDMB
    {
           public XYDMB(){


           }
           /// <summary>
           /// Desc:001学院代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XYDM {get;set;}

           /// <summary>
           /// Desc:002学院名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:003课程归属
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCGS {get;set;}

           /// <summary>
           /// Desc:004校区代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQDM {get;set;}

           /// <summary>
           /// Desc:099学院英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYYWMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYJC {get;set;}

           /// <summary>
           /// Desc:学院类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYLX {get;set;}

           /// <summary>
           /// Desc:099学院主页链接
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYZYLJ {get;set;}

           /// <summary>
           /// Desc:毕业论文是否可选
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYLWSFKX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYYHSFKX {get;set;}

           /// <summary>
           /// Desc:是否可以编辑【其它课上课时间地点安排】的时间、地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFBJ {get;set;}

           /// <summary>
           /// Desc:099是否计算每个开课学院每类教室类别的可用学时数,是：计算，否：不计算
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJSZHXS {get;set;}

           /// <summary>
           /// Desc:099学院是否参加统计高基报表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYJGDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TYJGMC {get;set;}

           /// <summary>
           /// Desc:099教室是否全校可借以及审批
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGY {get;set;}

    }
}

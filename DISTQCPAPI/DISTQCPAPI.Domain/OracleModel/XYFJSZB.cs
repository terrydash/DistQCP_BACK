﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XYFJSZB
    {
           public XYFJSZB(){


           }
           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short NJ {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099楼号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LH {get;set;}

           /// <summary>
           /// Desc:099房号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FH {get;set;}

           /// <summary>
           /// Desc:099学院房间设置表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? FPCWS {get;set;}

           /// <summary>
           /// Desc:099起始床位号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QSCWH {get;set;}

           /// <summary>
           /// Desc:099结束床位号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSCWH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SYCWS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYFPQSCWS {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CWH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BJMC {get;set;}

    }
}

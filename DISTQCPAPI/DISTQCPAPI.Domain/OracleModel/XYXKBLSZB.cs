﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XYXKBLSZB
    {
           public XYXKBLSZB(){


           }
           /// <summary>
           /// Desc:序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYDM {get;set;}

           /// <summary>
           /// Desc:学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJ {get;set;}

    }
}

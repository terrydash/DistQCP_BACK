﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XYYXBYLWB
    {
           public XYYXBYLWB(){


           }
           /// <summary>
           /// Desc:000编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BH {get;set;}

           /// <summary>
           /// Desc:000年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:000学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYDM {get;set;}

           /// <summary>
           /// Desc:000学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:000专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:000专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:000学生
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XS {get;set;}

           /// <summary>
           /// Desc:000论文题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LWTM {get;set;}

           /// <summary>
           /// Desc:000论文路径
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PATH {get;set;}

    }
}

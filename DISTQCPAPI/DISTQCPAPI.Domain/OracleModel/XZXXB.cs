﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class XZXXB
    {
           public XZXXB(){


           }
           /// <summary>
           /// Desc:说明文件名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZLMC {get;set;}

           /// <summary>
           /// Desc:上传时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? SCSJ {get;set;}

           /// <summary>
           /// Desc:路径
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PATH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MXDDX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WJLXDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WJLXMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SCR {get;set;}

    }
}

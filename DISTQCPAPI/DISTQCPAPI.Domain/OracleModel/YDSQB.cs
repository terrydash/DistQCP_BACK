﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YDSQB
    {
           public YDSQB(){


           }
           /// <summary>
           /// Desc:N02申请序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SQXH {get;set;}

           /// <summary>
           /// Desc:N02学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:N02姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSNY {get;set;}

           /// <summary>
           /// Desc:N02年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:N02学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:N02专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:N02学制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XZ {get;set;}

           /// <summary>
           /// Desc:N02行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:N02学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:N02学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:N02异动类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDLB {get;set;}

           /// <summary>
           /// Desc:N02异动原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDYY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDSM {get;set;}

           /// <summary>
           /// Desc:N02异动申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:N02审批人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SPR {get;set;}

           /// <summary>
           /// Desc:N02审批时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SPSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SPSFTG {get;set;}

           /// <summary>
           /// Desc:N02是否异动
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYD {get;set;}

           /// <summary>
           /// Desc:异动后班级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YDHBJ {get;set;}

           /// <summary>
           /// Desc:099学院审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHJG {get;set;}

           /// <summary>
           /// Desc:099学院审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHR {get;set;}

           /// <summary>
           /// Desc:099学院审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSHSJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YHB
    {
           public YHB(){


           }
           /// <summary>
           /// Desc:099用户名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHM {get;set;}

           /// <summary>
           /// Desc:099口令
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KL {get;set;}

           /// <summary>
           /// Desc:099角色
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JS {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099所在单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SZDW {get;set;}

           /// <summary>
           /// Desc:T:停用该用户，F:开启该用户
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CXYYHMM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string IPDZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MACDZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCQBXX {get;set;}

           /// <summary>
           /// Desc:N099 可以检测的功能
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KJCGN {get;set;}

           /// <summary>
           /// Desc:N099 登陆时提示检测的功能
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLSTSJCGN {get;set;}

           /// <summary>
           /// Desc:N099 是否强制检测"登陆时提示检测的功能"
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFQZJC {get;set;}

           /// <summary>
           /// Desc:N99 教师成绩录入密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSKCMM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XXMC {get;set;}

           /// <summary>
           /// Desc:099财务系统用户
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFCWYH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLMKL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AUTHORITIES {get;set;}

           /// <summary>
           /// Desc:密码生效时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MMSXSJ {get;set;}

    }
}

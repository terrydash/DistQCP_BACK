﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YHQXB
    {
           public YHQXB(){


           }
           /// <summary>
           /// Desc:099用户名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHM {get;set;}

           /// <summary>
           /// Desc:099功能模块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GNMKDM {get;set;}

           /// <summary>
           /// Desc:099功能模块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GNMKMC {get;set;}

           /// <summary>
           /// Desc:099对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? WR {get;set;}

           /// <summary>
           /// Desc:099不可以操作的学年学期的设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKCZXNXQ {get;set;}

           /// <summary>
           /// Desc:099是否自动提示操作说明，注：1表示自动提示，反之不做自动提示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZDTSBJ {get;set;}

    }
}

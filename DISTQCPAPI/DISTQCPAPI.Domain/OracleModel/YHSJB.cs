﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YHSJB
    {
           public YHSJB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM01 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM02 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM03 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM04 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM05 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM06 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM07 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM08 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM09 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM10 {get;set;}

           /// <summary>
           /// Desc:099序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM11 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM12 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM13 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM14 {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SFXM15 {get;set;}

           /// <summary>
           /// Desc:099实收合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? SSHJ {get;set;}

           /// <summary>
           /// Desc:099学院名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFLX {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DTJZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFDY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BDH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFRQ {get;set;}

           /// <summary>
           /// Desc:099流水号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LSH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CZLX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZKZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFBM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJMC {get;set;}

           /// <summary>
           /// Desc:099性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZH {get;set;}

           /// <summary>
           /// Desc:099银行帐号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YHZH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YJNX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? YHQJE {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099凭证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PZH {get;set;}

           /// <summary>
           /// Desc:099单据打印流水号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DJDYLSH {get;set;}

           /// <summary>
           /// Desc:099修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGSJ {get;set;}

           /// <summary>
           /// Desc:099修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGR {get;set;}

           /// <summary>
           /// Desc:099修改原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XGYY {get;set;}

           /// <summary>
           /// Desc:099标识流水号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZLSH {get;set;}

           /// <summary>
           /// Desc:099是否银行导入
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYHDR {get;set;}

           /// <summary>
           /// Desc:是否统计(连打250个学生后统计的记录)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTJ {get;set;}

           /// <summary>
           /// Desc:票据类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJLX {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YHSKKPSZB
    {
           public YHSKKPSZB(){


           }
           /// <summary>
           /// Desc:099用户名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KPR {get;set;}

           /// <summary>
           /// Desc:099执收单位编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSDWBM {get;set;}

    }
}

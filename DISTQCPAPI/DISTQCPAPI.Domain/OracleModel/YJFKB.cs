﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YJFKB
    {
           public YJFKB(){


           }
           /// <summary>
           /// Desc:099用户代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YHDM {get;set;}

           /// <summary>
           /// Desc:099用户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YHMC {get;set;}

           /// <summary>
           /// Desc:099教学方面课堂
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXFMKT {get;set;}

           /// <summary>
           /// Desc:099教学方面实践
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXFMSJ {get;set;}

           /// <summary>
           /// Desc:099教学管理方面
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXGLFM {get;set;}

           /// <summary>
           /// Desc:099学生学习课堂
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXXKT {get;set;}

           /// <summary>
           /// Desc:099学生学习实践
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXXSJ {get;set;}

           /// <summary>
           /// Desc:009教学设施方面
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXSSFM {get;set;}

           /// <summary>
           /// Desc:099其他方面
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QTFM {get;set;}

           /// <summary>
           /// Desc:099反馈年月
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FKNY {get;set;}

           /// <summary>
           /// Desc:099反馈时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FKSJ {get;set;}

           /// <summary>
           /// Desc:099录入状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRZT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FKSH {get;set;}

           /// <summary>
           /// Desc:099是否可查
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKC {get;set;}

    }
}

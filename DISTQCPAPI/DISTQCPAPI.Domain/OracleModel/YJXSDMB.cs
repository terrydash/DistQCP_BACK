﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YJXSDMB
    {
           public YJXSDMB(){


           }
           /// <summary>
           /// Desc:001研究形式代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YJXSDM {get;set;}

           /// <summary>
           /// Desc:002研究形式名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YJXSMC {get;set;}

    }
}

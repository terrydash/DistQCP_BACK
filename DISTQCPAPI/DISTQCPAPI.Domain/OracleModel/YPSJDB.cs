﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YPSJDB
    {
           public YPSJDB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099排课时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string WH {get;set;}

           /// <summary>
           /// Desc:099几节连排
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JJLP {get;set;}

           /// <summary>
           /// Desc:099单双周
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DSZ {get;set;}

    }
}

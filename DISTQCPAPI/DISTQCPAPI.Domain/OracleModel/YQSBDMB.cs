﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YQSBDMB
    {
           public YQSBDMB(){


           }
           /// <summary>
           /// Desc:001实验仪器代码（自动排课实验课考虑仪器是否够用）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YQDM {get;set;}

           /// <summary>
           /// Desc:002实验仪器名称（自动排课实验课考虑仪器是否够用）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQMC {get;set;}

           /// <summary>
           /// Desc:003实验仪器套数（自动排课实验课考虑仪器是否够用）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YQTS {get;set;}

    }
}

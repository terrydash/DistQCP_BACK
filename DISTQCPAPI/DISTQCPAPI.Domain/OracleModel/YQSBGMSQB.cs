﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YQSBGMSQB
    {
           public YQSBGMSQB(){


           }
           /// <summary>
           /// Desc:N99申请号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SQH {get;set;}

           /// <summary>
           /// Desc:N99仪器编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQBH {get;set;}

           /// <summary>
           /// Desc:N99仪器名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQMC {get;set;}

           /// <summary>
           /// Desc:N99型号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:N99规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GG {get;set;}

           /// <summary>
           /// Desc:N99数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SL {get;set;}

           /// <summary>
           /// Desc:N99单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? DJ {get;set;}

           /// <summary>
           /// Desc:N99申请日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQRQ {get;set;}

           /// <summary>
           /// Desc:N99设备性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SBXZ {get;set;}

           /// <summary>
           /// Desc:N99厂家或产地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJHCD {get;set;}

           /// <summary>
           /// Desc:N99仪器分类
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQFL {get;set;}

           /// <summary>
           /// Desc:N99隶属实验室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSSYS {get;set;}

           /// <summary>
           /// Desc:N99隶属部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LSBM {get;set;}

           /// <summary>
           /// Desc:N99所属实验名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSSYM {get;set;}

           /// <summary>
           /// Desc:N99状况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZK {get;set;}

           /// <summary>
           /// Desc:N99备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:N99申请人编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQRBH {get;set;}

           /// <summary>
           /// Desc:N99申请人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQRXM {get;set;}

           /// <summary>
           /// Desc:N99购买原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GMYY {get;set;}

           /// <summary>
           /// Desc:N99购买用途
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GMYT {get;set;}

           /// <summary>
           /// Desc:N99审核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHJG {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YQSBJYB
    {
           public YQSBJYB(){


           }
           /// <summary>
           /// Desc:N99借用号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal JYH {get;set;}

           /// <summary>
           /// Desc:N99仪器编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQBH {get;set;}

           /// <summary>
           /// Desc:N99仪器名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQMC {get;set;}

           /// <summary>
           /// Desc:N99借用人编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYRBH {get;set;}

           /// <summary>
           /// Desc:N99借用人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYRXM {get;set;}

           /// <summary>
           /// Desc:N99用途
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YT {get;set;}

           /// <summary>
           /// Desc:N99借用日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYRQ {get;set;}

           /// <summary>
           /// Desc:N99归还日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GHRQ {get;set;}

           /// <summary>
           /// Desc:N99经手人编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSRBH {get;set;}

           /// <summary>
           /// Desc:N99经手人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSRXM {get;set;}

           /// <summary>
           /// Desc:N99备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:N99是否已归还
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFJGH {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YQSBWXB
    {
           public YQSBWXB(){


           }
           /// <summary>
           /// Desc:N99维修号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal WXH {get;set;}

           /// <summary>
           /// Desc:N99仪器编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQBH {get;set;}

           /// <summary>
           /// Desc:N99仪器名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YQMC {get;set;}

           /// <summary>
           /// Desc:N99损坏日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHRQ {get;set;}

           /// <summary>
           /// Desc:N99责任人类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZRRLX {get;set;}

           /// <summary>
           /// Desc:N99责任人编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZRRBH {get;set;}

           /// <summary>
           /// Desc:N99责任人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZRR {get;set;}

           /// <summary>
           /// Desc:N99责任人单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZRRDW {get;set;}

           /// <summary>
           /// Desc:N99处理人编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLRBH {get;set;}

           /// <summary>
           /// Desc:N99处理人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLRXM {get;set;}

           /// <summary>
           /// Desc:N99原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YY {get;set;}

           /// <summary>
           /// Desc:N99处理结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CLJG {get;set;}

           /// <summary>
           /// Desc:N99维修人或单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXR {get;set;}

           /// <summary>
           /// Desc:N99维修结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXJG {get;set;}

           /// <summary>
           /// Desc:N99维修日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXRQ {get;set;}

           /// <summary>
           /// Desc:N99维修费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXFY {get;set;}

           /// <summary>
           /// Desc:N99备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

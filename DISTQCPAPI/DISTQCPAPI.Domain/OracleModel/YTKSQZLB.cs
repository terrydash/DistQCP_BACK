﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YTKSQZLB
    {
           public YTKSQZLB(){


           }
           /// <summary>
           /// Desc:ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ID {get;set;}

           /// <summary>
           /// Desc:学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:选课课号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:学号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:文件名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WJM {get;set;}

           /// <summary>
           /// Desc:文件格式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WJGS {get;set;}

           /// <summary>
           /// Desc:保存路径
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BCLJ {get;set;}

           /// <summary>
           /// Desc:上传时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? SCSJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YXBLSZB
    {
           public YXBLSZB(){


           }
           /// <summary>
           /// Desc:002学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:002学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:002分数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FS {get;set;}

           /// <summary>
           /// Desc:002比率
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BL {get;set;}

    }
}

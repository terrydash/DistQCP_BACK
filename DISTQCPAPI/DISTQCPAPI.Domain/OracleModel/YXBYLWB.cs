﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YXBYLWB
    {
           public YXBYLWB(){


           }
           /// <summary>
           /// Desc:N99学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:N99学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:N99学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:N99姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:N99选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:N99毕业设计题目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BYSJTM {get;set;}

           /// <summary>
           /// Desc:N99答辩委员会意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DBWYHYJ {get;set;}

           /// <summary>
           /// Desc:N99专家意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZJYJ {get;set;}

           /// <summary>
           /// Desc:N99学院委员会意见
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYWYHYJ {get;set;}

           /// <summary>
           /// Desc:N99备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:N099入选情况
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RXQK {get;set;}

           /// <summary>
           /// Desc:N099等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJ {get;set;}

           /// <summary>
           /// Desc:099是否获得证书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFHDZS {get;set;}

    }
}

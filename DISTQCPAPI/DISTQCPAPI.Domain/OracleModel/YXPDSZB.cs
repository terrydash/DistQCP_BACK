﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YXPDSZB
    {
           public YXPDSZB(){


           }
           /// <summary>
           /// Desc:099评定名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PDMC {get;set;}

           /// <summary>
           /// Desc:099院系代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YXDM {get;set;}

           /// <summary>
           /// Desc:099院系名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXMC {get;set;}

    }
}

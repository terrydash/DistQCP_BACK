﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YXXKLCB
    {
           public YXXKLCB(){


           }
           /// <summary>
           /// Desc:099轮次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short LC {get;set;}

           /// <summary>
           /// Desc:099起始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSSJ {get;set;}

           /// <summary>
           /// Desc:099结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:099是否改退选
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGTX {get;set;}

           /// <summary>
           /// Desc:099当前轮次
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DQLC {get;set;}

    }
}

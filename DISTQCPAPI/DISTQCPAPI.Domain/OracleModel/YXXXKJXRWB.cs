﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YXXXKJXRWB
    {
           public YXXXKJXRWB(){


           }
           /// <summary>
           /// Desc:099教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XF {get;set;}

           /// <summary>
           /// Desc:099周学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:099考核方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHFS {get;set;}

           /// <summary>
           /// Desc:099教学班序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JXBXH {get;set;}

           /// <summary>
           /// Desc:099课程性质
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCXZ {get;set;}

           /// <summary>
           /// Desc:099课程类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLB {get;set;}

           /// <summary>
           /// Desc:099教学班数上限
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JXBSSX {get;set;}

           /// <summary>
           /// Desc:099教学班数下限
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JXBSXX {get;set;}

           /// <summary>
           /// Desc:099收费项目
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JXBBS {get;set;}

           /// <summary>
           /// Desc:099人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? RS {get;set;}

           /// <summary>
           /// Desc:099开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:099开课系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKX {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099分组标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZBS {get;set;}

           /// <summary>
           /// Desc:099起始周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? QSZ {get;set;}

           /// <summary>
           /// Desc:099结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSZ {get;set;}

           /// <summary>
           /// Desc:099教材征订代号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZDDH {get;set;}

           /// <summary>
           /// Desc:099教材名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCMC {get;set;}

           /// <summary>
           /// Desc:099作者
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZ {get;set;}

           /// <summary>
           /// Desc:099出版社
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CBS {get;set;}

           /// <summary>
           /// Desc:099版别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BB {get;set;}

           /// <summary>
           /// Desc:099是否优秀教材
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYXJC {get;set;}

           /// <summary>
           /// Desc:099校区要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQYQ {get;set;}

           /// <summary>
           /// Desc:099场地标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CDBS {get;set;}

           /// <summary>
           /// Desc:099任务下发标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RWXFBS {get;set;}

           /// <summary>
           /// Desc:099安排标志
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string APBZ {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:099选课状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKZT {get;set;}

           /// <summary>
           /// Desc:099上课地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKDD {get;set;}

           /// <summary>
           /// Desc:099上课时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKSJ {get;set;}

           /// <summary>
           /// Desc:099辅修标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? FXBS {get;set;}

           /// <summary>
           /// Desc:099板块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMC {get;set;}

           /// <summary>
           /// Desc:099等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? DJ {get;set;}

           /// <summary>
           /// Desc:099教师序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JSXH {get;set;}

           /// <summary>
           /// Desc:099录入时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRSJ {get;set;}

           /// <summary>
           /// Desc:099密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MM {get;set;}

           /// <summary>
           /// Desc:099最多实验室个数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SYSGS {get;set;}

           /// <summary>
           /// Desc:099录入设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LRSZ {get;set;}

           /// <summary>
           /// Desc:099实验容量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SYRL {get;set;}

           /// <summary>
           /// Desc:099是否实验班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSYB {get;set;}

           /// <summary>
           /// Desc:099是否要教室
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYJS {get;set;}

           /// <summary>
           /// Desc:099教材出版时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCCBSJ {get;set;}

           /// <summary>
           /// Desc:099专业标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZYBS {get;set;}

           /// <summary>
           /// Desc:099考试时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ {get;set;}

           /// <summary>
           /// Desc:099限制对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZDX {get;set;}

           /// <summary>
           /// Desc:099容量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? RL {get;set;}

           /// <summary>
           /// Desc:099预选人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YXRS {get;set;}

           /// <summary>
           /// Desc:099平行班标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PXBBS {get;set;}

           /// <summary>
           /// Desc:099提交确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJQR {get;set;}

           /// <summary>
           /// Desc:099是否要机房
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYJF {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:099实验标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYBJ {get;set;}

           /// <summary>
           /// Desc:099职称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZC {get;set;}

           /// <summary>
           /// Desc:099班级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BJMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JYS {get;set;}

           /// <summary>
           /// Desc:099平时成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSCJ {get;set;}

           /// <summary>
           /// Desc:099期末成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QMCJ {get;set;}

           /// <summary>
           /// Desc:099实验成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYCJ {get;set;}

           /// <summary>
           /// Desc:099起始结束周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSJSZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYCDBS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZCJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SHTG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? BJGL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? YXL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PRICE {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJLRFS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKPJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSRQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCBH {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXRW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKFSMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JKXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SJXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKDC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFPDG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKPX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKQR1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MMFSCG {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCJSSL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCSJXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTKXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KNSJXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWSJXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KWXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYBJS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HJQK {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBL1 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBL2 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBL3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBL4 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBL5 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBL6 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBL7 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBL8 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBL9 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CJBL10 {get;set;}

           /// <summary>
           /// Desc:099提交成绩时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJCJSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSCJCX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZCJCX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QMCJCX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYCJCX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSLRSZ {get;set;}

           /// <summary>
           /// Desc:099院系提交确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXTJQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SYJX {get;set;}

           /// <summary>
           /// Desc:099备注信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZXX {get;set;}

           /// <summary>
           /// Desc:099提交类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJLX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFKZP {get;set;}

           /// <summary>
           /// Desc:099院系提交确认人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXTJQRR {get;set;}

           /// <summary>
           /// Desc:099院系提交确认时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXTJQRSJ {get;set;}

           /// <summary>
           /// Desc:099提交确认人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJQRR {get;set;}

           /// <summary>
           /// Desc:099提交确认时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJQRSJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? YSRL {get;set;}

           /// <summary>
           /// Desc:099可录成绩起始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KLCJQSSJ {get;set;}

           /// <summary>
           /// Desc:099可录成绩结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KLCJJSSJ {get;set;}

           /// <summary>
           /// Desc:099录成绩级制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LCJJZ {get;set;}

           /// <summary>
           /// Desc:099最高平时成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PSCJZG {get;set;}

           /// <summary>
           /// Desc:099最高期中成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QZCJZG {get;set;}

           /// <summary>
           /// Desc:099最高期末成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QMCJZG {get;set;}

           /// <summary>
           /// Desc:099最高实验成绩
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SYCJZG {get;set;}

           /// <summary>
           /// Desc:099分层教学成绩系数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FCJXCJXS {get;set;}

           /// <summary>
           /// Desc:099录入教材职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJCZGH {get;set;}

           /// <summary>
           /// Desc:099录入教材时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JCBZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LJCSJ {get;set;}

           /// <summary>
           /// Desc:099被改革的课程标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GGKC {get;set;}

           /// <summary>
           /// Desc:099录成绩教师职工号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LCJJSZGH {get;set;}

           /// <summary>
           /// Desc:099系提交确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTJQR {get;set;}

           /// <summary>
           /// Desc:099系提交确认人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTJQRR {get;set;}

           /// <summary>
           /// Desc:099系提交确认时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XTJQRSJ {get;set;}

           /// <summary>
           /// Desc:099工作量课程类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GZLKCLX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJ_QZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSSJD_QZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSRQ_QZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHFS_QZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KSFS_QZ {get;set;}

    }
}

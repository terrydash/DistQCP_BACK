﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YYDJDMB
    {
           public YYDJDMB(){


           }
           /// <summary>
           /// Desc:099英语等级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string YYDJ {get;set;}

           /// <summary>
           /// Desc:099等级名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FZ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YYJXCDXXB
    {
           public YYJXCDXXB(){


           }
           /// <summary>
           /// Desc:099教室编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSBH {get;set;}

           /// <summary>
           /// Desc:099教室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSMC {get;set;}

           /// <summary>
           /// Desc:099教室类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSLB {get;set;}

           /// <summary>
           /// Desc:099座位数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? ZWS {get;set;}

           /// <summary>
           /// Desc:099建筑面积
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? JZMJ {get;set;}

           /// <summary>
           /// Desc:099校区代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQDM {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

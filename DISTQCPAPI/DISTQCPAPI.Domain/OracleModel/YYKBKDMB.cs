﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class YYKBKDMB
    {
           public YYKBKDMB(){


           }
           /// <summary>
           /// Desc:099板块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BKDM {get;set;}

           /// <summary>
           /// Desc:099板块名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKMC {get;set;}

    }
}

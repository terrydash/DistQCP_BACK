﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZCKGSZB
    {
           public ZCKGSZB(){


           }
           /// <summary>
           /// Desc:099开关标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? KGBS1 {get;set;}

           /// <summary>
           /// Desc:099开关标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? KGBS2 {get;set;}

    }
}

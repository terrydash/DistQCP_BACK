﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZCTJB
    {
           public ZCTJB(){


           }
           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099总人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZRS {get;set;}

           /// <summary>
           /// Desc:099注册人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ZCRS {get;set;}

           /// <summary>
           /// Desc:099未注册人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? WZCRS {get;set;}

           /// <summary>
           /// Desc:099公费未注册人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? GFWZCRS {get;set;}

           /// <summary>
           /// Desc:099国有民办注册人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? GYMBZCRS {get;set;}

           /// <summary>
           /// Desc:099国有民办未注册人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? GYMBWZCRS {get;set;}

           /// <summary>
           /// Desc:099公费报到人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? GFBDRS {get;set;}

           /// <summary>
           /// Desc:099公费未报到人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? GFWBDRS {get;set;}

           /// <summary>
           /// Desc:099国有民办报到人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? GYMBBDRS {get;set;}

           /// <summary>
           /// Desc:099国有民办报未报到人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? GYMBWBDRS {get;set;}

           /// <summary>
           /// Desc:099行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:099变动人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? BDRS {get;set;}

           /// <summary>
           /// Desc:099未报到人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? WBDRS {get;set;}

           /// <summary>
           /// Desc:099公费注册人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? GFZCRS {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZGKDJSTKPFB
    {
           public ZGKDJSTKPFB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099听课次数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TKCS {get;set;}

           /// <summary>
           /// Desc:099评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short PJH {get;set;}

           /// <summary>
           /// Desc:099评价内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJNR {get;set;}

           /// <summary>
           /// Desc:099评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PF {get;set;}

           /// <summary>
           /// Desc:099是否提交
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SFTJ {get;set;}

           /// <summary>
           /// Desc:099评价时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJSJ {get;set;}

           /// <summary>
           /// Desc:099开课学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KKXY {get;set;}

           /// <summary>
           /// Desc:099上课地点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKDD {get;set;}

           /// <summary>
           /// Desc:099授课年级专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKNJZY {get;set;}

           /// <summary>
           /// Desc:099听课类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKLX {get;set;}

           /// <summary>
           /// Desc:099评价信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJXX {get;set;}

           /// <summary>
           /// Desc:099提交时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJSJ {get;set;}

           /// <summary>
           /// Desc:099听课时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKSJ {get;set;}

           /// <summary>
           /// Desc:099星期几
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XQJ {get;set;}

           /// <summary>
           /// Desc:099第几节
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DJJ {get;set;}

           /// <summary>
           /// Desc:099
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKDW {get;set;}

           /// <summary>
           /// Desc:009ting
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TKXM {get;set;}

           /// <summary>
           /// Desc:对教师授课或其它方面的具体意见或建议
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

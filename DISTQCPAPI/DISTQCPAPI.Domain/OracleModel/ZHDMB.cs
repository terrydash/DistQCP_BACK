﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZHDMB
    {
           public ZHDMB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099组号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZHDM {get;set;}

           /// <summary>
           /// Desc:099组号名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHMC {get;set;}

    }
}

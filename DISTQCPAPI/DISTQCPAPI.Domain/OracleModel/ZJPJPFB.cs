﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZJPJPFB
    {
           public ZJPJPFB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XQ {get;set;}

           /// <summary>
           /// Desc:099主管教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZGJS {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099选课课号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XKKH {get;set;}

           /// <summary>
           /// Desc:099课程代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCDM {get;set;}

           /// <summary>
           /// Desc:099课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099是否记分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? SFJF {get;set;}

           /// <summary>
           /// Desc:099评价号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PJH {get;set;}

           /// <summary>
           /// Desc:099评分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PF {get;set;}

           /// <summary>
           /// Desc:099对应分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public Single? DYF {get;set;}

           /// <summary>
           /// Desc:099次数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CS {get;set;}

           /// <summary>
           /// Desc:099评语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PY {get;set;}

           /// <summary>
           /// Desc:099评价时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJSJ {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZJPJXQSZB
    {
           public ZJPJXQSZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099起始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QSSJ {get;set;}

           /// <summary>
           /// Desc:099结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSSJ {get;set;}

           /// <summary>
           /// Desc:099评价方式 0 细评，1 粗评，2 两者皆有
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PJFS {get;set;}

    }
}

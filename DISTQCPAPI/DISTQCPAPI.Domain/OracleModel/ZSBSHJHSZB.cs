﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZSBSHJHSZB
    {
           public ZSBSHJHSZB(){


           }
           /// <summary>
           /// Desc:001教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:002升学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SXQ {get;set;}

           /// <summary>
           /// Desc:003学分要求1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ1 {get;set;}

           /// <summary>
           /// Desc:003学分要求2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ2 {get;set;}

           /// <summary>
           /// Desc:003学分要求3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ3 {get;set;}

           /// <summary>
           /// Desc:003学分要求4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ4 {get;set;}

           /// <summary>
           /// Desc:003学分要求5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ5 {get;set;}

           /// <summary>
           /// Desc:003学分要求6
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ6 {get;set;}

           /// <summary>
           /// Desc:003学分要求7
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ7 {get;set;}

           /// <summary>
           /// Desc:003学分要求8
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ8 {get;set;}

           /// <summary>
           /// Desc:003学分要求9
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ9 {get;set;}

           /// <summary>
           /// Desc:003学分要求10
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ10 {get;set;}

           /// <summary>
           /// Desc:003学分要求11
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ11 {get;set;}

           /// <summary>
           /// Desc:003学分要求12
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ12 {get;set;}

           /// <summary>
           /// Desc:003学分要求13
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ13 {get;set;}

           /// <summary>
           /// Desc:003学分要求14
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ14 {get;set;}

           /// <summary>
           /// Desc:003学分要求15
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ15 {get;set;}

           /// <summary>
           /// Desc:003学分要求16
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ16 {get;set;}

           /// <summary>
           /// Desc:003学分要求17
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ17 {get;set;}

           /// <summary>
           /// Desc:003学分要求18
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ18 {get;set;}

           /// <summary>
           /// Desc:003学分要求19
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ19 {get;set;}

           /// <summary>
           /// Desc:003学分要求20
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? XFYQ20 {get;set;}

           /// <summary>
           /// Desc:002最低毕业学分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZDBYXF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYFX {get;set;}

    }
}

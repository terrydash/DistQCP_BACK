﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZSJHB
    {
           public ZSJHB(){


           }
           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short NJ {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099招生人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZSRS {get;set;}

    }
}

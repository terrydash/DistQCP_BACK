﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZSRQFSB
    {
           public ZSRQFSB(){


           }
           /// <summary>
           /// Desc:011教学计划号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:011省市名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LYS {get;set;}

           /// <summary>
           /// Desc:011科类
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string KL {get;set;}

           /// <summary>
           /// Desc:011分数线
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FSX {get;set;}

           /// <summary>
           /// Desc:011计划人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JHRS {get;set;}

           /// <summary>
           /// Desc:011批次
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PC {get;set;}

    }
}

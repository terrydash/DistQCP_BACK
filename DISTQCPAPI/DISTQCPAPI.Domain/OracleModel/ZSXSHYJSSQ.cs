﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZSXSHYJSSQ
    {
           public ZSXSHYJSSQ(){


           }
           /// <summary>
           /// Desc:099申请学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099申请学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:099是否申请，区别申请和学生推荐
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSQ {get;set;}

           /// <summary>
           /// Desc:099学院审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYSH {get;set;}

           /// <summary>
           /// Desc:099教务处审批
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWCSP {get;set;}

           /// <summary>
           /// Desc:099教学班数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JXBS {get;set;}

           /// <summary>
           /// Desc:099课程门数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMS {get;set;}

           /// <summary>
           /// Desc:099总学时
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXS {get;set;}

           /// <summary>
           /// Desc:099网上申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:099上课学生人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKXSRS {get;set;}

           /// <summary>
           /// Desc:099投票有效人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TPYXRS {get;set;}

           /// <summary>
           /// Desc:099投票人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TPRS {get;set;}

           /// <summary>
           /// Desc:099评选结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PXJG {get;set;}

           /// <summary>
           /// Desc:099教师上传个人资料1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSGRZL1 {get;set;}

           /// <summary>
           /// Desc:099教师上传个人资料2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSGRZL2 {get;set;}

           /// <summary>
           /// Desc:099教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099科目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCMC {get;set;}

           /// <summary>
           /// Desc:099有效上课人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXSKXSRS {get;set;}

           /// <summary>
           /// Desc:099评选项目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PXXMDM {get;set;}

           /// <summary>
           /// Desc:099 有效上课教师投票人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXSKJSRS {get;set;}

           /// <summary>
           /// Desc:099 无效上课教师投票人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXSKJSRS {get;set;}

           /// <summary>
           /// Desc:099 行政部门教师投票人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZBMJSRS {get;set;}

    }
}

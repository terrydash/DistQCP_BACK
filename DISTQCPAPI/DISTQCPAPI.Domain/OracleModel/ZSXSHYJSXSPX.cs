﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZSXSHYJSXSPX
    {
           public ZSXSHYJSXSPX(){


           }
           /// <summary>
           /// Desc:当前学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:当前学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:学生学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:教师职工号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JSZGH {get;set;}

           /// <summary>
           /// Desc:投票时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TPSJ {get;set;}

           /// <summary>
           /// Desc:学生姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXM {get;set;}

           /// <summary>
           /// Desc:教师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSXM {get;set;}

           /// <summary>
           /// Desc:099投中人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TZRS {get;set;}

           /// <summary>
           /// Desc:099评选项目代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PXXMDM {get;set;}

           /// <summary>
           /// Desc:099评选项目类别（老师、学生、行政部门)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PXLB {get;set;}

    }
}

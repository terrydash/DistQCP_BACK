﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZXDMB
    {
           public ZXDMB(){


           }
           /// <summary>
           /// Desc:099中学代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZXDM {get;set;}

           /// <summary>
           /// Desc:099中学名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZXMC {get;set;}

    }
}

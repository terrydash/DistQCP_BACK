﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZXJHSQXGSZB
    {
           public ZXJHSQXGSZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099开关标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KGBS {get;set;}

    }
}

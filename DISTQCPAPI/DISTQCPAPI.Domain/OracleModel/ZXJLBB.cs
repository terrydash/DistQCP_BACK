﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZXJLBB
    {
           public ZXJLBB(){


           }
           /// <summary>
           /// Desc:099助学金类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZXJLBDM {get;set;}

           /// <summary>
           /// Desc:099助学金类别名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZXJLBMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JE {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JB {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZXSNLTJB
    {
           public ZXSNLTJB(){


           }
           /// <summary>
           /// Desc:099编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BH {get;set;}

           /// <summary>
           /// Desc:099合计
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? HJ {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL17 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL18 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL19 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL20 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL21 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL22 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL23 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL24 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL25 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL26 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL27 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL28 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL29 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL30 {get;set;}

           /// <summary>
           /// Desc:099年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? NL31 {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZXYHB
    {
           public ZXYHB(){


           }
           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099角色
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JS {get;set;}

           /// <summary>
           /// Desc:099部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BM {get;set;}

           /// <summary>
           /// Desc:099IP
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string IP {get;set;}

           /// <summary>
           /// Desc:099MAC
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MAC {get;set;}

           /// <summary>
           /// Desc:099登入时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DRSJ {get;set;}

    }
}

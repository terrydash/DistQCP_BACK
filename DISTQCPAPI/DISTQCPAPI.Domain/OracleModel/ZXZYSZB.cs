﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZXZYSZB
    {
           public ZXZYSZB(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string JXJHH {get;set;}

           /// <summary>
           /// Desc:学生申请开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQKSSJ {get;set;}

           /// <summary>
           /// Desc:学生申请结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQJSSJ {get;set;}

           /// <summary>
           /// Desc:面试考试开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MSKSSJ {get;set;}

           /// <summary>
           /// Desc:面试考试结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MSJSSJ {get;set;}

           /// <summary>
           /// Desc:公示开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GSKSSJ {get;set;}

           /// <summary>
           /// Desc:公示结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GSJSSJ {get;set;}

           /// <summary>
           /// Desc:人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? RS {get;set;}

           /// <summary>
           /// Desc:专业接受条件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYJSTJ {get;set;}

           /// <summary>
           /// Desc:计划人数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? JHRS {get;set;}

           /// <summary>
           /// Desc:学生查看学院审核结果开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CKKSSJ {get;set;}

           /// <summary>
           /// Desc:学生所需预修课程占所有预修课程的比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXBL {get;set;}

    }
}

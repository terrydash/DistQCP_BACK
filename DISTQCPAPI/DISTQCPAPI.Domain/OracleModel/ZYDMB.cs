﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZYDMB
    {
           public ZYDMB(){


           }
           /// <summary>
           /// Desc:001专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:002专业名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:003专业类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYLB {get;set;}

           /// <summary>
           /// Desc:004学制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? XZ {get;set;}

           /// <summary>
           /// Desc:005学位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XW {get;set;}

           /// <summary>
           /// Desc:006专业培养目标
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYPYMB {get;set;}

           /// <summary>
           /// Desc:007专业培养要求
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYPYYQ {get;set;}

           /// <summary>
           /// Desc:008专业课程
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYKC {get;set;}

           /// <summary>
           /// Desc:009特色名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TSKC {get;set;}

           /// <summary>
           /// Desc:099专业英文名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYYWMC {get;set;}

           /// <summary>
           /// Desc:099托管学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXYDM {get;set;}

           /// <summary>
           /// Desc:099优先级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YXJ {get;set;}

           /// <summary>
           /// Desc:099所属系代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXDM {get;set;}

           /// <summary>
           /// Desc:099专业简称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYJC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKLB {get;set;}

           /// <summary>
           /// Desc:099备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZSZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TJZYMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFWY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFSF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFEXW {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KXLB {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKS {get;set;}

           /// <summary>
           /// Desc:099所属学院代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSXSXYDM {get;set;}

           /// <summary>
           /// Desc:099收费形式。设置为0表示按学年收费；设置为1表示按学期收费。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFXS {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XKJXPT {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BKFLMC {get;set;}

           /// <summary>
           /// Desc:099英文学位类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWXWLB {get;set;}

           /// <summary>
           /// Desc:099国编序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GBXH {get;set;}

    }
}

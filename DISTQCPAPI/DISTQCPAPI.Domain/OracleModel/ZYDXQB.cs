﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZYDXQB
    {
           public ZYDXQB(){


           }
           /// <summary>
           /// Desc:年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:短1学期社会实践
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXQSJ1 {get;set;}

           /// <summary>
           /// Desc:短1学期总教学周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXQZHXS1 {get;set;}

           /// <summary>
           /// Desc:短2学期社会实践
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXQSJ2 {get;set;}

           /// <summary>
           /// Desc:短2学期总教学周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXQZHXS2 {get;set;}

           /// <summary>
           /// Desc:短3学期社会实践
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXQSJ3 {get;set;}

           /// <summary>
           /// Desc:短3学期总教学周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DXQZHXS3 {get;set;}

           /// <summary>
           /// Desc:第1学期总教学周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHJSZ1 {get;set;}

           /// <summary>
           /// Desc:第2学期总教学周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHJSZ2 {get;set;}

           /// <summary>
           /// Desc:第3学期总教学周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHJSZ3 {get;set;}

           /// <summary>
           /// Desc:短4学期总教学周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHJSZ4 {get;set;}

           /// <summary>
           /// Desc:第5学期总教学周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHJSZ5 {get;set;}

           /// <summary>
           /// Desc:第6学期总教学周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHJSZ6 {get;set;}

           /// <summary>
           /// Desc:第7学期总教学周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHJSZ7 {get;set;}

           /// <summary>
           /// Desc:第8学期总教学周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZHJSZ8 {get;set;}

           /// <summary>
           /// Desc:专业制定人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYZDR {get;set;}

           /// <summary>
           /// Desc:专业审校人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYSXR {get;set;}

    }
}

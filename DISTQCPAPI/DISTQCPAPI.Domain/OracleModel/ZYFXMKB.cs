﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZYFXMKB
    {
           public ZYFXMKB(){


           }
           /// <summary>
           /// Desc:099专业方向模块代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYFXMKDM {get;set;}

           /// <summary>
           /// Desc:099专业方向模块名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYFXMKMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XFYQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BXXFYQ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MS {get;set;}

    }
}

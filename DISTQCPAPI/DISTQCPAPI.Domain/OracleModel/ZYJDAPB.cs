﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZYJDAPB
    {
           public ZYJDAPB(){


           }
           /// <summary>
           /// Desc:099专业代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z6 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z7 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z8 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z9 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z10 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z11 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z12 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z13 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z14 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z15 {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z16 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z17 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z18 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z19 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z20 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z21 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z22 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z23 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z24 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z25 {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XQ {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z1 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z2 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z3 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z4 {get;set;}

           /// <summary>
           /// Desc:099周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Z5 {get;set;}

    }
}

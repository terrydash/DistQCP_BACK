﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZYLBDMB
    {
           public ZYLBDMB(){


           }
           /// <summary>
           /// Desc:001专业类别代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYLBDM {get;set;}

           /// <summary>
           /// Desc:002专业类别名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYLBMC {get;set;}

    }
}

﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZYNLB
    {
           public ZYNLB(){


           }
           /// <summary>
           /// Desc:项目序号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XMXH {get;set;}

           /// <summary>
           /// Desc:年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NJ {get;set;}

           /// <summary>
           /// Desc:专业代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:项目名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMMC {get;set;}

           /// <summary>
           /// Desc:项目类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMLX {get;set;}

           /// <summary>
           /// Desc:项目内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XMNR {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BZ {get;set;}

    }
}

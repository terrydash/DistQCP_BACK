﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZYZCFSZB
    {
           public ZYZCFSZB(){


           }
           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short NJ {get;set;}

           /// <summary>
           /// Desc:099专业名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZYMC {get;set;}

           /// <summary>
           /// Desc:099大类代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DLDM {get;set;}

           /// <summary>
           /// Desc:099专业注册费
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? ZYZCF {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

    }
}

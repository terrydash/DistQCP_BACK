﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZZMMDMB
    {
           public ZZMMDMB(){


           }
           /// <summary>
           /// Desc:099政治面貌代码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZZMMDM {get;set;}

           /// <summary>
           /// Desc:099政治面貌名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZZMMMC {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string YWZZMMMC {get;set;}

    }
}

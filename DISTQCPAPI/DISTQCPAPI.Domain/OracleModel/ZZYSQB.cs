﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZZYSQB
    {
           public ZZYSQB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public short? NJ {get;set;}

           /// <summary>
           /// Desc:099学号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XH {get;set;}

           /// <summary>
           /// Desc:099姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XM {get;set;}

           /// <summary>
           /// Desc:099性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XB {get;set;}

           /// <summary>
           /// Desc:099出生年月
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CSNY {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZY {get;set;}

           /// <summary>
           /// Desc:099行政班
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XZB {get;set;}

           /// <summary>
           /// Desc:099宿舍号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SSH {get;set;}

           /// <summary>
           /// Desc:099联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LXDH {get;set;}

           /// <summary>
           /// Desc:099入学年月
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RXNY {get;set;}

           /// <summary>
           /// Desc:099申请转入学院
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQZRXY {get;set;}

           /// <summary>
           /// Desc:099申请转入专业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQZRZY {get;set;}

           /// <summary>
           /// Desc:099课程累计绩点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KCLJJD {get;set;}

           /// <summary>
           /// Desc:099高考总分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GKZF {get;set;}

           /// <summary>
           /// Desc:099高考地
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GKD {get;set;}

           /// <summary>
           /// Desc:099申请理由
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQLY {get;set;}

           /// <summary>
           /// Desc:099申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQSJ {get;set;}

           /// <summary>
           /// Desc:099缴费确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JFQR {get;set;}

           /// <summary>
           /// Desc:099考核结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHJG {get;set;}

           /// <summary>
           /// Desc:099考核评语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHPY {get;set;}

           /// <summary>
           /// Desc:099教务部确认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWBQR {get;set;}

           /// <summary>
           /// Desc:099是否异动
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFYD {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQZRZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQZRZYFX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQZRBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYDM {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZYFX {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SQZRXSCBJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXYQR {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XSXYQRYJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JWBQRYJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string XYQRYJ {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ZY_ZZY {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ZZYLB {get;set;}

           /// <summary>
           /// Desc:处分和奖励
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CFHJL {get;set;}

           /// <summary>
           /// Desc:099身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFZH {get;set;}

           /// <summary>
           /// Desc:是否国防生
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFGF {get;set;}

           /// <summary>
           /// Desc:是否特招生
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SFTZ {get;set;}

    }
}

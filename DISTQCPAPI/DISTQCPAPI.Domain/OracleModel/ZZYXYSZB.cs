﻿using System;
using System.Linq;
using System.Text;

namespace JWXTService.Domain.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class ZZYXYSZB
    {
           public ZZYXYSZB(){


           }
           /// <summary>
           /// Desc:099学年
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XN {get;set;}

           /// <summary>
           /// Desc:099学期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short XQ {get;set;}

           /// <summary>
           /// Desc:099年级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short NJ {get;set;}

           /// <summary>
           /// Desc:099学院
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string XY {get;set;}

           /// <summary>
           /// Desc:099考核成员
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KHCY {get;set;}

           /// <summary>
           /// Desc:099接收条件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JSTJ {get;set;}

    }
}

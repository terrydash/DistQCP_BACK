﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DISTQCPAPI.Domain.ViewModel
{
    public class JsonModel
    {
        public ResponseResult ResponseResult { get; set; }
        public object Data { get; set; }
        public string Msg { get; set; }
        public int DataCount { get; set; }
    }
    public enum ResponseResult
    {
        Ok = 1,//返回正常
        Notlogin = 2,//没有登录
        Powerlimit = 3,//没有权限
        Error = 4,//服务器问题
        Validationerror = 5,//提交的内容出现验证问题
        DataCollision = 6,//提交的数据与数据库冲突等问题
    }
}

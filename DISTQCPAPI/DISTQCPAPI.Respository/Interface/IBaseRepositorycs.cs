﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DISTQCPAPI.Respository.Interface
{
    public interface IBaseRepository<T> where T : class
    {
        T FindSingle(Expression<Func<T, bool>> exp = null);
        bool IsExist(Expression<Func<T, bool>> exp);
        IQueryable<T> Find(Expression<Func<T, bool>> exp = null);

        IQueryable<T> PageFind(int pageindex = 1, int pagesize = 10, Func<T, T> orderby = null,
            Expression<Func<T, bool>> exp = null);

        int GetCount(Expression<Func<T, bool>> exp = null);

        void Add(T entity);

        void BatchAdd(List<T> entities);
        

        void Update(T entity);

        void Delete(T entity);

        void Update(Expression<Func<T, bool>> where, Expression<Func<T, T>> entity);

        


        void Update(string setvalue, Expression<Func<T, bool>> where, T entity);
      


        void Delete(Expression<Func<T, bool>> exp);

    }
}

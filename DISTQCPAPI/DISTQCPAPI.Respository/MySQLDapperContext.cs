﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace DISTQCPAPI.Respository
{
    public class MySQLDapperContext
    {
        private static string ip;
        private static string port;
        private static string username;
        private static string password;
        private static string database;
        private static string connectionstring;

        static MySQLDapperContext()
        {
            var builder = new ConfigurationBuilder().SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("setting.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();
            ip = string.IsNullOrEmpty(configuration.GetSection("MySQL").GetSection("IP").Value)
                ? "127.0.0.1"
                : configuration.GetSection("MySQL").GetSection("IP").Value;
            port = string.IsNullOrEmpty(configuration.GetSection("MySQL").GetSection("Port").Value)
                ? "3306"
                : configuration.GetSection("MySQL").GetSection("Port").Value;
            username = string.IsNullOrEmpty(configuration.GetSection("MySQL").GetSection("Username").Value)
                ? "root"
                : configuration.GetSection("MySQL").GetSection("Username").Value;
            password = string.IsNullOrEmpty(configuration.GetSection("MySQL").GetSection("Password").Value)
                ? "root"
                : configuration.GetSection("MySQL").GetSection("Password").Value;
            database = string.IsNullOrEmpty(configuration.GetSection("MySQL").GetSection("Database").Value)
                ? "distqcp"
                : configuration.GetSection("MySQL").GetSection("Database").Value;
            connectionstring = "server=" + ip + ";user id=" + username + ";password=" + password + ";persistsecurityinfo=True;database=" + database + ";port=" + port;
            Console.WriteLine(connectionstring);
        }
        public MySqlConnection GetConnection()
        {
            return new MySqlConnection("server="+ip+";user id="+username+";password="+password+";persistsecurityinfo=True;database="+database+";port="+port);
        }
    }
}

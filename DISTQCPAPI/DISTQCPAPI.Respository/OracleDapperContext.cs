﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;

namespace DISTQCPAPI.Respository
{
    public class OracleDapperContext
    {
        private static string ip;
        private static string port;
        private static string username;
        private static string password;
        private static string database;
        private static string connectionstring;
        private static string servicename;
        static OracleDapperContext()
        {
            var builder = new ConfigurationBuilder().SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("setting.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();
            ip = string.IsNullOrEmpty(configuration.GetSection("Oracle").GetSection("IP").Value)
               ? "127.0.0.1"
               : configuration.GetSection("Oracle").GetSection("IP").Value;
            port = string.IsNullOrEmpty(configuration.GetSection("Oracle").GetSection("Port").Value)
                ? "3306"
                : configuration.GetSection("Oracle").GetSection("Port").Value;
            username = string.IsNullOrEmpty(configuration.GetSection("Oracle").GetSection("Username").Value)
                ? "root"
                : configuration.GetSection("Oracle").GetSection("Username").Value;
            password = string.IsNullOrEmpty(configuration.GetSection("Oracle").GetSection("Password").Value)
                ? "root"
                : configuration.GetSection("Oracle").GetSection("Password").Value;
            database = string.IsNullOrEmpty(configuration.GetSection("Oracle").GetSection("Database").Value)
                ? "distqcp"
                : configuration.GetSection("Oracle").GetSection("Database").Value;
            servicename = string.IsNullOrEmpty(configuration.GetSection("Oracle").GetSection("Servicename").Value)
                ? "orcl"
                : configuration.GetSection("Oracle").GetSection("Servicename").Value;
            connectionstring = @"Data Source=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = " + ip + ")(PORT = " + port + ")))(CONNECT_DATA =(SERVICE_NAME = " + servicename + ")));User ID=" + username + ";Password=" + password + ";";
            Console.WriteLine(connectionstring);
        }

        public DbConnection GetConnection()
        {
            return new OracleConnection(connectionstring);
        }
    }
}

﻿using System.Threading.Tasks;
using Surging.Core.CPlatform.Ioc;
using Surging.Core.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;

namespace DISTQCPAPI.Service.Bussiness
{
    [ServiceBundle("api/{Service}")]
   public  interface IUserService: IServiceKey
    {
        Task<string> SayHello(string username);
        
    }
}

﻿using System;
using System.Threading.Tasks;
using DISTQCPAPI.Application;
using Surging.Core.CPlatform.Ioc;
using Surging.Core.ProxyGenerator;

namespace DISTQCPAPI.Service.Bussiness
{
    [ModuleName("User")]
    public class UserService : ProxyServiceBase, IUserService
    {
        public Task<string> SayHello(string username)
        {
            return Task<string>.Factory.StartNew(() =>
            {
                UserApplaction userApplaction=new UserApplaction();
                return userApplaction.Encrypt(username);
                ;
            });
        }
    }
}

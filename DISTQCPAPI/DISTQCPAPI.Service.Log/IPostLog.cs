﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DISTQCPAPI.Service.Log
{
   public interface IPostLog
   {
       Task<bool> Log(LogInfo logInfo,out string errMsg);
       Task<bool> Log(List<LogInfo> logInfos, out string errMsg);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DISTQCPAPI.Service.Log
{
    public class LogInfo
    {
        public string Describe { get; set; }
        public string ModuleName { get; set; }
        public LogLevel LogLevel { get; set; }
        public string Detail { get; set; }
    }
    public enum LogLevel{
        Info=1,
        Warning=2,
        Debug=4,
        Error =5,
        Fatal=6
    }
    
}

using System;
using System.Linq;
using DISTQCPAPI.Respository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dapper;
using Newtonsoft.Json;

namespace DISTQCPAPI.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestConnetMySQL()
        {
            var dbContext = new MySqlSugarContext().GetClient();
            dbContext.DbFirst.CreateClassFile(@"e:\models", "DISTQCPAPI.Domain.Model");
            Assert.IsNotNull(dbContext);
        }
        [TestMethod]
        public void TestConnetMySQLByDapper()
        {
            var context = new MySQLDapperContext();
            var conn = context.GetConnection();
            var infos = conn.Query<DISTQCPAPI.Domain.MySQLModel.info>("select * from logs").ToList();
            
            Console.WriteLine(JsonConvert.SerializeObject(infos));
            Assert.IsTrue(infos.Count>0);
        }
        [TestMethod]
        public void TestConnetOracle()
        {
            var dbContext = new OracleSugarContext().GetClient();
            dbContext.DbFirst.CreateClassFile(@"e:\models", "DISTQCPAPI.Domain.OracleModel");
            Assert.IsNotNull(dbContext);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using DISTQCPAPI.Application;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DISTQCPAPI.UnitTest
{
    [TestClass]
    public class UserApplactionTest
    {
        [TestMethod]
        public void TestCryptoPassword()
        {
            string str = "我是原始密码";
            UserApplaction userApplaction=new UserApplaction();
            var jiamistr = userApplaction.Encrypt(str);
            Console.WriteLine("加密后的密码:"+jiamistr);
            var jiemistr = userApplaction.Decrypt(jiamistr);
            Console.WriteLine("解密后的密码:" +jiemistr );
            Assert.AreEqual(str,jiemistr);
        }
    }
}

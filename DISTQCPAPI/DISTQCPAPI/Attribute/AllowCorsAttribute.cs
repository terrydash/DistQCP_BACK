﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using System.IO;

namespace DISTQCPAPI.WEBAPI.Attribute
{
    /// <summary>
    /// 用于跨域访问的特性
    /// </summary>
    public class AllowCorsAttribute:ActionFilterAttribute
    {
        public string AllowSite = "*";
        
        public override Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Task.Factory.StartNew(() =>
            {

            var response = context.HttpContext.Response;
                Console.WriteLine(Path.DirectorySeparatorChar.ToString());
                response.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", AllowSite);
                response.Headers.Add("Access-Control-Allow-Headers", "authorization, content-type");
                response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            });
            return base.OnActionExecutionAsync(context, next);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using DISTQCPAPI.Common;
using DISTQCPAPI.WEBAPI.Attribute;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;


namespace DISTQCPAPI.WEBAPI.Controllers
{
    /// <summary>
    /// 控制登录的控制器
    /// </summary>
    [Produces("application/json")]
    [Route("api/Login")]
    public class LoginController : Controller
    {
        /// <summary>
        /// 获取Guid
        /// </summary>
        /// <returns></returns>
        [HttpPost("index")]
        //[AllowCors]
        
        public async Task<dynamic> GetGuid()
        {
            return await Task.Factory.StartNew(() =>
            {
                var guid=Guid.NewGuid().ToString("N");
                RedisHelper.SetStringKey(guid, DateTime.Now, new TimeSpan(0, 0, 10, 0));
                return new {guid=guid};
            });
        }
    }
}
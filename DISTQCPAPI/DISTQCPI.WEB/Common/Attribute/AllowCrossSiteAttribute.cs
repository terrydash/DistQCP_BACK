﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DISTQCPI.WEB.Common.Attribute
{
    /// <inheritdoc />
    public class AllowCrossSiteAttribute:ActionFilterAttribute
    {
        /// <inheritdoc />
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
           
            filterContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            filterContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "authorization, content-type");
            filterContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");

            base.OnActionExecuted(filterContext);
        }

    }
}

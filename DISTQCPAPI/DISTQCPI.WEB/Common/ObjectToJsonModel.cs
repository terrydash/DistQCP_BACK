﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DISTQCPAPI.Domain.ViewModel;
using Newtonsoft.Json;

namespace DISTQCPI.WEB.Common
{
    public class ObjectToJsonModel
    {
        private object _o;
        private string _msg;
        private ResponseResult _r;
        private int _datacount;
        public ObjectToJsonModel(object o, string msg = "", ResponseResult responseResult = ResponseResult.Ok, int datacount = 0)
        {
            _o = o;
            _msg = msg;
            _r = responseResult;
            _datacount = datacount;
        }
        public  string Convert()
        {
            JsonModel jsonModel = new JsonModel();
            StringBuilder sb = new StringBuilder();
            try
            {
                jsonModel.Data = _o;
                jsonModel.Msg = _msg;
                jsonModel.ResponseResult = _r;
                jsonModel.DataCount = _datacount;
            }
            catch (Exception e)
            {
                jsonModel.Data = null;
                jsonModel.ResponseResult = ResponseResult.Error;
                jsonModel.Msg = e.ToString();
                jsonModel.DataCount = 0;
            }

            sb.Append(JsonConvert.SerializeObject(jsonModel));
            return sb.ToString();
        }
    }
}

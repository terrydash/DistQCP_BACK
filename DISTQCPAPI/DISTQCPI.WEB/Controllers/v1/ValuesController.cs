﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Surging.Core.CPlatform.Utilities;
using Surging.Core.ProxyGenerator;
using DISTQCPAPI.Service;
using DISTQCPAPI.Service.Bussiness;
using DISTQCPI.WEB.Common.Attribute;
using Microsoft.AspNetCore.Cors;

namespace DISTQCPI.WEB.Controllers.v1
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    [EnableCors("AllowSameDomain")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        [AllowCrossSite]
        public async Task<IEnumerable<string>> Get()
        {
            var user = ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy<IUserService>("User");
           var str= await user.SayHello("HI");
            return new string[] { "value1", "value2" ,str};
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
